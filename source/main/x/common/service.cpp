/*
 * service.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <x/common/service.h>

#include <atomic>
#include <chrono>
#include <map>


namespace x
{
  namespace common
  {
    namespace impl
    {
      using namespace std;
      using namespace std::chrono;

      using namespace x::concurrent;


      class ServiceManagerBackendDefault: public ServiceManagerBackend, public Task
      {
        public:
          ServiceManagerBackendDefault():
            Task("common::ServiceManagerBackend")
          {
            dbgi("Create.");
          }


          virtual ~ServiceManagerBackendDefault()
          {
            dbgi("Destroy.");

            for(auto& entry : m_registry)
              entry.second->teardown();

            m_registry.clear();
          }


          virtual Service* resolve(String const& name) override
          {
            dbgi4("Resolve Service [" << name << "].");

            return m_registry[name].get();
          }

          virtual void create(String const& name, unique_ptr<Service> service) override
          {
            dbgi4("Create Service [" << name << "].");

            m_registry[name]=move(service);
            m_registry[name]->setup();
          }

          virtual void createWorker(String const& name, unique_ptr<ServiceWorker> service) override
          {
            dbgi4("Create Service worker [" << name << "].");

            m_registry[name]=move(service);
            m_registry[name]->setup();

            m_workerRegistry[name]=static_cast<ServiceWorker*>(m_registry[name].get());

            // TODO Memory barrier effect?
            ++m_workerCount;

            if(!running())
              start();
          }

          virtual void destroy(String const& name) override
          {
            dbgi("Destroy Service [" << name << "].");

            auto iterator=m_workerRegistry.find(name);

            if(iterator!=m_workerRegistry.end())
            {
              dbgi("Interrupt Service Worker [" << name << "].");

              iterator->second->interrupt();

              dbgi("Await Service Worker Termination [" << name << "].");

              if(!iterator->second->wait(seconds{1}))
                dbgi("Service Worker Termination Timed-out [" << name << "].");

              m_workerRegistry.erase(name);

              // TODO Memory barrier effect?
              --m_workerCount;
            }

            m_registry[name]->teardown();
            // TODO Verify.
            // m_registry[name].reset();

            m_registry.erase(name);
          }


        protected:
          virtual void run() override
          {
            dbgsi("Enter Service Worker Monitor.");

            while(0<m_workerCount && !interrupted())
            {
              for(auto& entry : m_workerRegistry)
              {
                if(!entry.second->running())
                {
                  if(entry.second->failed())
                  {
                    dbgi("Server Worker Interrupted By Exception [" << entry.first << ", " << entry.second->exception() << "].");

                    dbgi("Restart Service Worker.");

                    entry.second->start();
                  }
                  else if(!entry.second->started())
                  {
                    dbgi("Start Service Worker.");

                    entry.second->start();
                  }
                }
              }

              sleep(milliseconds{100});
            }

            dbgsi("Leave Service Worker Monitor.");
          }


        private:
          map<String, unique_ptr<Service>> m_registry;

          map<String, ServiceWorker*> m_workerRegistry;
          atomic<Int> m_workerCount{0};
      };


      static unique_ptr<ServiceManagerBackendDefault> backend;
    }
  }
}


x::common::ServiceManagerBackend* __x_common_service_manager_backend()
{
  return x::common::impl::backend.get();
}
