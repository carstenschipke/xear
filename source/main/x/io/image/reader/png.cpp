/*
 * png.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <x/io/image/reader/png.h>


#include <unistd.h>
#include <stdio.h>
#include <stdarg.h>

#include <png.h>


namespace x
{
  namespace io
  {
    namespace image
    {
      namespace reader
      {
        Png::Png()
        {
          dbgi4("Create.");
        }


        Png::~Png()
        {
          dbgi4("Destroy.");
        }


        void Png::read(String path, Ref& image) const
        {
          Byte header[8];

          FILE* file=fopen(path.c_str(), "rb");

          if(!file)
            throw "Unable to open file for reading.";

          fread(header, 1, 8, file);

          if(png_sig_cmp(header, 0, 8))
            throw "Unable to open file as PNG.";

          png_struct* png;
          png_info* info;

          png=png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);

          if(!png)
            throw "Unable to read PNG.";

          info=png_create_info_struct(png);

          if(!info)
            throw "Unable to read PNG.";

          if(setjmp(png_jmpbuf(png)))
            throw "I/O error.";

          png_init_io(png, file);
          png_set_sig_bytes(png, 8);
          png_read_info(png, info);

          image.width=(Int)png_get_image_width(png, info);
          image.height=(Int)png_get_image_height(png, info);
          image.size=image.height*info->rowbytes;

          Byte const colorspace=png_get_color_type(png, info);

          if(colorspace==PNG_COLOR_TYPE_RGB_ALPHA)
            image.colorspace=COLORSPACE_RGBA;
          else if(colorspace==PNG_COLOR_TYPE_RGB)
            image.colorspace=COLORSPACE_RGB;
          else
            throw "Unsupported colorspace.";

          png_read_update_info(png, info);

          if(setjmp(png_jmpbuf(png)))
            throw "I/O error.";

          image.data=(Byte*)malloc(image.size);

          for(Int y=0; y<image.height; y++)
            png_read_row(png, image.data+(y*info->rowbytes), nullptr);

          fclose(file);
        }
      }
    }
  }
}
