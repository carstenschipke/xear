/*
 * decoder.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_VIDEO_FFMPEG_DECODER_H
#define X_IO_VIDEO_FFMPEG_DECODER_H


#include <x/io/video/decoder.h>


namespace x
{
  namespace io
  {
    namespace video
    {
      namespace ffmpeg
      {
        using namespace std;


        class Decoder: public video::Decoder
        {
          public:
            virtual ~Decoder()
            {

            }


            virtual void read(String path, image::Ref& image) const override;
        };
      }
    }
  }
}


#endif
