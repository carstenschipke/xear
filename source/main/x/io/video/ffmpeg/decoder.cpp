/*
 * decoder.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear.h>

#include "decoder.h"


extern "C"
{
# include <libavcodec/avcodec.h>
# include <libavformat/avformat.h>
# include <libswscale/swscale.h>
}


namespace x
{
  namespace io
  {
    namespace video
    {
      namespace ffmpeg
      {
        void Decoder::read(String path, image::Ref& image) const
        {
          dbgsi("READ VIDEO " << path);

          av_register_all();

          AVFormatContext* context=nullptr;
          AVDictionary* options=nullptr;

          auto status=avformat_open_input(&context, path.c_str(), nullptr, &options);

          if(0!=status)
          {
            if(context)
              avformat_close_input(&context);

            return;
          }

          status=avformat_find_stream_info(context, &options);

          if(0>status)
          {
            if(context)
              avformat_close_input(&context);

            return;
          }

          dbgi("CONTEXT " << context);
          dbgi("STREAMS " << context->nb_streams);

          AVStream* stream=context->streams[0];
          AVCodecContext* codec=stream->codec;
          AVCodecDescriptor const* codecdesc=codec->codec_descriptor;

          dbgi("STREAM #0 DURATION " << stream->duration);
          dbgi("STREAM #0 OFFSET " << stream->start_time);
          dbgi("STREAM #0 CODEC ID " << codecdesc->id);
          dbgi("STREAM #0 CODEC NAME " << codecdesc->name);

          AVCodec* decoder=avcodec_find_decoder(codecdesc->id);

          dbgi("STREAM #0 DECODER " << decoder);

          if(decoder)
          {
            status=avcodec_open2(codec, decoder, &options);

            auto width=codec->coded_width;
            auto height=codec->coded_height;

            dbgi("STREAM #0 WIDTH " << width);
            dbgi("STREAM #0 HEIGHT " << height);

            AVPacket frame;
            image::Ref frameRgb;

            av_read_frame(context, &frame);
            av_read_frame(context, &frame);

            auto swscontext=sws_getContext(width, height, codec->pix_fmt, width, height, PIX_FMT_BGR24, SWS_BICUBIC, nullptr, nullptr, nullptr);

            dbgi("READ FRAME " << frame.buf->size);

            for(auto i=0; i<3; i++)
              printf("FRAME BGR24 %.2X %.2X %.2X\n", ((Byte*)frame.buf->data)[3*i+0], ((Byte*)frame.buf->data)[3*i+1], ((Byte*)frame.buf->data)[3*i+2]);
          }

          avcodec_close(codec);

          avformat_close_input(&context);
        }
      }
    }
  }
}
