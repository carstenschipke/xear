/*
 * surface.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include "surface.h"


#include <X11/Xlib.h>

#include <GL/glx.h>
#include <GL/glxext.h>


namespace xear
{
  namespace driver
  {
    namespace glx
    {
      using namespace std;


      namespace impl
      {
        struct Surface
        {
          Display* display;
          XVisualInfo* info;
          GLXFBConfig* config;

          map<String, GLXPbuffer> buffer;
          map<String, GLXContext> context;
          map<String, vec4> viewport;
        };


        static Surface surface;
      }


      void Surface::setup()
      {
        dbgi("Setup GLX.");

        Int visualAttributes[]={
          GLX_USE_GL,
          GLX_RGBA,
          GLX_DOUBLEBUFFER,
          None
        };

        Int attributes[]={
          GLX_X_RENDERABLE, True,
          GLX_DRAWABLE_TYPE, GLX_PBUFFER_BIT,
          GLX_RENDER_TYPE, GLX_RGBA_BIT,
          GLX_X_VISUAL_TYPE, GLX_TRUE_COLOR,
          GLX_RED_SIZE, 8,
          GLX_GREEN_SIZE, 8,
          GLX_BLUE_SIZE, 8,
//          GLX_ALPHA_SIZE, 8,
          GLX_DEPTH_SIZE, 24,
          GLX_STENCIL_SIZE, 8,
          GLX_DOUBLEBUFFER, True,
          GLX_SAMPLE_BUFFERS, 1,
          GLX_SAMPLES, 4,
          None
        };

        impl::surface.display=XOpenDisplay(0);
        auto screen=DefaultScreen(impl::surface.display);

        auto count=0;
        impl::surface.config=glXChooseFBConfig(impl::surface.display, screen, attributes, &count);
        impl::surface.info=glXChooseVisual(impl::surface.display, screen, visualAttributes);

        XSync(impl::surface.display, false);
      }

      void Surface::teardown()
      {
        dbgi("Teardown GLX.");

        for(auto& entry : impl::surface.buffer)
          destroyBuffer(entry.first);

        for(auto& entry : impl::surface.context)
          destroyContext(entry.first);

        XCloseDisplay(impl::surface.display);

        XFree(impl::surface.config);
        XFree(impl::surface.info);
      }


      Boolean Surface::create(String name, vec4 const& viewport, Boolean fullscreen)
      {
        dbgsi("Create surface [name: " << name << ", viewport: " << glm::to_string(viewport) << "].");

        createContext(name);
        createBuffer(name, viewport);

        return true;
      }

      Boolean Surface::createBuffer(String name, vec4 const& viewport)
      {
        dbgsi("Create surface buffer [name: " << name << ", viewport: " << glm::to_string(viewport) << "].");

        Int attributes[5]={
          GLX_PBUFFER_WIDTH, (Int)viewport.z,
          GLX_PBUFFER_HEIGHT, (Int)viewport.w,
          None
        };

        impl::surface.buffer[name]=glXCreatePbuffer(impl::surface.display, impl::surface.config[0], attributes);
        impl::surface.viewport[name]=viewport;

        return true;
      }

      Boolean Surface::createContext(String name)
      {
        dbgsi("Create surface context [name: " << name << "].");

        impl::surface.context[name]=glXCreateContext(impl::surface.display, impl::surface.info, 0, true);

        return true;
      }


      void Surface::destroy(String name)
      {
        dbgsi("Destroy surface [name: " << name << ", buffer: " << impl::surface.buffer[name] << ", context: " << impl::surface.context[name] << "].");

        destroyBuffer(name);
        destroyContext(name);
      }

      void Surface::destroyBuffer(String name)
      {
        dbgsi("Destroy surface buffer [name: " << name << ", buffer: " << impl::surface.buffer[name] << "].");

        glXDestroyPbuffer(impl::surface.display, impl::surface.buffer[name]);

        impl::surface.buffer.erase(name);
      }

      void Surface::destroyContext(String name)
      {
        dbgsi("Destroy surface context [name: " << name << ", context: " << impl::surface.context[name] << "].");

        glXDestroyContext(impl::surface.display, impl::surface.context[name]);

        impl::surface.context.erase(name);
      }


      void Surface::use(String name)
      {
        dbgsi("Use surface [name: " << name << ", buffer: " << impl::surface.buffer[name] << ", context: " << impl::surface.context[name] << "].");

        glXMakeContextCurrent(impl::surface.display, impl::surface.buffer[name], impl::surface.buffer[name], impl::surface.context[name]);
      }

      void Surface::sync(String name)
      {
        dbgsi("Sync surface [name: " << name << ", buffer: " << impl::surface.buffer[name] << ", context: " << impl::surface.context[name] << "].");

        glXWaitGL();
      }

      void Surface::flush(String name)
      {
        dbgsi("Swap surface [name: " << name << ", buffer: " << impl::surface.buffer[name] << ", context: " << impl::surface.context[name] << "].");

        glXSwapBuffers(impl::surface.display, impl::surface.buffer[name]);
      }


      vec4 const& Surface::viewport(String name)
      {
        return impl::surface.viewport[name];
      }
    }
  }
}
