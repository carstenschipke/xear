/*
 * surface.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_DRIVER_GLX_SURFACE_H_
#define XEAR_DRIVER_GLX_SURFACE_H_


#include <xear/driver/surface.h>


namespace xear
{
  namespace driver
  {
    namespace glx
    {
      using namespace std;

      using namespace x;


      class Surface: public driver::Surface
      {
        public:
          virtual void setup() override;
          virtual void teardown() override;


          virtual Boolean create(String name, vec4 const& viewport, Boolean fullscreen=false) override;
          virtual Boolean createContext(String name) override;
          virtual Boolean createBuffer(String name, vec4 const& viewport) override;

          virtual void destroy(String const name) override;
          virtual void destroyContext(String const name) override;
          virtual void destroyBuffer(String const name) override;

          virtual void use(String const name) override;
          virtual void sync(String const name) override;
          virtual void flush(String const name) override;

          virtual vec4 const& viewport(String name) override;
      };
    }
  }
}


#endif
