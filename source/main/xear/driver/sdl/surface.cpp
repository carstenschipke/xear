/*
 * surface.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include "surface.h"


namespace xear
{
  namespace driver
  {
    namespace sdl
    {
      void Surface::setup()
      {
        dbgi("Setup SDL.");

        SDL_Init(SDL_INIT_EVERYTHING);
      }

      void Surface::teardown()
      {
        dbgi("Teardown SDL.");

        SDL_Quit();
      }


      Boolean Surface::create(String name, vec4 const& viewport, Boolean fullscreen)
      {
        // TODO Auto-detect.
#       ifdef XEAR_GLES
          SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);

          SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
          SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);
#       else
          SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_COMPATIBILITY);

          SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
#       endif

        // TODO Auto-detect.
        SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, XEAR_GL_VERSION_MAJOR);

        SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
        SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

        IntU flags=SDL_WINDOW_OPENGL|SDL_WINDOW_SHOWN;

#       if defined(TARGET_OS_IPHONE) && 0<TARGET_OS_IPHONE
          flags|=SDL_WINDOW_FULLSCREEN;
          flags|=SDL_WINDOW_BORDERLESS;
          flags|=SDL_WINDOW_RESIZABLE;

          SDL_DisplayMode displayMode;
          SDL_GetCurrentDisplayMode(0, &displayMode);

          m_viewport.z=displayMode.w;
          m_viewport.w=displayMode.h;
#       else
          if(fullscreen)
            flags|=SDL_WINDOW_FULLSCREEN;

          m_viewport=viewport;
#       endif

        m_window=SDL_CreateWindow(name.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, m_viewport.z, m_viewport.w, flags);

        if(!m_window)
        {
          dbge3("Failed to initialize window [" << SDL_GetError() << "]");

          return false;
        }

        return createContext(name);
      }

      Boolean Surface::createContext(String name)
      {
        m_context=SDL_GL_CreateContext(m_window);

        if(!m_context)
        {
          dbge3("Failed to initialize OpenGL context [" << SDL_GetError() << "]");

          return false;
        }

        if(!vsync)
          SDL_GL_SetSwapInterval(0);

        return true;
      }

      Boolean Surface::createBuffer(String name, vec4 const& viewport)
      {
        // TODO Implement pbuffer..

        return true;
      }

      void Surface::destroy(String name)
      {
        Surface::destroyContext(name);

        if(m_window)
          SDL_DestroyWindow(m_window);
      }

      void Surface::destroyContext(String name)
      {
        if(m_context)
          SDL_GL_DeleteContext(m_context);
      }

      void Surface::destroyBuffer(String name)
      {
        // TODO Implement pbuffer..
      }

      void Surface::use(String name)
      {
        SDL_GL_MakeCurrent(m_window, m_context);
      }

      void Surface::sync(String name)
      {
        // TODO Implement?
      }

      void Surface::flush(String name)
      {
        SDL_GL_SwapWindow(m_window);
      }


      vec4 const& Surface::viewport(String name)
      {
        return m_viewport;
      }
    }
  }
}
