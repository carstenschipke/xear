/*
 * surface.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_DRIVER_SDL_SURFACE_H_
#define XEAR_DRIVER_SDL_SURFACE_H_


#include <xear/driver/surface.h>

#include <SDL2/SDL.h>


namespace xear
{
  namespace driver
  {
    namespace sdl
    {
      using namespace std;

      using namespace x;


      class Surface: public driver::Surface
      {
        public:
          Boolean vsync=true;


          virtual void setup() override;
          virtual void teardown() override;


          virtual Boolean create(String const name, vec4 const& viewport, Boolean fullscreen) override;
          virtual Boolean createContext(String const name) override;
          virtual Boolean createBuffer(String const name, vec4 const& viewport) override;

          virtual void destroy(String const name) override;
          virtual void destroyContext(String const name) override;
          virtual void destroyBuffer(String const name) override;

          virtual void use(String const name) override;
          virtual void sync(String const name) override;
          virtual void flush(String const name) override;

          virtual vec4 const& viewport(String name) override;


        private:
          SDL_Window* m_window=nullptr;
          SDL_GLContext m_context=nullptr;

          vec4 m_viewport;
      };
    }
  }
}


#endif
