/*
 * rdi.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RENDERER_GL_RDI_H_
#define XEAR_RENDERER_GL_RDI_H_


#include <map>
#include <deque>
#include <vector>

#include <xear.h>

#include <xear/common/fps.h>
#include <xear/common/primitive.h>

#include <xear/driver/gl.h>

#include <xear/renderer/rdi.h>
#include <xear/renderer/layouts.h>
#include <xear/renderer/shader.h>
#include <xear/renderer/texture.h>

#include <xear/scene/data/geometry.h>

#include <x/io/colorspace.h>
#include <x/io/image/ref.h>
#include <x/misc/slots.h>

#include <debug/service.h>


// TOOD Cleanup / Remove everything built for configurability / extensibility thats not used
// TODO Shader compiler, parse attributes etc. pre-initiailze statically with VAOs
// TODO uniform callbacks or observable uniforms / resolve values on demand & bind on change
// TODO Pack (multiple) geometry into vertex buffers & create named index/offsets
// TODO Same with textures
// TODO De-/Linearization / gamma correction / GL_FRAMEBUFFER_SRGB, GL_SRGB_EXT
namespace xear
{
  namespace renderer
  {
    namespace gl
    {
      using namespace driver::gl;

      using namespace x;
      using namespace x::common;
      using namespace x::io;
      using namespace x::misc;

      using namespace xear::common;


      String const BUFFER_LAYOUT_PLAIN="plain";
      String const MATERIAL_LAYOUT_PLAIN="plain";

      String const DEFAULT_SHADER_LAYOUT=MATERIAL_LAYOUT_PLAIN;
      String const DEFAULT_MATERIAL="plain";

      String const NAME_BUFFER_OFFSCREEN="offscreen";


      String const SHAPES[]={
        "cube",
        "quad",
        "bufferdebug"
      };

      CullFace const CULLFACE[]={
        CullFace::NONE,
        CullFace::FRONT,
        CullFace::BACK
      };


      struct HandleBuffer
      {
        Int count;
        IntU index;

        IntU* buffer;


        HandleBuffer():
          count(0), index(0), buffer(nullptr)
        {

        }

        HandleBuffer(Int count):
          count(count), index(0), buffer(new IntU[count])
        {

        }


        HandleBuffer(HandleBuffer const& rhs):
          count(rhs.count), index(rhs.index), buffer(new IntU[rhs.count])
        {
          for(auto i=0; i<count; i++)
            buffer[i]=rhs.buffer[i];
        }

        HandleBuffer& operator=(HandleBuffer const& rhs)
        {
          count=rhs.count;
          index=rhs.index;
          buffer=new IntU[rhs.count];

          for(auto i=0; i<count; i++)
            buffer[i]=rhs.buffer[i];

          return *this;
        }


        ~HandleBuffer()
        {
          if(0<count)
            delete[] buffer;
        }
      };


      struct HandleGeometry
      {
        String name;
        String layout;

        Primitive primitive;
        Scalar typeIndices;
        Scalar typeVertices;

        Int countIndices;
        Int countVertices;

        HandleBuffer buffer;


        HandleGeometry(String name, String layout):
          HandleGeometry(name, layout, Primitive::TRIANGLE, Scalar::INT, Scalar::FLOAT, 0, 0, 2)
        {

        }

        HandleGeometry(String name, String layout, Primitive primitive, Scalar typeIndices, Scalar typeVertices, Int countIndices, Int countVertices, Int countBuffers):
          name(name),
          layout(layout),
          primitive(primitive),
          typeIndices(typeIndices),
          typeVertices(typeVertices),
          countIndices(countIndices),
          countVertices(countVertices),
          buffer(countBuffers)
        {

        }
      };


      struct HandleTexture
      {
        String name;
        TextureType type;

        IntU index;
        IntU texture;

        HandleBuffer buffer;
      };


      struct HandleMaterial
      {
        String name;
        String layout;

        IntU shader;
      };


      struct HandlePath
      {
        String name;
        IntU path;
      };


      struct Framebuffer
      {
        String name;
        IntU index;

        vector<String> textures;

        HandleBuffer depth{1};
      };


      // TODO Track & commit state changes properly (i.e. vertex attrib pointer, uniforms etc.)
      class Rdi: public renderer::Rdi
      {
        public:
          using renderer::Rdi::Rdi;


          virtual ~Rdi()
          {
            dbgi4("Destroy.");

            // FIXME Delete buffers, geometries, materials/shaders, textures etc.
          }


          virtual void init(script::Context& context) override
          {
            context.bind0("rdi_clear", [this](script::Context& context) {
              clear();
            });

            context.bind1("rdi_flags", [this](script::Context& context, Any& arg0) {
              flags(arg0.as<IntU>());
            });

            context.bind1("rdi_draw", [this](script::Context& context, Any& arg0) {
              draw(arg0.as<String>());
            });

            context.bind1("rdi_material_create", [this](script::Context& context, Any& arg0) {
              materialCreate(arg0.as<String>());
            });
            context.bind1("rdi_material_use", [this](script::Context& context, Any& arg0) {
              materialUse(arg0.as<String>());
            });

            context.bind2("rdi_path_create", [this](script::Context& context, Any& arg0, Any& arg1) {
              pathCreate(arg0.as<String>(), arg1.as<String>());
            });
            context.bind1("rdi_path_draw", [this](script::Context& context, Any& arg0) {
              // FIXME Pass mvp by reference & export matrix accessors/manipulators ..
              drawPath(arg0.as<String>(), mat4{});
            });
            context.bind2("rdi_path_update", [this](script::Context& context, Any& arg0, Any& arg1) {
              pathUpdate(arg0.as<String>(), arg1.as<String>());
            });

            context.bind2("rdi_uniform1i", [this](script::Context& context, Any& arg0, Any& arg1) {
              bindUniform1i(arg0.as<String>(), arg1.as<Int>());
            });
          }


          virtual void init() override
          {
            gl::init();
            gl::enablePrimitiveRestart();

            gl::clearColor(m_clearColor);
            gl::viewport(m_viewport);

            gl::cullFace(CullFace::BACK);

            registerDefaultAttributeLayouts();
            registerDefaultGeometry();

            materialCreate(DEFAULT_MATERIAL);
            materialUse(DEFAULT_MATERIAL);

            flags(Flag::DEFAULT);

            // TODO Add HTTP / REST-like API to resource manager to POST/PUT/etc. files & and publish separate methods of Rdi, e.g. DELETE/PUT material|texture etc.
            x::common::ServiceManager::resolve<debug::Service>("debug")->registerMethod("MATUPD", [this](debug::Request const& request) {
              dbgsi("MATERIAL UPDATE REQUESTED [" << request.command << "].");
              // request.session->send("0");
            });

            dbgi2("Initialized device [" << gl::versionApi().name << " " << gl::versionApi().string << ", " << "GLSL " << to_string(gl::versionApi().glsl) << "].");
          }

          virtual void init(Target target, Colorspace const& colorspace, vec4 const& viewport) override
          {
            m_viewport=viewport;

            init();

            if(Target::BUFFER==target)
            {
              gl::bufferCreate(NAME_BUFFER_OFFSCREEN, m_offscreenBuffer.count, m_offscreenBuffer.buffer);

              gl::pixelPackBufferInit(NAME_BUFFER_OFFSCREEN, viewport.z, viewport.w, colorspace.size(viewport.z, viewport.w), m_offscreenBuffer.buffer, m_offscreenBuffer.index);
            }
          }

          virtual void begin() override
          {
            fps.update();
          }

          virtual void fetch(image::Ref& frame) override
          {
            gl::pixelPackBufferSwap(NAME_BUFFER_OFFSCREEN, m_viewport.x, m_viewport.y, frame.width, frame.height, frame.size, frame.data, m_offscreenBuffer.buffer, m_offscreenBuffer.index);
          }

          virtual void clear() override
          {
            gl::clear();
          }


          virtual void clearColor(vec4 const& color) override
          {
            dbgi3("Set clear color [value: " << glm::to_string(color) << "].");

            m_clearColor=color;

            gl::clearColor(color);
          }

          virtual vec4 const& viewport() override
          {
            return m_viewport;
          }

          virtual void viewport(vec4 const& viewport) override
          {
            dbgi3("Set viewport [value: " << glm::to_string(viewport) << "].");

            m_viewport=viewport;

            gl::viewport(viewport);
          }

          virtual IntU flags() override
          {
            return m_flags;
          }

          virtual void flags(IntU flags) override
          {
            if(m_flags!=flags)
              bindUniform1i("u_intFlags", flags);

            m_flags=flags;
          }

          virtual void primitive(Primitive primitive) override
          {
            m_primitive=primitive;
          }

          virtual Primitive primitive() override
          {
            return m_primitive;
          }

          virtual void cull(Cull face) override
          {
            gl::cullFace(CULLFACE[face]);
          }


          // TODO Merge draw methods, i.e. drawPath (hold an index of all 'Drawable's).
          virtual void draw() override
          {
            draw(0, m_geometry->countIndices);
          }

          virtual void draw(String name) override
          {
            geometryUse(name);

            draw();
          }

          virtual void draw(String name, Long offset, Int count) override
          {
            geometryUse(name);

            draw(offset, count);
          }

          virtual void draw(Shape shape) override
          {
            geometryUse(SHAPES[shape]);

            draw();
          }

          virtual void draw(Shape shape, Long offset, Int count) override
          {
            geometryUse(SHAPES[shape]);

            draw(offset, count);
          }

          virtual void draw(Long offset, Int count) override
          {
            dbgi3("DRAW " << m_geometry->name << ", " << offset << ", " << count);

            Primitive primitive=m_primitive;

            if(Primitive::DEFAULT==primitive)
              primitive=m_geometry->primitive;

            gl::elementsDraw(gl::primitive(primitive), count, m_geometry->typeIndices, offset);
          }

          virtual void drawRange(IntU min, IntU max, Long offset, Int count) override
          {
            dbgi3("DRAW " << offset << ", " << count << ", " << min << ", " << max);

            Primitive primitive=m_primitive;

            if(Primitive::DEFAULT==primitive)
              primitive=m_geometry->primitive;

            gl::elementsDrawRange(gl::primitive(primitive), min, max, count, m_geometry->typeIndices, offset);
          }


          // TODO Bind lazy.
          // TODO Support binding to callbacks/lambdas.
          virtual Boolean bindUniformMatrix3f(String name, mat3 value) override
          {
            auto location=resolveUniform(name, false);

            if(-1!=location)
              gl::uniformMatrix3fv(name, location, 1, value);

            return true;
          }

          virtual Boolean bindUniformMatrix4f(String name, mat4 value) override
          {
            auto location=resolveUniform(name, false);

            if(-1!=location)
              gl::uniformMatrix4fv(name, location, 1, value);

            return true;
          }

          virtual Boolean bindUniform1i(String name, Int value) override
          {
            auto location=resolveUniform(name, false);

            if(-1!=location)
              gl::uniform1i(name, location, value);

            return true;
          }

          virtual Boolean bindUniform3f(String name, vec3 value) override
          {
            auto location=resolveUniform(name, false);

            if(-1!=location)
              gl::uniform3f(name, location, value);

            return true;
          }

          virtual Boolean bindUniform4f(String name, vec4 value) override
          {
            auto location=resolveUniform(name, false);

            if(-1!=location)
              gl::uniform4f(name, location, value);

            return true;
          }

          virtual Boolean bindLights(Float* data, Int countLights, Int countProperties) override
          {
            if(!textureExists("lights"))
            {
              if(!textureAllocate("lights", TextureType::TEXTURE_2D))
                return false;
            }

            auto idx=0u;
            auto count=3*countProperties*countLights;

            // FIXME Should only be fallback if floating point textures are not supported.
            Byte bytes[count*sizeof(Float)];

            for(auto i=0u; i<count; i++)
            {
              auto fptr=reinterpret_cast<Byte const*>(&data[i]);

              for(auto j=0u; j<sizeof(Float); j++)
                bytes[idx++]=fptr[j];
            }

            auto iterator=m_textures.find("lights");

            if(iterator==m_textures.end())
              return false;

            gl::textureActive("lights", iterator->second.index);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, 3*countProperties, countLights, 0, GL_RGBA, GL_UNSIGNED_BYTE, bytes);

            textureUse("lights", "u_smpTextureLights");

            bindUniform1i("u_intLights", countLights);

            return true;
          }


          // TODO Use single VAO & VBO (update & draw ranges etc.)
          virtual Boolean geometryUse(String name) override
          {
            dbgi4("Request geometry [name: " << name << "].");

            if(m_geometry && 0==strcmp(m_geometry->name.c_str(), name.c_str()))
              return true;

            auto iterator=m_geometries.find(name);

            if(iterator==m_geometries.end())
              return false;

            dbgi3("Use geometry [name: " << name << "].");

            m_geometry=&iterator->second;

            gl::bufferBind(name, BufferTarget::ARRAY, m_geometry->buffer.buffer[0]);
            gl::bufferBind(name, BufferTarget::ARRAY_ELEMENT, m_geometry->buffer.buffer[1]);

            m_layouts.useBufferLayout(m_geometry->layout);

            updateAttributeBindings();

            return true;
          }

          virtual Boolean geometryExists(String name) override
          {
            auto iterator=m_geometries.find(name);

            return iterator!=m_geometries.end();
          }

          virtual Boolean geometryCreate(String name, scene::data::Geometry<> const& data) override
          {
            auto iterator=m_geometries.find(name);

            if(iterator!=m_geometries.end())
              return false;

            HandleGeometry geometry{
              name,
              data.layout,
              data.primitive,
              data.typeScalarIndex(),
              data.typeScalarVertex(),
              data.countIndices,
              data.countVertices,
              2
            };

            dbgi3("Create geometry [name: " << name << ", size: " << data.countVertices*scalarSize(geometry.typeVertices) << "].");

            gl::bufferCreate(name, geometry.buffer.count, geometry.buffer.buffer);

            gl::bufferBind(name, BufferTarget::ARRAY, geometry.buffer.buffer[0]);
            gl::bufferData(name, BufferTarget::ARRAY, data.countVertices*(Int)scalarSize(geometry.typeVertices), data.vertices.data(), BufferDataUsage::DYNAMIC_DRAW);
            gl::bufferBind(name, BufferTarget::ARRAY, 0);

            gl::bufferBind(name, BufferTarget::ARRAY_ELEMENT, geometry.buffer.buffer[1]);
            gl::bufferData(name, BufferTarget::ARRAY_ELEMENT, data.countIndices*(Int)scalarSize(geometry.typeIndices), data.indices.data(), BufferDataUsage::DYNAMIC_DRAW);
            gl::bufferBind(name, BufferTarget::ARRAY_ELEMENT, 0);

            m_geometries.insert({name, geometry});

            return true;
          }

          virtual Boolean geometryUpdate(String name, Int offset, Int count, void const* const data) override
          {
            auto iterator=m_geometries.find(name);

            if(iterator==m_geometries.end())
              return false;

            dbgi3("Update geometry [name: " << name << ", offset: " << offset << ", count: " << count << "].");

            auto& geometry=iterator->second;

            gl::bufferBind(name, BufferTarget::ARRAY, geometry.buffer.buffer[0]);

            if(0==offset && count==geometry.countVertices)
              gl::bufferData(name, BufferTarget::ARRAY, count*(Int)scalarSize(geometry.typeVertices), data, BufferDataUsage::DYNAMIC_DRAW);
            else
              gl::bufferDataSub(name, BufferTarget::ARRAY, offset, count*(Int)scalarSize(geometry.typeVertices), data);

            gl::bufferBind(name, BufferTarget::ARRAY, 0);

            return true;
          }

          virtual Boolean geometryRemove(String name) override
          {
            auto iterator=m_geometries.find(name);

            if(iterator==m_geometries.end())
              return false;

            gl::bufferDelete(name, 2, iterator->second.buffer.buffer);

            m_geometries.erase(name);

            return true;
          }

          virtual Boolean materialClear() override
          {
            if(m_material)
            {
              m_material=nullptr;

              gl::programUse("clear", 0u);
            }

            return true;
          }

          virtual Boolean materialUse(String name) override
          {
            dbgi4("Request material [name: " << name << "].");

            if(m_material && 0==strcmp(m_material->name.c_str(), name.c_str()))
              return true;

            auto iterator=m_materials.find(name);

            if(iterator==m_materials.end())
            {
              if(0==strcmp(DEFAULT_MATERIAL.c_str(), name.c_str()))
                return false;

              dbgi3("Fallback to default material [name: " << DEFAULT_MATERIAL << "].");

              return materialUse(DEFAULT_MATERIAL);
            }

            dbgi3("Use material [name: " << name << "].");

            m_material=&iterator->second;

            gl::programUse(name, m_material->shader);

            if(m_layouts.useMaterialLayout(m_material->layout))
              updateAttributeBindings();

            return true;
          }

          virtual Boolean materialExists(String name) override
          {
            return m_materials.find(name)!=m_materials.end();
          }

          virtual Boolean materialCreate(String name) override
          {
            auto iterator=m_materials.find(name);

            if(iterator==m_materials.end())
            {
              dbgi3("Create material [name: " << name << "].");

              auto vertexShader=resources.get<Shader>(ResourceType::SHADER, mime::Type::TEXT_GLSL_VERTEX_SHADER, name);
              auto fragmentShader=resources.get<Shader>(ResourceType::SHADER, mime::Type::TEXT_GLSL_FRAGMENT_SHADER, name);

              HandleMaterial material;
              material.name=name;
              material.shader=compileShader(name, vertexShader.content(), fragmentShader.content());

              if(0==material.shader)
              {
                dbgi3("Unable to create material [name: " << name << "].");
              }
              else
              {
                material.layout=vertexShader.layout();

                m_materials.insert({name, material});
              }

              return true;
            }

            return false;
          }

          virtual Boolean materialRemove(String name) override
          {
            auto iterator=m_materials.find(name);

            if(iterator==m_materials.end())
              return false;

            dbgi3("Remove material [name: " << name << "].");

            auto& material=iterator->second;

            gl::programDelete(name, material.shader);

            m_materials.erase(name);

            return true;
          }

          virtual Boolean textureUse(String name, String location) override
          {
            auto mapped=m_texturesMapped.find(location);

            if(mapped!=m_texturesMapped.end() && 0==strcmp(name.c_str(), mapped->second.c_str()))
              return true;

            auto iterator=m_textures.find(name);

            if(iterator==m_textures.end())
              return false;

            dbgi3("Bind texture [name: " << name << ", index: " << iterator->second.index << "].");

            bindUniform1i(location, iterator->second.index);

            m_texturesMapped[location]=name;

            return true;
          }

          virtual Boolean textureExists(String name) override
          {
            return m_textures.find(name)!=m_textures.end();
          }

          virtual Boolean textureCreate(String name) override
          {
            if(!textureAllocate(name, TextureType::TEXTURE_2D))
              return false;

            dbgi3("Create texture [name: " << name << "].");

            auto texture=resources.get<Texture>(ResourceType::TEXTURE, mime::Type::IMAGE_KTX, name);

            texture.load();

            auto idx=0;
            auto width=texture.width;

            for(auto& level : texture.mipmap)
            {
              gl::textureImage2dCompressed(name, TextureType::TEXTURE_2D, idx++, texture.formatInternal, width, width, 0, level.size, level.bytes);

              width>>=1;
            }

            gl::textureParametersDefault(name, TextureType::TEXTURE_2D, 0, (Int)texture.mipmap.size()-1, TextureFilter::LINEAR_MIPMAP_LINEAR, TextureFilter::LINEAR);

            return true;
          }

          virtual Boolean textureCreate(String name, Int width, Int height, void const* const data) override
          {
            if(!textureAllocate(name, TextureType::TEXTURE_2D))
              return false;

            dbgi3("Create texture [name: " << name << "].");

            gl::textureImage2d(name, TextureType::TEXTURE_2D, 0, TextureFormat::RGB, width, height, 0, TextureFormat::BGRA, DataType::UNSIGNED_BYTE, data);

            return true;
          }

          virtual Boolean textureUpdate(String name, Int width, Int height, void const* const data) override
          {
            auto iterator=m_textures.find(name);

            if(iterator==m_textures.end())
              return false;

            gl::textureActive(name, iterator->second.index);
            gl::textureImage2d(name, TextureType::TEXTURE_2D, 0, TextureFormat::RGB, width, height, 0, TextureFormat::BGRA, DataType::UNSIGNED_BYTE, data);

            return true;
          }

          virtual Boolean textureRemove(String name) override
          {
            auto iterator=m_textures.find(name);

            if(iterator==m_textures.end())
              return false;

            gl::textureDelete(name, 1, &iterator->second.texture);

            m_textureSlots.lose(iterator->second.index);
            m_textures.erase(name);

            return true;
          }

          virtual Boolean textureBufferExists(String name, Int width, Int height, Int bytes) override
          {
            return textureExists(name);
          }

          virtual Boolean textureBufferCreate(String name, Int width, Int height, Int bytes) override
          {
            if(!textureAllocate(name, TextureType::TEXTURE_2D))
              return false;

            auto iterator=m_textures.find(name);

            if(iterator==m_textures.end())
              return false;

            iterator->second.buffer.count=2;
            iterator->second.buffer.index=0;
            iterator->second.buffer.buffer=new IntU[2];

            gl::textureBufferCreate(name, width, height, bytes, iterator->second.buffer.buffer);

            return true;
          }

          virtual Boolean textureBufferUpdate(String name, Int offsetX, Int offsetY, Int width, Int height, Int bytes, Byte const* const data) override
          {
            auto iterator=m_textures.find(name);

            if(iterator==m_textures.end())
              return false;

            gl::textureActive(name, iterator->second.index);
            gl::textureBufferUpdate(name, offsetX, offsetY, width, height, bytes, data, iterator->second.buffer.buffer, iterator->second.buffer.index);

            return true;
          }

          virtual Boolean textureBufferResize(String name, Int width, Int height) override
          {
            auto iterator=m_textures.find(name);

            if(iterator==m_textures.end())
              return false;

            gl::textureActive(name, iterator->second.index);
            gl::textureBufferResize(name, width, height);

            return true;
          }

          virtual Boolean textureBufferRemove(String name) override
          {
            return textureRemove(name);
          }


          // TODO Configurable Rendering & Framebuffer API (textures & attachments, rendering passes & deferred shader, post-processing shaders etc.).
          virtual Boolean deferredShading() override
          {
            materialCreate("post");
            materialCreate("pre");

            materialUse("pre");


            m_framebufferDeferred.name="deferred";
            gl::framebufferCreate(m_framebufferDeferred.name, 1, &m_framebufferDeferred.index);
            gl::framebufferBind(m_framebufferDeferred.name, BufferTarget::FRAMEBUFFER_DRAW, m_framebufferDeferred.index);

            Int i=0;

            // Framebuffer (texture attachments)
            auto textureNames={"deferalbedo", "deferspecular", "defernormal", "deferenv"};

            for(auto& textureName : textureNames)
            {
              if(!textureAllocate(textureName, TextureType::TEXTURE_2D))
                return false;

              auto iterator=m_textures.find(textureName);
              auto& texture=iterator->second;

              gl::textureParameteri(texture.name, texture.type, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
              gl::textureParameteri(texture.name, texture.type, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

              gl::textureFilter(texture.name, TextureType::TEXTURE_2D, TextureFilter::LINEAR, TextureFilter::LINEAR);

              glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_viewport.z, m_viewport.w, 0, GL_RGBA, GL_FLOAT, nullptr);
              // FIXME Something is wrong now with ES 3.0 (or iOS9)?
              glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+i++, GL_TEXTURE_2D, texture.texture, 0);

              m_framebufferDeferred.textures.push_back(texture.name);
            }

            // TODO Remove
#           ifdef XEAR_GLES
              IntU drawBuffers[]={GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3};
              glDrawBuffers(4, drawBuffers);
#           else
              textureAllocate("defdepth", TextureType::TEXTURE_2D);
              glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, m_viewport.z, m_viewport.w, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, nullptr);
              glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_TEXTURE_2D, m_textures["defdepth"].texture, 0);

              IntU drawBuffers[]={GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3, GL_DEPTH_STENCIL_ATTACHMENT};
              glDrawBuffers(5, drawBuffers);
#           endif

            // Renderbuffer (depth & stencil attachment)
            gl::renderbufferCreate(m_framebufferDeferred.name, m_framebufferDeferred.depth.count, m_framebufferDeferred.depth.buffer);
            gl::renderbufferBind(m_framebufferDeferred.name, BufferTarget::RENDERBUFFER, m_framebufferDeferred.depth.buffer[0]);

            gl::renderbufferStorage(m_framebufferDeferred.name, BufferTarget::RENDERBUFFER, RenderbufferAttachment::DEPTH24_STENCIL8, m_viewport.z, m_viewport.w);

            gl::framebufferRenderbuffer(m_framebufferDeferred.name, BufferTarget::FRAMEBUFFER_DRAW, BufferTarget::RENDERBUFFER, FramebufferAttachment::DEPTH_STENCIL_ATTACHMENT, m_framebufferDeferred.depth.buffer[0]);

            // TODO Roll-back setup - if failed, and proceed with forward-shading.
            auto status=gl::framebufferStatus(m_framebufferDeferred.name, BufferTarget::FRAMEBUFFER_DRAW);

            gl::framebufferBind(m_framebufferDeferred.name, BufferTarget::FRAMEBUFFER_DRAW, 0);

            return status;
          }

          virtual void deferredShadingPassOne() override
          {
            gl::framebufferBind(m_framebufferDeferred.name, BufferTarget::FRAMEBUFFER_DRAW, m_framebufferDeferred.index);

            materialUse("pre");

            IntU drawBuffers[]={GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2, GL_COLOR_ATTACHMENT3};
            glDrawBuffers(4, drawBuffers);

            glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

            DEVICE_CHECK_ERROR("Unable to bind draw buffer");
          }

          virtual void deferredShadingPassTwo() override
          {
            gl::framebufferBind(m_framebufferDeferred.name, BufferTarget::FRAMEBUFFER, 0);

            materialUse("post");

            glDisable(GL_DEPTH_TEST);

            glEnable(GL_BLEND);

            glBlendEquation(GL_FUNC_ADD);
            glBlendFunc(GL_ONE, GL_ONE);

            glClear(GL_COLOR_BUFFER_BIT);

            glUniform4f(glGetUniformLocation(m_material->shader, "u_vecViewport"), m_viewport.x, m_viewport.y, m_viewport.z, m_viewport.w);

            glActiveTexture(GL_TEXTURE0+0);
            glBindTexture(GL_TEXTURE_2D, m_textures[m_framebufferDeferred.textures[0]].texture);
            glUniform1i(glGetUniformLocation(m_material->shader, "u_smpTextureAlbedo"), 0);

            glActiveTexture(GL_TEXTURE0+1);
            glBindTexture(GL_TEXTURE_2D, m_textures[m_framebufferDeferred.textures[1]].texture);
            glUniform1i(glGetUniformLocation(m_material->shader, "u_smpTextureSpecular"), 1);

            glActiveTexture(GL_TEXTURE0+2);
            glBindTexture(GL_TEXTURE_2D, m_textures[m_framebufferDeferred.textures[2]].texture);
            glUniform1i(glGetUniformLocation(m_material->shader, "u_smpTextureNormal"), 2);

            glActiveTexture(GL_TEXTURE0+3);
            glBindTexture(GL_TEXTURE_2D, m_textures[m_framebufferDeferred.textures[3]].texture);
            glUniform1i(glGetUniformLocation(m_material->shader, "u_smpTextureEnvironment"), 3);

            // TODO Remove
            glActiveTexture(GL_TEXTURE0+4);
            glBindTexture(GL_TEXTURE_2D, m_textures["defdepth"].texture);
            glUniform1i(glGetUniformLocation(m_material->shader, "u_smpTextureDepth"), 4);

            draw(Shape::QUAD);
          }

          virtual void deferredShadingPassTwoInit() override
          {

          }

          virtual Boolean pathCreate(String const& name, String const& path) override
          {
#           ifdef XEAR_GLES
              return false;
#           else
              auto iterator=m_paths.find(name);

              if(iterator!=m_paths.end())
                return false;

              HandlePath handle;
              handle.name=name;
              handle.path=glGenPathsNV(1);
              DEVICE_CHECK_ERROR("Unable to allocate path [path: " << name << "].");

              glPathStringNV(handle.path, GL_PATH_FORMAT_SVG_NV, path.size(), path.c_str());
              DEVICE_CHECK_ERROR("Unable to create path [path: " << name << "].");

              glPathParameteriNV(handle.path, GL_PATH_JOIN_STYLE_NV, GL_ROUND_NV);
              glPathParameteriNV(handle.path, GL_PATH_STROKE_WIDTH_NV, 2);
              DEVICE_CHECK_ERROR("Unable to configure path [path: " << name << "].");

              m_paths.insert({name, handle});

              return true;
#           endif
          }

          virtual Boolean pathUpdate(String const& name, String const& path) override
          {
#           ifndef XEAR_GLES
              auto iterator=m_paths.find(name);

              if(iterator==m_paths.end())
                return false;

              glPathStringNV(iterator->second.path, GL_PATH_FORMAT_SVG_NV, path.size(), path.c_str());
              DEVICE_CHECK_ERROR("Unable to update path [path: " << name << "].");
#           endif

            return true;
          }

          virtual void drawPath(String name, mat4 const& modelViewProjection) override
          {
#           ifndef XEAR_GLES
              auto iterator=m_paths.find(name);

              if(iterator==m_paths.end())
                return;

              gl::cullFace(CullFace::NONE);
              gl::clear(GL_STENCIL_BUFFER_BIT);

              flags(Flag::COLOR|Flag::BLUE);

              glLoadMatrixf(glm::value_ptr(modelViewProjection));
              DEVICE_CHECK_ERROR("Unable to translate path [path: " << name << "].");

              glStencilStrokePathNV(iterator->second.path, 0x1, 0xffffffff);
              glCoverStrokePathNV(iterator->second.path, GL_CONVEX_HULL_NV);
              DEVICE_CHECK_ERROR("Unable to draw path [path: " << name << "].");

              flags(Flag::DEFAULT);

              gl::cullFace(CullFace::BACK);
#           endif
          }

          virtual void drawDebug(mat4 const& model, mat4 const& viewProjection) override
          {
            // coordinates
            mat4 modelCoordinates;
            modelCoordinates=glm::translate(modelCoordinates, vec3{0.0f, 0.0f, 0.0f});

            bindUniformMatrix4f("u_matModel", modelCoordinates);
            bindUniformMatrix3f("u_matNormal", glm::inverse(glm::transpose(mat3{modelCoordinates})));

            flags(Flag::TRANSFORM);
            primitive(Primitive::LINE);

            glLineWidth(1.5f);

            draw(Shape::BUFFER_DEBUG);

            glLineWidth(1.0f);

            primitive(Primitive::DEFAULT);

            // cube
            mat4 modelCube;
            modelCube=glm::scale(modelCube, vec3{0.3f, 0.3f, 0.3f});
            modelCube=glm::translate(modelCube, vec3{0.0f, 0.0f, 0.0f});

            bindUniformMatrix4f("u_matModel", modelCube);
            bindUniformMatrix3f("u_matNormal", glm::inverse(glm::transpose(mat3{modelCube})));

            draw(Shape::CUBE);


            // reset
            flags(Flag::DEFAULT);
          }


        private:
          vec4 m_clearColor={0.2f, 0.2f, 0.2f, 0.0f};
          vec4 m_viewport={0.0f, 0.0f, 0.0f, 0.0f};
          IntU m_flags=Flag::DEFAULT;

          Primitive m_primitive=Primitive::DEFAULT;

          Layouts m_layouts;

          HandleMaterial* m_material;
          HandleGeometry* m_geometry;

          HandleBuffer m_offscreenBuffer{2};
          Framebuffer m_framebufferDeferred;

          map<String, HandleGeometry> m_geometries;
          map<String, HandleMaterial> m_materials;
          map<String, HandlePath> m_paths;

          map<String, HandleTexture> m_textures;
          map<String, String> m_texturesMapped;
          x::misc::Slots<IntU32> m_textureSlots;


          // TODO Cleanup.
          void registerDefaultAttributeLayouts()
          {
            dbgi3("Register default attribute layouts");

            m_layouts.useBufferLayout(BUFFER_LAYOUT_PLAIN);
            m_layouts.buffer->add("a_vecPosition", AttributeType::ATTRIBUTE, Scalar::FLOAT, 3);
            m_layouts.buffer->add("a_vecTexCoords", AttributeType::ATTRIBUTE, Scalar::FLOAT, 4);
            m_layouts.buffer->add("a_vecNormal", AttributeType::ATTRIBUTE, Scalar::FLOAT, 3);
            m_layouts.buffer->add("a_vecTangent", AttributeType::ATTRIBUTE, Scalar::FLOAT, 3);
            m_layouts.buffer->add("a_vecBiTangent", AttributeType::ATTRIBUTE, Scalar::FLOAT, 3);

            m_layouts.useMaterialLayout(MATERIAL_LAYOUT_PLAIN);
            m_layouts.material->add("a_vecPosition", AttributeType::ATTRIBUTE, Scalar::FLOAT, 3);
            m_layouts.material->add("a_vecTexCoords", AttributeType::ATTRIBUTE, Scalar::FLOAT, 4);
            m_layouts.material->add("a_vecNormal", AttributeType::ATTRIBUTE, Scalar::FLOAT, 3);
            m_layouts.material->add("a_vecTangent", AttributeType::ATTRIBUTE, Scalar::FLOAT, 3);
            m_layouts.material->add("a_vecBiTangent", AttributeType::ATTRIBUTE, Scalar::FLOAT, 3);

            m_layouts.material->add("u_intFlags", AttributeType::UNIFORM, Scalar::INT, 1);
            m_layouts.material->add("u_intLights", AttributeType::UNIFORM, Scalar::INT, 1);
            m_layouts.material->add("u_vecViewport", AttributeType::UNIFORM, Scalar::FLOAT, 4);

            m_layouts.material->add("u_matNormal", AttributeType::UNIFORM, Scalar::FLOAT, 3);
            m_layouts.material->add("u_matModel", AttributeType::UNIFORM, Scalar::FLOAT, 4);
            m_layouts.material->add("u_matViewProjection", AttributeType::UNIFORM, Scalar::FLOAT, 4);

            m_layouts.material->add("u_matCamera", AttributeType::UNIFORM, Scalar::FLOAT, 3);
            m_layouts.material->add("u_vecSurface", AttributeType::UNIFORM, Scalar::FLOAT, 4);

            m_layouts.material->add("u_smpTextureLights", AttributeType::UNIFORM, Scalar::INT, 1);
            m_layouts.material->add("u_smpTextureAlbedo", AttributeType::UNIFORM, Scalar::INT, 1);
            m_layouts.material->add("u_smpTextureSpecular", AttributeType::UNIFORM, Scalar::INT, 1);
            m_layouts.material->add("u_smpTextureNormal", AttributeType::UNIFORM, Scalar::INT, 1);
            m_layouts.material->add("u_smpTextureEnvironment", AttributeType::UNIFORM, Scalar::INT, 1);
            m_layouts.material->add("u_smpTextureDepth", AttributeType::UNIFORM, Scalar::INT, 1);
            m_layouts.material->add("u_smpTextureBuffer", AttributeType::UNIFORM, Scalar::INT, 1);

            // TODO Remove/cleanup ...
            m_layouts.material->add("u_smpTextureDiffuse", AttributeType::UNIFORM, Scalar::INT, 1);
            m_layouts.material->add("u_smpTextureMetalness", AttributeType::UNIFORM, Scalar::INT, 1);
            m_layouts.material->add("u_smpTextureIrradiance", AttributeType::UNIFORM, Scalar::INT, 1);
            m_layouts.material->add("u_smpTextureSurface0", AttributeType::UNIFORM, Scalar::INT, 1);
            m_layouts.material->add("u_smpTextureSurface1", AttributeType::UNIFORM, Scalar::INT, 1);
          }

          void registerDefaultGeometry()
          {
            geometryCreate(SHAPES[Shape::CUBE], scene::data::geometry::createCube(BUFFER_LAYOUT_PLAIN));
            geometryCreate(SHAPES[Shape::QUAD], scene::data::geometry::createQuad(BUFFER_LAYOUT_PLAIN));

            scene::data::Geometry<> debug{BUFFER_LAYOUT_PLAIN, Primitive::LINE};

            typedef decltype(debug)::typeIndex Index;
            typedef decltype(debug)::typeVertex Vertex;

            debug << vector<Index>{0, 1, 0xffffffff, 2, 3, 0xffffffff, 4, 5, 0xffffffff};

            debug << vector<Vertex>{
              -1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
               1.0f,  0.0f,  0.0f,  1.0f, 0.0f, 0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
               0.0f, -1.0f,  0.0f,  0.0f, 1.0f, 0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
               0.0f,  1.0f,  0.0f,  0.0f, 1.0f, 0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
               0.0f,  0.0f, -1.0f,  0.0f, 0.0f, 1.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
               0.0f,  0.0f,  1.0f,  0.0f, 0.0f, 1.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f};

            geometryCreate(SHAPES[Shape::BUFFER_DEBUG], debug);
          }

          void updateAttributeBindings()
          {
            if(!m_layouts.material || !m_layouts.buffer)
              return;

            dbgi3("Update attribute bindings [bufferLayout: " << m_layouts.buffer->name << ", shaderLayout: " << m_layouts.material->name << "].");

            auto stride=m_layouts.buffer->stride();
            auto offset=0L;

            for(auto attribute : *m_layouts.material)
            {
              if(AttributeType::UNIFORM==attribute->type)
                attribute->location=resolveUniform(attribute->name, false);
              else
                attribute->location=resolveAttribute(attribute->name, false);
            }

            for(auto attribute : *m_layouts.buffer)
            {
              auto materialAttribute=m_layouts.material->get(attribute->name);

              if(-1!=materialAttribute->location)
              {
                dbgi3("Bind buffer attribute [attribute: " << attribute->name
                  << ", count: " << attribute->count
                  << ", scalar: " << attribute->scalar
                  << ", stride: " << stride
                  << ", offset: " << offset
                  << ", location: " << materialAttribute->location << "].");

                gl::vertexAttributePointer(materialAttribute->name,
                                           materialAttribute->location,
                                           attribute->count,
                                           attribute->scalar,
                                           stride,
                                           offset);

                gl::vertexAttributeArrayEnable(materialAttribute->name, materialAttribute->location);
              }

              offset+=attribute->length;
            }


            // FIXME Keep map of bound uniforms etc. and re-bind on material change...
            bindUniform1i("u_intFlags", m_flags);
          }

          Int resolveAttribute(String name, Boolean force)
          {
            if(!m_material)
            {
              dbgi3("Unable to resolve attribute - no shader active.");

              return -1;
            }

            if(!m_layouts.material)
            {
              dbgi3("Unable to resolve attribute - no shader layout active.");

              return -1;
            }

            auto attribute=m_layouts.material->get(name);

            if(attribute && (force || !attribute->resolved))
            {
              attribute->location=gl::attributeLocation(m_material->shader, name.c_str());
              attribute->resolved=true;

              dbgi3("Resolved attribute location [name: " << attribute->name << ", location: " << attribute->location << "].");
            }

            return attribute->location;
          }

          Int resolveUniform(String name, Boolean force)
          {
            if(!m_material)
            {
              dbgi3("Unable to resolve uniform - no shader active.");

              return -1;
            }

            if(!m_layouts.material)
            {
              dbgi3("Unable to resolve uniform - no shader layout active.");

              return -1;
            }

            auto attribute=m_layouts.material->get(name);

            if(attribute && (force || !attribute->resolved))
            {
              attribute->location=gl::uniformLocation(m_material->shader, name.c_str());
              attribute->resolved=true;

              dbgi3("Resolved uniform location [name: " << attribute->name << ", location: " << attribute->location << "].");
            }

            return attribute->location;
          }

          Boolean textureAllocate(String name, TextureType type)
          {
            auto iterator=m_textures.find(name);

            if(iterator!=m_textures.end())
              return false;

            dbgi3("Allocate texture [name: " << name << "].");

            HandleTexture handle;
            handle.name=name;
            handle.type=type;
            handle.index=m_textureSlots.acquire();

            gl::textureCreate(name, 1, &handle.texture);

            gl::textureActive(name, handle.index);
            gl::textureBind(name, type, handle.texture);

            m_textures[name]=handle;

            return true;
          }

          IntU compileShader(String name, String vertexShaderSource, String fragmentShaderSource)
          {
            auto shaderVertex=gl::shaderCreate(name, mime::Type::TEXT_GLSL_VERTEX_SHADER);
            gl::shaderSource(name, shaderVertex, 1, vertexShaderSource);

            if(!gl::shaderCompile(name, shaderVertex))
            {
              dbgi3("Shader compilation failed [name: " << name << ", type: VERTEX, source: " << vertexShaderSource << "].");

              return 0;
            }

            auto shaderFragment=gl::shaderCreate(name, mime::Type::TEXT_GLSL_FRAGMENT_SHADER);
            gl::shaderSource(name, shaderFragment, 1, fragmentShaderSource);

            if(!gl::shaderCompile(name, shaderFragment))
            {
              dbgi3("Shader compilation failed [name: " << name << ", type: FRAGMENT, source: " << fragmentShaderSource << "].");

              return 0;
            }

            vector<IntU> shaders;
            shaders.push_back(shaderVertex);
            shaders.push_back(shaderFragment);

            return gl::programCreate(name, shaders);
          }
      };
    }
  }
}


#endif
