/*
 * service.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RENDERER_GL_SERVICE_H_
#define XEAR_RENDERER_GL_SERVICE_H_


#include <xear.h>

#include <xear/renderer/service.h>


namespace xear
{
  namespace renderer
  {
    namespace gl
    {
      using namespace x;


      class Service: public renderer::Service
      {
        public:
          virtual ~Service()
          {

          }


          virtual void setup() override;
          virtual void teardown() override;


          virtual renderer::Rdi& get(String const& name) override;
          virtual void destroy(String const& name) override;
      };
    }
  }
}


#endif
