/*
 * service.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <map>

#include "service.h"
#include "rdi.h"

#include <xear/common/resources.h>


namespace xear
{
  namespace renderer
  {
    namespace gl
    {
      namespace impl
      {
        struct Holder
        {
          std::map<String, Rdi> instance;
        };


        static unique_ptr<Holder> HOLDER;
      }


      void Service::setup()
      {
        dbgi("Setup GL.");
      }

      void Service::teardown()
      {
        dbgi("Teardown GL.");
      }


      renderer::Rdi& Service::get(String const& name)
      {
        return impl::HOLDER->instance[name];
      }

      void Service::destroy(String const& name)
      {
        impl::HOLDER->instance.erase(name);
      }
    }
  }
}
