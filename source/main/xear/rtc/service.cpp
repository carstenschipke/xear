/*
 * service.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include "service.h"


#pragma clang diagnostic push
# pragma clang diagnostic ignored "-Winconsistent-missing-override"
#
# include <webrtc/base/fileutils.h>
# include <webrtc/base/logging.h>
# include <webrtc/base/pathutils.h>
# include <webrtc/base/ssladapter.h>
# include <webrtc/base/stream.h>
#pragma clang diagnostic pop


namespace xear
{
  namespace rtc
  {
    void Service::setup()
    {
      dbgi("Setup WebRTC.");

#     ifdef DEBUG_DEPS
//        auto pathname="/tmp/libxear_rtc.log";

//#       if 3<DEBUG
          ::rtc::LogMessage::LogThreads(true);
          ::rtc::LogMessage::LogTimestamps(true);
//#         if 4<DEBUG
            ::rtc::LogMessage::LogToDebug(::rtc::LS_VERBOSE);
//#         else
//            ::rtc::LogMessage::LogToDebug(::rtc::LS_INFO);
//#         endif
//#       endif
//
//        ::rtc::StreamInterface* stream=::rtc::Filesystem::OpenFile(::rtc::Pathname{pathname}, "a");
//
//        if(stream)
//          ::rtc::LogMessage::AddLogToStream(stream, ::rtc::LS_VERBOSE);
//        else
          ::rtc::LogMessage::LogToDebug(::rtc::LS_VERBOSE);
#     endif

      ::rtc::InitializeSSL();
    }

    void Service::teardown()
    {
      dbgi("Teardown WebRTC.");

      ::rtc::CleanupSSL();
    }
  }
}
