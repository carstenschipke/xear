/*
 * decoder.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear/rtc/decoder.h>


namespace xear
{
  namespace rtc
  {
    RtcDecoder::RtcDecoder(RtcVideoSink* sink):
      m_sink(sink)
    {
      dbgi4("Create.");
    }

    RtcDecoder::~RtcDecoder()
    {
      dbgi4("Destroy.");
    }


    void RtcDecoder::SetSize(Int width, Int height)
    {
      dbgi3("Set size " << width << ", " << height);
    }

    void RtcDecoder::RenderFrame(VideoFrame const* videoFrame)
    {
      dbgi3("Render frame " << videoFrame->GetTimeStamp() << ", " << videoFrame->GetWidth() << ", " << videoFrame->GetHeight());

      auto resolution=m_sink->resolution();

      auto width=resolution.width;
      auto height=resolution.height;

      auto aspectRenderer=(Float)width/height;
      auto aspectFrame=(Float)videoFrame->GetWidth()/videoFrame->GetHeight();

      /**
       * TODO Evaluate possibilities to move colorspace conversion and/or scaling to shader.
       */
      if(aspectRenderer==aspectFrame)
      {
        auto size=sizeof(Byte)*3*videoFrame->GetWidth()*videoFrame->GetHeight();
        auto frameRgb=make_unique<io::Image>(io::image::Ref{(Int)videoFrame->GetWidth(), (Int)videoFrame->GetHeight(), size});

        videoFrame->ConvertToRgbBuffer(FourCC::FOURCC_RGB3, frameRgb->get()->data, size, 0);

#       if defined(DEBUG) && 4<DEBUG
          dbgi4("CAM FRAME RGB " << frameRgb->size() << ", " << frameRgb->width() << ", " << frameRgb->height());
          for(auto i=0; i<3; i++)
            printf("CAM FRAME RGB %.2X %.2X %.2X\n", ((Byte*)frameRgb->data())[3*i+0], ((Byte*)frameRgb->data())[3*i+1], ((Byte*)frameRgb->data())[3*i+2]);
#       endif

        m_sink->append(move(frameRgb));
      }
      else
      {
        auto heightTo=videoFrame->GetHeight();
        auto widthTo=static_cast<Int>(aspectRenderer*heightTo);

#       if defined(DEBUG) && 3<DEBUG
          dbgi3("Scaling camera frame :o( [source-aspect: " << aspectFrame
            << ", source-width: " << videoFrame->GetWidth()
            << ", source-height: " << videoFrame->GetHeight()
            << ", target-aspect: " << aspectRenderer
            << ", target-width: " << widthTo
            << ", target-height: " << heightTo
            << "]");
#       endif

        auto size=sizeof(Byte)*3*widthTo*heightTo;
        auto frameRgb=make_unique<io::Image>(io::image::Ref{(Int)widthTo, (Int)heightTo, size});

        videoFrame->Stretch(widthTo, heightTo, true, true)
          ->ConvertToRgbBuffer(FourCC::FOURCC_RGB3, frameRgb->get()->data, size, 0);

#       if defined(DEBUG) && 4<DEBUG
          dbgi4("CAM FRAME RGB " << frameRgb->size() << ", " << frameRgb->width() << ", " << frameRgb->height());
          for(auto i=0; i<3; i++)
            printf("CAM FRAME RGB %.2X %.2X %.2X\n", ((Byte*)frameRgb->data())[3*i+0], ((Byte*)frameRgb->data())[3*i+1], ((Byte*)frameRgb->data())[3*i+2]);
#       endif

        m_sink->append(move(frameRgb));
      }

      m_fps.update();
    }


    void RtcDecoder::Stop()
    {
      dbgi2("Stop");
    }
  }
}
