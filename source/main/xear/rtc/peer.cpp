/*
 * peer.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear/rtc/peer.h>

#include <x/common/config.h>


namespace xear
{
  namespace rtc
  {
    RtcPeer::RtcPeer(String id, RtcSupervisor* supervisor):
      Task("rtc::RtcPeer"), id(id), m_supervisor(supervisor)
    {
      dbgi4("Create [" << id << "].");
    }

    RtcPeer::~RtcPeer()
    {
      dbgi4("Destroy [" << id << "].");

      dbgi("Disconnect.");

      m_connection->Close();

      m_thread->Quit();
    }


    void RtcPeer::sdp(SessionDescriptionInterface* description)
    {
      m_connection->SetLocalDescription(
        new RtcSessionDescriptionObserverSet(), description);

      String sdp;
      description->ToString(&sdp);

      dbgi2("Create session description [" << sdp << "].");

      ::Json::StyledWriter writer;

      ::Json::Value json;
      json["type"]=description->type();
      json["sdp"]=sdp;

      lock_guard<mutex> lock{m_sdpMonitor};

      m_sdp=writer.write(json);
      m_sdpUpdated.notify_all();
    }

    String RtcPeer::sdp()
    {
      unique_lock<mutex> lock{m_sdpMonitor};

      m_sdpUpdated.wait(lock, [&] {
        return !m_sdp.empty();
      });

      return m_sdp;
    }

    String RtcPeer::candidate()
    {
      unique_lock<mutex> lock{m_candidatesMonitor};

      if(!m_candidatesUpdated.wait_for(lock, seconds{1}, [&] { return !m_candidates.empty(); }))
        return "";

      auto candidate=m_candidates.back();
      m_candidates.pop_back();

      return candidate;
    }


    void RtcPeer::queue(String message)
    {
      dbgi3("[" << id << "] Queue [" << message << "].");

      m_queue.push_back(message);
    }


    void RtcPeer::OnAddStream(MediaStreamInterface* stream)
    {
      dbgi3("[" << id << "] On add stream [" << stream->label() << "].");

      stream->AddRef();

      auto tracks=stream->GetVideoTracks();

      if(!tracks.empty())
        tracks[0]->AddRenderer(m_decoder.get());
    }

    void RtcPeer::OnRemoveStream(MediaStreamInterface* stream)
    {
      dbgi3("[" << id << "] On remove stream [" << stream->label() << "].");

      stream->Release();
    }

    void RtcPeer::OnSignalingChange(PeerConnectionInterface::SignalingState state)
    {
      dbgi3("SIGNALING STATE CHANGE " << state);
    }

    void RtcPeer::OnStateChange(PeerConnectionObserver::StateType state)
    {
      dbgi3("STATE CHANGE " << state);
    }

    void RtcPeer::OnRenegotiationNeeded()
    {
      // Do nothing ...
    }


    void RtcPeer::OnIceCandidate(IceCandidateInterface const* candidate)
    {
      dbgi3("[" << id << "] On ICE candidate [" << candidate << "].");

      String sdp;

      if(!candidate->ToString(&sdp))
      {
        dbge3("[" << id << "] Unable to serialize candidate [" << candidate << "].");

        return;
      }

      ::Json::Value value;
      ::Json::StyledWriter writer;

      value["sdpMid"]=candidate->sdp_mid();
      value["sdpMLineIndex"]=candidate->sdp_mline_index();
      value["candidate"]=sdp;

      auto json=writer.write(value);

      lock_guard<mutex> lock{m_candidatesMonitor};

      m_candidates.push_back(json);
      m_candidatesUpdated.notify_one();
    }


    void RtcPeer::OnDataChannel(DataChannelInterface* channel)
    {
      dbgi("[" << id << "] On data channel [" << channel << "].");

      m_channel=make_unique<RtcChannel>(channel->label(), this, m_connection.get(), channel);
    }


    void RtcPeer::run()
    {
      m_thread=::rtc::ThreadManager::Instance()->WrapCurrentThread();

      ::Json::Reader parser;

      while(!interrupted())
      {
        while(!m_queue.empty())
        {
          auto message=m_queue.front();
          m_queue.pop_front();

          dbgi4("[" << id << "] Process message [" << message << "].");

          ::Json::Value value;

          auto isJson=parser.parse(message, value);

          if(isJson && value.isArray())
          {
            m_supervisor->peerKeepAlive(id);

            ::Json::ArrayIndex idx=0;

            auto type=value.get(idx, ::Json::Value{});
            auto typeStr=type.asString();

            // Assume its the device state array
            if(15==value.size())
            {
              auto eventType=value.get(::Json::ArrayIndex{0}, {});

              if(::Json::ValueType::intValue==eventType.type() && 1==eventType.asInt())
              {
                /**
                 * TODO Integrate.
                 *
                 * [01] motion.x
                 * [02] motion.y
                 * [03] motion.z
                 * [04] orientation.x
                 * [05] orientation.y
                 * [06] orientation.z
                 * [07] orientation.absolute
                 * [08] screen-orientation
                 * [09] screen-dimensions.w
                 * [10] screen-dimensions.h
                 * [11] latitude
                 * [12] longitude
                 * [13] altitude
                 * [14] touch
                 */

                dbgi2("DEVICE EVENT " << value.toStyledString());

                // m_delegate->onEvent(device::Event{});
              }
            }
            // Offer
            else if(0==strcmp("sdp", typeStr.c_str()))
            {
              onOffer(value);
            }
            // ICE candidate
            else if(0==strcmp("cnd", typeStr.c_str()))
            {
              onCandidate(value);
            }
          }
          else
          {
            dbge("[" << id << "] Unable to parse message [" << message << "].");
          }
        }


        if(m_initialized)
          m_encoder->captureFrame();


        m_thread->ProcessMessages(1);
      }
    }


    void RtcPeer::onCandidate(::Json::Value& message)
    {
      dbgi3("[" << id << "] On candidate.");

      ::Json::ArrayIndex idx=1;

      auto candidate=message.get(idx++, ::Json::Value{});

      if(m_initialized)
      {
        while(!m_candidatesPending.empty())
        {
          auto pending=m_candidatesPending.front();
          m_candidatesPending.pop_front();

          onCandidate(pending);
        }

        SdpParseError error;

        IceCandidateInterface* cnd=CreateIceCandidate(
          candidate.get("candidate", ::Json::Value{}).asString(),
          candidate.get("sdpMLineIndex", ::Json::Value{}).asInt(),
          candidate.get("sdpMid", ::Json::Value{}).asString(),
          &error
        );

        m_connection->AddIceCandidate(cnd);

        delete cnd;
      }
      else
      {
        m_candidatesPending.push_back(message);
      }
    }

    void RtcPeer::onOffer(::Json::Value& message)
    {
      if(m_initialized)
        return;


      dbgi3("[" << id << "] On offer.");


      ::Json::ArrayIndex idx=1;


      // Remote description
      auto sdp=message.get(idx++, ::Json::Value{});


      // Remote device configuration
      auto dimensions=message.get(idx++, ::Json::Value{});
      auto angle=message.get(idx++, ::Json::Value{});
      auto depth=message.get(idx++, ::Json::Value{});
      auto ratio=message.get(idx++, ::Json::Value{});
      auto orientation=message.get(idx++, ::Json::Value{});
      auto networkQuality=message.get(idx++, ::Json::Value{});
      auto locale=message.get(idx++, ::Json::Value{});
      auto targetFps=message.get(idx++, ::Json::Value{});

      idx=0;

      auto width=dimensions.get(idx++, ::Json::Value{}).asInt();
      auto height=dimensions.get(idx++, ::Json::Value{}).asInt();


      device::Config configDevice;

      // FIXME Need to multiply by pixel ratio - however keep the incoming video stream at its natural resolution.
      auto resolution=device::Resolution::find(width, height);

      configDevice.resolution.width=resolution.width;
      configDevice.resolution.height=resolution.height;

      configDevice.colorspace=io::COLORSPACE_RGB;
      configDevice.angle=angle.asInt();
      configDevice.depth=depth.asInt();
      configDevice.ratio=ratio.asFloat();
      configDevice.orientation=device::SCREEN_ORIENTATION[orientation.asInt()];
      configDevice.networkQuality=device::NETWORK_QUALITY[networkQuality.asInt()];
      configDevice.locale=locale.asString();
      configDevice.targetFps=targetFps.asInt();

      dbgi("[" << id << "] [" << configDevice << "].");


      // STUN configuration
      String configPathname=XEAR_PATH_ETC;
      String configFilename=XEAR_FILE_ETC;

      auto config=Config::create(configPathname+"/"+configFilename);

      auto stunHost=String{XEAR_RTC_STUN_HOST_DEFAULT};
      auto stunPort=XEAR_RTC_STUN_PORT_DEFAULT;

      if(config)
      {
        stunHost=config->get<String>("stun.host", XEAR_RTC_STUN_HOST_DEFAULT);
        stunPort=config->get<Int>("stun.port", XEAR_RTC_STUN_PORT_DEFAULT);
      }


      // Connection
      auto factory=CreatePeerConnectionFactory(m_thread, m_thread, nullptr, nullptr, nullptr);

      PeerConnectionInterface::IceServer server;
      server.uri="stun:"+stunHost+":"+to_string(stunPort);

      auto configuration=make_unique<PeerConnectionInterface::RTCConfiguration>();
      configuration->servers.push_back(server);

      RtcConstraints constraints;

      m_connection=factory->CreatePeerConnection(
        *configuration, &constraints, nullptr, nullptr, this);

      // m_channel=make_unique<RtcChannel>("xear", this, m_connection.get());

      m_observerSessionDescriptionCreate=make_unique<RtcSessionDescriptionObserverCreate>(this);
      m_observerSessionDescriptionSet=make_unique<RtcSessionDescriptionObserverSet>();


      // Event delegate
      m_delegate=m_supervisor->peerEventDelegate(id, configDevice);


      // Incoming stream
      m_decoder=make_unique<RtcDecoder>(m_supervisor->peerVideoSink(id, configDevice));


      // Outgoing stream
      m_encoder=make_unique<RtcEncoder>(m_thread,
                                        m_supervisor->peerVideoSource(id, configDevice),
                                        configDevice.resolution,
                                        configDevice.colorspace,
                                        configDevice.targetFps);

      m_stream=factory->CreateLocalMediaStream("stream");
      m_stream->AddTrack(factory->CreateVideoTrack(
        "video", factory->CreateVideoSource(m_encoder.get(), nullptr)
      ));

      m_connection->AddStream(m_stream);


      // Answer
      SdpParseError error;
      SessionDescriptionInterface* remoteSessionDescription=
        CreateSessionDescription("offer", sdp.get("sdp", {}).asString(), &error);

      if(!error.description.empty())
        dbge(error.description);

      m_connection->SetRemoteDescription(m_observerSessionDescriptionSet.get(), remoteSessionDescription);
      m_connection->CreateAnswer(m_observerSessionDescriptionCreate.get(), &constraints);

      m_thread->ProcessMessages(1);
      m_initialized=true;
    }
  }
}
