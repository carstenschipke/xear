/*
 * channel.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear/rtc/channel.h>


namespace xear
{
  namespace rtc
  {
    RtcChannel::RtcChannel(String name, RtcPeer* const peer, PeerConnectionInterface* const connection):
      name(name), m_peer(peer), m_connection(connection)
    {
      dbgi("Create [" << name << "].");

      DataChannelInit config;
      config.id=1;
//      config.reliable=false;
//      config.ordered=true;
//      config.maxRetransmits=1;

      m_channelRef=m_connection->CreateDataChannel(name, &config);

      m_channel=m_channelRef.get();
      m_channel->RegisterObserver(this);

      dbgi("CHANNEL STATE " << m_channel->state());
    }

    RtcChannel::RtcChannel(String name, RtcPeer* const peer, PeerConnectionInterface* const connection, DataChannelInterface* const channel):
      name(name), m_peer(peer), m_connection(connection), m_channel(channel)
    {
      dbgi("Wrap [" << name << "].");

      m_channel->AddRef();
      m_channel->RegisterObserver(this);

      dbgi("CHANNEL STATE " << m_channel->state());
    }

    RtcChannel::~RtcChannel()
    {
      dbgi("Destroy [" << name << "].");

      m_channel->Release();
    }


    const Boolean RtcChannel::isOpen()
    {
      return m_open;
    }

    void RtcChannel::send(String message)
    {
      dbgi("[" << m_peer->id << "] Send [" << message << "].");

      dbgi("CHANNEL STATE " << m_channel->state());
      m_channel->Send(DataBuffer{message});
    }


    void RtcChannel::OnStateChange()
    {
      dbgi("[" << m_peer->id << "] On state change [" << m_channel << "].");

      if(DataChannelInterface::DataState::kOpen==m_channel->state())
        onOpen();
      else if(DataChannelInterface::DataState::kClosing==m_channel->state())
        onClose();
    }

    void RtcChannel::OnMessage(DataBuffer const& buffer)
    {
      String const message{(Char*)buffer.data.data(), buffer.data.size()};

      dbgi("[" << m_peer->id << "] On message [" << message << "].");

      m_peer->queue(message);
    }


    void RtcChannel::onOpen()
    {
      dbgi("[" << m_peer->id << "] On open.");

      m_open=true;
    }

    void RtcChannel::onClose()
    {
      dbgi("[" << m_peer->id << "] On close.");

      m_open=false;
    }
  }
}
