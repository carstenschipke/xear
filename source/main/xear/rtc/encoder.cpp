/*
 * encoder.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear/rtc/encoder.h>


namespace xear
{
  namespace rtc
  {
    RtcEncoder::RtcEncoder(::rtc::Thread* thread,
                           RtcVideoSource* source,
                           device::Resolution const& resolution,
                           io::Colorspace const& colorspace,
                           Int targetFps):
      VideoCapturer(thread), m_source(source), m_start(chrono::high_resolution_clock::now())
    {
      dbgi4("Create.");

      vector<VideoFormat> formats;
      formats.push_back(VideoFormat(
        resolution.width,
        resolution.height,
        VideoFormat::FpsToInterval(targetFps),
        FOURCC_RGB3));

      m_image=make_unique<io::Image>(resolution.width, resolution.height, colorspace);

      SetSupportedFormats(formats);
    }

    RtcEncoder::~RtcEncoder()
    {
      dbgi4("Destroy.");
    }


    Boolean RtcEncoder::captureFrame()
    {
      if(CS_RUNNING!=capture_state())
        return false;


      dbgi3("Capture frame.");

      auto& image=m_image->ref();
      auto captureFormat=GetCaptureFormat();

      m_source->fetch(image);


#     if defined(DEBUG) && 4<DEBUG
        dbgi4("OGL FRAME RGB " << image.size);
        for(auto i=0; i<3; i++)
          printf("OGL FRAME RGB %.2X %.2X %.2X\n", ((Byte*)image.data)[3*i+0], ((Byte*)image.data)[3*i+1], ((Byte*)image.data)[3*i+2]);
#     endif


      if(image.width!=captureFormat->width || image.height!=captureFormat->height)
      {
        dbgi3("Attempt to update capture resolution.");

        vector<VideoFormat> formats;
        formats.push_back(VideoFormat(image.width, image.height, captureFormat->interval, captureFormat->fourcc));

        SetSupportedFormats(formats);
        SetCaptureFormat(&formats[0]);
      }


      // FIXME Get device orientation & resolution - scale to constant dimensions (can resize gl texture later on efficiently when webrtc decides to change resolution).
      CapturedFrame frame;
      frame.width=image.width;
      frame.height=image.height;
      frame.fourcc=FOURCC_RGB3;
      frame.data_size=(IntU)image.size;
      frame.data=image.data;
      frame.time_stamp=chrono::duration_cast<chrono::nanoseconds>(chrono::high_resolution_clock::now()-m_start).count();

      SignalFrameCaptured(this, &frame);


      // TODO Adaptively adjust target fps based on client device & network properties.
      this_thread::sleep_for(nanoseconds{captureFormat->interval}-m_fps.durationNs());


      // Print FPS.
      m_fps.update();


      return true;
    }

    CaptureState RtcEncoder::Start(VideoFormat const& format)
    {
      dbgi2("Start.");

      if(CS_RUNNING==capture_state())
        return CS_RUNNING;

      VideoFormat formatSupported;

      if(GetBestCaptureFormat(format, &formatSupported))
        SetCaptureFormat(&formatSupported);

      return CS_RUNNING;
    }

    void RtcEncoder::Stop()
    {
      dbgi("Stop.");

      SetCaptureState(CS_STOPPED);
    }

    Boolean RtcEncoder::IsRunning()
    {
      return CS_RUNNING==capture_state();
    }

    Boolean RtcEncoder::IsScreencast() const
    {
      return false;
    }

    Boolean RtcEncoder::GetPreferredFourccs(vector<IntU>* fourccs)
    {
      if(!fourccs)
        return false;

      fourccs->push_back(FOURCC_RGB3);

      return true;
    }

    Boolean RtcEncoder::GetBestCaptureFormat(VideoFormat const& formatDesired, VideoFormat* formatBest)
    {
      dbgi3("GetBestCaptureFormat [" << formatDesired.width << ", " << formatDesired.height << ", " << formatBest->fourcc << ", " << formatBest->interval << "].");

      if(!formatBest)
        return false;

      formatBest->width=formatDesired.width;
      formatBest->height=formatDesired.height;
      formatBest->fourcc=FOURCC_RGB3;
      formatBest->interval=formatDesired.interval;

      return true;
    }
  }
}
