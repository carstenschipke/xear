/*
 * service.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RTC_SERVICE_H_
#define XEAR_RTC_SERVICE_H_


#include <xear.h>

#include <x/common/service.h>


namespace xear
{
  namespace rtc
  {
    using namespace std;

    using namespace x;


    class Service: public x::common::Service
    {
      public:
        virtual ~Service()
        {

        }


        virtual void setup() override;
        virtual void teardown() override;
    };
  }
}


#endif
