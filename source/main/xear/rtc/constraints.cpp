/*
 * constraints.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear/rtc/constraints.h>


namespace xear
{
  namespace rtc
  {
    RtcConstraints::RtcConstraints()
    {
      dbgi4("Create.");
    }

    RtcConstraints::~RtcConstraints()
    {
      dbgi4("Destroy.");
    }


    MediaConstraintsInterface::Constraints const& RtcConstraints::GetMandatory() const
    {
      return m_mandatory;
    }

    MediaConstraintsInterface::Constraints const& RtcConstraints::GetOptional() const
    {
      return m_optional;
    }


    void RtcConstraints::setMandatory(String key, String value)
    {
      set(m_mandatory, key, value);
    }

    void RtcConstraints::setOptional(String key, String value)
    {
      set(m_optional, key, value);
    }

    void RtcConstraints::set(MediaConstraintsInterface::Constraints& container, String key, String value)
    {
      auto i=0;

      for(auto& constraint : container)
      {
        if(0==constraint.key.compare(key))
        {
          container.emplace(container.begin()+i, key, value);

          return;
        }

        ++i;
      }

      container.emplace_back(key, value);
    }
  }
}
