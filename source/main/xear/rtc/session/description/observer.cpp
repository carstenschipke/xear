/*
 * observer.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear/rtc/session/description/observer.h>


namespace xear
{
  namespace rtc
  {
    RtcSessionDescriptionObserverSet::RtcSessionDescriptionObserverSet()
    {
      dbgi4("Create.");
    }

    RtcSessionDescriptionObserverSet::~RtcSessionDescriptionObserverSet()
    {
      dbgi4("Destroy.");
    }


    void RtcSessionDescriptionObserverSet::OnSuccess()
    {
      // Do nothing.
    }

    void RtcSessionDescriptionObserverSet::OnFailure(String const& error)
    {
      dbge(error);
    }


    Int RtcSessionDescriptionObserverSet::AddRef() const
    {
      // Do nothing.
      return 1;
    }

    Int RtcSessionDescriptionObserverSet::Release() const
    {
      // Do nothing.
      return 0;
    }


    RtcSessionDescriptionObserverCreate::RtcSessionDescriptionObserverCreate(RtcPeer* const peer):
      m_peer(peer)
    {
      dbgi4("Create.");
    }

    RtcSessionDescriptionObserverCreate::~RtcSessionDescriptionObserverCreate()
    {
      dbgi4("Destroy.");
    }


    void RtcSessionDescriptionObserverCreate::OnSuccess(SessionDescriptionInterface* description)
    {
      m_peer->sdp(description);
    }

    void RtcSessionDescriptionObserverCreate::OnFailure(String const& error)
    {
      dbge(error);
    }


    Int RtcSessionDescriptionObserverCreate::AddRef() const
    {
      // Do nothing.
      return 1;
    }

    Int RtcSessionDescriptionObserverCreate::Release() const
    {
      // Do nothing.
      return 0;
    }
  }
}
