/*
 * player.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_PLAYER_H_
#define XEAR_PLAYER_H_


#include <xear.h>

#include <xear/input/events.h>
#include <xear/input/controller/default.h>

#include <xear/renderer/service.h>
#include <xear/driver/surface.h>

#include <xear/scene/scene.h>
#include <xear/scene/camera.h>


namespace xear
{
  using namespace std;

  using namespace x;

  using namespace xear::common;
  using namespace xear::input;
  using namespace xear::scene;


  class Player: public EventLoopDelegate
  {
    public:
      virtual ~Player()
      {
        dbgi("Destroy");
      }


      void run()
      {
        m_surface=ServiceManager::resolve<driver::Surface>("surface");
        m_surface->create("default", {0, 0, 1024, 768});

        auto& rdi=ServiceManager::resolve<xear::renderer::Service>("renderer")->get("default");
        rdi.resources.registerResourceType(ResourceType::GEOMETRY, XEAR_PATH_MODEL);
        rdi.resources.registerResourceType(ResourceType::MARKER, XEAR_PATH_MARKER);
        rdi.resources.registerResourceType(ResourceType::MESH, XEAR_PATH_MODEL);
        rdi.resources.registerResourceType(ResourceType::SCENE, XEAR_PATH_SCENE);
        rdi.resources.registerResourceType(ResourceType::SCRIPT, XEAR_PATH_SCRIPT);
        rdi.resources.registerResourceType(ResourceType::SHADER, XEAR_PATH_SHADER);
        rdi.resources.registerResourceType(ResourceType::TEXTURE, XEAR_PATH_TEXTURE);

        rdi.init(renderer::Target::SCREEN, io::COLORSPACE_RGBA, m_surface->viewport("default"));
        rdi.clearColor(vec4{0.1f, 0.1f, 0.1f, 1.0f});

        rdi.materialCreate("pbs");
        rdi.materialUse("pbs");

        rdi.pathCreate("foo", "M100,180 L40,10 L190,120 L10,120 L160,10 z");

//        rdi.deferredShading();

        // m_scene=rdi.resources.load<Scene>(ResourceType::SCENE, Mime_Type::TEXT_JSON, "world");
        m_scene=new Scene("world", &rdi);

        m_scene->load();
        m_scene->init();

        // TODO Define camera & player in json.
        DefaultController controller{"controller"};
        controller.attach(m_scene->camera);

        m_events.delegateEventLoop=this;
        m_events.listenerEvent=&controller;
        m_events.listenerMouse=&controller;
        m_events.listenerMouse->relative=true;

        m_events.listen();

        // TODO Quit ...

        m_surface->destroy("default");
      }


      virtual void loop() override
      {
        m_scene->renderer->begin();

//        m_scene->renderer->deferredShadingPassOne();

        m_scene->renderer->clear();

        m_scene->update();

        // TODO Render environment, bind environment map(s), render models ...

        m_scene->render();

        mat4 model;
        model=glm::translate(model, vec3{0.0f, 0.0f, -300.0f});
        m_scene->renderer->drawPath("foo", m_scene->camera->viewProjection*model);

        m_scene->renderer->drawDebug({}, m_scene->camera->viewProjection);

//        m_scene->renderer->deferredShadingPassTwoInit();

        // TODO Transparent models, lighting, postprocessing ...

//        m_scene->renderer->deferredShadingPassTwo();

        m_surface->flush("default");
      }


  private:
    Events m_events;

    Scene* m_scene;
    driver::Surface* m_surface;
  };
}


#endif
