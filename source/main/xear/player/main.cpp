/*
 * player/main.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include "player.h"

#ifdef DEBUG
# if defined(TARGET_OS_IPHONE) && 0<TARGET_OS_IPHONE
#   include <debug/IosDebugService.h>
# endif
#endif

#include <driver/sdl/surface.h>
#include <renderer/gl/service.h>


using namespace std;

using namespace x;
using namespace xear;


Int main(Int argc, Char *argv[])
{
  dbgsi("Launch");

# ifdef DEBUG
#   if defined(TARGET_OS_IPHONE) && 0<TARGET_OS_IPHONE
      x::common::ServiceManager::create<DebugService>("debug");
#   endif
# endif

  x::common::ServiceManager::create<driver::sdl::Surface>("surface");
  x::common::ServiceManager::create<renderer::gl::Service>("renderer");

  x::common::ServiceManager::resolve<driver::sdl::Surface>("surface")->vsync=true;

  Player player;
  player.run();

  dbgsi("Leave");

  x::common::ServiceManager::destroy("renderer");
  x::common::ServiceManager::destroy("surface");

# ifdef DEBUG
    x::common::ServiceManager::destroy("debug");
# endif

  return EXIT_SUCCESS;
}
