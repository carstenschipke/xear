/*
 * engine.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include "engine.h"

#include <chrono>
#include <mutex>

#include <libplatform/libplatform.h>
#include <v8.h>

#include <x/io/file.h>


namespace xear
{
  namespace script
  {
    namespace v8
    {
      using namespace std;
      using namespace std::chrono;


      namespace impl=::v8;


      namespace
      {
        inline String decodeStringV8(impl::Local<impl::Value> const& value)
        {
          impl::String::Utf8Value utf8{value};

          return String{*utf8};
        }


        inline Any to_any(impl::Local<impl::Value> const& value)
        {
          if(value->IsBoolean())
            return Any::of<Boolean>(value->BooleanValue());
          if(value->IsInt32())
            return Any::of<Int>(value->Int32Value());
          if(value->IsString())
            return Any::of<String>(decodeStringV8(value->ToString()));

          return Any::null();
        }


        class AllocatorV8: public impl::ArrayBuffer::Allocator
        {
          public:
            virtual void* Allocate(Size length)
            {
              void* data=AllocateUninitialized(length);

              if(data)
                return memset(data, 0, length);

              return data;
            }

            virtual void* AllocateUninitialized(Size length)
            {
              return malloc(length);
            }

            virtual void Free(void* data, Size length)
            {
              free(data);
            }
        };


        struct ScriptV8
        {
          public:
            ScriptV8():
              m_compiled(false), m_loaded(false)
            {
              dbgi("Create.");
            }

            ~ScriptV8()
            {
              dbgi("Destroy.");
            }


            void load(impl::Isolate* isolate, String path)
            {
              lock_guard<decltype(m_monitor)> lock{m_monitor};

              x::io::File file{path};

              if(!m_loaded || file.modifiedSince(m_loadedAt))
              {
                dbgi("(Re-)load [" << file.name() << "].");

                auto source=file.readText();

                m_source=impl::String::NewFromUtf8(isolate, source.c_str(), impl::NewStringType::kNormal).ToLocalChecked();

                m_loadedAt=system_clock::now();
                m_loaded=true;

                m_compiled=false;
              }
            }

            void compile(impl::Isolate* isolate, impl::Local<impl::Context> context)
            {
              lock_guard<decltype(m_monitor)> lock{m_monitor};

              if(!m_compiled)
              {
                impl::TryCatch tryCatchCompile{isolate};

                if(!impl::Script::Compile(context, m_source).ToLocal(&m_script))
                  throw decodeStringV8(tryCatchCompile.Exception());

                m_script->Run(context).ToLocalChecked();

                impl::TryCatch tryCatchInvoke{isolate};

                if(tryCatchInvoke.HasCaught())
                  throw decodeStringV8(tryCatchInvoke.Exception());
              }
            }


          private:
            Boolean m_compiled;
            Boolean m_loaded;
            time_point<system_clock> m_loadedAt;

            impl::Local<impl::Script> m_script;
            impl::Local<impl::String> m_source;

            mutex m_monitor;
        };


        class ContextV8: public script::Context
        {
          public:
            ContextV8(impl::Isolate::CreateParams config):
              m_config(config),
              m_isolate(impl::Isolate::New(m_config)),
              m_scopeIsolate(m_isolate),
              m_scopeHandle(m_isolate),
              m_globals(impl::ObjectTemplate::New(m_isolate))
            {
              dbgi4("Create.");

              m_isolate->SetData(0, this);
            }

            virtual ~ContextV8()
            {
              dbgi4("Destroy.");
            }


            virtual void load(String path) override
            {
              dbgi("Load script [" << path << "].");

              m_scripts[path].load(m_isolate, path);
            }

            virtual void compile() override
            {
              dbgi("Compile scripts.");

              // SCOPE
              impl::HandleScope scopeHandle{m_isolate};

              auto context=impl::Context::New(m_isolate, nullptr, m_globals);

              m_contextGlobal.Reset(m_isolate, context);
              impl::Context::Scope scopeContext{context};


              // COMPILE SCRIPTS
              for(auto& script : m_scripts)
              {
                dbgi("Load script [" << script.first << "].");

                script.second.compile(m_isolate, context);
              }
            }

            virtual void add(Module* module) override
            {
              module->init(*this);
            }

            virtual void add(String nameModule) override
            {
              x::common::ServiceManager::resolve<script::Module>(nameModule)->init(*this);
            }

            virtual void bind0(String name, function<void(Context&)> callback) override
            {
              m_globals->Set(impl::String::NewFromUtf8(m_isolate, name.c_str()), impl::FunctionTemplate::New(
                m_isolate, [](impl::FunctionCallbackInfo<impl::Value> const& info) {

                  auto nameCallbackV8=info.Callee().As<impl::Function>()->GetName();
                  auto nameCallback=decodeStringV8(nameCallbackV8);

                  ((ContextV8*)info.GetIsolate()->GetData(0))->m_callbacks0[nameCallback](
                    *((ContextV8*)info.GetIsolate()->GetData(0))
                  );
              }));

              m_callbacks0[name]=callback;
            }

            virtual void bind1(String name, function<void(Context&, Any&)> callback) override
            {
              m_globals->Set(impl::String::NewFromUtf8(m_isolate, name.c_str()), impl::FunctionTemplate::New(
                m_isolate, [](impl::FunctionCallbackInfo<impl::Value> const& info) {

                  auto nameCallbackV8=info.Callee().As<impl::Function>()->GetName();
                  auto nameCallback=decodeStringV8(nameCallbackV8);

                  auto arg0=to_any(info[0]);

                  ((ContextV8*)info.GetIsolate()->GetData(0))->m_callbacks1[nameCallback](
                    *((ContextV8*)info.GetIsolate()->GetData(0)), arg0
                  );
              }));

              m_callbacks1[name]=callback;
            }

            virtual void bind2(String name, function<void(Context&, Any&, Any&)> callback) override
            {
              m_globals->Set(impl::String::NewFromUtf8(m_isolate, name.c_str()), impl::FunctionTemplate::New(
                m_isolate, [](impl::FunctionCallbackInfo<impl::Value> const& info) {

                  auto nameCallbackV8=info.Callee().As<impl::Function>()->GetName();
                  auto nameCallback=decodeStringV8(nameCallbackV8);

                  auto arg0=to_any(info[0]);
                  auto arg1=to_any(info[1]);

                  ((ContextV8*)info.GetIsolate()->GetData(0))->m_callbacks2[nameCallback](
                    *((ContextV8*)info.GetIsolate()->GetData(0)), arg0, arg1
                  );
              }));

              m_callbacks2[name]=callback;
            }

            virtual void call(String name) override
            {
              // SCOPE
              impl::HandleScope scopeHandle{m_isolate};

              auto context=impl::Local<impl::Context>::New(m_isolate, m_contextGlobal);
              impl::Context::Scope scopeContext{context};


              // RESOLVE
              impl::Local<impl::Value> func;
              auto funcName=impl::String::NewFromUtf8(m_isolate, name.c_str(), impl::NewStringType::kNormal).ToLocalChecked();

              if(!context->Global()->Get(context, funcName).ToLocal(&func) || !func->IsFunction())
                dbgi("Unable to resolve function [" << name << "].");

              auto funcHandle=impl::Local<impl::Function>::Cast(func);


              // INVOKE
              impl::TryCatch tryCatch{m_isolate};
              funcHandle->Call(context, context->Global(), 0, {}).ToLocalChecked();

              if(tryCatch.HasCaught())
                dbge(decodeStringV8(tryCatch.Message()->Get()));
            }


          private:
            impl::Isolate::CreateParams m_config;
            impl::Isolate* m_isolate;
            impl::Isolate::Scope m_scopeIsolate;
            impl::HandleScope m_scopeHandle;

            impl::Global<impl::Context> m_contextGlobal;
            impl::Local<impl::ObjectTemplate> m_globals;

            map<String, ScriptV8> m_scripts;

            map<String, function<void(Context&)>> m_callbacks0;
            map<String, function<void(Context&, Any&)>> m_callbacks1;
            map<String, function<void(Context&, Any&, Any&)>> m_callbacks2;
        };


        static AllocatorV8* allocatorV8;
        static impl::Platform* platformV8;
      }


      void Engine::setup()
      {
        dbgi("Setup V8.");

        impl::V8::InitializeICU();

        platformV8=impl::platform::CreateDefaultPlatform();

        impl::V8::InitializePlatform(platformV8);
        impl::V8::Initialize();

        allocatorV8=new AllocatorV8();
      }

      void Engine::teardown()
      {
        dbgi("Teardown V8.");

        impl::V8::Dispose();
        impl::V8::ShutdownPlatform();

        delete platformV8;
        delete allocatorV8;
      }


      script::Context* Engine::create(String name)
      {
        lock_guard<decltype(m_monitor)> lock{m_monitor};

        impl::Isolate::CreateParams config;
        config.array_buffer_allocator=allocatorV8;

        m_instance[name]=make_unique<ContextV8>(config);

        return m_instance[name].get();
      }

      void Engine::destroy(String name)
      {
        lock_guard<decltype(m_monitor)> lock{m_monitor};

        m_instance.erase(name);
      }
    }
  }
}
