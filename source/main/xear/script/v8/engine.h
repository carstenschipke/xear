/*
 * engine.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_SCRIPT_V8_ENGINE_H_
#define XEAR_SCRIPT_V8_ENGINE_H_


#include <functional>
#include <mutex>

#include <xear.h>
#include <xear/script/engine.h>


namespace xear
{
  namespace script
  {
    namespace v8
    {
      using namespace std;

      using namespace x;


      class Engine: public script::Engine
      {
        public:
          virtual void setup() override;
          virtual void teardown() override;


          virtual script::Context* create(String name) override;
          virtual void destroy(String name) override;


        private:
          mutex m_monitor;
          map<String, unique_ptr<script::Context>> m_instance;
      };
    }
  }
}


#endif
