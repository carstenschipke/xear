/*
 * light.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear/scene/light.h>
#include <xear/scene/scene.h>


namespace xear
{
  namespace scene
  {
    Light::Light(String const& name):
      Node(NodeType::LIGHT, name)
    {
      dbgi4("Create [" << *this << "].");
    }


    Light::~Light()
    {
      dbgi4("Destroy [" << *this << "].");
    }


    void Light::load(Json::Value const& value)
    {
      dbgi2("Loaded [" << *this << "].");

      auto& valueDirection=value.at("LD");
      auto& valueColor=value.at("LC");
      auto& valueProperties=value.at("LP");

      if(valueDirection.isArray())
      {
        direction=vec3{
          valueDirection.at<Float>(0, 0.0f),
          valueDirection.at<Float>(1, 0.0f),
          valueDirection.at<Float>(2, 0.0f)};
      }

      if(valueColor.isArray())
      {
        color=vec3{
          valueColor.at<Float>(0, 0.0f),
          valueColor.at<Float>(1, 0.0f),
          valueColor.at<Float>(2, 0.0f)};
      }

      if(valueProperties.isArray())
      {
        properties=vec3{
          valueProperties.at<Float>(0, 0.0f),
          valueProperties.at<Float>(1, 0.0f),
          valueProperties.at<Float>(2, 0.0f)};
      }

      Node::load(value);
    }

    void Light::init()
    {
      dbgi2("Initialize [" << *this << "].");

      scene().add(this);

      Node::init();
    }

    void Light::update()
    {
      Node::update();
    }

    void Light::render()
    {
      dbgi2("Render [" << *this << "].");

      auto& scene=this->scene();
      auto model=matModel();

      scene.renderer->bindUniformMatrix4f("u_matModel", model);
      scene.renderer->bindUniformMatrix3f("u_matNormal", glm::inverse(glm::transpose(mat3{model})));

      for(auto& node : children)
        node->render();
    }
  }
}
