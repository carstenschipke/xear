/*
 * camera.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear/scene/camera.h>
#include <xear/scene/scene.h>


namespace xear
{
  namespace scene
  {
    Camera::Camera(String const& name):
      Node(NodeType::CAMERA, name)
    {
      dbgi4("Create.");
    }

    Camera::~Camera()
    {
      dbgi4("Destroy.");
    }


    Float Camera::aspectRatio()
    {
      return m_aspectRatio;
    }

    vec4 Camera::viewport()
    {
      return m_viewport;
    }

    void Camera::viewport(vec4 const& viewport)
    {
      m_aspectRatio=viewport.z/viewport.w;
      m_viewport=viewport;
    }

    void Camera::move(Keys& keys)
    {
      Float const timeDelta=scene().fps().frameDuration().count();

      if(0<(keys.mask&Key::STRAFE_LEFT))
        position-=glm::normalize(glm::cross(front, up))*timeDelta*keys.velocityX;
      else if(0<(keys.mask&Key::STRAFE_RIGHT))
        position+=glm::normalize(glm::cross(front, up))*timeDelta*keys.velocityX;

      if(0<(keys.mask&Key::MOVE_FORWARD))
        position+=front*timeDelta*keys.velocityZ;
      else if(0<(keys.mask&Key::MOVE_BACKWARD))
        position-=front*timeDelta*keys.velocityZ;
    }

    void Camera::look(Float x, Float y)
    {
      if(0!=x || 0!=y)
      {
        Float const timeDelta=scene().fps().frameDuration().count();

        yaw+=x*timeDelta;
        pitch+=-y*timeDelta;

        if(89.0f<pitch)
          pitch=89.0f;
        if(-89.0f>pitch)
          pitch=-89.0f;
      }
    }

    void Camera::scroll(Float degree)
    {
      if(1.0f<=fieldOfView && 45.0f>=fieldOfView)
        fieldOfView-=degree;
      if(1.0f>=fieldOfView)
        fieldOfView=1.0f;
      if(45.0f<=fieldOfView)
        fieldOfView=45.0f;
    }


    void Camera::init()
    {
      dbgi2("Initialize [" << *this << "].");

      viewport(scene().renderer->viewport());

      scene().add(this);
    }

    void Camera::update()
    {
      front=glm::normalize(vec3{
        cos(glm::radians(pitch))*cos(glm::radians(yaw)),
        sin(glm::radians(pitch)),
        cos(glm::radians(pitch))*sin(glm::radians(yaw)),
      });

      direction=glm::normalize(position-target);
      vec3 cameraRight=glm::normalize(glm::cross(vecUp, direction));

      up=glm::normalize(glm::cross(direction, cameraRight));

      view=glm::lookAt(position, position+front, up);
      projection=glm::perspective(fieldOfView, m_aspectRatio, near, far);

      viewProjection=projection*view;

      dbgi4("Update [" << *this << "].");
    }
    
    void Camera::activate()
    {
      scene().camera=this;
    }

    void Camera::deactivate()
    {

    }
  }
}
