/*
 * node.h
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear/scene/node.h>

#include <xear/scene/camera.h>
#include <xear/scene/light.h>
#include <xear/scene/mesh.h>
#include <xear/scene/scene.h>

#include <xear/common/resources.h>

#include <x/io/resource/json.h>


namespace xear
{
  namespace scene
  {
    using namespace x::io;

    using namespace xear::common;


    Node::Node(NodeType type, String const& name):
      type(type), name(name)
    {
      dbgi4("Create [" << *this << "].");
    }


    Node::~Node()
    {
      dbgi4("Destroy [" << *this << "].");
    }


    void Node::load()
    {
      dbgi2("Load [" << *this << "].");

      auto resource=scene().renderer->resources.get<resource::Json>(ResourceType::SCENE, mime::Type::TEXT_JSON, name);
      auto& root=resource.resolve();

      if(root.isObject())
        load(root);
      else
        dbgi("Unable to load scene [" << name << "].");
    }

    void Node::load(Json::Value const& value)
    {
      auto& valuePosition=value.at("P");
      auto& valueScale=value.at("S");
      auto& valueRotation=value.at("R");

      if(valuePosition.isArray())
      {
        position=vec3{
          valuePosition.at<Float>(0, 0.0f),
          valuePosition.at<Float>(1, 0.0f),
          valuePosition.at<Float>(2, 0.0f)};
      }

      if(valueScale.isArray())
      {
        scale=vec3{
          valueScale.at<Float>(0, 1.0f),
          valueScale.at<Float>(1, 1.0f),
          valueScale.at<Float>(2, 1.0f)};
      }

      if(valueRotation.isArray())
      {
        rotation=vec3{
          valueRotation.at<Float>(0, 0.0f),
          valueRotation.at<Float>(1, 0.0f),
          valueRotation.at<Float>(2, 0.0f)};
      }

      if(value.has("G"))
      {
        auto& scene=this->scene();

        // TODO Fallback geometry (& meshes) for debugging (or leave blank and decide in renderer).
        geometry=value.at<String>("G", "");

        if(!scene.renderer->geometryExists(geometry))
        {
          auto resource=scene.renderer->resources.get(ResourceType::GEOMETRY, mime::Type::APPLICATION_XEAR_GEOMETRY, geometry);
          auto data=data::Geometry<>::unserialize(resource->streamInput());

          scene.renderer->geometryCreate(geometry, *data);
        }
      }

      if(value.has("C"))
      {
        for(auto& child : value.at("C"))
        {
          auto nodeName=child.at<String>("N", "");

          if(nodeName.empty())
            continue;

          Node* node=nullptr;
          NodeType const nodeType=NODE_TYPES[child.at<Int>("T", 1)];

          auto tmp=create(nodeType, nodeName);
          node=tmp.get();

          children.push_back(move(tmp));

          node->parent=this;

          node->load(child);
          node->load();
        }
      }

      dbgi2("Loaded [" << *this << "].");
    }

    void Node::init()
    {
      dbgi2("Initialize [" << *this << "].");

      for(auto& node : children)
        node->init();
    }

    void Node::update()
    {
      for(auto& node : children)
        node->update();
    }

    void Node::render()
    {
      dbgi4("Render [" << *this << "].");

      if(NodeType::MODEL==type)
      {
        auto& scene=this->scene();

        if(360<=rotation.x)
          rotation.x=0.0f;
        if(0>=rotation.y)
          rotation.y=360.0f;

        rotation.x+=0.01f;
        rotation.y-=0.01f;

        auto model=matModel();

        scene.renderer->bindUniformMatrix4f("u_matModel", model);

        // TODO Can use glUniform transpose flag?
        scene.renderer->bindUniformMatrix3f("u_matNormal", glm::inverse(glm::transpose(mat3{model})));
      }

      for(auto& node : children)
        node->render();
    }


    Node* const Node::get(String const& name)
    {
      for(auto& node : children)
      {
        if(0==strcmp(name.c_str(), node->name.c_str()))
          return node.get();
      }

      return nullptr;
    }

    void Node::add(unique_ptr<Node> node)
    {
      dbgi2("Add [" << *node << "].");

      node->parent=this;
      node->init();

      children.push_back(std::move(node));
    }

    // TODO Verify.
    unique_ptr<Node> Node::remove(String const& name)
    {
      auto i=0;

      for(auto& node : children)
      {
        if(0==strcmp(name.c_str(), node->name.c_str()))
        {
          auto tmp=move(children[i]);

          children.erase(children.begin()+i);

          return tmp;
        }

        ++i;
      }

      return nullptr;
    }

    void Node::activate()
    {

    }

    void Node::deactivate()
    {

    }


    Scene& Node::scene()
    {
      if(m_scene)
        return *m_scene;

      if(parent)
        m_scene=&parent->scene();

      return *m_scene;
    }

    mat4 Node::matModel()
    {
      auto model=this->model;

      if(parent)
        model+=parent->matModel();

      model=glm::translate(model, position);

      model=glm::rotate(model, rotation.x, vec3{1.0, 0.0, 0.0});
      model=glm::rotate(model, rotation.y, vec3{0.0, 1.0, 0.0});
      model=glm::rotate(model, rotation.z, vec3{0.0, 0.0, 1.0});

      model=glm::scale(model, scale);

      return model;
    }


    unique_ptr<Node> Node::create(NodeType type, String const& name)
    {
      switch(type)
      {
        case NodeType::MESH:
          return move(make_unique<Mesh>(name));
        case NodeType::LIGHT:
          return move(make_unique<Light>(name));
        case NodeType::CAMERA:
          return move(make_unique<Camera>(name));
        default:
          return move(make_unique<Node>(type, name));
      }
    }
  }
}
