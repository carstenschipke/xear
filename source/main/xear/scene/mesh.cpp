/*
 * mesh.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear/scene/mesh.h>
#include <xear/scene/scene.h>

#include <xear/common/resources.h>


namespace xear
{
  namespace scene
  {
    using namespace xear::common;


    Mesh::Mesh(String const& name):
      Node(NodeType::MESH, name)
    {
      dbgi4("Create [" << *this << "].");
    }


    Mesh::~Mesh()
    {
      dbgi4("Destroy [" << *this << "].");
    }


    void Mesh::load(Json::Value const& value)
    {
      auto resource=scene().renderer->resources.get(ResourceType::MESH, mime::Type::APPLICATION_XEAR_MESH, name);

      data=data::Mesh::unserialize(resource->streamInput());
      data->material=value.at<String>("M", "");

      dbgi2("Loaded [" << *this << "].");
    }

    void Mesh::init()
    {
      dbgi2("Initialize [" << *this << "].");

      auto& scene=this->scene();

      // TODO We will work with a generic PBS shader and just define some subroutines for specific stuff.
      // scene->renderer->materialCreate(data->material);

      // TODO Auto-allocate / generate memory-mapped texture atlas and return handles.
      // if(0<data->texture.size())
      //   scene->renderer->textureCreate(data->texture);

      scene.renderer->textureCreate("diffuse");
      scene.renderer->textureCreate("specular");
      scene.renderer->textureCreate("normal");
    }

    void Mesh::update()
    {

    }

    void Mesh::render()
    {
      dbgi4("Render [" << *this << "].");

      auto& scene=this->scene();

      scene.renderer->geometryUse(parent->geometry);

      scene.renderer->textureUse("diffuse", "u_smpTextureAlbedo");
      scene.renderer->textureUse("specular", "u_smpTextureSpecular");
      scene.renderer->textureUse("normal", "u_smpTextureNormal");

      // TODO Remove dummies.
      // X  : ROUGHNESS
      // Y  : METALNESS
      // Z  : ALBEDO
      // W  : METAL
      scene.renderer->bindUniform4f("u_vecSurface", {0.5f, 1.0f, 0.1f, 1.0f});

      // if(0<data.texture.size())
      //   scene->renderer->textureUse(data.texture, "u_smpTexture0");

      scene.renderer->draw(data->offset, data->count);
    }
  }
}
