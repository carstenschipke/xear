/*
 * scene.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear/scene/scene.h>


namespace xear
{
  namespace scene
  {
    Scene::Scene(String const& name, renderer::Rdi* renderer):
      Node(NodeType::SCENE, name), renderer(renderer)
    {
      dbgi4("Create [" << *this << "].");

      m_scene=this;
    }


    Scene::~Scene()
    {
      dbgi4("Destroy [" << *this << "].");
    }


    void Scene::update()
    {
      dbgi4("Update [" << *this << "].");

      camera->update();

      renderer->bindUniformMatrix3f("u_matCamera", {
        camera->position.x,  camera->position.y,  camera->position.z,
        camera->direction.x, camera->direction.y, camera->direction.z,
        0.0f,                0.0f,                0.0f
      });

      dbgi3("Bind projection matrix [" << glm::to_string(camera->viewProjection) << "].");

      renderer->bindUniformMatrix4f("u_matViewProjection", camera->viewProjection);

      auto const count=m_lights.size();

      if(0<count)
      {
        auto const properties=4;
        auto const stride=3*properties;

        auto idx=0u;

        Float lights[count*stride];

        for(auto light : m_lights)
        {
          lights[idx*stride+ 0]=light->position.x;
          lights[idx*stride+ 1]=light->position.y;
          lights[idx*stride+ 2]=light->position.z;

          lights[idx*stride+ 3]=light->direction.x;
          lights[idx*stride+ 4]=light->direction.y;
          lights[idx*stride+ 5]=light->direction.z;

          lights[idx*stride+ 6]=light->color.r;
          lights[idx*stride+ 7]=light->color.g;
          lights[idx*stride+ 8]=light->color.b;

          lights[idx*stride+ 9]=light->properties.x;
          lights[idx*stride+10]=light->properties.y;
          lights[idx*stride+11]=light->properties.z;

          ++idx;
        }

        renderer->bindLights(&lights[0], (Int)count, properties);
      }
    }


    common::Fps const& Scene::fps()
    {
      return renderer->fps;
    }

    void Scene::add(Light* light)
    {
      m_lights.push_back(light);
    }

    void Scene::add(Camera* camera)
    {
      if(!this->camera)
        this->camera=camera;

      m_cameras.push_back(camera);
    }
  }
}
