/*
 * socket.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include "socket.h"

#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>


namespace xear
{
  namespace debug
  {
      namespace impl
      {
        using namespace x;


        class Host: public debug::Host
        {
          public:
            virtual void handle() override
            {
              dbgsi("Handle Debug Socket Data.");

              struct sockaddr_in addressRemote;
              socklen_t addressRemoteSize=sizeof(addressRemote);

              Int socketRemote=-1;

              if(-1<(socketRemote=accept(m_socket, (struct sockaddr*)&addressRemote, &addressRemoteSize)))
              {
                dbgsi("Accepted connection [" << socketRemote << "].");
              }
              else
              {
                dbgsi("Failed to accept connection.");
              }

//              Size const size=128;
//              Char buffer[size];
//
//              Size const received=recv(m_socket, buffer, size, 0);
//              // Size const received=recvfrom(m_socket, buffer, size, 0, &m_addressRemote, &m_addressRemoteSize);
//
//              if(-1<received)
//              {
//                dbgsi("Message received [recv: " << received << ", size: " << size << ", buffer: " << buffer << "]");
//              }
            }


            virtual void start() override
            {
              dbgsi("Start Debug Socket.");

              m_socket=socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);

              struct sockaddr_in address;
              socklen_t addressSize=sizeof(address);
              memset(&address, 0, addressSize);

              address.sin_len=addressSize;
              address.sin_family=AF_INET;
              address.sin_port=htons(0);
              address.sin_addr.s_addr=htonl(INADDR_ANY);

              dbgsi("Bind Debug Socket [socket: " << m_socket << ", address: " << address.sin_addr.s_addr << ", port: " << ntohs(address.sin_port) << "].");
              bind(m_socket, (struct sockaddr*)&address, addressSize);

              getsockname(m_socket, (struct sockaddr*)&address, &addressSize);

              dbgsi("Bound " << ntohs(address.sin_port));
              dbgsi("Listening? " << listen(m_socket, 10));
            }

            virtual void stop() override
            {
              dbgsi("Stop Debug Socket.");

              close(m_socket);
            }


          private:
            Int m_socket;
        };
      }


      void Socket::setup()
      {
        dbgi("Setup Debug Socket.");

        m_host=make_unique<impl::Host>();
      }

      void Socket::teardown()
      {
        dbgi("Teardown Debug Socket.");
      }

      void Socket::run()
      {
        m_host->start();

        while(!interrupted())
          m_host->handle();

        m_host->stop();
      }
  }
}
