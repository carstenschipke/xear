/*
 * socket.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_DEBUG_SOCKET_H_
#define XEAR_DEBUG_SOCKET_H_


#include <xear.h>

#include <x/common/service.h>


namespace xear
{
  namespace debug
  {
      using namespace std;

      using namespace x;


      class Host
      {
        public:
          virtual ~Host()
          {

          }


          virtual void handle() = 0;


          virtual void start() = 0;
          virtual void stop() = 0;
      };


      class Socket: public x::common::ServiceWorker
      {
        public:
          virtual ~Socket()
          {

          }


          virtual void setup() override;
          virtual void teardown() override;


        protected:
          virtual void run() override;


        private:
          unique_ptr<Host> m_host;
      };
  }
}


#endif
