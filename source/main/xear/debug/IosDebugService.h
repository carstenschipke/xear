/*
 * IosDebugService.h
 *
 * @author carsten.schipke@gmail.com
 */
// TODO Create a lib to abstract os/platform specifics.
#ifndef XEAR_DEBUG_SERVICE_IOS_H_
#define XEAR_DEBUG_SERVICE_IOS_H_


#include <map>

#include <xear.h>

#include "service.h"


#ifndef XEAR_DEBUG_SERVICE_IOS_PORT
# define XEAR_DEBUG_SERVICE_IOS_PORT 9999
#endif


class DebugService;


#ifdef __OBJC__
# import <CoreFoundation/CoreFoundation.h>


  @interface IosDebugService: NSObject


  -(id)initWithService:(DebugService*)service;

  -(void)start;
  -(void)stop;


  -(void)accept:(CFSocketNativeHandle)socket withAddress:(NSData*)address;


  @end


  typedef IosDebugService* DebugServiceImpl;
#else
  typedef void* DebugServiceImpl;
#endif


class DebugService: public xear::debug::Service
{
  public:
    DebugService();

    virtual ~DebugService()
    {

    }


    virtual void setup() override;
    virtual void teardown() override;


    virtual void invokeMethod(x::String const& name, xear::debug::Request const& request) override;
    virtual void registerMethod(x::String const& name, std::function<void(xear::debug::Request const&)> handler) override;

    virtual void dispatch(x::String const& command) override;


  private:
    std::map<x::String, std::function<void(xear::debug::Request const&)>> m_methods;

    DebugServiceImpl m_impl;
};


#endif
