/*
 * service.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_DEBUG_SERVICE_H_
#define XEAR_DEBUG_SERVICE_H_


#include <xear.h>

#include <x/common/service.h>


namespace xear
{
  namespace debug
  {
    using namespace std;

    using namespace x;


    class Session
    {
      public:
        void send(String const& message);
    };


    struct Request
    {
      String command;
      Session* session;
    };


    class Service: public x::common::Service
    {
      public:
        virtual ~Service()
        {

        }


        virtual void setup() override = 0;
        virtual void teardown() override = 0;


        virtual void invokeMethod(String const& name, Request const& request) = 0;
        virtual void registerMethod(String const& name, function<void(Request const&)> handler) = 0;

        virtual void dispatch(String const& command) = 0;
    };
  }
}


#endif
