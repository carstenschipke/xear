/*
 * IosDebugService.mm
 *
 * @author carsten.schipke@gmail.com
 */
#include "IosDebugService.h"

#include <netinet/in.h>
#include <sys/socket.h>

#include <x/misc/string.h>

#import <CFNetwork/CFNetwork.h>
#import <Foundation/Foundation.h>


DebugService::DebugService():
  m_impl([[IosDebugService alloc] initWithService:this])
{

}


void DebugService::setup()
{
  dbgi("Setup Debug Service.");

  [m_impl start];
}

void DebugService::teardown()
{
  dbgi("Teardown Debug Service.");

  [m_impl stop];
}


void DebugService::invokeMethod(x::String const& name, xear::debug::Request const& request)
{
  x::String methodName;
  x::misc::string::lowercase(name, methodName);

  auto entry=m_methods.find(methodName);

  if(m_methods.end()==entry)
    dbge("Debug method not implemented [name: " << methodName << "].");
    // request.session->send("1");
  else
    m_methods[methodName](request);
}

void DebugService::registerMethod(x::String const& name, std::function<void(xear::debug::Request const&)> handler)
{
  x::String methodName;
  x::misc::string::lowercase(name, methodName);

  m_methods[methodName]=handler;
}

void DebugService::dispatch(x::String const& command)
{
  auto offsetSub=command.find(" ");

  xear::debug::Request request;
  request.command=command.substr(offsetSub+1);

  invokeMethod(command.substr(0, offsetSub), request);
}


static void cf_socket_handle_tcp_accept(CFSocketRef socket, CFSocketCallBackType type, CFDataRef address, void const* data, void* info)
{
	@autoreleasepool
  {
    if(kCFSocketAcceptCallBack==type)
      [(__bridge IosDebugService*)info accept:*(CFSocketNativeHandle*)data withAddress:[(__bridge NSData*)address copy]];
    else
      close(*(CFSocketNativeHandle*)data);
	}
}

static void cf_socket_handle_udp_data(CFSocketRef socket, CFSocketCallBackType type, CFDataRef address, void const* data, void* info)
{
	@autoreleasepool
  {
    auto dataRef=(CFDataRef)data;
    NSData* bytes=[(__bridge NSData*)dataRef copy];

    auto string=[[NSString alloc] initWithData:bytes encoding:NSUTF8StringEncoding];

    dbgsi("UDP RECV [size: " << std::to_string([string length]) << ", data: " << [string UTF8String] << "].");

    // TODO Buffer command & flush at \x04.
	}
}


@interface IosDebugServiceConnection: NSObject <NSStreamDelegate>
{
  DebugService* m_service;

  CFSocketNativeHandle m_socket;

  BOOL m_opened;

  NSInputStream* m_streamInput;
  NSOutputStream* m_streamOutput;
}


-(id)initWithService:(DebugService*)service withSocket:(CFSocketNativeHandle)socket withInput:(NSInputStream*)streamInput withOutput:(NSOutputStream*)streamOutput;


-(void)open;
-(void)close;


@end


@implementation IosDebugServiceConnection


-(id)initWithService:(DebugService*)service withSocket:(CFSocketNativeHandle)socket withInput:(NSInputStream*)streamInput withOutput:(NSOutputStream*)streamOutput;
{
  if(self=[super init])
  {
    m_service=service;

    m_socket=socket;
    m_opened=false;

    m_streamInput=streamInput;
    m_streamOutput=streamOutput;

    [self open];
  }

  return self;
}

-(void)dealloc
{
  [self close];
}


-(void)open
{
  if(m_opened)
    return;

  dbgsi2("Open Debug Session.");

  [m_streamInput setDelegate:self];
  [m_streamOutput setDelegate:self];

  [m_streamInput scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
  [m_streamOutput scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];

  [m_streamInput open];
  [m_streamOutput open];

  m_opened=true;
}

-(void)close
{
  if(!m_opened)
    return;

  dbgsi2("Close Debug Session.");

  [m_streamInput removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
  [m_streamOutput removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];

  [m_streamInput close];
  [m_streamOutput close];

  close(m_socket);

  m_opened=false;
}

-(void)stream:(NSStream*)streamSource handleEvent:(NSStreamEvent)event
{
  if(NSStreamEventHasBytesAvailable==event && m_streamInput==streamSource)
  {
    auto const bufferSize=4096u;
    auto data=[NSMutableData data];

    NSInteger read=0;
    UInt8 buffer[bufferSize];

    while(0<(read=[m_streamInput read:buffer maxLength:bufferSize]))
      [data appendBytes:buffer length:read];

    if(0<[data length])
    {
      if(4==((uint8_t*)[data bytes])[[data length]-1])
      {
        [data setLength:[data length]-1];

        [self close];
      }

      auto message=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];

      dbgsi("TCP RECV [size: " << std::to_string([message length]) << ", type: " << [message UTF8String] << "].");

      m_service->dispatch([message UTF8String]);
    }
    else
    {
      [self close];
    }
  }
}


@end


@implementation IosDebugService
{
  DebugService* m_service;

  CFSocketRef m_socket;
  NSMutableSet<IosDebugServiceConnection*>* m_connections;
}


-(id)initWithService:(DebugService*)service
{
  if(self=[super init])
    m_service=service;

  return self;
}


-(void)start
{
  m_connections=[[NSMutableSet alloc] init];

  CFSocketContext context;
  context.version=0;
	context.info=(__bridge void*)(self);
  context.retain=nil;
  context.release=nil;
  context.copyDescription=nil;

  // TODO Configurable.
  // TCP
  if(true)
  {
    m_socket=CFSocketCreate(kCFAllocatorDefault, PF_INET, SOCK_STREAM, IPPROTO_TCP, kCFSocketAcceptCallBack, cf_socket_handle_tcp_accept, &context);
  }
  // UDP
  else
  {
    m_socket=CFSocketCreate(kCFAllocatorDefault, PF_INET, SOCK_DGRAM, IPPROTO_UDP, kCFSocketDataCallBack, cf_socket_handle_udp_data, &context);

    auto options=CFSocketGetSocketFlags(m_socket);
    options|=kCFSocketCloseOnInvalidate|kCFSocketAutomaticallyReenableReadCallBack;

    CFSocketSetSocketFlags(m_socket, options);
  }

  auto reuseEnabled=1;
  setsockopt(CFSocketGetNative(m_socket), SOL_SOCKET, SO_REUSEADDR, (void*)&reuseEnabled, sizeof(reuseEnabled));

  struct sockaddr_in address;
  memset(&address, 0, sizeof(address));

  address.sin_len=sizeof(address);
  address.sin_family=AF_INET;
  address.sin_port=htons(XEAR_DEBUG_SERVICE_IOS_PORT);
  address.sin_addr.s_addr=htonl(INADDR_ANY);
 
  auto data=CFDataCreate(kCFAllocatorDefault, (UInt8*)&address, sizeof(address));
  CFSocketSetAddress(m_socket, data);
  CFRelease(data);

  auto loopSource=CFSocketCreateRunLoopSource(kCFAllocatorDefault, m_socket, 0);
  CFRunLoopAddSource(CFRunLoopGetCurrent(), loopSource, kCFRunLoopDefaultMode);
  CFRelease(loopSource);

  dbgsi("Debug Service Listener Ready [host: ANY, port: " << std::to_string(XEAR_DEBUG_SERVICE_IOS_PORT) << ", protocol: TCP].");
}

-(void)stop
{
  CFSocketInvalidate(m_socket);
  CFRelease(m_socket);
}


-(void)accept:(CFSocketNativeHandle)socket withAddress:(NSData*)address
{
  dbgsi("TCP ACPT [socket: " << std::to_string(socket) << "].");

  CFReadStreamRef refStreamIn=nullptr;
  CFWriteStreamRef refStreamOut=nullptr;

  CFStreamCreatePairWithSocket(kCFAllocatorDefault, socket, &refStreamIn, &refStreamOut);
        
  if(refStreamIn && refStreamOut)
  {
    CFReadStreamSetProperty(refStreamIn, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);
    CFWriteStreamSetProperty(refStreamOut, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);

    [m_connections addObject:[[IosDebugServiceConnection alloc] initWithService:m_service withSocket:socket withInput:(__bridge NSInputStream*)refStreamIn withOutput:(__bridge NSOutputStream*)refStreamOut]];
  }

  if(refStreamIn)
    CFRelease(refStreamIn);

  if(refStreamOut)
    CFRelease(refStreamOut);
}


@end