/*
 * calibration.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_VISION_OPENCV_CALIBRATION_H_
#define XEAR_VISION_OPENCV_CALIBRATION_H_


#include <opencv2/opencv.hpp>


#include <xear.h>


namespace xear
{
  namespace vision
  {
    using namespace std;

    using namespace x;


    class Calibration
    {
      public:
        mat3 intrinsic;
        // mat4<Float> distortion;


        Calibration(Double fx, Double fy, Double cx, Double cy);
        // Calibration(Double fx, Double fy, Double cx, Double cy, Double distorsionCoeff[5]);


        Double fx() const;
        Double fy() const;

        Double cx() const;
        Double cy() const;
    };
  }
}


#endif
