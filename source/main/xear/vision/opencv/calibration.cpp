/*
 * calibration.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include "calibration.h"


namespace xear
{
  namespace vision
  {
    Calibration::Calibration(Double fx_, Double fy_, Double cx_, Double cy_)
    {
      intrinsic[1][1]=fx_;
      intrinsic[0][0]=fy_;
      intrinsic[0][2]=cx_;
      intrinsic[1][2]=cy_;

//      distortion.create(5, 1);
//
//      for(Int i=0; i<5; i++)
//        distortion(i)=0;
    }

//    Calibration::Calibration(Double fx_, Double fy_, Double cx_, Double cy_, Double distorsionCoefficient_[5])
//    {
//      intrinsic=cv::Matx33f::zeros();
//
//      intrinsic(1, 1)=fx_;
//      intrinsic(0, 0)=fy_;
//      intrinsic(0, 2)=cx_;
//      intrinsic(1, 2)=cy_;
//
//      distortion.create(5, 1);
//
//      for(Int i=0; i<5; i++)
//        distortion(i)=distorsionCoefficient_[i];
//    }


    Double Calibration::fx() const
    {
      return intrinsic[1][1];
    }

    Double Calibration::fy() const
    {
      return intrinsic[0][0];
    }

    Double Calibration::cx() const
    {
      return intrinsic[0][2];
    }

    Double Calibration::cy() const
    {
      return intrinsic[1][2];
    }
  }
}
