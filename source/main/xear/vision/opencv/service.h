/*
 * service.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_VISION_OPENCV_SERVICE_H_
#define XEAR_VISION_OPENCV_SERVICE_H_


#include <xear.h>
#include <xear/vision/service.h>


namespace xear
{
  namespace vision
  {
    namespace opencv
    {
      using namespace std;

      using namespace x;


      class Service: public vision::Service
      {
        public:
          virtual ~Service()
          {

          }


          virtual void setup() override;
          virtual void teardown() override;


          virtual Result& markerMatch(String queue, String name) override;
          virtual vec2& vanishingPoint(String queue) override;

          virtual void markerAdd(String queue, MatchMethod method, String name, String path) override;

          virtual void queue(String name, io::Image&& frame) override;
          virtual void queue(String name, io::Image const& frame) override;

          virtual void queueAdd(String name) override;
          virtual void queueRemove(String name) override;


        protected:
          virtual void run() override;
      };
    }
  }
}


#endif

