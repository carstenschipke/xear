/*
*service.cpp
 *
*@author carsten.schipke@gmail.com
 */
#include "service.h"
#include "calibration.h"

#include <chrono>
#include <vector>

#include <opencv2/opencv_modules.hpp>
#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/cudafeatures2d.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/cuda.hpp>

#include <x/concurrent/queue.h>


namespace xear
{
  namespace vision
  {
    namespace opencv
    {
      namespace impl
      {
        using namespace std;
        using namespace std::chrono;

        using namespace x::concurrent;


        struct Bgr
        {
          Byte b;
          Byte g;
          Byte r;
        };


        struct CvImage
        {
          cv::cuda::GpuMat data;

          cv::cuda::GpuMat descriptors;

          vector<cv::KeyPoint> keypoints;
          cv::cuda::GpuMat keypointsDevice;
        };


        struct CvMarker
        {
          public:
            String name;
            String path;
            MatchMethod method;

            Boolean initialized=false;
            CvImage image;


            CvMarker(String name, String path, MatchMethod method):
              name(name), path(path), method(method)
            {
              dbgi4("Create [" << name << "].");
            }

            ~CvMarker()
            {
              dbgi4("Destroy [" << name << "].");
            }
        };


        struct CvFrame
        {
          public:
            String queue;
            io::Image in;

            Boolean initialized=false;
            CvImage image;

            vec2 vanishingPoint;


            CvFrame(String queue, io::Image&& image):
              queue(queue), in(move(image))
            {
              dbgi4("Create [" << queue << "].");
            }

            CvFrame(String queue, io::Image const& image):
              queue(queue), in(image)
            {
              dbgi4("Create [" << queue << "].");
            }

            ~CvFrame()
            {
              dbgi4("Destroy [" << queue << "].");
            }
        };


        struct CvMatchCompare
        {
          Boolean operator()(cv::DMatch const& m0, cv::DMatch const& m1) const
          {
            return m0.distance<m1.distance;
          }
        };


        class CvMatcher
        {
          public:
            virtual ~CvMatcher()
            {

            }


            virtual void init()=0;
            virtual void match(CvMarker& marker, CvFrame& frame)=0;
        };


        class CvServiceBackend
        {
          public:
            unique_ptr<CvFrame> take(microseconds duration)
            {
              return m_queueFrame.take(duration);
            }

            void queue(String name, io::Image&& frame)
            {
              m_queueFrame.offer(make_unique<CvFrame>(name, move(frame)));
            }

            void queue(String name, io::Image const& frame)
            {
              m_queueFrame.offer(make_unique<CvFrame>(name, frame));
            }


            CvMarker& marker(String name)
            {
              return *m_marker[name];
            }

            vector<shared_ptr<CvMarker>>& queueMarker(String queue)
            {
              return m_queueMarker[queue];
            }

            void queueMarker(String queue, String name, String path, MatchMethod method)
            {
              m_marker[name]=make_shared<CvMarker>(name, path, method);
              m_queueMarker[queue].push_back(m_marker[name]);
            }


            Result& result(String queue, String marker)
            {
              lock_guard<decltype(m_monitorResult)> lock{m_monitorResult};

              return m_queueResult[queue][marker];
            }


            vec2& vanishingPoint(String queue)
            {
              lock_guard<decltype(m_monitorVanishingPoint)> lock{m_monitorVanishingPoint};

              return m_queueVanishingPoint[queue];
            }

            void vanishingPoint(String queue, vec2& vanishingPoint)
            {
              lock_guard<decltype(m_monitorVanishingPoint)> lock{m_monitorVanishingPoint};

              m_queueVanishingPoint[queue]=vanishingPoint;
            }


          private:
            map<String, shared_ptr<CvMarker>> m_marker;

            Queue<unique_ptr<CvFrame>> m_queueFrame;
            map<String, vector<shared_ptr<CvMarker>>> m_queueMarker;

            mutex m_monitorResult;
            map<String, map<String, Result>> m_queueResult;

            mutex m_monitorVanishingPoint;
            map<String, vec2> m_queueVanishingPoint;
        };


        /**
         * Ultra naive implementation - for demo purposes only.
         *
         * TODO Correct implementation with proper line detection,
         * importance sampling, vanishing point estimation & prediction/morphing.
         */
        class CvCudaVanishingPointEstimator
        {
          public:
            static Int constexpr CANNY_THRESHOLD_LOW=99;
            static Int constexpr CANNY_THRESHOLD_HIGH=297;
            static Int constexpr CANNY_APPERTURE_SIZE=3;

            static Int constexpr SEGMENT_SIZE=40;
            static Int constexpr SAMPLES_LIMIT=50;
            static Int constexpr SAMPLING_RATE=20;


            CvCudaVanishingPointEstimator():
              m_edgeDetector(cv::cuda::createCannyEdgeDetector(CANNY_THRESHOLD_LOW, CANNY_THRESHOLD_HIGH, CANNY_APPERTURE_SIZE, true))
            {
              dbgi4("Create.");
            }


            Boolean operator()(CvFrame& frame) const
            {
              cv::cuda::GpuMat out;
              m_edgeDetector->detect(frame.image.data, out);

              cv::Mat target;
              cv::Mat tmp;

              target.create(frame.image.data.size(), CV_8UC3);
              tmp.create(frame.image.data.size(), frame.image.data.type());

              out.download(tmp);

              vector<vec2> samples;
              vector<vec2> samplesImportant;

              for(auto y=0; y<tmp.rows; ++y)
              {
                for(auto x=0; x<tmp.cols; ++x)
                {
                  if(0<tmp.ptr<Byte>(y)[x])
                  {
                    if(0==y%SAMPLING_RATE)
                      samples.emplace_back(x, y);
                  }
                }
              }

              auto count=samples.size();

              if(1>count)
                return false;

              auto limit=SAMPLES_LIMIT;

              IntU skip=count/limit;

              for(auto i=0; i<count; i+=skip)
                samplesImportant.emplace_back(samples[i].x, samples[i].y);

              limit=samplesImportant.size();

              for(auto i=0; i<limit; ++i)
              {
                for(auto j=0; j<limit; ++j)
                {
                  cv::line(target,
                    cv::Point(samplesImportant[i].x, samplesImportant[i].y),
                    cv::Point(samplesImportant[j].x, samplesImportant[j].y),
                    cv::Scalar(255, 0, 0),
                    1,
                    8);
                }
              }

              IntU highscore=0;

              for(auto y=0; y<target.rows; y+=SEGMENT_SIZE)
              {
                for(auto x=0; x<target.cols; x+=SEGMENT_SIZE)
                {
                  IntU score=0;

                  for(auto i=0; i<SEGMENT_SIZE; i++)
                  {
                    for(auto j=0; j<SEGMENT_SIZE; j++)
                      score+=(IntU)target.ptr<Bgr>(y+i)[x+j].b;
                  }

                  if(highscore<score)
                  {
                    frame.vanishingPoint.x=x;
                    frame.vanishingPoint.y=y;

                    highscore=score;
                  }
                }
              }

              return true;
            }


          private:
            cv::Ptr<cv::cuda::CannyEdgeDetector> m_edgeDetector;
        };


        class CvMatcherCudaSurf: public CvMatcher
        {
          public:
            static Float constexpr INLIER_THRESHOLD=2.5f;
            static Float constexpr MATCH_RATIO=0.8f;


            CvMatcherCudaSurf(CvServiceBackend* backend, Int deviceId):
              m_deviceId(deviceId),
              m_backend(backend),
              /**
               * Float _hessianThreshold
               * Int _nOctaves=4
               * Int _nOctaveLayers=2,
               * Boolean _extended=false
               * Float _keypointsRatio=0.01f
               * Boolean _upright=false
               */
              m_featureDetector(800),
              // NORM_L1, NORM_L2
              m_descriptorMatcher(cv::cuda::DescriptorMatcher::createBFMatcher(m_featureDetector.defaultNorm()))
            {
              dbgi4("Create.");
            }

            virtual ~CvMatcherCudaSurf()
            {
              dbgi4("Destroy.");
            }


            virtual void init()
            {
              dbgi2("Initialize.");
            }


            virtual void match(CvMarker& marker, CvFrame& frame)
            {
              detect(marker);
              detect(frame);


              auto& imageMarker=marker.image;
              auto& imageFrame=frame.image;

              return;

              vector<vector<cv::DMatch>> matchesKnn;
              m_descriptorMatcher->knnMatch(imageMarker.descriptors, imageFrame.descriptors, matchesKnn, 2, cv::noArray(), true);

              // Download
              m_featureDetector.downloadKeypoints(imageMarker.keypointsDevice, imageMarker.keypoints);
              m_featureDetector.downloadKeypoints(imageFrame.keypointsDevice, imageFrame.keypoints);

              dbgi3("[" << marker.name << "] Marker keypoints [" << imageMarker.keypoints.size() << "].");
              dbgi3("[" << marker.name << "] Marker descriptors [" << imageMarker.descriptors.size() << "].");

              dbgi3("[" << marker.name << "] Frame keypoints [" << imageFrame.keypoints.size() << "].");
              dbgi3("[" << marker.name << "] Frame descriptors [" << imageFrame.descriptors.size() << "].");


              vector<cv::KeyPoint> matchesMarker;
              vector<cv::KeyPoint> matchesFrame;

              for(auto i=0u; i<matchesKnn.size(); i++)
              {
                if(matchesKnn[i][0].distance<MATCH_RATIO*matchesKnn[i][1].distance)
                {
                  matchesMarker.push_back(imageMarker.keypoints[matchesKnn[i][0].queryIdx]);
                  matchesFrame.push_back(imageFrame.keypoints[matchesKnn[i][0].trainIdx]);
                }
              }

              dbgi("MATCHES " << matchesMarker.size());

              cv::Mat inlierMask;
              cv::Mat homography;

              vector<cv::KeyPoint> inliersMarker;
              vector<cv::KeyPoint> inliersFrame;

              vector<cv::DMatch> matches;

              if(4<=matchesMarker.size())
                homography=cv::findHomography(points(matchesMarker), points(matchesFrame), cv::RANSAC, INLIER_THRESHOLD, inlierMask);

              if(4>matchesMarker.size() || homography.empty())
              {
                m_backend->result(frame.queue, marker.name).match(false);

                return;
              }

              for(auto i=0u; i<matchesMarker.size(); i++)
              {
                if(inlierMask.at<Byte>(i))
                {
                  auto inlier=static_cast<int>(inliersMarker.size());

                  inliersMarker.push_back(matchesMarker[i]);
                  inliersFrame.push_back(matchesFrame[i]);

                  matches.push_back(cv::DMatch(inlier, inlier, 0));
                }
              }

              if(20>matches.size())
              {
                m_backend->result(frame.queue, marker.name).match(false);

                return;
              }

              dbgi("INLIERS " << matches.size());

              vector<cv::Point2f> cornersMarker{4};
              vector<cv::Point2f> cornersFrame{4};

              cornersMarker[0]=cv::Point2f(0,                     0);
              cornersMarker[1]=cv::Point2f(imageMarker.data.cols, 0);
              cornersMarker[2]=cv::Point2f(imageMarker.data.cols, imageMarker.data.rows);
              cornersMarker[3]=cv::Point2f(0,                     imageMarker.data.rows);

              cv::perspectiveTransform(cornersMarker, cornersFrame, homography);

              auto mat=cv::getPerspectiveTransform(cornersMarker, cornersFrame);

              m_backend->result(frame.queue, marker.name)
                .match({
                  mat.at<Double>(0, 0), mat.at<Double>(1, 0), mat.at<Double>(2, 0),
                  mat.at<Double>(0, 1), mat.at<Double>(1, 1), mat.at<Double>(2, 1),
                  mat.at<Double>(0, 2), mat.at<Double>(1, 2), mat.at<Double>(2, 2)
                })
                .box({
                  cornersFrame[0].x, cornersFrame[0].y, 0.0f,  1.0f, 1.0f,  0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
                  cornersFrame[1].x, cornersFrame[1].y, 0.0f,  0.0f, 1.0f,  0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
                  cornersFrame[2].x, cornersFrame[2].y, 0.0f,  0.0f, 0.0f,  1.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
                  cornersFrame[3].x, cornersFrame[3].y, 0.0f,  1.0f, 0.0f,  0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f
                });

//
//              // Filter
//              dbgi3("Filter matches [" << matchesKnn.size() << "].");
//
//              vector<cv::DMatch> matches;
//
//              for(auto i=0; i<matchesKnn.size(); ++i)
//              {
//                if(2!=matchesKnn[i].size())
//                  continue;
//
//                auto m0=matchesKnn[i][0];
//                auto m1=matchesKnn[i][1];
//
//                if(m0.distance<(MATCH_RATIO*m1.distance))
//                  matches.push_back(m0);
//              }
//
//
//              dbgi3("Sort matches [" << matches.size() << "].");
//
//              std::sort(matches.begin(), matches.end(), CvMatchCompare{});
//
//              if(10<=matches.size())
//              {
//                dbgi3("Translate matches [" << matches.size() << "].");
//
//                vector<cv::Point2f> keypointsMarker;
//                vector<cv::Point2f> keypointsFrame;
//
//                keypointsMarker.resize(matches.size());
//                keypointsFrame.resize(matches.size());
//
//                for(auto i=0; i<matches.size(); ++i)
//                {
//                  auto match=matches[i];
//
//                  auto keypointMarker=imageMarker.keypoints[match.queryIdx];
//                  auto keypointFrame=imageFrame.keypoints[match.trainIdx];
//
//                  keypointsMarker[i]=keypointMarker.pt;
//                  keypointsFrame[i]=keypointFrame.pt;
//                }
//
//                auto H=cv::findHomography(keypointsMarker, keypointsFrame, cv::RANSAC);
//
//                vector<cv::Point2f> cornersMarker{4};
//                vector<cv::Point2f> cornersFrame{4};
//
//                cornersMarker[0]=cv::Point2f(0,                     0);
//                cornersMarker[1]=cv::Point2f(imageMarker.data.cols, 0);
//                cornersMarker[2]=cv::Point2f(imageMarker.data.cols, imageMarker.data.rows);
//                cornersMarker[3]=cv::Point2f(0,                     imageMarker.data.rows);
//
//                cv::perspectiveTransform(cornersMarker, cornersFrame, H);
//
//                auto mat=cv::getPerspectiveTransform(cornersMarker, cornersFrame);
//
//                m_backend->result(frame.queue, marker.name)
//                  .match({
//                    mat.at<Double>(0, 0), mat.at<Double>(1, 0), mat.at<Double>(2, 0),
//                    mat.at<Double>(0, 1), mat.at<Double>(1, 1), mat.at<Double>(2, 1),
//                    mat.at<Double>(0, 2), mat.at<Double>(1, 2), mat.at<Double>(2, 2)
//                  })
//                  .modelView(modelView(H));
//              }
//              else
//              {
//                m_backend->result(frame.queue, marker.name).match(false);
//              }
            }


          private:
            Int const m_deviceId;

            CvServiceBackend* m_backend;
            cv::cuda::SURF_CUDA m_featureDetector;
            cv::Ptr<cv::cuda::DescriptorMatcher> m_descriptorMatcher;

            // TODO Implement a composable chain of modular filters, matchers, pre- & post-processors etc.
            CvCudaVanishingPointEstimator m_vanishingPointEstimator;


            vector<cv::Point2f> points(vector<cv::KeyPoint> const& keypoints)
            {
              vector<cv::Point2f> points;

              for(auto i=0u; i<keypoints.size(); i++)
                points.push_back(keypoints[i].pt);

              return points;
            }

            mat4 modelView(cv::Mat homography)
            {
              Double focalLengthX=786.42938232;
              Double focalLengthY=786.42938232;
              Double cameraPrimaryX=217.01358032;
              Double cameraPrimaryY=311.25384521;

//              Float viewportWidth=480;
//              Float viewportHeight=640;
//
//              Float fieldOfView=1/(focalLengthX/viewportHeight*2);
//              Float aspectRatio=viewportWidth/viewportHeight*focalLengthY/focalLengthX;
//              Float nearPlane=0.1;
//              Float farPlane=1000;
//              Float frustumHeight=nearPlane*fieldOfView;
//              Float frustumWidth=frustumHeight*aspectRatio;
//
//              Float offsetX=(viewportWidth /2-cameraPrimaryX)/viewportWidth *frustumWidth *2;
//              Float offsetY=(viewportHeight/2-cameraPrimaryY)/viewportHeight*frustumHeight*2;

//              // Build and apply the projection matrix
//              glFrustumf(-frustumWidth-offsetX, frustumWidth-offsetX, -frustumHeight-offsetY, frustumHeight-offsetY, near, far);

              // Decompose the Homography into translation and rotation vectors
              // Based on: https://gist.github.com/740979/97f54a63eb5f61f8f2eb578d60eb44839556ff3f
              Calibration camera{focalLengthX, focalLengthY, cameraPrimaryX, cameraPrimaryY};

              mat3 cameraIntrinsicInverse{
                1/camera.intrinsic[0][0], 0,                        -camera.intrinsic[0][2]/camera.intrinsic[0][0],
                0,                        1/camera.intrinsic[1][1], -camera.intrinsic[1][2]/camera.intrinsic[1][1],
                0,                        0,                        1
              };

              vec3 h1{homography.at<Double>(0, 0), homography.at<Double>(1, 0), homography.at<Double>(2, 0)};
              vec3 h2{homography.at<Double>(0, 1), homography.at<Double>(1, 1), homography.at<Double>(2, 1)};
              vec3 h3{homography.at<Double>(0, 2), homography.at<Double>(1, 2), homography.at<Double>(2, 2)};

              vec3 h1Inverse=cameraIntrinsicInverse*h1;

              Double lambda=sqrt(h1Inverse[0]*h1Inverse[0]
               +h1Inverse[1]*h1Inverse[1]
               +h1Inverse[2]*h1Inverse[2]);

              cv::Mat rotation;
              vec3 translation;

              if(0!=lambda)
              {
                lambda=1/lambda;

                // Normalize inverseCameraMatrix
                cameraIntrinsicInverse[0][0]*=lambda;
                cameraIntrinsicInverse[1][0]*=lambda;
                cameraIntrinsicInverse[2][0]*=lambda;
                cameraIntrinsicInverse[0][1]*=lambda;
                cameraIntrinsicInverse[1][1]*=lambda;
                cameraIntrinsicInverse[2][1]*=lambda;
                cameraIntrinsicInverse[0][2]*=lambda;
                cameraIntrinsicInverse[1][2]*=lambda;
                cameraIntrinsicInverse[2][2]*=lambda;

                // Column vectors of rotation matrix
                vec3 r1=cameraIntrinsicInverse*h1;
                vec3 r2=cameraIntrinsicInverse*h2;
                // Orthogonal to r1 and r2
                vec3 r3=glm::cross(r1, r2);

                // Put rotation columns into rotation matrix... with some unexplained sign changes
                rotation=(cv::Mat_<Double>(3,3) <<
                   r1[0], -r2[0], -r3[0],
                  -r1[1],  r2[1],  r3[1],
                  -r1[2],  r2[2],  r3[2]);

                // Translation vector T
                translation=cameraIntrinsicInverse*h3;
                translation[0]*= 1;
                translation[1]*=-1;
                translation[2]*=-1;

                cv::SVD decomposed(rotation);
                rotation=decomposed.u*decomposed.vt;
              }

              return mat4{
                rotation.at<Double>(0, 0), rotation.at<Double>(0, 1), rotation.at<Double>(0, 2), translation[0],
                rotation.at<Double>(1, 0), rotation.at<Double>(1, 1), rotation.at<Double>(1, 2), translation[1],
                rotation.at<Double>(2, 0), rotation.at<Double>(2, 1), rotation.at<Double>(2, 2), translation[2],
                0,                         0,                         0,                         1
              };
            }


            void detect(CvImage& image)
            {
              dbgi3("Detect features.");

              m_featureDetector(image.data, cv::cuda::GpuMat{}, image.keypointsDevice, image.descriptors);
            }

            void detect(CvFrame& frame)
            {
              if(!frame.initialized)
              {
                dbgi3("Initialize frame [" << frame.queue << "].");

                cv::Mat frameGry;
//                cv::Mat frameBlr;
                cv::Mat frameRgb{(Int)frame.in.height(), (Int)frame.in.width(), CV_8UC3, const_cast<Byte*>(frame.in.data())};

//                cv::blur(frameRgb, frameBlr, cv::Size{4, 4});
                cv::cvtColor(frameRgb, frameGry, CV_RGB2GRAY);

                frame.image.data.create(frameGry.size(), frameGry.type());
                frame.image.data.upload(frameGry);

//                if(m_vanishingPointEstimator(frame))
//                  m_backend->vanishingPoint(frame.queue, frame.vanishingPoint);

                detect(frame.image);

                frame.initialized=true;
              }
            }

            void detect(CvMarker& marker)
            {
              if(!marker.initialized)
              {
                dbgi3("Initialize marker [" << marker.name << "].");

//                cv::Mat rgb;
                auto raw=cv::imread(marker.path, cv::IMREAD_GRAYSCALE);
//                cv::blur(rgb, raw, cv::Size{4, 4});

                marker.image.data.create(raw.size(), raw.type());
                marker.image.data.upload(raw);

                detect(marker.image);

                marker.initialized=true;
              }
            }
        };


        class CvServiceWorker: public Task
        {
          public:
            Int const id;


            CvServiceWorker(Int id, CvServiceBackend* backend, CvMatcher* matcher):
              Task("vision::opencv::CvServiceWorker #"+to_string(id)),
              id(id),
              m_backend(backend),
              m_matcher(matcher)
            {
              dbgi4("Create.");
            }


            virtual ~CvServiceWorker()
            {
              dbgi4("Destroy.");
            }


          protected:
            virtual void run() override
            {
              dbgi2("Enter OpenCV Service Worker.");

              while(!interrupted())
              {
                auto frame=m_backend->take(milliseconds{100});

                if(!frame)
                  continue;

                dbgi3("OpenCV Service Worker: Took Frame [" << frame->queue << "].");

                if(200>frame->in.width() || 200>frame->in.height())
                {
                  dbgi3("Skipping frame [" << frame->queue << ", TOO SMALL].");

                  continue;
                }

                for(auto& marker : m_backend->queueMarker(frame->queue))
                  m_matcher->match(*marker, *frame);

                frame.reset();
              }

              dbgi2("Leave OpenCV Service Worker.");
            }


          private:
            CvServiceBackend* m_backend;
            CvMatcher* m_matcher;
        };


        class CvService
        {
          public:
            CvService()
            {
              dbgi4("Create.");

              m_backend=new CvServiceBackend();

              // TODO Utilize multiple devices if available.
              if(0<cv::cuda::getCudaEnabledDeviceCount())
              {
                cv::cuda::printCudaDeviceInfo(cv::cuda::getDevice());

                m_matcher=new CvMatcherCudaSurf(m_backend, cv::cuda::getDevice());
              }
              // else
              // TODO Implement fallback.

              // TODO Create/destroy according to demand...
              m_worker.push_back(new CvServiceWorker((Int)m_worker.size(), m_backend, m_matcher));
              m_worker.push_back(new CvServiceWorker((Int)m_worker.size(), m_backend, m_matcher));
            }


            ~CvService()
            {
              dbgi4("Destroy.");

              for(auto& worker : m_worker)
              {
                dbgi3("Interrupt OpenCV Service Worker.");

                worker->interrupt();

                dbgi3("Await OpenCV Service Worker Termination.");

                if(!worker->wait(seconds{1}))
                  dbgi3("OpenCV Service Worker Termination Timed-out.");
              }

              m_worker.clear();

              delete m_matcher;
              delete m_backend;
            }


            void init()
            {
              m_matcher->init();
            }


            vec2& vanishingPoint(String queue)
            {
              return m_backend->vanishingPoint(queue);
            }

            Result& markerMatch(String queue, String marker)
            {
              return m_backend->result(queue, marker);
            }


            void markerAdd(String queue, String name, String path, MatchMethod method)
            {
              m_backend->queueMarker(queue, name, path, method);
            }

            void queue(String queue, io::Image&& frame)
            {
              m_backend->queue(queue, move(frame));
            }

            void queue(String queue, io::Image const& frame)
            {
              m_backend->queue(queue, frame);
            }

            void queueAdd(String name)
            {
              // Do nothing ...
            }

            void queueRemove(String name)
            {
              // Do nothing ...
            }

            vector<CvServiceWorker*>& worker()
            {
              return m_worker;
            }


          private:
            CvServiceBackend* m_backend;
            CvMatcher* m_matcher;

            vector<CvServiceWorker*> m_worker;
        };


        static CvService* service;
      }


      void Service::setup()
      {
        dbgi("Setup OpenCV Service.");

        name("vision::opencv::Service");

        impl::service=new impl::CvService();
        impl::service->init();
      }

      void Service::teardown()
      {
        dbgi("Teardown OpenCV Service.");

        // FIXME Need to wait for CUDA shutdown.
        delete impl::service;
      }


      Result& Service::markerMatch(String queue, String marker)
      {
        return impl::service->markerMatch(queue, marker);
      }

      vec2& Service::vanishingPoint(String queue)
      {
        return impl::service->vanishingPoint(queue);
      }


      void Service::markerAdd(String queue, MatchMethod method, String name, String path)
      {
        impl::service->markerAdd(queue, name, path, method);
      }

      void Service::queue(String name, io::Image&& frame)
      {
        impl::service->queue(name, move(frame));
      }

      void Service::queue(String name, io::Image const& frame)
      {
        impl::service->queue(name, frame);
      }

      void Service::queueAdd(String name)
      {
        impl::service->queueAdd(name);
      }

      void Service::queueRemove(String name)
      {
        impl::service->queueRemove(name);
      }


      void Service::run()
      {
        dbgi2("Enter OpenCV Service.");

        while(!interrupted())
        {
          for(auto worker : impl::service->worker())
          {
            if(!worker->running())
            {
              if(worker->failed())
                dbgi("OpenCV Service Worker Exception [" << worker->exception() << "].");

              if(worker->started())
                worker->reset();

              worker->start();
            }
          }

          sleep(chrono::milliseconds{100});
        }

        dbgi2("Leave OpenCV Service.");
      }
    }
  }
}
