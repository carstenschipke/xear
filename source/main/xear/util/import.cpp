/*
 * util/import.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include "import.h"


using namespace xear;


Int main(Int const argc, Char const* const argv[])
{
  dbgsi("Enter");

  Import importer;
  importer.import(argv[1], argv[2]);

  dbgsi("Leave");

  return 0;
}
