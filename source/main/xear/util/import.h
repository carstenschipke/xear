/*
 * import.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_IMPORT_H_
#define XEAR_IMPORT_H_


#include <algorithm>
#include <fstream>
#include <map>
#include <vector>

#include <xear.h>

#include <xear/scene/data/geometry.h>
#include <xear/scene/data/mesh.h>

#include <x/misc/math.h>


namespace xear
{
    using namespace std;

    using namespace x;


    struct ImportFace
    {
      Int o;
      Int m;

      Int v;
      Int t;
      Int n;
    };

    struct ImportMesh
    {
      Int object;
      Int material;

      Int offset;
      Int count;
    };


    class Import
    {
      public:
        void import(String name, String filename)
        {
          dbgsi("Enter");

          String filenameMaterial;

          ifstream file;
          file.open(filename);

          if(!file.good())
          {
            dbgse("Unable to locate asset {file: " << filename << "}.");

            return;
          }

          file.clear();
          file.seekg(0, ios::beg);

          while(!file.eof())
          {
            String line;
            getline(file, line);

            String type=line.substr(0, 2);

            if(0==type.compare("o "))
            {
              String object=line.substr(2);
              m_object.push_back(object);

              dbgsi("Enter object {name: " << object << "}.");
            }
            else if(0==type.compare("v "))
            {
              parseVertex(line);
            }
            else if(0==type.compare("vt"))
            {
              parseUv(line);
            }
            else if(0==type.compare("vn"))
            {
              parseNormal(line);
            }
            else if(0==type.compare("f "))
            {
              parseFace(line.substr(2));
            }
            else
            {
              if(0==line.substr(0, 6).compare("usemtl"))
              {
                String material=line.substr(7);
                m_material.push_back(material);

                dbgsi("Enter material {name: " << material << "}.");
              }
              else if(0==line.substr(0, 6).compare("mtllib"))
              {
                filenameMaterial=line.substr(7);

                dbgsi2("Remember material file {file: " << filenameMaterial << "}.");
              }
            }
          }

          file.close();


          map<String, String> textures;

          if(0<filenameMaterial.size())
          {
            dbgsi("Enter material file {file: " << filename.substr(0, filename.rfind("/")+1)+filenameMaterial << "}.");

            file.open(filename.substr(0, filename.rfind("/")+1)+filenameMaterial);

            file.clear();
            file.seekg(0, ios::beg);

            String currentMaterial;

            while(!file.eof())
            {
              String line;
              getline(file, line);

              String type=line.substr(0, 6);

              // TODO Parse material properties...

              if(0==type.compare("newmtl"))
              {
                currentMaterial=line.substr(7);
              }
              else if(0==type.compare("map_Kd"))
              {
                String texture=line.substr(7);

                textures.insert({currentMaterial, texture.substr(0, texture.rfind("."))});
              }
            }

            file.close();
          }

          vector<ImportFace> faces;
          vector<ImportMesh> meshes;

          ImportMesh mesh{0, 0, 0, 0};

          // TODO Convert to strips? (e.g. see NvTriStrip)
          // TODO Define layouts
          xear::scene::data::Geometry<> geometry{"plain", xear::common::Primitive::TRIANGLE, 0, 0};

          typedef decltype(geometry)::typeIndex Index;

          geometry << (Index)0xffffffff;

          Long idxFace=0;

          vec3 tangent;
          vec3 biTangent;

          for(auto& face : m_face)
          {
            if(face.o!=mesh.object || face.m!=mesh.material)
            {
              mesh.count=(Int)geometry.indices.size()-mesh.offset;
              meshes.push_back(mesh);

              mesh.material=face.m;
              mesh.object=face.o;
              mesh.offset=(Int)geometry.indices.size();

              geometry << (Index)0xffffffff;
            }

            if(1>idxFace || 0==idxFace%3)
            {
              vec3 v0=m_vertex[m_face[idxFace+0].v];
              vec3 v1=m_vertex[m_face[idxFace+1].v];
              vec3 v2=m_vertex[m_face[idxFace+2].v];

              vec2 u0=m_uv[m_face[idxFace+0].t];
              vec2 u1=m_uv[m_face[idxFace+1].t];
              vec2 u2=m_uv[m_face[idxFace+2].t];

              vec3 n0=m_normal[m_face[idxFace+0].n];

              x::misc::math::tangentBiTangent(v0, v1, v2, u0, u1, u2, n0, tangent, biTangent);
            }

            auto exists=find_if(faces.begin(), faces.end(), [&face](ImportFace const& current) {
              return face.v==current.v && face.t==current.t && face.n==current.n;
            });

            if(exists==faces.end())
            {
              faces.push_back(face);

              geometry << m_vertex[face.v].x;
              geometry << m_vertex[face.v].y;
              geometry << m_vertex[face.v].z;

              geometry << m_uv[face.t].s;
              geometry << m_uv[face.t].t;

              // Reserved for multi texturing / alternatively 4 multi-purpose components.
              geometry << 0.0f;
              geometry << 0.0f;

              geometry << m_normal[face.n].x;
              geometry << m_normal[face.n].y;
              geometry << m_normal[face.n].z;

              geometry << tangent.x;
              geometry << tangent.y;
              geometry << tangent.z;

              geometry << biTangent.x;
              geometry << biTangent.y;
              geometry << biTangent.z;

              geometry << (Index)(faces.size()-1);

#             if defined(DEBUG) && 2<DEBUG
                dbgsi("Append vertex {i: " << (Int)(faces.size()-1)
                  << ", v: " << glm::to_string(m_vertex[face.v])
                  << ", u: " << glm::to_string(m_uv[face.t])
                  << ", n: " << glm::to_string(m_normal[face.n])
                  << ", t: " << glm::to_string(tangent)
                  << ", b: " << glm::to_string(biTangent) << "}");
#             endif
            }
            else
            {
              geometry << (Index)(exists-faces.begin());

              dbgsi2("Reference vertex {i: " << (Int)(exists-faces.begin()) << "}");
            }

            ++idxFace;
          }

          mesh.count=(Int)geometry.indices.size()-mesh.offset;
          meshes.push_back(mesh);

          geometry.countIndices=(Int)geometry.indices.size();
          geometry.countVertices=(Int)geometry.vertices.size();

          Int i=0;

          for(auto& mesh : meshes)
          {
            xear::scene::data::Mesh m;
            m.material=m_material[mesh.material];
            m.texture=textures[m.material];
            m.offset=mesh.offset;
            m.count=mesh.count;

            m.serialize(name+"-"+to_string(i)+"-"+m_material[mesh.material]+".am");

            i++;
          }


          geometry.serialize(name+".av");


          dbgsi("Leave");
        }


      private:
        Int m_idxVertex=0;
        map<Int, Int> m_mapVertex;
        vector<vec3> m_vertex;

        Int m_idxUv=0;
        map<Int, Int> m_mapUv;
        vector<vec2> m_uv;

        Int m_idxNormal=0;
        map<Int, Int> m_mapNormal;
        vector<vec3> m_normal;

        Int m_idxFace=0;
        map<Int, Int> m_mapFace;
        vector<ImportFace> m_face;

        vector<Int> m_index;

        vector<String> m_material;
        vector<String> m_object;


        inline void parseVertex(String line)
        {
          vec3 vertex=parseVec3(line.substr(2), ' ');

          auto exists=find_if(m_vertex.begin(), m_vertex.end(), [&vertex](vec3 const& current) {
            return vertex.x==current.x && vertex.y==current.y && vertex.z==current.z;
          });

          if(exists==m_vertex.end())
          {
            m_vertex.push_back(vertex);

            m_mapVertex[m_idxVertex]=(Int)m_vertex.size()-1;
          }
          else
          {
            m_mapVertex[m_idxVertex]=(Int)(exists-m_vertex.begin());
          }

          m_idxVertex++;
        }

        inline void parseNormal(String line)
        {
          vec3 normal=parseVec3(line.substr(3), ' ');

          auto exists=find_if(m_normal.begin(), m_normal.end(), [&normal](vec3 const& current) {
            return normal.x==current.x && normal.y==current.y && normal.z==current.z;
          });

          if(exists==m_normal.end())
          {
            m_normal.push_back(normal);

            m_mapNormal[m_idxNormal]=(Int)m_normal.size()-1;
          }
          else
          {
            m_mapNormal[m_idxNormal]=(Int)(exists-m_normal.begin());
          }

          m_idxNormal++;
        }

        inline void parseUv(String line)
        {
          vec2 uv=parseVec2(line.substr(3), ' ');

          auto exists=find_if(m_uv.begin(), m_uv.end(), [&uv](vec2 const& current) {
            return uv.x==current.x && uv.y==current.y;
          });

          if(exists==m_uv.end())
          {
            m_uv.push_back(uv);

            m_mapUv[m_idxUv]=(Int)m_uv.size()-1;
          }
          else
          {
            m_mapUv[m_idxUv]=(Int)(exists-m_uv.begin());
          }

          m_idxUv++;
        }

        inline void parseFace(String line)
        {
          stringstream s{line};
          string chunkFace;

          while(std::getline(s, chunkFace, ' '))
          {
            stringstream schunk{chunkFace};
            string chunk;

            ImportFace face{0, 0, 0, 0, 0};
            Int idx=0;

            face.o=(Int)m_object.size()-1;
            face.m=(Int)m_material.size()-1;

            while(std::getline(schunk, chunk, '/'))
            {
              if(0<chunk.size())
              {
                if(0==idx)
                  face.v=m_mapVertex[stoi(chunk)-1];
                else if(1==idx)
                  face.t=m_mapUv[stoi(chunk)-1];
                else
                  face.n=m_mapNormal[stoi(chunk)-1];
              }

              idx++;
            }

            m_face.push_back(face);
          }
        }

        inline vec3 parseVec3(String line, Char delimiter)
        {
          vec3 vector;

          stringstream s{line};
          string chunk;

          Int idx=0;

          while(std::getline(s, chunk, ' '))
          {
            if(0<chunk.size())
            {
              if(0==idx)
                vector.x=stof(chunk);
              else if(1==idx)
                vector.y=stof(chunk);
              else
                vector.z=stof(chunk);
            }

            idx++;
          }

          return vector;
        }

        inline vec2 parseVec2(String line, Char delimiter)
        {
          vec2 vector;

          stringstream s{line};
          string chunk;

          Int idx=0;

          while(std::getline(s, chunk, ' '))
          {
            if(0<chunk.size())
            {
              if(0==idx)
                vector.x=stof(chunk);
              else
                vector.y=stof(chunk);
            }

            idx++;
          }

          return vector;
        }
    };
}


#endif
