/*
 * backend.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_BACKEND_H_
#define XEAR_BACKEND_H_


#include <atomic>
#include <chrono>
#include <mutex>
#include <thread>

#include <xear.h>
#include <xear/rtc/peer.h>
#include <xear/rtc/supervisor.h>

#include <x/concurrent/task.h>
#include <x/web/http/common.h>

#include "instance.h"


namespace xear
{
    using namespace std;
    using namespace std::chrono;

    using namespace x;
    using namespace x::common;
    using namespace x::concurrent;

    using namespace rtc;


    struct Peer
    {
      public:
        Peer(String peerId, String app, RtcSupervisor* supervisor):
          m_peer(new RtcPeer(peerId, supervisor)), m_appName(app)
        {
          dbgi4("Create [" << *this << "].");
        }


        ~Peer()
        {
          dbgi4("Destroy [" << *this << "].");
        }


        RtcPeer* peer()
        {
          return m_peer;
        }

        // TODO Verify validity.
        Instance* app(device::Config config)
        {
          Boolean expected=false;

          if(m_appInitialized.compare_exchange_strong(expected, true))
          {
            m_app=new Instance(m_peer->id, m_appName);
            m_app->init(config);
          }

          return m_app;
        }

        time_point<system_clock> heartbeat()
        {
          lock_guard<mutex> lock{m_monitorHeartbeat};

          return m_heartbeat;
        }

        void keepAlive()
        {
          lock_guard<mutex> lock{m_monitorHeartbeat};

          m_heartbeat=system_clock::now();
        }

        Boolean expired() const
        {
          return m_expired;
        }

        void expire()
        {
          m_expired=true;
        }

        void destroy()
        {
          // FIXME Segfault in webrtc...
          delete m_peer;

          Boolean expected=true;

          if(m_appInitialized.compare_exchange_strong(expected, false))
            delete m_app;
        }


        friend std::ostream& operator<<(std::ostream& stream, Peer const& peer)
        {
          stream << "Peer{id: " << peer.m_peer->id
            << ", app: " << peer.m_appName
            << ", expired: " << peer.m_expired
            << "}";

          return stream;
        }


      private:
        atomic<Boolean> m_expired{false};
        time_point<system_clock> m_heartbeat=system_clock::now();

        Instance* m_app=nullptr;
        atomic<Boolean> m_appInitialized{false};

        RtcPeer* m_peer;
        String m_appName;

        mutex m_monitorHeartbeat;
    };


    class Backend: public Task, public RtcSupervisor
    {
      public:
        Backend(Config const& config):
          Task("xear::Backend"), m_config(config)
        {
          dbgi4("Create.");
        }


        Backend(Backend const&) = delete;
        Backend& operator=(Backend const&) = delete;


        virtual ~Backend()
        {
          dbgi4("Destroy.");
        }


        inline RtcPeer* peer(String peerId)
        {
          if(!peerExists(peerId))
            return nullptr;

          m_peers[peerId]->keepAlive();

          return m_peers[peerId]->peer();
        }

        /**
         * TODO Probably more logical to have a backend instance per app ..
         *
         * ... instead of this strange indirection.
         *
         * We also want to share as many resources as possible between
         * clients of the same app, i.e. down to gl context,
         * uploaded textures & geometry etc..
         *
         * Accordingly there needs to be a clear definition and authority,
         * for example this 'Backend'.
         *
         * But then again, it should be possible to load new scripts/apps
         * at runtime - so such an authority/backend can not really be
         * instantiated up-front.
         *
         * Anyway its still a prototype :)
         */
        inline void peerQueue(String peerId, web::http::Request const& request, web::http::Response& response)
        {
          if(!peerExists(peerId))
          {
            dbgi("CREATE PEER " << peerId);

            // TODO Support parameterized configuration variables.
            auto appName=m_config.get<String>("script.name", XEAR_SCRIPT_NAME_DEFAULT);

            if(0==strcasecmp("{request_uri.host}", appName.c_str()))
              appName=request.host;

            if(appName.empty())
              appName=XEAR_SCRIPT_NAME_DEFAULT;

            m_peers[peerId]=make_unique<Peer>(peerId, appName, this);
            m_peers[peerId]->peer()->start();
          }

          m_peers[peerId]->peer()->queue(request.content);
        }

        inline Boolean peerExists(String peerId)
        {
          auto iterator=m_peers.find(peerId);

          return iterator!=m_peers.end() && !iterator->second->expired();
        }


        virtual Boolean peerKeepAlive(String peerId)
        {
          auto iterator=m_peers.find(peerId);

          if(iterator==m_peers.end())
            return false;

          iterator->second->keepAlive();

          return true;
        }

        virtual Boolean peerDisconnect(String peerId)
        {
          auto iterator=m_peers.find(peerId);

          if(iterator==m_peers.end())
            return false;

          dbgi("Expire peer [" << peerId << "].");

          iterator->second->expire();

          return true;
        }


        virtual RtcEventDelegate* peerEventDelegate(String peerId, device::Config config)
        {
          auto iterator=m_peers.find(peerId);

          if(iterator==m_peers.end())
            throw "Unable to resolve instance for peer ["+peerId+"].";

          return iterator->second->app(config);
        }

        virtual RtcVideoSink* peerVideoSink(String peerId, device::Config config)
        {
          auto iterator=m_peers.find(peerId);

          if(iterator==m_peers.end())
            throw "Unable to resolve instance for peer ["+peerId+"].";

          return iterator->second->app(config);
        }

        virtual RtcVideoSource* peerVideoSource(String peerId, device::Config config)
        {
          auto iterator=m_peers.find(peerId);

          if(iterator==m_peers.end())
            throw "Unable to resolve instance for peer ["+peerId+"].";

          return iterator->second->app(config);
        }


        virtual void run()
        {
          auto keepAliveMs=duration_cast<milliseconds>(seconds{XEAR_BACKEND_KEEP_ALIVE_DEFAULT});

          while(!interrupted())
          {
            for(auto& entry : m_peers)
            {
              // FIXME RTC data channels / heartbeat not working since update...
//              if(keepAliveMs<duration_cast<milliseconds>(system_clock::now()-entry.second->heartbeat()) && !entry.second->expired())
//              {
//                dbgi("Expire peer [" << entry.first << "].");
//
//                entry.second->expire();
//              }

              if(entry.second->expired() && !entry.second->peer()->interrupted())
              {
                entry.second->peer()->interrupt();
                entry.second->peer()->wait();

                dbgi("Cancelled peer [" << entry.first << "].");

                entry.second->destroy();
              }
            }

            this_thread::sleep_for(milliseconds{100});
          }
        }


      private:
        Config const& m_config;
        map<String, unique_ptr<Peer>> m_peers;
    };
}


#endif
