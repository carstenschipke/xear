/*
 * daemon/main.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <signal.h>

#include <xear.h>

#include <x/common/config.h>

#include "backend.h"
#include "http/daemon.h"

#include <driver/glx/surface.h>
#include <renderer/gl/service.h>
#include <rtc/service.h>
#include <script/v8/engine.h>
#include <vision/opencv/service.h>


using namespace std;

using namespace x;

using namespace xear;


class Process
{
  public:
    Process(x::common::Config const& config):
      m_backend(config), m_daemon(config, m_backend)
    {

    }


    void run()
    {
      m_daemon.start();
      m_backend.start();
    }

    void wait()
    {
      m_daemon.wait();
    }

    void interrupt()
    {
      m_backend.interrupt();
      m_daemon.interrupt();
    }


  private:
    Backend m_backend;
    http::Daemon m_daemon;
};


static unique_ptr<Process> process;


void xear_signal_handler(Int signal)
{
  process->interrupt();
}


int main()
{
  dbgsi("Startup.");

  // TODO Replace daemon/http by standalone / 3rd party HTTP daemon (e.g. nginx).

  // TODO Refactor renderer & script engine into loadable modules
  // TODO Create a kind of "app service" daemon managing app instances (integrating renderer & script engine).
  x::common::ServiceManager::create<renderer::gl::Service>("renderer");
  x::common::ServiceManager::create<driver::glx::Surface>("surface");

  x::common::ServiceManager::create<script::v8::Engine>("script");
  // TODO Externalize as standalone WebRTC service daemon.
  x::common::ServiceManager::create<xear::rtc::Service>("rtc");

  // TODO Externalize as standalone CV service daemon.
  x::common::ServiceManager::createWorker<vision::opencv::Service>("vision");

  // TODO Service compositioning through wsbroker.

  String configPathname=XEAR_PATH_ETC;
  String configFilename=XEAR_FILE_ETC;

  auto config=x::common::Config::create(configPathname+"/"+configFilename);

  if(!config)
    return EXIT_FAILURE;

  dbgsi(*config);

  process=make_unique<Process>(*config);

  signal(SIGINT, xear_signal_handler);
  signal(SIGTERM, xear_signal_handler);
  signal(SIGKILL, xear_signal_handler);

  process->run();
  process->wait();

  dbgsi("Shutdown.");

  x::common::ServiceManager::destroy("rtc");
  x::common::ServiceManager::destroy("script");
  x::common::ServiceManager::destroy("vision");

  x::common::ServiceManager::destroy("renderer");
  x::common::ServiceManager::destroy("surface");

  return EXIT_SUCCESS;
}
