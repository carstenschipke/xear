/*
 * instance.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_INSTANCE_H_
#define XEAR_INSTANCE_H_


#include <chrono>

#include <xear.h>

#include <xear/common/device/config.h>
#include <xear/common/device/event.h>

#include <xear/driver/surface.h>
#include <xear/renderer/service.h>

#include <xear/rtc/event/delegate.h>
#include <xear/rtc/video/sink.h>
#include <xear/rtc/video/source.h>

#include <xear/scene/scene.h>

#include <xear/script/engine.h>
#include <xear/vision/service.h>

#include <x/common/service.h>
#include <x/concurrent/queue.h>
#include <x/io/image.h>


namespace xear
{
  using namespace std;
  using namespace std::chrono;

  using namespace x;
  using namespace x::common;
  using namespace x::concurrent;
  using namespace x::io;

  using namespace common;
  using namespace renderer;
  using namespace scene;
  using namespace script;
  using namespace rtc;
  using namespace vision;


  class Instance: public RtcEventDelegate, public RtcVideoSink, public RtcVideoSource, public script::Module
  {
    public:
      String const id;
      String const app;


      Instance(String id, String app):
        id(id), app(app), m_pathScript(String{XEAR_PATH_SCRIPT}+"/"+app)
      {
        dbgi4("Create [" << *this << "].");
      }


      // FIXME Member destructors are not invoked... there must be some circular references.
      ~Instance()
      {
        dbgi4("Destroy" << *this << "].");

        x::common::ServiceManager::resolve<renderer::Service>("renderer")->destroy(id);
        x::common::ServiceManager::resolve<driver::Surface>("surface")->destroy(id);
        x::common::ServiceManager::resolve<vision::Service>("vision")->queueRemove(id);

        m_streamBufferFrameFallback.reset();

        Boolean expected=true;

        if(m_initialized.compare_exchange_strong(expected, false))
        {
          m_context=nullptr;

          x::common::ServiceManager::resolve<script::Engine>("script")->destroy(id);

          delete m_scene;
        }
      }


      virtual void init(script::Context& context) override
      {
        // FIXME context.bind1("log", std::bind(&Instance::log, std::placeholders::_1, std::placeholders::_2)));
        context.bind1("log", [this](script::Context& context, Any& arg0) {
          log(context, arg0);
        });

        context.bind1("scene_create", [this](script::Context& context, Any& arg0) {
          auto nameScene=arg0.as<String>();

          dbgi("Using scene [" << nameScene << "].");

          m_scene=new Scene(nameScene, m_rdi);
          m_scene->load();
          m_scene->init();
        });

        context.bind0("scene_update", [this](script::Context& context) {
          m_scene->update();
        });
        context.bind0("scene_render", [this](script::Context& context) {
          m_scene->render();
        });

        // TODO Move to vision.
        context.bind2("marker_create", [this](script::Context& context, Any& arg0, Any& arg1) {
          x::common::ServiceManager::resolve<vision::Service>("vision")->markerAdd(
            id, MatchMethod::BRUTEFORCE, arg0.as<String>(), arg1.as<String>());
        });
      }

      virtual void init(device::Config config) override
      {
        Boolean expected=false;

        if(m_initialized.compare_exchange_strong(expected, true))
        {
          m_config=config;


          // Setup CV
          x::common::ServiceManager::resolve<vision::Service>("vision")->queueAdd(id);


          // Setup off-screen rendering context
          x::common::ServiceManager::resolve<driver::Surface>("surface")
            ->create(id, {0, 0, m_config.resolution.width, m_config.resolution.height});
          x::common::ServiceManager::resolve<driver::Surface>("surface")->use(id);


          // Setup renderer
          m_rdi=x::common::ServiceManager::resolve<renderer::Service>("renderer")->create(id);
          m_rdi->init(renderer::Target::BUFFER, m_config.colorspace, x::common::ServiceManager::resolve<driver::Surface>("surface")->viewport(id));


          // Setup texture buffer (camera stream)
          m_streamBufferFrameFallback=make_unique<Image>(String{XEAR_PATH_MARKER}+"/xear.png");
          m_streamBufferFrameResolution={m_streamBufferFrameFallback->width(), m_streamBufferFrameFallback->height()};

          m_rdi->textureBufferCreate("camera", m_streamBufferFrameFallback->width(), m_streamBufferFrameFallback->height(), (Int)m_streamBufferFrameFallback->size());
          m_rdi->textureBufferUpdate("camera", 0, 0, m_streamBufferFrameFallback->width(), m_streamBufferFrameFallback->height(), (Int)m_streamBufferFrameFallback->size(), m_streamBufferFrameFallback->data());
          m_rdi->textureUse("camera", "u_smpTextureBuffer");


          // Setup scripting
          m_context=x::common::ServiceManager::resolve<script::Engine>("script")->create(id);
          m_context->add(this);
          m_context->add(m_rdi);

          m_context->load(m_pathScript+"/main.js");
          m_context->compile();

          m_context->call("init");
        }
      }

      virtual void fetch(image::Ref& frame) override
      {
        m_rdi->begin();


        // Update scene
        m_context->call("update");


        // Render scene
        m_context->call("render");


        // Update texture buffer (camera stream)
        Image* inputImage=nullptr;

        auto input=m_streamBuffer.take(microseconds{0});

        if(!m_streamBufferEnabled)
          inputImage=m_streamBufferFrameFallback.get();
        else if(input)
          inputImage=input.get();

        if(inputImage)
        {
          if(m_streamBufferFrameResolution.width!=inputImage->width()
            || m_streamBufferFrameResolution.height!=inputImage->height())
          {
            dbgi("RESIZE CAMERA FRAME " << inputImage->width() << ", " << inputImage->height() << ", " << inputImage->size() << ".");

            m_rdi->textureBufferResize("camera", inputImage->width(), inputImage->height());

            m_streamBufferFrameResolution=device::Resolution{inputImage->width(), inputImage->height()};
          }

          m_rdi->textureBufferUpdate("camera", 0, 0, inputImage->width(), inputImage->height(), (Int)inputImage->size(), inputImage->data());


          // Queue for CV
          if(input)
          {
            input.release();

            x::common::ServiceManager::resolve<vision::Service>("vision")->queue(id, std::move(*inputImage));
          }
          else
          {
            x::common::ServiceManager::resolve<vision::Service>("vision")->queue(id, *inputImage);
          }
        }


        // Render camera stream (move to script)
        m_rdi->flags(Flag::TEXTURE|Flag::TEXTURE_BUFFER);
        m_rdi->draw(Shape::QUAD);


        // Fetch framebuffer contents
        m_rdi->fetch(frame);
      }


      virtual void append(unique_ptr<Image> frame) override
      {
        m_streamBuffer.offer(std::move(frame));

        m_streamBufferEnabled=true;
      }

      virtual device::Resolution const& resolution() override
      {
        return m_config.resolution;
      }


      virtual void onEvent(device::Event const event) override
      {
        // TODO Implement ...
      }


      friend std::ostream& operator<<(std::ostream& stream, Instance const& instance)
      {
        stream << "Instance{id: " << instance.id << ", app: " << instance.app << "}";

        return stream;
      }


      // JS Module API
      void log(script::Context& context, Any& any)
      {
        dbgi(any.as<String>());
      }


    private:
      device::Config m_config;

      Boolean m_debugEnabled=false;
      Boolean m_wireframeEnabled=false;
      Boolean m_streamBufferEnabled=false;
      Queue<unique_ptr<Image>, Int, 5> m_streamBuffer;
      unique_ptr<Image> m_streamBufferFrameFallback;
      device::Resolution m_streamBufferFrameResolution;
      String m_pathScript;

      Context* m_context=nullptr;
      Scene* m_scene=nullptr;
      Rdi* m_rdi=nullptr;

      atomic<Boolean> m_initialized{false};
  };
}


#endif
