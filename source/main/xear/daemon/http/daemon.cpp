/*
 * daemon.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include "daemon.h"

#include <chrono>
#include <functional>

#include <cpprest/http_listener.h>
#include <cpprest/http_msg.h>

#include <x/web/http/common.h>

#include "handler.h"


namespace xear
{
    namespace http
    {
      using namespace std;
      using namespace std::chrono;

      using namespace x;
      using namespace x::io;
      using namespace x::web::http;

      using namespace ::web;
      using namespace ::web::http;
      using namespace ::web::http::experimental::listener;


      inline void http_request_map(http_request& source, Request& destination)
      {
        for(auto& header : source.headers())
        {
          auto h=Header{header.first, header.second};

          dbgsi4("Request header [" << h << "].");

          destination.headers.push_back(h);
        }

        auto content=source.extract_vector().get();
        destination.content.append(content.begin(), content.end());

        destination.method=x::web::http::methodForName(source.method());
        destination.version={1, 1, 0};
        // FIXME absoluts_uri is not absolute - following information is missing.
        destination.scheme=source.absolute_uri().scheme();
        destination.user=source.absolute_uri().user_info();
        destination.host=source.absolute_uri().host();
        destination.port=source.absolute_uri().port();
        destination.path=source.absolute_uri().path();
        destination.query=source.absolute_uri().query();
        destination.fragment=source.absolute_uri().fragment();
      }

      inline void http_response_map(Request const& request, Response const& source, http_response& destination)
      {
        destination.set_status_code(source.status);

        for(auto& header : source.headers)
        {
          dbgsi4("Response header [" << header << "].");

          destination.headers().add(header.name, header.value);
        }

        if(source.content.empty())
        {
          if(Method::GET==request.method && Status::OK!=source.status)
            destination.set_body(status_html(source.status), "text/html; charset=utf-8");
        }
        else
        {
          destination.set_body(source.content, Mime::nameForType(source.mimetype)+"; charset=utf-8");
        }
      }


      Daemon::Daemon(Config const& config, Backend& backend):
        Task("xear::http::Daemon"), m_config(config), m_backend(backend)
      {
        dbgi4("Create.");
     }

      Daemon::~Daemon()
      {
        dbgi4("Destroy.");
      }


      void Daemon::run()
      {
        dbgi2("Start.");

        // TODO Support multiple listeners.
        auto name=m_config.get<String>("http.0.name", XEAR_HTTP_NAME_DEFAULT);
        auto host=m_config.get<String>("http.0.host", XEAR_HTTP_HOST_DEFAULT);
        auto base=m_config.get<String>("http.0.base", XEAR_HTTP_BASE_DEFAULT);
        auto record=gethostbyname(host.c_str());

        String addr;

        for(Int i=0; i<record->h_length; i++)
        {
          if(!record->h_addr_list[i])
            break;

          auto address=reinterpret_cast<in_addr*>(record->h_addr_list[i]);

          addr=inet_ntoa(*address);
        }


        HandlerConfig configHandler;

        configHandler.sessionName=m_config.get<String>("http.0.session.name", XEAR_SESSION_DOMAIN_WILDCARD);
        configHandler.sessionPath=m_config.get<String>("http.0.session.path", base);
        configHandler.sessionTtl=m_config.get<Int>("http.0.session.ttl", XEAR_SESSION_LIFETIME_DEFAULT);

        configHandler.documentMain=m_config.get<String>("web.main", XEAR_WEB_MAIN_DEFAULT);
        configHandler.documentRoot=m_config.get<String>("web.root", XEAR_WEB_ROOT_DEFAULT);

        Handler handler{configHandler, m_backend};

        auto invokeHandler=[&handler](http_request request_) {

          dbgsi2("Dispatch [" << request_.relative_uri().to_string() << "].");

          http_response responseImpl;

          Request request;
          Response response;

          http_request_map(request_, request);

          handler.dispatch(request, response);

          http_response_map(request, response, responseImpl);
          request_.reply(responseImpl);
        };

        uri_builder builder;
        builder.set_host(addr);
        builder.set_scheme(m_config.get<String>("http.0.scheme", XEAR_HTTP_SCHEME_DEFAULT));
        builder.set_port(m_config.get<Int>("http.0.port", XEAR_HTTP_PORT_DEFAULT));
        builder.set_path(base);

        http_listener_config httpListenerConfig;
        httpListenerConfig.set_timeout(seconds{120});

        http_listener listener{builder.to_uri(), httpListenerConfig};
        listener.support("GET", invokeHandler);
        listener.support("POST", invokeHandler);
        listener.open().wait();

        dbgi("Listening [" << builder.to_uri().to_string() << "].");

        wait(State::INTERRUPTED);

        dbgi2("Stop");

        listener.close().wait();
      }
    }
}
