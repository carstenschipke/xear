/*
 * daemon.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_HTTP_DAEMON_H_
#define XEAR_HTTP_DAEMON_H_


#include <xear.h>

#include <x/common/config.h>
#include <x/concurrent/task.h>
#include <x/web/http/common.h>

#include "handler.h"

#include <daemon/backend.h>


namespace xear
{
    namespace http
    {
      using namespace std;

      using namespace x::common;
      using namespace x::concurrent;
      using namespace x::web::http;


      class Daemon: public Task
      {
        public:
          Daemon(Config const& config, Backend& backend);


          Daemon(Daemon const&) = delete;
          Daemon& operator=(Daemon const&) = delete;


          virtual ~Daemon();


        protected:
          virtual void run() override;


        private:
          Config const& m_config;
          Backend& m_backend;
      };
    }
}


#endif
