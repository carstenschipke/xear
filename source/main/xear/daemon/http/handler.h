/*
 * handler.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_HTTP_HANDLER_H_
#define XEAR_HTTP_HANDLER_H_


#include <regex>

#include <x/io/file.h>
#include <x/web/http/common.h>

#include "../backend.h"


namespace xear
{
    namespace http
    {
      using namespace std;

      using namespace x;
      using namespace x::common;
      using namespace x::io;
      using namespace x::web::http;


      struct FileCacheValue
      {
        Status status;
        Mimetype mimetype;
        String content;
      };


      struct HandlerConfig
      {
        String documentRoot;
        String documentMain;

        String sessionName;
        String sessionPath;
        Int sessionTtl;
      };


      class Handler: public RequestHandler
      {
        public:
          Handler(HandlerConfig& config, Backend& backend):
            m_backend(backend)
          {
            m_root=config.documentRoot;
            m_main=config.documentMain;

            m_sessions=make_unique<Sessions>(config.sessionName, config.sessionPath, config.sessionTtl);

            serve("/", Method::GET, bind(&Handler::index, this, placeholders::_1, placeholders::_2));
            serve("/"+m_main, Method::GET, bind(&Handler::index, this, placeholders::_1, placeholders::_2));

            serve("/candidate", Method::GET, bind(&Handler::candidate, this, placeholders::_1, placeholders::_2));
            serve("/message", Method::POST, bind(&Handler::message, this, placeholders::_1, placeholders::_2));
            serve("/session", Method::GET, bind(&Handler::session, this, placeholders::_1, placeholders::_2));
            serve("/setup", Method::POST, bind(&Handler::setup, this, placeholders::_1, placeholders::_2));
            serve("/setup/(.*)", Method::POST, bind(&Handler::setup, this, placeholders::_1, placeholders::_2));
          }


          Handler(Handler const&) = delete;
          Handler& operator=(Handler const&) = delete;


          virtual ~Handler()
          {

          }


          void index(Request const& request, Response& response)
          {
            auto peerIdPrevious=m_sessions->renew(request, response);

            if(!peerIdPrevious.empty())
              m_backend.peerDisconnect(peerIdPrevious);

            file("/"+m_main, response);
          }


          void candidate(Request const& request, Response& response)
          {
            if(!m_backend.peerExists(response.session->id))
            {
              response.status=Status::NOT_FOUND;

              return;
            }

            dbgi3("Request peer candidate [" << response.session->id << "].");

            response.status=Status::OK;
            response.mimetype=Mimetype::APPLICATION_JSON;

            RtcPeer* peer=m_backend.peer(response.session->id);

            if(peer)
              response.content=peer->candidate();
            else
              response.status=Status::NOT_FOUND;
          }

          void message(Request const& request, Response& response)
          {
            if(!m_backend.peerExists(response.session->id))
            {
              response.status=Status::NOT_FOUND;

              return;
            }

            dbgi3("Queue peer message [" << response.session->id << "].");

            queue(request, response);
          }

          void session(Request const& request, Response& response)
          {
            if(!m_backend.peerExists(response.session->id))
            {
              response.status=Status::NOT_FOUND;

              return;
            }

            response.status=Status::OK;
            response.mimetype=Mimetype::APPLICATION_JSON;

            RtcPeer* peer=m_backend.peer(response.session->id);

            dbgi3("Request peer SDP [" << response.session->id << "].");

            if(peer)
              response.content=peer->sdp();
            else
              response.status=Status::NOT_FOUND;
          }

          void setup(Request const& request, Response& response)
          {
            dbgi3("Setup peer [" << response.session->id << "].");

            queue(request, response);
          }

          void queue(Request const& request, Response& response)
          {
            dbgi3("Queue peer request [" << response.session->id << "].");

            response.status=Status::OK;
            response.mimetype=Mimetype::APPLICATION_JSON;
            response.content="";

            m_backend.peerQueue(response.session->id, request, response);
          }


          virtual void dispatch(Request const& request_, Response& response_) override
          {
            file(request_, response_);

            if(Status::OK!=response_.status)
            {
              String route=match(request_);

              if(!route.empty())
              {
                m_sessions->open(request_, response_);

                if(!dispatch(route, request_, response_))
                  response_.status=Status::NOT_FOUND;
              }
            }
          }


        private:
          Backend& m_backend;

          String m_root;
          String m_main;

          map<String, String> m_methodsHttp;
          // TODO Limit size / lifetime.
          map<String, FileCacheValue> m_cacheFile;
          map<String, function<void(Request const&, Response&)>> m_nethodsResource;

          unique_ptr<Sessions> m_sessions;


          void serve(String name, http::Method const methodHttp, function<void(Request const&, Response&)> methodResource)
          {
            m_methodsHttp[name]=http::METHODS[methodHttp];
            m_nethodsResource[name]=methodResource;
          }

          String match(Request const& request)
          {
            String requestMethod=x::web::http::nameForMethod(request.method);

            for(auto& entry : m_methodsHttp)
            {
              dbgi5("Match HTTP request method [supported: " << entry.second.c_str() << ", requested: " << requestMethod.c_str() << "].");

              if(0==strcmp(entry.second.c_str(), requestMethod.c_str()))
              {
                dbgi5("Match HTTP request path [supported: " << entry.first.c_str() << ", requested: " << request.path.c_str() << "].");

                if(0==strcmp(entry.first.c_str(), request.path.c_str()))
                  return entry.first;

                regex sexearh{entry.first, regex_constants::icase};

                if(regex_match(request.path, sexearh))
                  return entry.first;
              }
            }

            return "";
          }

          Boolean dispatch(String name, Request const& request, Response& response)
          {
            auto iterator=m_nethodsResource.find(name);

            if(iterator==m_nethodsResource.end())
              return false;

            function<void(Request const&, Response&)> func=iterator->second;

            func(request, response);

            return true;
          }

          void file(Request const& request, Response& response)
          {
            file(request.path, response);
          }

          void file(String requestUri, Response& response)
          {
#           ifndef CACHE
              response.status=Status::NOT_FOUND;

              Int idx=requestUri.find("/");

              String filepath;

              if(0==idx)
                filepath=m_root+requestUri;
              else
                filepath=m_root+"/"+requestUri;

              if(!filepath.empty())
              {
                File file{filepath};

                if(file.isFile())
                {
                  response.status=Status::OK;
                  response.content=file.readText();
                  response.mimetype=file.mimetype();
                }
                else
                {
                  dbgi5("File not found [" << filepath << "].");
                }
              }
#           else
              auto cached=m_cacheFile.find(requestUri);

              if(cached==m_cacheFile.end())
              {
                Int idx=requestUri.find("/");

                String filepath;

                FileCacheValue cache{Status::NOT_FOUND};

                if(0==idx)
                  filepath=m_root+requestUri;
                else
                  filepath=m_root+"/"+requestUri;

                if(!filepath.empty())
                {
                  File file{filepath};

                  if(file.isFile())
                  {
                    cache.status=Status::OK;
                    cache.readText=file.readText();
                    cache.mimetype=file.mimetype();
                  }
                }

                m_cacheFile.insert({requestUri, cache});
              }

              response.status=m_cacheFile[requestUri].status;
              response.readText=m_cacheFile[requestUri].readText;
              response.mimetype=m_cacheFile[requestUri].mimetype;
#           endif
          }
      };
    }
}


#endif
