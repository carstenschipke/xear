/*
 * sandbox/main.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <xear.h>

#include <x/misc/test.h>
#include "../x/io/video/ffmpeg/decoder.h"

#include <opencv2/opencv_modules.hpp>
#include <opencv2/core.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/cudaimgproc.hpp>
#include <opencv2/cudafeatures2d.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/cuda.hpp>


using namespace std;

using namespace x;

using namespace cv;


struct Bgr
{
  Byte b;
  Byte g;
  Byte r;
};


Int main(Int const argc, Char const* const argv[])
{
  dbgsi("Startup.");

  ::String filename{argv[1]};

  dbgsi("READ " << filename);

  x::io::image::Ref frame;

  x::io::video::ffmpeg::Decoder decoder;
  decoder.read(filename, frame);

  return EXIT_SUCCESS;

  cuda::printCudaDeviceInfo(cuda::getDevice());

  TIMER_BEGIN();
  Mat source=imread(filename, IMREAD_GRAYSCALE);
  TIMER_DUMP("READ SOURCE");

  TIMER_BEGIN();
  Mat tmp;
  tmp.create(source.size(), source.type());

  Mat target;
  target.create(source.size(), CV_8UC3);
  TIMER_DUMP("ALLOCATE TARGET");

//  TIMER_BEGIN();
//  blur(source, tmp, cv::Size(3, 3));
//  TIMER_DUMP("BLUR");

  TIMER_BEGIN();
  cuda::GpuMat in;
  cuda::GpuMat out;
  Ptr<cuda::CannyEdgeDetector> canny=cuda::createCannyEdgeDetector(99, 297, 3, true);
  TIMER_DUMP("CUDA ALLOC");

  TIMER_BEGIN();
  in.upload(source);
  TIMER_DUMP("CUDA UPLOAD");

  TIMER_BEGIN();
  canny->detect(in, out);
  TIMER_DUMP("CUDA EXEC");

  TIMER_BEGIN();
  out.download(tmp);
  TIMER_DUMP("CUDA DOWNLOAD");

//  Canny(tmp, tmp, 99, 297, 3);
//  TIMER_DUMP("CANNY EDGE DETECTION");

  TIMER_BEGIN();
  vector<vec2> samples;

  for(auto y=0; y<source.rows; ++y)
  {
    for(auto x=0; x<source.cols; ++x)
    {
      if(0<tmp.ptr<Byte>(y)[x])
      {
        if(0==y%20)
        {
//          target.ptr<Bgr>(y)[x]={0, 0, 255};
          samples.emplace_back(x, y);
        }
//        else
//        {
//          target.ptr<Bgr>(y)[x]={255, 255, 255};
//        }
      }
    }
  }

  TIMER_DUMP("COLLECT SAMPLES");

  TIMER_BEGIN();
  auto count=samples.size();
  auto limit=50;
  IntU skip=count/limit;

  vector<vec2> samplesr;

  for(auto i=0; i<count; i+=skip)
    samplesr.emplace_back(samples[i].x, samples[i].y);
  TIMER_DUMP("FILTER SAMPLES");

  limit=samplesr.size();

  TIMER_BEGIN();

  for(auto i=0; i<limit; ++i)
  {
    for(auto j=0; j<limit; ++j)
      line(target, Point(samplesr[i].x, samplesr[i].y), Point(samplesr[j].x, samplesr[j].y), Scalar(255, 0, 0), 1, 8);
  }

  vec2 winner;
  IntU highscore=0;

  for(auto y=0; y<target.rows; y+=40)
  {
    for(auto x=0; x<target.cols; x+=40)
    {
      IntU score=0;

      for(auto i=0; i<40; i++)
      {
        for(auto j=0; j<40; j++)
          score+=(IntU)target.ptr<Bgr>(y+i)[x+j].b;
      }

      if(highscore<score)
      {
        highscore=score;
        winner={x, y};
      }
    }
  }

  TIMER_DUMP("SELECT VANISHING POINT");

  rectangle(target, Point(winner.x, winner.y), Point(winner.x+40, winner.y+40), Scalar(0, 0, 255), 3, 8);

  imwrite("/tmp/output.png", target);

  dbgsi("POIs " << samples.size());
  dbgsi("Important POIs " << samplesr.size());
  dbgsi("Vanishing Point " << winner.x << ", " << winner.y);

  dbgsi("Shutdown.");

  return EXIT_SUCCESS;
}
