/*
 * service.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_VISION_SERVICE_H_
#define XEAR_VISION_SERVICE_H_


#include <mutex>

#include <xear.h>

#include <x/common/service.h>
#include <x/io/image.h>


namespace xear
{
  namespace vision
  {
    using namespace std;

    using namespace x;
    using namespace x::io;


    struct Result
    {
      public:
        Result():
          m_match(false)
        {
          dbgi6("Create [" << *this << "].");
        }

        Result(Boolean match):
          m_match(match)
        {
          dbgi6("Create [" << *this << "].");
        }


        Result(Result const& result):
          m_match(!!result.m_match), m_perspective(result.m_perspective)
        {
          dbgi6("Copy-construct [" << *this << "].");
        }

        Result(Result&& result):
          m_match(!!result.m_match), m_perspective(move(result.m_perspective))
        {
          dbgi6("Move-construct [" << *this << "].");
        }


        Result& operator=(Result const& result)
        {
          dbgi6("Copy-assign [" << *this << "].");

          m_match=result.m_match;
          m_perspective=result.m_perspective;

          return *this;
        }

        Result& operator=(Result&& result)
        {
          dbgi6("Move-assign [" << *this << "].");

          m_match=result.m_match;
          m_perspective=move(result.m_perspective);

          return *this;
        }


        ~Result()
        {
          dbgi6("Destroy [" << *this << "].");
        }


        Boolean match()
        {
          lock_guard<mutex> lock{m_monitor};

          return m_match;
        }

        Result& match(Boolean match)
        {
          lock_guard<mutex> lock{m_monitor};

          if(!match)
          {
            if(1>--m_threshold)
              m_match=false;
          }
          else
          {
            m_threshold=3;

            m_match=true;
          }

          return *this;
        }

        // FIXME Remove 'perspective' and create a projection matrix accessor per service instance.
        Result& match(mat3 const& perspective)
        {
          lock_guard<mutex> lock{m_monitor};

          m_threshold=3;

          m_match=true;
          m_perspective=perspective;

          return *this;
        }

        mat3 const& perspective()
        {
          lock_guard<mutex> lock{m_monitor};

          return m_perspective;
        }

        Result& modelView(mat4 const& modelView)
        {
          lock_guard<mutex> lock{m_monitor};

          m_modelView=modelView;

          return *this;
        }

        mat4 const& modelView()
        {
          lock_guard<mutex> lock{m_monitor};

          return m_modelView;
        }

        Result& box(vector<Float> const& box)
        {
          lock_guard<mutex> lock{m_monitor};

          m_box=box;

          return *this;
        }

        vector<Float> const& box()
        {
          lock_guard<mutex> lock{m_monitor};

          return m_box;
        }


        friend std::ostream& operator<<(std::ostream& stream, Result const& result)
        {
          stream << "Result{id: " << &result
            << ", match: " << result.m_match
            << ", perspective: " << glm::to_string(result.m_perspective)
            << ", model-view: " << glm::to_string(result.m_modelView)
            << "}";

          return stream;
        }


      private:
        Boolean m_match;
        mat4 m_modelView;
        mat3 m_perspective;
        Int m_threshold=3;
        vector<Float> m_box;

        mutex m_monitor;
    };


    enum MatchMethod
    {
      BRUTEFORCE
    };


    class Service: public x::common::ServiceWorker
    {
      public:
        virtual ~Service()
        {

        }


        virtual void setup() override = 0;
        virtual void teardown() override = 0;


        virtual Result& markerMatch(String queue, String name) = 0;
        virtual vec2& vanishingPoint(String queue) = 0;

        virtual void markerAdd(String queue, MatchMethod method, String name, String path) = 0;

        virtual void queue(String name, io::Image&& frame) = 0;
        virtual void queue(String name, io::Image const& frame) = 0;

        virtual void queueAdd(String name) = 0;
        virtual void queueRemove(String name) = 0;


      protected:
        virtual void run() override = 0;
    };
  }
}


#endif
