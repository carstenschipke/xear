/*
 * node.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_SCENE_NODE_H_
#define XEAR_SCENE_NODE_H_


#include <vector>

#include <xear.h>

#include <x/io/json.h>


namespace xear
{
  namespace scene
  {
    using namespace std;

    using namespace x;
    using namespace x::io;


    enum NodeType
    {
      SCENE,
      MODEL,
      MESH,
      CAMERA,
      LIGHT,
      PLAYER
    };


    NodeType const NODE_TYPES[]={
      NodeType::SCENE,
      NodeType::MODEL,
      NodeType::MESH,
      NodeType::CAMERA,
      NodeType::LIGHT,
      NodeType::PLAYER
    };


    String const NAME_NODE_TYPE[]={
      "SCENE",
      "MODEL",
      "MESH",
      "CAMERA",
      "LIGHT",
      "PLAYER"
    };


    inline std::ostream& operator<<(std::ostream& stream, NodeType const& type)
    {
      stream << NAME_NODE_TYPE[type];

      return stream;
    }


    class Scene;


    class Node
    {
      public:
        NodeType const type;
        String const name;

        Node* parent=nullptr;
        vector<unique_ptr<Node>> children;

        String geometry;

        vec3 position={0.0f, 0.0f, 0.0f};

        vec3 rotation={0.0f, 0.0f, 0.0f};
        vec3 scale={1.0f, 1.0f, 1.0f};
        vec3 translation={0.0f, 0.0f, 0.0f};

        mat4 model;

        Boolean enabled=true;
        Boolean visible=true;


        Node(NodeType type, String const& name);


        Node(Node const&) = delete;
        Node& operator=(Node const&) = delete;


        virtual ~Node();


        virtual void load();
        virtual void load(Json::Value const& value);

        virtual void init();
        virtual void update();
        virtual void render();

        virtual void activate();
        virtual void deactivate();

        Node* const get(String const& name);
        void add(unique_ptr<Node> node);
        unique_ptr<Node> remove(String const& name);

        Scene& scene();


        friend std::ostream& operator<<(std::ostream& stream, Node const& node)
        {
          stream << "Node{id: " << &node
            << ", type: " << node.type
            << ", name: " << node.name
            << ", enabled: " << node.enabled
            << ", visible: " << node.visible
            << ", parent: " << node.parent
            << ", children: " << node.children.size();

          if(NodeType::MODEL==node.type)
          {
            stream << ", geometry: " << node.geometry
              << ", position: " << glm::to_string(node.position)
              << ", rotation: " << glm::to_string(node.rotation)
              << ", scale: " << glm::to_string(node.scale)
              << ", translation: " << glm::to_string(node.translation);
          }

          stream << "}";

          return stream;
        }


      protected:
        Scene* m_scene;


        mat4 matModel();


      private:
        static unique_ptr<Node> create(NodeType type, String const& name);
    };
  }
}


#endif
