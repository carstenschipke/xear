/*
 * light.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_SCENE_LIGHT_H_
#define XEAR_SCENE_LIGHT_H_


#include <xear.h>
#include <xear/scene/node.h>

#include <x/io/json.h>


namespace xear
{
  namespace scene
  {
    using namespace std;

    using namespace x;
    using namespace x::io;


    enum LightType
    {
      POINT,
      SPOT,
      DIRECTIONAL
    };


    LightType const LIGHT_TYPES[]={
      LightType::POINT,
      LightType::SPOT,
      LightType::DIRECTIONAL
    };


    String const NAME_LIGHT_TYPE[]={
      "POINT",
      "SPOT",
      "DIRECTIONAL"
    };


    inline std::ostream& operator<<(std::ostream& stream, LightType const& type)
    {
      stream << NAME_LIGHT_TYPE[type];

      return stream;
    }


    class Light: public Node
    {
      public:
        LightType type;

        vec3 direction;
        vec3 color;
        vec3 properties;


        Light(String const& name);


        Light(Light const&) = delete;
        Light& operator=(Light const&) = delete;


        virtual ~Light();


        virtual void load(Json::Value const& value) override;

        virtual void init() override;
        virtual void update() override;
        virtual void render() override;


        friend std::ostream& operator<<(std::ostream& stream, Light const& light)
        {
          stream << "Light{id: " << &light << ", name: " << light.name << ", type: " << light.type << "}";

          return stream;
        }
    };
  }
}


#endif
