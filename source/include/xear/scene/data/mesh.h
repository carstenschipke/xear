/*
 * mesh.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_SCENE_DATA_MESH_H_
#define XEAR_SCENE_DATA_MESH_H_


#include <fstream>

#include <xear.h>


#define XEAR_SCENE_DATA_MESH_VERSION 1


namespace xear
{
  namespace scene
  {
    namespace data
    {
      using namespace std;

      using namespace x;


      struct Mesh
      {
        String material;
        String texture;

        Int offset;
        Int count;


        void serialize(String const& path)
        {
          dbgsi("Write mesh {file: " << path << "}.");

          std::ofstream file{path};

          serialize(file);
        }

        void serialize(std::ostream& stream)
        {
          Int sizeMaterial=sizeof(Char)*(Int)material.size()+1;
          Int sizeTexture=sizeof(Char)*(Int)texture.size()+1;

          Int version=XEAR_SCENE_DATA_MESH_VERSION;

          stream.write((Char*)&version, sizeof(Int));
          stream.write((Char*)&offset, sizeof(Int));
          stream.write((Char*)&count, sizeof(Int));
          stream.write((Char*)&sizeMaterial, sizeof(Int));
          stream.write((Char*)&sizeTexture, sizeof(Int));

          if(0<sizeMaterial)
            stream.write((Char*)material.c_str(), sizeMaterial);

          if(0<sizeTexture)
            stream.write((Char*)texture.c_str(), sizeTexture);
        }


        static unique_ptr<Mesh> unserialize(String const& path)
        {
          dbgsi("Read mesh {file: " << path << "}.");

          std::ifstream file{path};

          return unserialize(file);
        }

        static unique_ptr<Mesh> unserialize(std::istream& stream)
        {
          auto mesh=make_unique<Mesh>();

          Int version;
          Int sizeMaterial;
          Int sizeTexture;

          stream.read((Char*)&version, sizeof(Int));

          stream.read((Char*)&mesh->offset, sizeof(Int));
          stream.read((Char*)&mesh->count, sizeof(Int));

          stream.read((Char*)&sizeMaterial, sizeof(Int));
          stream.read((Char*)&sizeTexture, sizeof(Int));

          if(0<sizeMaterial)
          {
            Char* material=(Char*)malloc(sizeMaterial);
            stream.read((Char*)material, sizeMaterial);
            mesh->material=material;

            free(material);
          }
          else
          {
            mesh->material="plain";
          }

          if(0<sizeTexture)
          {
            Char* texture=(Char*)malloc(sizeTexture);
            stream.read((Char*)texture, sizeTexture);
            mesh->texture=texture;

            free(texture);
          }

          dbgsi2("Read mesh [" << *mesh << "].");

          return mesh;
        }


        friend std::ostream& operator<<(std::ostream& stream, Mesh const& mesh)
        {
          stream << "Mesh{material: " << mesh.material
            << ", texture: " << mesh.texture
            << ", offset: " << mesh.offset
            << ", count: " << mesh.count
            << "}";

          return stream;
        }
      };
    }
  }
}


#endif
