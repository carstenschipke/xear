/*
 * geometry.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_SCENE_DATA_GEOMETRY_H_
#define XEAR_SCENE_DATA_GEOMETRY_H_


#include <fstream>

// TODO Solve linker issues.
//#include <boost/archive/text_iarchive.hpp>
//#include <boost/archive/text_oarchive.hpp>
//#include <boost/serialization/vector.hpp>

#include <xear.h>

#include <xear/common/primitive.h>


#define XEAR_SCENE_DATA_GEOMETRY_VERSION 1


namespace xear
{
  namespace scene
  {
    namespace data
    {
      using namespace std;

      using namespace x;

      using namespace xear::common;


      template<typename TypeIndex=IntU, typename TypeVertex=Float>
      struct Geometry
      {
        String layout;

        Primitive primitive;

        Int countIndices;
        Int countVertices;

        vector<TypeIndex> indices;
        vector<TypeVertex> vertices;

        typedef TypeIndex typeIndex;
        typedef TypeVertex typeVertex;


        Geometry(String const& layout, Primitive const& primitive):
          Geometry(layout, primitive, 0, 0)
        {

        }

        Geometry(String const& layout, Primitive const& primitive, Int const& countIndices, Int const& countVertices):
          layout(layout),
          primitive(primitive),
          countIndices(countIndices),
          countVertices(countVertices),
          indices(countIndices),
          vertices(countVertices)
        {
          dbgi7("Create [" << *this << "].");
        }

        Geometry(String const& layout,
                 Primitive const& primitive,
                 Int const& countIndices,
                 Int const& countVertices,
                 vector<TypeIndex> const& indices,
                 vector<TypeVertex> const& vertices):
          layout(layout),
          primitive(primitive),
          countIndices(countIndices),
          countVertices(countVertices),
          indices(indices),
          vertices(vertices)
        {
          dbgi7("Create [" << *this << ", " << &vertices << "].");
        }

        Geometry(String const& layout,
                 Primitive const& primitive,
                 Int const& countIndices,
                 Int const& countVertices,
                 initializer_list<TypeIndex> const& indices,
                 initializer_list<TypeVertex> const& vertices):
          layout(layout),
          primitive(primitive),
          countIndices(countIndices),
          countVertices(countVertices),
          indices(indices),
          vertices(vertices)
        {
          dbgi7("Create [" << *this << ", " << &vertices << "].");
        }


        ~Geometry()
        {
          dbgi7("Destroy.");
        }


        void operator<<(vector<TypeIndex> const& append)
        {
          indices.insert(indices.end(), append.begin(), append.end());

          countIndices+=append.size();
        }

        void operator<<(vector<TypeVertex> const& append)
        {
          vertices.insert(vertices.end(), append.begin(), append.end());

          countVertices+=append.size();
        }

        void operator<<(TypeIndex index)
        {
          indices.push_back(index);

          ++countIndices;
        }

        void operator<<(TypeVertex vertex)
        {
          vertices.push_back(vertex);

          ++countVertices;
        }


        Scalar typeScalarIndex() const
        {
          return toScalar<typeIndex>();
        }

        Scalar typeScalarVertex() const
        {
          return toScalar<typeVertex>();
        }


        void serialize(String const& path)
        {
          dbgsi("Write geometry {file: " << path << "}");

          ofstream file{path};

          serialize(file);
        }

        void serialize(ostream& stream)
        {
// TODO Solve linker issues.
//          boost::archive::text_oarchive archive(stream);
//          archive << *this;

          Int sizeLayout=sizeof(Char)*(Int)layout.size()+1;
          Int primitiveInt=toInt(primitive);

          Int version=XEAR_SCENE_DATA_GEOMETRY_VERSION;

          stream.write((Char*)&version, sizeof(Int));
          stream.write((Char*)&sizeLayout, sizeof(Int));

          if(0<sizeLayout)
            stream.write((Char*)layout.c_str(), sizeLayout);

          stream.write((Char*)&primitiveInt, sizeof(Int));
          stream.write((Char*)&countIndices, sizeof(Int));
          stream.write((Char*)&countVertices, sizeof(Int));
          stream.write((Char*)indices.data(), sizeof(typeIndex)*countIndices);
          stream.write((Char*)vertices.data(), sizeof(typeVertex)*countVertices);
        }


// TODO Solve linker issues.
//        void unserialize(String path)
//        {
//          dbgsi("Read geometry {file: " << path << "}.");
//
//          ifstream stream{path};
//
//          boost::archive::text_iarchive archive(stream);
//
//          archive & layout;
//          archive & primitive;
//          archive & countIndices;
//          archive & countVertices;
//
//          boost::serialization::load(archive, indices, 1);
//          boost::serialization::load(archive, vertices, 1);
//        }

        static unique_ptr<Geometry<>> unserialize(String const& path)
        {
          dbgsi("Read geometry {file: " << path << "}.");

          ifstream file{path};

          return unserialize(file);
        }

        static unique_ptr<Geometry<>> unserialize(istream& stream)
        {
          Int version;
          Int sizeLayout;

          String layout="plain";

          stream.read((Char*)&version, sizeof(Int));
          stream.read((Char*)&sizeLayout, sizeof(Int));

          if(0<sizeLayout)
          {
            Char* layoutChr=(Char*)malloc(sizeLayout);
            stream.read((Char*)layoutChr, sizeLayout);
            layout=layoutChr;

            free(layoutChr);
          }

          Int primitiveInt;
          Int countIndices;
          Int countVertices;

          stream.read((Char*)&primitiveInt, sizeof(Int));
          stream.read((Char*)&countIndices, sizeof(Int));
          stream.read((Char*)&countVertices, sizeof(Int));

          auto geometry=make_unique<Geometry<>>(layout, toPrimitive(primitiveInt), countIndices, countVertices);

          stream.read((Char*)geometry->indices.data(), sizeof(typeIndex)*geometry->countIndices);
          stream.read((Char*)geometry->vertices.data(), sizeof(typeVertex)*geometry->countVertices);

          dbgsi("Read geometry [" << *geometry << "].");

          return geometry;
        }


        friend ostream& operator<<(ostream& stream, Geometry<> const& geometry)
        {
          stream << "Geometry{layout: " << geometry.layout
            << ", primitive: " << geometry.primitive
            << ", indices: " << &geometry.indices
            << ", indices-type: " << toScalar<TypeIndex>()
            << ", indices-count: " << geometry.countIndices
            << ", vertices: " << &geometry.vertices
            << ", vertices-type: " << toScalar<TypeVertex>()
            << ", vertices-count: " << geometry.countVertices
            << "}";

          return stream;
        }


// TODO Solve linker issues.
//        private:
//          friend class boost::serialization::access;
//
//
//          template<class Archive>
//          void serialize(Archive& ar, IntU const version)
//          {
//            ar & layout;
//            ar & primitive;
//            ar & countIndices;
//            ar & countVertices;
//
//            boost::serialization::save(ar, indices, version);
//            boost::serialization::save(ar, vertices, version);
//          }
      };


      // TODO Respect defined layout.
      // TODO Support some modifiers? E.g. per-face-callback for custom color, uv etc.?
      // TODO Alternatively per-face-segmentation / primitive restart?
      namespace geometry
      {
        inline Geometry<> createQuad(String const& layout)
        {
          return {
            layout,
            Primitive::TRIANGLE,
            7,
            64,
            {0xffffffff, 0, 1, 2, 0, 2, 3},
            {
              -1.0f, -1.0f, 1.0f,  0.0f, 0.0f,  1.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
               1.0f, -1.0f, 1.0f,  1.0f, 0.0f,  1.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
               1.0f,  1.0f, 1.0f,  1.0f, 1.0f,  1.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
              -1.0f,  1.0f, 1.0f,  0.0f, 1.0f,  1.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f
            }
          };
        }

        inline Geometry<> createCube(String const& layout, Float width=1.0f, Float height=1.0f, Float depth=1.0f)
        {
          Geometry<> cube{layout, Primitive::TRIANGLE_STRIP};

          typedef decltype(cube)::typeIndex Index;
          typedef decltype(cube)::typeVertex Vertex;

          cube << vector<Index>{
            0xffffffff, 0, 1, 2, 3, 7, 1, 6, 4, 7, 5, 2, 4, 0, 1
          };

          cube << vector<Vertex>{
            -width, -height,  depth,  1.0f, 1.0f,  0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
             width, -height,  depth,  0.0f, 1.0f,  0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
            -width,  height,  depth,  1.0f, 0.0f,  0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
             width,  height,  depth,  0.0f, 0.0f,  0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
            -width, -height, -depth,  1.0f, 1.0f,  1.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
             width, -height, -depth,  0.0f, 1.0f,  1.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
            -width,  height, -depth,  1.0f, 0.0f,  1.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
             width,  height, -depth,  0.0f, 0.0f,  1.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f
           };

          return cube;
        }

        // FIXME Not quite right ...
        /**
         * See http://paulbourke.net/geometry/circlesphere/
         */
        inline Geometry<> createCone(String const& layout,
                                     vec3 centerBottom=vec3{0.0f, 1.0f, 0.0f},
                                     vec3 centerTop=vec3{0.0f, -1.0f, 0.0f},
                                     Float radiusBottom=1.0f,
                                     Float radiusTop=1.0f,
                                     Int precision=360,
                                     Float theta0=1.0f,
                                     Float theta1=2.0f)
        {
          Geometry<> cone{layout, Primitive::TRIANGLE_STRIP};

          typedef decltype(cone)::typeIndex Index;
          typedef decltype(cone)::typeVertex Vertex;

          Float theta;
          vec3 normal, vertex, q, perp;

          // Normal pointing from centerBottom to centerTop.
          normal.x=centerBottom.x-centerTop.x;
          normal.y=centerBottom.y-centerTop.y;
          normal.z=centerBottom.z-centerTop.z;

          // Create two perpendicular vectors perp and q on the plane of the disk.
          perp=normal;

          if(0==normal.x && 0==normal.z)
            ++perp.x;
          else
            ++perp.y;

          q=glm::cross(perp, normal);
          perp=glm::cross(normal, q);

          q=glm::normalize(q);
          perp=glm::normalize(perp);

          cone << (Index)0xffffffff;

          for(Int i=0, j=0u; i<=precision; i++, j+=2u)
          {
            theta=theta0+i*(theta1-theta0)/precision;

            normal.x=cos(theta)*perp.x+sin(theta)*q.x;
            normal.y=cos(theta)*perp.y+sin(theta)*q.y;
            normal.z=cos(theta)*perp.z+sin(theta)*q.z;
            normal=glm::normalize(normal);

            cone << (Index)j+0;
            cone << (Index)j+1;

            cone << vector<Vertex>{
              // vertex
              centerTop.x+radiusTop*normal.x,
              centerTop.y+radiusTop*normal.y,
              centerTop.z+radiusTop*normal.z,
              // uv / color
              i/(Float)precision,
              1.0f,
              0.0f,
              1.0f,
              // normal
              normal.x,
              normal.y,
              normal.z,
              // tangent
              0.0f,
              0.0f,
              0.0f,
              // bitangent
              0.0f,
              0.0f,
              0.0f,

              // vertex
              centerBottom.x+radiusBottom*normal.x,
              centerBottom.y+radiusBottom*normal.y,
              centerBottom.z+radiusBottom*normal.z,
              // uv / color
              i/(Float)precision,
              0.0f,
              0.0f,
              1.0f,
              // normal
              normal.x,
              normal.y,
              normal.z,
              // tangent
              0.0f,
              0.0f,
              0.0f,
              // bitangent
              0.0f,
              0.0f,
              0.0f
            };
          }

          return cone;
        }
      }
    }
  }
}


//BOOST_CLASS_VERSION(xear::scene::data::Geometry<>, XEAR_SCENE_DATA_GEOMETRY_VERSION)


#endif
