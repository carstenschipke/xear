/*
 * camera.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_SCENE_CAMERA_H_
#define XEAR_SCENE_CAMERA_H_


#include <xear.h>
#include <xear/scene/node.h>

#include <x/io/keymap.h>


namespace xear
{
  namespace scene
  {
    using namespace std;

    using namespace x;
    using namespace x::io;


    enum Direction
    {
      UP,
      DOWN,
      LEFT,
      RIGHT
    };


    class Camera: public Node
    {
      public:
        Float fieldOfView=45.0f;
        Float near=0.1f;
        Float far=1000.0f;

        Float yaw=-90.0f;
        Float pitch=0.0f;

        vec3 position={0.0f, 0.0f, 3.0f};
        vec3 target={0.0f, 0.0f, 0.0f};

        vec3 direction={0.0f, 0.0f, 0.0f};
        vec3 front={0.0f, 0.0f, -1.0f};
        vec3 up={0.0f, 1.0f, 0.0f};

        vec3 vecUp={0.0f, 1.0f, 0.0f};

        mat4 projection;

        mat4 view;
        mat4 viewProjection;


        Camera(String const& name);


        Camera(Camera const&) = delete;
        Camera& operator=(Camera const&) = delete;


        virtual ~Camera();


        Float aspectRatio();
        vec4 viewport();
        void viewport(vec4 const& viewport);

        void move(Keys& keys);
        void look(Float x, Float y);
        void scroll(Float degree);

        virtual void init() override;
        virtual void update() override;

        virtual void activate() override;
        virtual void deactivate() override;


        friend std::ostream& operator<<(std::ostream& stream, Camera const& camera)
        {
          stream << "Camera{id: " << &camera
            << ", viewport: " << glm::to_string(camera.m_viewport)
            << ", aspect-ratio: " << camera.m_aspectRatio
            << ", field-of-view: " << camera.fieldOfView
            << ", yaw: " << camera.yaw
            << ", pitch: " << camera.pitch
            << ", position: " << glm::to_string(camera.position)
            << ", target: " << glm::to_string(camera.target)
            << ", front: " << glm::to_string(camera.front)
            << ", up: " << glm::to_string(camera.up)
            << ", view: " << glm::to_string(camera.view)
            << ", projection: " << glm::to_string(camera.projection)
            << "}";

          return stream;
        }


      private:
        vec4 m_viewport={0, 0, 0, 0};
        Float m_aspectRatio=4.0f/3.0f;
    };
  }
}


#endif
