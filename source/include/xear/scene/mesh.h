/*
 * mesh.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_SCENE_MESH_H_
#define XEAR_SCENE_MESH_H_


#include <xear.h>
#include <xear/scene/node.h>
#include <xear/scene/data/mesh.h>

#include <x/io/json.h>


namespace xear
{
  namespace scene
  {
    using namespace std;

    using namespace x;
    using namespace x::io;


    class Mesh: public Node
    {
      public:
        unique_ptr<data::Mesh> data;


        Mesh(String const& name);


        Mesh(Mesh const&) = delete;
        Mesh& operator=(Mesh const&) = delete;


        virtual ~Mesh();


        virtual void load(Json::Value const& value) override;

        virtual void init() override;
        virtual void update() override;
        virtual void render() override;


        friend std::ostream& operator<<(std::ostream& stream, Mesh const& mesh)
        {
          stream << "Mesh{id: " << &mesh << ", name: " << mesh.name << "}";

          return stream;
        }
    };
  }
}


#endif
