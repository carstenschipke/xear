/*
 * scene.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_SCENE_SCENE_H_
#define XEAR_SCENE_SCENE_H_


#include <xear.h>
#include <xear/common/fps.h>

#include <xear/renderer/rdi.h>

#include <xear/scene/camera.h>
#include <xear/scene/light.h>
#include <xear/scene/node.h>


namespace xear
{
  namespace scene
  {
    using namespace std;

    using namespace x;


    class Scene: public Node
    {
      public:
        scene::Camera* camera;
        renderer::Rdi* renderer;


        Scene(String const& name, renderer::Rdi* renderer);


        Scene(Scene const&) = delete;
        Scene& operator=(Scene const&) = delete;


        virtual ~Scene();


        virtual void update() override;


        void add(Camera* camera);
        void add(Light* light);

        common::Fps const& fps();


        friend std::ostream& operator<<(std::ostream& stream, Scene const& scene)
        {
          stream << "Scene{id: " << &scene
            << ", name: " << scene.name
            << ", children: " << scene.children.size()
            << "}";

          return stream;
        }


      private:
        vector<Light*> m_lights;
        vector<Camera*> m_cameras;
    };
  }
}


#endif
