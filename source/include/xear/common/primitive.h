/*
 * primitive.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_COMMON_PRIMITIVE_H_
#define XEAR_COMMON_PRIMITIVE_H_


#include <xear.h>


namespace xear
{
  namespace common
  {
    using namespace x;


    enum Primitive: Int
    {
      LINE,
      LINE_STRIP,
      LINE_LOOP,
      POINT,
      TRIANGLE,
      TRIANGLE_STRIP,
      TRIANGLE_FAN,
      DEFAULT
    };

    enum Scalar: Int
    {
      BYTE,
      CHAR,
      SHORT,
      SHORTU,
      INT,
      INTU,
      FLOAT
    };


    Primitive const INT_PRIMITIVE[]={
      Primitive::LINE,
      Primitive::LINE_STRIP,
      Primitive::LINE_LOOP,
      Primitive::POINT,
      Primitive::TRIANGLE,
      Primitive::TRIANGLE_STRIP,
      Primitive::TRIANGLE_FAN
    };

    Scalar const INT_SCALAR[]={
      Scalar::BYTE,
      Scalar::CHAR,
      Scalar::SHORT,
      Scalar::SHORTU,
      Scalar::INT,
      Scalar::INTU,
      Scalar::FLOAT
    };

    Size const SIZE_SCALAR[]={
      sizeof(Byte),
      sizeof(Char),
      sizeof(Short),
      sizeof(ShortU),
      sizeof(Int),
      sizeof(IntU),
      sizeof(Float)
    };


    String const NAME_PRIMITIVE[]={
      "LINE",
      "LINE_STRIP",
      "LINE_LOOP",
      "POINT",
      "TRIANGLE",
      "TRIANGLE_STRIP",
      "TRIANGLE_FAN"
    };

    String const NAME_SCALAR[]={
      "Byte",
      "Char",
      "Short",
      "ShortU",
      "Int",
      "IntU",
      "Float"
    };


    template<typename TypeScalar> inline Scalar toScalar() { return Scalar::BYTE; }
    template<> inline Scalar toScalar<Char>() { return Scalar::CHAR; }
    template<> inline Scalar toScalar<Short>() { return Scalar::SHORT; }
    template<> inline Scalar toScalar<ShortU>() { return Scalar::SHORTU; }
    template<> inline Scalar toScalar<Int>() { return Scalar::INT; }
    template<> inline Scalar toScalar<IntU>() { return Scalar::INTU; }
    template<> inline Scalar toScalar<Float>() { return Scalar::FLOAT; }


    inline Int toInt(Scalar scalar)
    {
      return (Int)scalar;
    }

    inline Int toInt(Primitive primitive)
    {
      return (Int)primitive;
    }

    inline Scalar toScalar(Int scalar)
    {
      return INT_SCALAR[scalar];
    }

    inline Primitive toPrimitive(Int primitive)
    {
      return INT_PRIMITIVE[primitive];
    }


    inline std::ostream& operator<<(std::ostream& stream, Primitive const& primitive)
    {
      stream << NAME_PRIMITIVE[primitive];

      return stream;
    }


    inline std::ostream& operator<<(std::ostream& stream, Scalar const& scalar)
    {
      stream << NAME_SCALAR[scalar];

      return stream;
    }
  }
}


#endif
