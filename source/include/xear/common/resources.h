/*
 * resources.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_COMMON_RESOURCES_H_
#define XEAR_COMMON_RESOURCES_H_


#include <xear.h>

#include <x/io/resources.h>
#include <x/io/resource/handler.h>


namespace xear
{
  namespace common
  {
    using namespace x;
    using namespace x::io;


    enum ResourceType
    {
      GEOMETRY,
      MARKER,
      MESH,
      SCENE,
      SCRIPT,
      SHADER,
      TEXTURE
    };


    class Resources
    {
      public:
        void registerResourceType(ResourceType const& type, String const& location)
        {
          m_locations[type]=location;
        }


        std::unique_ptr<Resource> get(ResourceType const& resourceType, String const& name)
        {
          return m_resources.get(m_locations[resourceType]+name);
        }

        std::unique_ptr<Resource> get(ResourceType const& resourceType, mime::Type const& mimeType, String const& name)
        {
          auto location=m_locations[resourceType];
          auto extension=Mimetype::extensionForType(mimeType);

          return m_resources.get(location+name+"."+extension);
        }


        void save(std::unique_ptr<Resource> const& resource)
        {

        }


        template<class T>
        T get(ResourceType const& resourceType, String const& name)
        {
          static_assert(std::is_base_of<resource::Handler, T>::value, "Expected T of type x::io::resource::Handler.");

          return {get(resourceType, name)};
        }

        template<class T>
        T get(ResourceType const& resourceType, mime::Type const& mimeType, String const& name)
        {
          static_assert(std::is_base_of<resource::Handler, T>::value, "Expected T of type x::io::resource::Handler.");

          return {get(resourceType, mimeType, name)};
        }


      private:
        io::Resources m_resources;

        std::map<ResourceType, String> m_locations;
    };
  }
}


#endif
