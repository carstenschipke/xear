/*
 * fps.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_COMMON_FPS_H_
#define XEAR_COMMON_FPS_H_


#include <chrono>

#include <xear.h>


namespace xear
{
  namespace common
  {
    using namespace std;
    using namespace std::chrono;

    using namespace x;


    class Fps
    {
      public:
        String const label;


        Fps(String const& label, Boolean print):
          label(label), m_print(print), m_fps(0), m_frame(0), m_frameDuration(0), m_frameTimestamp(system_clock::now())
        {

        }


        inline Int fps() const noexcept
        {
          return m_fps;
        }

        inline Int frame() const noexcept
        {
          return m_frame;
        }

        inline milliseconds frameDuration() const noexcept
        {
          return m_frameDuration;
        }

        inline time_point<system_clock> frameTimestamp() const noexcept
        {
          return m_frameTimestamp;
        }

        inline nanoseconds durationNs() const
        {
          return duration_cast<nanoseconds>(system_clock::now()-m_frameTimestamp);
        }

        inline milliseconds durationMs() const
        {
          return duration_cast<milliseconds>(system_clock::now()-m_frameTimestamp);
        }

        inline void update()
        {
          ++m_frame;

          time_point<system_clock> now=system_clock::now();
          m_frameDuration=duration_cast<milliseconds>(now-m_frameTimestamp);

          if(1000<=duration_cast<milliseconds>(now-m_timeInterval).count())
          {
            m_fps=m_frame;
            m_frame=0;

            m_timeInterval=now;

            if(m_print)
              sout(*this);
          }

          m_frameTimestamp=system_clock::now();
        }


        friend std::ostream& operator<<(std::ostream& stream, Fps const& fps)
        {
          stream << fps.label << " FRAME " << fps.m_fps << "/s, DURATION " << fps.m_frameDuration.count() << " ms";

          return stream;
        }


      private:
        Boolean m_print;
        time_point<system_clock> m_timeInterval;

        Int m_fps;
        Int m_frame;
        milliseconds m_frameDuration;
        time_point<system_clock> m_frameTimestamp;
    };
  }
}


#endif
