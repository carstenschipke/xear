/*
 * config.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_COMMON_DEVICE_CONFIG_H_
#define XEAR_COMMON_DEVICE_CONFIG_H_


#include <xear.h>

#include <xear/common/device/network.h>
#include <xear/common/device/screen.h>

#include <x/io/colorspace.h>


namespace xear
{
  namespace common
  {
    namespace device
    {
      using namespace std;

      using namespace x;


      struct Config
      {
        public:
          Resolution resolution;
          io::Colorspace colorspace;

          Int angle;
          Int depth;
          Float ratio;

          Orientation orientation;
          NetworkQuality networkQuality;
          String locale;

          Int targetFps;


          friend std::ostream& operator<<(std::ostream& stream, Config const& config)
          {
            stream << "Config{resolution: " << config.resolution
              << ", colorspace: " << config.colorspace
              << ", angle: " << config.angle
              << ", depth: " << config.depth
              << ", ratio: " << config.ratio
              << ", orientation: " << device::SCREEN_ORIENTATION_NAME[config.orientation]
              << ", network-quality: " << device::NETWORK_QUALITY_NAME[config.networkQuality]
              << ", locale: " << config.locale
              << ", target-fps: " << config.targetFps
              << "}";

            return stream;
          }
      };

    }
  }
}


#endif
