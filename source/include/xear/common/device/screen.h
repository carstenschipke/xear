/*
 * screen.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_COMMON_DEVICE_SCREEN_H_
#define XEAR_COMMON_DEVICE_SCREEN_H_


#include <xear.h>


#define RESOLUTION_MAX 1200


namespace xear
{
  namespace common
  {
    namespace device
    {
      using namespace std;

      using namespace x;


      enum Orientation: IntU
      {
        PORTRAIT,
        LANDSCAPE
      };


      static Orientation const SCREEN_ORIENTATION[]={
        Orientation::PORTRAIT,
        Orientation::LANDSCAPE
      };

      static String const SCREEN_ORIENTATION_NAME[]={
        "PORTRAIT",
        "LANDSCAPE"
      };


      struct Resolution
      {
        Int width;
        Int height;


        Resolution():
          width(0), height(0)
        {

        }

        Resolution(Int width, Int height):
          width(width), height(height)
        {

        }

        Resolution(Resolution const& resolution):
          width(resolution.width), height(resolution.height)
        {

        }

        Resolution& operator=(Resolution const& resolution)
        {
          width=resolution.width;
          height=resolution.height;

          return *this;
        }


        // TODO Do we need to enforce appropriate aspect ratio's & channels to use 4-byte pixel alignment (gl optimization)?
        static Resolution find(Int width, Int height)
        {
          if(RESOLUTION_MAX<std::max(width, height))
            return max(width, height);

          return Resolution{width, height};
        }

        static Resolution max(Int width, Int height)
        {
          Float ratio=(Float)width/height;

          if(width<height)
            return Resolution{static_cast<Int>(RESOLUTION_MAX/ratio), RESOLUTION_MAX};

          return Resolution{RESOLUTION_MAX, static_cast<Int>(RESOLUTION_MAX/ratio)};
        }

        friend std::ostream& operator<<(std::ostream& stream, Resolution const& resolution)
        {
          stream << "Resolution{width: " << resolution.width
            << ", height: " << resolution.height
            << "}";

          return stream;
        }
      };
    }
  }
}


#endif
