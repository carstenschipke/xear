/*
 * network.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_COMMON_DEVICE_NETWORK_H_
#define XEAR_COMMON_DEVICE_NETWORK_H_


#include <xear.h>


#define RESOLUTION_MAX 1200


namespace xear
{
  namespace common
  {
    namespace device
    {
      using namespace std;

      using namespace x;


      enum NetworkQuality: IntU
      {
        HIGH,
        MID,
        LOW
      };

      static NetworkQuality const NETWORK_QUALITY[3]={
        NetworkQuality::HIGH,
        NetworkQuality::MID,
        NetworkQuality::LOW
      };

      static String const NETWORK_QUALITY_NAME[3]={
        "HIGH",
        "MID",
        "LOW"
      };
    }
  }
}


#endif
