/*
 * controller.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_INPUT_CONTROLLER_H_
#define XEAR_INPUT_CONTROLLER_H_


#include <xear.h>

#include <xear/scene/camera.h>

#include <x/io/keymap.h>


namespace xear
{
  namespace input
  {
    using namespace scene;

    using namespace x;


    class Controller
    {
      public:
        String const name;


        Controller(String name):
          name(name)
        {

        }

        virtual ~Controller()
        {

        }


        void attach(Camera* camera)
        {
          dbgi("Attach controller [controller: " << name << ", camera: " << camera->name << "]");

          m_camera=camera;
          m_attached=true;
        }

        void detach()
        {
          dbgi("Detach controller [controller: " << name << "]");

          m_attached=false;
        }


      protected:
        Boolean m_attached=false;


        inline void onInput(io::Keys& keys)
        {
          m_camera->move(keys);
        }

        inline void onMove(Float x, Float y)
        {
          m_camera->look(x, y);
        }


      private:
        Camera* m_camera=nullptr;
    };
  }
}


#endif
