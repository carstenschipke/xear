/*
 * default.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_INPUT_CONTROLLER_DEFAULT_H_
#define XEAR_INPUT_CONTROLLER_DEFAULT_H_


#include <xear.h>

#include <xear/input/events.h>
#include <xear/input/controller.h>


namespace xear
{
  namespace input
  {
    using namespace x;


    class DefaultController: public Controller, public EventListener, public MouseListener
    {
      public:
        DefaultController(String name):
          Controller(name)
        {

        }


        virtual void onEvent(Keys& keys) override
        {
          if(!m_attached)
            return;

          onInput(keys);
        }

        virtual void move(Float x, Float y) override
        {
          onMove(x, y);
        }

        virtual Boolean moveTo(Float x, Float y, Int* resetX, Int* resetY) override
        {
          onMove(x, y);

          return false;
        }
    };
  }
}


#endif
