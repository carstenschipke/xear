/*
 * events.h
 *
 * @author carsten.schipke@gmail.com
 */
// TODO Refactor to driver/input.h (service) & driver/sdl/input.cpp..
// TODO Modular abstraction of controllers (touch, mouse, keyboard, joystick, gamepad etc.), with configurable mapping & sensitivities..
#ifndef XEAR_INPUT_EVENTS_H_
#define XEAR_INPUT_EVENTS_H_


#include <xear.h>

#include <x/io/keymap.h>

#include <SDL2/SDL.h>


namespace xear
{
  namespace input
  {
    using namespace std;

    using namespace x;


    enum Event
    {
      KEY_DOWN=SDL_EventType::SDL_KEYDOWN,
      KEY_UP=SDL_EventType::SDL_KEYUP,
      QUIT=SDL_EventType::SDL_QUIT,
      USER=SDL_EventType::SDL_USEREVENT
    };


    class EventLoopDelegate
    {
      public:
        virtual ~EventLoopDelegate()
        {

        }

        virtual void loop()
        {

        }
    };


    class EventListener
    {
      public:
        virtual ~EventListener()
        {

        }

        virtual void onEvent(io::Keys& keys)
        {

        }
    };


    class MouseListener
    {
      public:
        Boolean relative=true;


        virtual ~MouseListener()
        {

        }

        virtual void move(Float x, Float y)
        {

        }

        virtual Boolean moveTo(Float x, Float y, Int* resetX, Int* resetY)
        {
          return false;
        }
    };


    static int filterEvents(void* data, SDL_Event* event)
    {
      return SDL_EventType::SDL_JOYAXISMOTION!=event->type
        && SDL_EventType::SDL_MOUSEMOTION!=event->type
        && SDL_EventType::SDL_KEYDOWN!=event->type
        && SDL_EventType::SDL_KEYUP!=event->type;
    }


    class Events
    {
      public:
        EventLoopDelegate* delegateEventLoop=nullptr;
        EventListener* listenerEvent=nullptr;
        MouseListener* listenerMouse=nullptr;


        Events()
        {
          dbgi("Create event manager");
        }


        ~Events()
        {
          dbgi("Destroy event manager");
        }


        void listen()
        {
          dbgi("Listening for events");

          Boolean listening=true;

          if(!delegateEventLoop || !listenerMouse || !listenerEvent)
          {
            if(!delegateEventLoop)
              dbge("Missing event loop delegate.");
            if(!listenerMouse)
              dbge("Missing mouse event listener.");
            if(!listenerEvent)
              dbge("Missing event listener.");

            listening=false;
          }

          io::Keys keys{0, 1.0f, 1.0f, 1.0f};
          EventListener* eventListener=listenerEvent;
          std::function<void(void)> updateMovement=nullptr;

          if(0<SDL_NumJoysticks())
          {
            SDL_Joystick* joystick=SDL_JoystickOpen(0);

            updateMovement=[joystick, eventListener, &keys] {
              updateJoystick(joystick, eventListener, keys);
            };
          }
          else
          {
            updateMovement=[eventListener, &keys] {
              updateKeyboard(eventListener, keys);
            };
          }

          Boolean touchEnabled=0<SDL_GetNumTouchDevices();

          SDL_Event event;
          SDL_SetEventFilter(filterEvents, nullptr);

          Float orientationX=0.0f;
          Float orientationY=0.0f;

          Float motionX=0.0f;
          Float motionY=0.0f;
          Float lastMotionX=0.0f;
          Float lastMotionY=0.0f;

          Boolean motionActive=false;
          SDL_FingerID motionFinger=0;

          Int mouseX=0;
          Int mouseY=0;
          Int resetMouseX=0;
          Int resetMouseY=0;

          if(listenerMouse->relative)
            SDL_SetRelativeMouseMode(SDL_TRUE);

          while(listening)
          {
            SDL_PumpEvents();

            while(0!=SDL_PollEvent(&event))
            {
              if(SDL_EventType::SDL_QUIT==event.type)
              {
                listening=false;

                break;
              }

              if(touchEnabled)
              {
                if(SDL_EventType::SDL_FINGERMOTION==event.type)
                {
                  if(0.5f>event.mgesture.x && motionActive)
                  {
                    motionX=20.0f*event.mgesture.x;
                    motionY=10.0f*event.mgesture.y;
                  }
                }
                else if(SDL_EventType::SDL_FINGERDOWN==event.type || SDL_EventType::SDL_FINGERUP==event.type)
                {
                  if(0.5f>event.tfinger.x && SDL_EventType::SDL_FINGERDOWN)
                  {
                    motionX=lastMotionX=20.0f*event.mgesture.x;
                    motionY=lastMotionY=10.0f*event.mgesture.y;

                    motionFinger=event.tfinger.fingerId;
                    motionActive=true;
                  }
                  else if(SDL_EventType::SDL_FINGERUP && motionFinger==event.tfinger.fingerId)
                  {
                    motionFinger=0;
                    motionActive=false;
                  }
                }
              }
            }

            if(touchEnabled)
            {
              orientationX=lastMotionX>motionX?-(lastMotionX-motionX):abs(lastMotionX-motionX);
              orientationY=lastMotionY>motionY?-(lastMotionY-motionY):abs(lastMotionY-motionY);

              lastMotionX=motionX;
              lastMotionY=motionY;

              listenerMouse->move(orientationX, orientationY);
            }
            else
            {
              if(listenerMouse->relative)
              {
                SDL_GetRelativeMouseState(&mouseX, &mouseY);

                listenerMouse->move(mouseX, mouseY);
              }
              else
              {
                SDL_GetMouseState(&mouseX, &mouseY);

                if(listenerMouse->moveTo(mouseX, mouseY, &resetMouseX, &resetMouseY))
                  SDL_WarpMouseInWindow(SDL_GL_GetCurrentWindow(), resetMouseX, resetMouseY);
              }
            }

            updateMovement();

            delegateEventLoop->loop();
          }
        }


    private:
      inline static void updateKeyboard(EventListener* listenerEvent, io::Keys& keys)
      {
        Byte const* stateKeyboard=SDL_GetKeyboardState(0);

        if(stateKeyboard[SDL_SCANCODE_W])
          keys.mask|=io::Key::MOVE_FORWARD;
        else
          keys.mask&=~io::Key::MOVE_FORWARD;

        if(stateKeyboard[SDL_SCANCODE_S])
          keys.mask|=io::Key::MOVE_BACKWARD;
        else
          keys.mask&=~io::Key::MOVE_BACKWARD;

        if(stateKeyboard[SDL_SCANCODE_A])
          keys.mask|=io::Key::STRAFE_LEFT;
        else
          keys.mask&=~io::Key::STRAFE_LEFT;

        if(stateKeyboard[SDL_SCANCODE_D])
          keys.mask|=io::Key::STRAFE_RIGHT;
        else
          keys.mask&=~io::Key::STRAFE_RIGHT;

        listenerEvent->onEvent(keys);
      }

      inline static void updateJoystick(SDL_Joystick* joystick, EventListener* listenerEvent, io::Keys& keys)
      {
        Int x=SDL_JoystickGetAxis(joystick, 1);
        Int z=6000-SDL_JoystickGetAxis(joystick, 0);

        if(1500<z)
        {
          if(2000<z)
            keys.velocityZ=0.00001f*abs(z);
          else
            keys.velocityZ=0.000005f*abs(z);

          keys.mask|=io::Key::MOVE_FORWARD;
          keys.mask&=~io::Key::MOVE_BACKWARD;
        }
        else if(500<z)
        {
          keys.velocityZ=0.0f;
        }
        else
        {
          keys.velocityZ=0.01f;

          keys.mask&=~io::Key::MOVE_FORWARD;
          keys.mask|=io::Key::MOVE_BACKWARD;
        }

        if(100<x)
        {
          keys.velocityX=0.00002f*min(abs(x), 2000);

          keys.mask&=~io::Key::STRAFE_RIGHT;
          keys.mask|=io::Key::STRAFE_LEFT;
        }
        else if(-100>x)
        {
          keys.velocityX=0.00002f*min(abs(x), 2000);

          keys.mask|=io::Key::STRAFE_RIGHT;
          keys.mask&=~io::Key::STRAFE_LEFT;
        }
        else
        {
          keys.velocityX=0;
        }

        listenerEvent->onEvent(keys);
      }
    };
  }
}


#endif
