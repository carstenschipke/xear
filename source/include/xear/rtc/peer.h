/*
 * peer.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RTC_PEER_H_
#define XEAR_RTC_PEER_H_


#include <chrono>
#include <condition_variable>
#include <deque>
#include <mutex>
#include <vector>

#include <xear.h>

#include <xear/rtc/channel.h>
#include <xear/rtc/constraints.h>
#include <xear/rtc/decoder.h>
#include <xear/rtc/encoder.h>
#include <xear/rtc/supervisor.h>
#include <xear/rtc/session/description/observer.h>

#include <x/concurrent/task.h>

#include <json/json.h>

#pragma clang diagnostic push
# pragma clang diagnostic ignored "-Winconsistent-missing-override"
#
# include <webrtc/base/scoped_ref_ptr.h>
#
# include <talk/app/webrtc/jsep.h>
#
# include <talk/app/webrtc/datachannelinterface.h>
# include <talk/app/webrtc/mediastreaminterface.h>
# include <talk/app/webrtc/peerconnectioninterface.h>
# include <talk/app/webrtc/videosourceinterface.h>
#pragma clang diagnostic pop


namespace xear
{
  namespace rtc
  {
    using namespace std;
    using namespace std::chrono;

    using namespace webrtc;

    using namespace x;
    using namespace x::common;
    using namespace x::concurrent;

    using namespace xear::common;


    class RtcChannel;
    class RtcSessionDescriptionObserverSet;
    class RtcSessionDescriptionObserverCreate;


    class RtcPeer: public Task, public PeerConnectionObserver
    {
      public:
        String const id;


        RtcPeer(String id, RtcSupervisor* supervisor);


        RtcPeer(RtcPeer const&) = delete;
        RtcPeer& operator=(RtcPeer const&) = delete;


        virtual ~RtcPeer();


        String sdp();
        void sdp(SessionDescriptionInterface* description);

        String candidate();

        void queue(String message);


        virtual void OnAddStream(MediaStreamInterface* stream) override;
        virtual void OnRemoveStream(MediaStreamInterface* stream) override;
        virtual void OnSignalingChange(PeerConnectionInterface::SignalingState state) override;
        virtual void OnStateChange(PeerConnectionObserver::StateType state) override;
        virtual void OnRenegotiationNeeded() override;

        virtual void OnIceCandidate(IceCandidateInterface const* candidate) override;

        virtual void OnDataChannel(DataChannelInterface* channel) override;


      protected:
        virtual void run() override;


      private:
        Boolean m_initialized=false;

        RtcSupervisor* m_supervisor;
        RtcEventDelegate* m_delegate=nullptr;

        deque<String> m_queue;

        ::rtc::Thread* m_thread=nullptr;
        ::rtc::scoped_refptr<MediaStreamInterface> m_stream;
        ::rtc::scoped_refptr<PeerConnectionInterface> m_connection;

        unique_ptr<RtcChannel> m_channel;
        unique_ptr<RtcDecoder> m_decoder;
        unique_ptr<RtcEncoder> m_encoder;

        unique_ptr<RtcSessionDescriptionObserverCreate> m_observerSessionDescriptionCreate;
        unique_ptr<RtcSessionDescriptionObserverSet> m_observerSessionDescriptionSet;

        String m_sdp;
        mutex m_sdpMonitor;
        condition_variable m_sdpUpdated;

        vector<String> m_candidates;
        mutex m_candidatesMonitor;
        condition_variable m_candidatesUpdated;
        deque<::Json::Value> m_candidatesPending;


        void onCandidate(::Json::Value& message);
        void onOffer(::Json::Value& message);
    };
  }
}


#endif
