/*
 * constraints.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RTC_CONSTRAINTS_H_
#define XEAR_RTC_CONSTRAINTS_H_


#include <xear.h>

#pragma clang diagnostic push
# pragma clang diagnostic ignored "-Winconsistent-missing-override"
#
# include <talk/app/webrtc/mediaconstraintsinterface.h>
#pragma clang diagnostic pop


namespace xear
{
  namespace rtc
  {
    using namespace std;

    using namespace x;

    using namespace webrtc;


    class RtcConstraints: public MediaConstraintsInterface
    {
      public:
        RtcConstraints();


        RtcConstraints(RtcConstraints const&) = delete;
        RtcConstraints& operator=(RtcConstraints const&) = delete;


        virtual ~RtcConstraints();


        virtual MediaConstraintsInterface::Constraints const& GetMandatory() const override;
        virtual MediaConstraintsInterface::Constraints const& GetOptional() const override;

        void setMandatory(String key, String value);
        void setOptional(String key, String value);


      private:
        MediaConstraintsInterface::Constraints m_mandatory;
        MediaConstraintsInterface::Constraints m_optional;


        void set(MediaConstraintsInterface::Constraints& container, String key, String value);
    };
  }
}


#endif
