/*
 * decoder.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RTC_DECODER_H_
#define XEAR_RTC_DECODER_H_


#include <xear.h>

#include <xear/common/fps.h>
#include <xear/rtc/video/sink.h>


#pragma clang diagnostic push
# pragma clang diagnostic ignored "-Winconsistent-missing-override"
#
# include <talk/app/webrtc/mediastreaminterface.h>
# include <talk/media/base/videocommon.h>
# include <talk/media/base/videoframe.h>
#pragma clang diagnostic pop


namespace xear
{
  namespace rtc
  {
    using namespace cricket;
    using namespace webrtc;

    using namespace x;

    using namespace xear::common;


    class RtcDecoder: public VideoRendererInterface
    {
      public:
        RtcDecoder(RtcVideoSink* sink);


        RtcDecoder(RtcDecoder const&) = delete;
        RtcDecoder& operator=(RtcDecoder const&) = delete;


        virtual ~RtcDecoder();


        void Stop();


        virtual void SetSize(Int width, Int height) override;
        virtual void RenderFrame(VideoFrame const* frame) override;


      private:
        Fps m_fps{"rtcdec", true};
        RtcVideoSink* m_sink;
    };
  }
}


#endif
