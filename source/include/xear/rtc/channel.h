/*
 * channel.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RTC_CHANNEL_H_
#define XEAR_RTC_CHANNEL_H_


#include <xear.h>
#include <xear/rtc/peer.h>

#pragma clang diagnostic push
# pragma clang diagnostic ignored "-Winconsistent-missing-override"
#
# include <webrtc/base/scoped_ref_ptr.h>
#
# include <talk/app/webrtc/datachannelinterface.h>
# include <talk/app/webrtc/peerconnectioninterface.h>
#pragma clang diagnostic pop


namespace xear
{
  namespace rtc
  {
    using namespace x;

    using namespace webrtc;


    class RtcPeer;


    class RtcChannel: public DataChannelObserver
    {
      public:
        String const name;


        RtcChannel(String name, RtcPeer* const peer, PeerConnectionInterface* const connection);
        RtcChannel(String name, RtcPeer* const peer, PeerConnectionInterface* const connection, DataChannelInterface* const channel);


        RtcChannel(RtcChannel const&) = delete;
        RtcChannel& operator=(RtcChannel const&) = delete;


        ~RtcChannel();


        Boolean const isOpen();
        void send(String message);


        virtual void OnStateChange() override;
        virtual void OnMessage(DataBuffer const& buffer) override;


      private:
        RtcPeer* const m_peer;
        PeerConnectionInterface* const m_connection;

        Boolean m_open=false;
        DataChannelInterface* m_channel;
        ::rtc::scoped_refptr<DataChannelInterface> m_channelRef=nullptr;


        void onOpen();
        void onClose();
    };
  }
}


#endif
