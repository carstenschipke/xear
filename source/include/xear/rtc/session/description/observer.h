/*
 * observer.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RTC_SESSION_DESCRIPTION_OBSERVER_H_
#define XEAR_RTC_SESSION_DESCRIPTION_OBSERVER_H_


#include <mutex>
#include <condition_variable>

#include <xear.h>
#include <xear/rtc/peer.h>

#pragma clang diagnostic push
# pragma clang diagnostic ignored "-Winconsistent-missing-override"
#
# include <talk/app/webrtc/jsep.h>
# include <webrtc/base/refcount.h>
#pragma clang diagnostic pop


namespace xear
{
  namespace rtc
  {
    using namespace std;

    using namespace x;

    using namespace webrtc;


    class RtcPeer;


    class RtcSessionDescriptionObserverSet: public SetSessionDescriptionObserver
    {
      public:
        RtcSessionDescriptionObserverSet();
        ~RtcSessionDescriptionObserverSet();


        virtual void OnSuccess() override;
        virtual void OnFailure(String const& error) override;


        virtual Int AddRef() const override;
        virtual Int Release() const override;
    };


    class RtcSessionDescriptionObserverCreate: public CreateSessionDescriptionObserver
    {
      public:
        RtcSessionDescriptionObserverCreate(RtcPeer* const peer);
        ~RtcSessionDescriptionObserverCreate();


        virtual void OnSuccess(SessionDescriptionInterface* description) override;
        virtual void OnFailure(String const& error) override;


        virtual Int AddRef() const override;
        virtual Int Release() const override;


      private:
        RtcPeer* const m_peer;
    };
  }
}


#endif
