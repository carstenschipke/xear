/*
 * delegate.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RTC_EVENT_DELEGATE_H_
#define XEAR_RTC_EVENT_DELEGATE_H_


#include <xear.h>
#include <xear/common/device/event.h>


namespace xear
{
  namespace rtc
  {
    using namespace std;

    using namespace x;

    using namespace xear::common;


    class RtcEventDelegate
    {
      public:
        virtual ~RtcEventDelegate()
        {

        }


        virtual void init(device::Config config) = 0;
        virtual void onEvent(device::Event event) = 0;
    };
  }
}


#endif
