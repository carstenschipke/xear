/*
 * encoder.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RTC_ENCODER_H_
#define XEAR_RTC_ENCODER_H_


#include <chrono>
#include <vector>

#include <xear.h>

#include <xear/common/fps.h>
#include <xear/common/device/screen.h>

#include <xear/rtc/video/source.h>

#include <x/io/colorspace.h>
#include <x/io/image.h>

#pragma clang diagnostic push
# pragma clang diagnostic ignored "-Winconsistent-missing-override"
#
# include <talk/media/base/videocapturer.h>
# include <talk/media/base/videocommon.h>
#pragma clang diagnostic pop


namespace xear
{
  namespace rtc
  {
    using namespace cricket;
    using namespace webrtc;

    using namespace x;

    using namespace xear::common;


    class RtcEncoder: public VideoCapturer
    {
      public:
        RtcEncoder(::rtc::Thread* thread,
                   RtcVideoSource* source,
                   device::Resolution const& resolution,
                   io::Colorspace const& colorspace,
                   Int targetFps);


        RtcEncoder(RtcEncoder const&) = delete;
        RtcEncoder& operator=(RtcEncoder const&) = delete;


        virtual ~RtcEncoder();


        Boolean captureFrame();

        virtual CaptureState Start(VideoFormat const& format) override;
        virtual void Stop() override;

        virtual Boolean IsRunning() override;
        virtual Boolean IsScreencast() const override;
        virtual Boolean GetPreferredFourccs(vector<IntU>* fourccs) override;
        virtual Boolean GetBestCaptureFormat(VideoFormat const& formatDesired, VideoFormat* formatBest) override;


      private:
        Fps m_fps{"rtcenc", true};
        RtcVideoSource* m_source;
        chrono::time_point<chrono::high_resolution_clock> m_start;

        unique_ptr<io::Image> m_image;
    };
  }
}


#endif
