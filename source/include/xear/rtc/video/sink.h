/*
 * video.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RTC_VIDEO_SINK_H_
#define XEAR_RTC_VIDEO_SINK_H_


#include <xear.h>

#include <xear/common/device/config.h>

#include <x/io/image.h>


namespace xear
{
  namespace rtc
  {
    using namespace std;

    using namespace x;

    using namespace xear::common;


    class RtcVideoSink
    {
      public:
        virtual ~RtcVideoSink()
        {

        }


        virtual void init(device::Config config) = 0;
        virtual void append(unique_ptr<io::Image> frame) = 0;

        virtual device::Resolution const& resolution() = 0;
    };
  }
}


#endif
