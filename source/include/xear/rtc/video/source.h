/*
 * video.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RTC_VIDEO_SOURCE_H_
#define XEAR_RTC_VIDEO_SOURCE_H_


#include <xear.h>

#include <xear/common/device/config.h>

#include <x/io/image.h>


namespace xear
{
  namespace rtc
  {
    using namespace std;

    using namespace x;

    using namespace xear::common;


    class RtcVideoSource
    {
      public:
        virtual ~RtcVideoSource()
        {

        }


        virtual void init(device::Config config) = 0;
        virtual void fetch(io::image::Ref& frame) = 0;
    };
  }
}


#endif
