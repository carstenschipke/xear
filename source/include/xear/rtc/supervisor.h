/*
 * supervisor.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RTC_SUPERVISOR_H_
#define XEAR_RTC_SUPERVISOR_H_


#include <xear.h>

#include <xear/rtc/event/delegate.h>

#include <xear/rtc/video/sink.h>
#include <xear/rtc/video/source.h>


namespace xear
{
  namespace rtc
  {
    using namespace std;

    using namespace x;


    class RtcSupervisor
    {
      public:
        virtual ~RtcSupervisor()
        {

        }


        virtual Boolean peerKeepAlive(String peerId) = 0;
        virtual Boolean peerDisconnect(String peerId) = 0;

        virtual RtcEventDelegate* peerEventDelegate(String peerId, device::Config config) = 0;

        virtual RtcVideoSink* peerVideoSink(String peerId, device::Config config) = 0;
        virtual RtcVideoSource* peerVideoSource(String peerId, device::Config config) = 0;
    };
  }
}


#endif
