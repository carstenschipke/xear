/*
 * layouts.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RENDERER_LAYOUTS_H_
#define XEAR_RENDERER_LAYOUTS_H_


#include <map>
#include <vector>

#include <xear.h>
#include <xear/common/primitive.h>


namespace xear
{
  namespace renderer
  {
    using namespace std;

    using namespace x;

    using namespace xear::common;


    Int const ATTRIBUTE_COUNT_DEFAULT=16;


    enum AttributeType: Int
    {
      ATTRIBUTE=0,
      UNIFORM=1
    };


    class Attribute
    {
      public:
        String const name;

        AttributeType const type;
        Scalar const scalar;

        Int const count;

        Size size;
        Int length;

        Int location=-1;
        Boolean resolved=false;


        Attribute(String const& name, AttributeType type, Scalar scalar, Int count):
          name(name), type(type), scalar(scalar), count(count)
        {
          size=SIZE_SCALAR[scalar];
          length=count*(Int)size;
        }
    };


    class Layout
    {
      public:
        String const name;


        Layout(String const& name):
          name(name)
        {

        }


        Int stride()
        {
          if(-1==m_stride)
          {
            m_stride=0;

            for(Attribute* attribute : *this)
              m_stride+=attribute->length;
          }

          return m_stride;
        }

        void add(String const& name, AttributeType type, Scalar scalar, Int count)
        {
          auto attribute=make_unique<Attribute>(name, type, scalar, count);

          m_attributes.push_back(attribute.get());
          m_storage[name]=move(attribute);

          m_stride=-1;
        }

        Attribute* const get(String const& name)
        {
          auto iterator=m_storage.find(name);

          if(iterator==m_storage.end())
            return nullptr;

          return iterator->second.get();
        }

        vector<Attribute*>::iterator begin()
        {
          return m_attributes.begin();
        }

        vector<Attribute*>::iterator end()
        {
          return m_attributes.end();
        }


      private:
        Int m_stride=-1;

        vector<Attribute*> m_attributes;
        map<String, unique_ptr<Attribute>> m_storage;
    };


    class Layouts
    {
      public:
        Layout* buffer=nullptr;
        Layout* material=nullptr;


        Boolean useBufferLayout(String const& name)
        {
          if(0==strcmp(name.c_str(), m_buffer.c_str()))
            return false;

          dbgi4("Use buffer layout [name: " << name << "].");

          auto iterator=m_layoutsBuffer.find(name);

          if(iterator==m_layoutsBuffer.end())
            m_layoutsBuffer[name]=make_unique<Layout>(name);

          buffer=m_layoutsBuffer[name].get();
          m_buffer=name;

          return true;
        }

        Boolean useMaterialLayout(String const& name)
        {
          if(0==strcmp(name.c_str(), m_material.c_str()))
            return false;

          dbgi4("Use material layout [name: " << name << "].");

          auto iterator=m_layoutsMaterial.find(name);

          if(iterator==m_layoutsMaterial.end())
            m_layoutsMaterial[name]=make_unique<Layout>(name);

          material=m_layoutsMaterial[name].get();
          m_material=name;

          return true;
        }


      private:
        String m_buffer;
        String m_material;

        map<String, unique_ptr<Layout>> m_layoutsMaterial;
        map<String, unique_ptr<Layout>> m_layoutsBuffer;
    };
  }
}


#endif
