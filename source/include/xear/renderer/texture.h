/*
 * texture.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RENDERER_TEXTURE_H_
#define XEAR_RENDERER_TEXTURE_H_


#include <mutex>
#include <vector>

#include <xear.h>

#include <x/io/resource/handler.h>


namespace xear
{
  namespace renderer
  {
    using namespace std;

    using namespace x;
    using namespace x::io;

    using namespace xear::common;


    class Texture: public resource::Handler
    {
      public:
        struct Data
        {
          IntU size;

          void* bytes;
        };


      private:
        struct Header
        {
          IntU glType;
          IntU glTypeSize;
          IntU glFormat;
          IntU glInternalFormat;
          IntU glBaseInternalFormat;
          IntU pixelWidth;
          IntU pixelHeight;
          IntU pixelDepth;
          IntU numberOfArrayElements;
          IntU numberOfFaces;
          IntU numberOfMipmapLevels;
          IntU bytesOfKeyValueData;
        };


      public:
        IntU width=0u;
        IntU height=0u;
        IntU format=0u;
        IntU formatInternal=0u;

        vector<Data> mipmap;


        using resource::Handler::Handler;


        Texture(Texture&& rhs):
          resource::Handler(std::forward<Texture>(rhs))
        {
          dbgsi("MOVE CONSTRUCTION " << &rhs << " -> " << this);
        }


        Texture(Texture const&) = delete;

        Texture& operator=(Texture&&) = delete;
        Texture& operator=(Texture const&) = delete;


        virtual ~Texture()
        {
          dbgi7("Destroy [" << *this << "].");

          for(auto& current : mipmap)
            free(current.bytes);
        }


        void load()
        {
          lock_guard<decltype(m_monitor)> lock{m_monitor};

          if(!m_loaded)
          {
            Header header;

            auto& stream=resource().streamInput();

            stream.seekg(16, ios::beg);
            stream.read((Char*)&header, sizeof(header));

            format=header.glFormat;
            formatInternal=header.glInternalFormat;

            width=header.pixelWidth;
            height=header.pixelHeight;

            stream.ignore(header.bytesOfKeyValueData);

            mipmap.resize(header.numberOfMipmapLevels);

            for(auto& current : mipmap)
            {
              stream.read((Char*)&current.size, sizeof(current.size));

              current.bytes=malloc(current.size);
              stream.read((Char*)current.bytes, current.size);
            }

            m_loaded=true;
          }
        }


        friend std::ostream& operator<<(std::ostream& stream, Texture const& texture)
        {
          stream << "Texture{id: " << &texture
            << ", name: " << texture.resource().source().path()
            << ", width: " << texture.width
            << ", height: " << texture.height
            << ", format: " << texture.format
            << ", format-internal: " << texture.formatInternal
            << ", mipmap-levels: " << texture.mipmap.size()
            << "}";

          return stream;
        }


      private:
        Boolean m_loaded=false;
        mutex m_monitor;
    };
  }
}


#endif
