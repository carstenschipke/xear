/*
 * rdi.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RENDERER_RDI_H_
#define XEAR_RENDERER_RDI_H_


#include <xear.h>

#include <xear/common/fps.h>
#include <xear/common/primitive.h>
#include <xear/common/resources.h>

#include <xear/scene/data/geometry.h>

#include <xear/script/engine.h>

#include <x/io/colorspace.h>
#include <x/io/image/ref.h>


namespace xear
{
  namespace renderer
  {
    using namespace std;

    using namespace x;

    using namespace xear::common;


    enum Shape: IntU
    {
      CUBE,
      QUAD,
      BUFFER_DEBUG
    };


    enum Target: IntU
    {
      SCREEN,
      BUFFER
    };


    enum Flag: IntU
    {
      TRANSFORM=2,
      SHADE=4,
      TEXTURE=8,
      TEXTURE_BUFFER=16,
      WIREFRAME=32,
      COLOR=64,
      RED=128,
      GREEN=256,
      BLUE=512,
      DEFAULT=14
    };

    enum Cull: IntU
    {
      NONE,
      FRONT,
      BACK
    };


    class Rdi: public script::Module
    {
      public:
        Fps fps{"rdi", true};
        Resources resources;


        virtual ~Rdi()
        {

        }


        virtual void init(script::Context& context) override = 0;

        virtual void init() = 0;
        virtual void init(Target target, io::Colorspace const& colorspace, vec4 const& viewport) = 0;

        virtual void begin() = 0;

        // TODO Abstract render targets (screen, buffer) & access via configured render target (i.e. buffer).
        virtual void fetch(io::image::Ref& frame) = 0;

        virtual void clear() = 0;
        virtual void clearColor(vec4 const& color) = 0;

        virtual vec4 const& viewport() = 0;
        virtual void viewport(vec4 const& viewport) = 0;

        virtual IntU flags() = 0;
        virtual void flags(IntU flags) = 0;

        virtual void primitive(Primitive primitive) = 0;
        virtual Primitive primitive() = 0;

        virtual void cull(Cull face) = 0;

        virtual void draw() = 0;
        virtual void draw(Long offset, Int count) = 0;

        virtual void draw(String name) = 0;
        virtual void draw(String name, Long offset, Int count) = 0;

        virtual void draw(Shape shape) = 0;
        virtual void draw(Shape shape, Long offset, Int count) = 0;

        virtual void drawRange(IntU min, IntU max, Long offset, Int count) = 0;

        virtual void drawPath(String name, mat4 const& modelViewProjection) = 0;
        virtual void drawDebug(mat4 const& model, mat4 const& viewProjection) = 0;

        virtual Boolean bindUniformMatrix3f(String name, mat3 value) = 0;
        virtual Boolean bindUniformMatrix4f(String name, mat4 value) = 0;

        virtual Boolean bindUniform1i(String name, Int value) = 0;
        virtual Boolean bindUniform3f(String name, vec3 value) = 0;
        virtual Boolean bindUniform4f(String name, vec4 value) = 0;

        virtual Boolean bindLights(Float* data, Int countLights, Int countProperties) = 0;

        virtual Boolean pathCreate(String const& name, String const& path) = 0;
        virtual Boolean pathUpdate(String const& name, String const& path) = 0;

        virtual Boolean geometryUse(String name) = 0;
        virtual Boolean geometryExists(String name) = 0;
        virtual Boolean geometryCreate(String name, scene::data::Geometry<> const& data) = 0;
        virtual Boolean geometryUpdate(String name, Int offset, Int count, void const* const data) = 0;
        virtual Boolean geometryRemove(String name) = 0;

        virtual Boolean materialUse(String name) = 0;
        virtual Boolean materialExists(String name) = 0;
        virtual Boolean materialCreate(String name) = 0;
        virtual Boolean materialRemove(String name) = 0;
        virtual Boolean materialClear() = 0;

        virtual Boolean textureUse(String name, String location) = 0;
        virtual Boolean textureExists(String name) = 0;
        virtual Boolean textureCreate(String name) = 0;
        virtual Boolean textureCreate(String name, Int width, Int height, void const* const data) = 0;
        virtual Boolean textureUpdate(String name, Int width, Int height, void const* const data) = 0;
        virtual Boolean textureRemove(String name) = 0;

        virtual Boolean textureBufferExists(String name, Int width, Int height, Int bytes) = 0;
        virtual Boolean textureBufferCreate(String name, Int width, Int height, Int bytes) = 0;
        virtual Boolean textureBufferUpdate(String name, Int offsetX, Int offsetY, Int width, Int height, Int bytes, Byte const* const data) = 0;
        virtual Boolean textureBufferResize(String name, Int width, Int height) = 0;
        virtual Boolean textureBufferRemove(String name) = 0;


        // TODO Implement DeferredRenderer, ForwardRenderer etc. as RDI frontends.
        virtual Boolean deferredShading() = 0;
        virtual void deferredShadingPassOne() = 0;
        virtual void deferredShadingPassTwo() = 0;
        virtual void deferredShadingPassTwoInit() = 0;
    };
  }
}


#endif
