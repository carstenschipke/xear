/*
 * shader.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RENDERER_SHADER_H_
#define XEAR_RENDERER_SHADER_H_


#include <regex>

#include <xear.h>

#include <x/io/resource/handler.h>


namespace xear
{
  namespace renderer
  {
    using namespace std;

    using namespace x;
    using namespace x::io;

    using namespace xear::common;


    class Shader: public resource::Handler
    {
      public:
        using resource::Handler::Handler;


        Shader(Shader&& rhs):
          resource::Handler(std::forward<Shader>(rhs))
        {
          dbgsi("MOVE CONSTRUCTION " << &rhs << " -> " << this);
        }


        Shader(Shader const&) = delete;

        Shader& operator=(Shader&&) = delete;
        Shader& operator=(Shader const&) = delete;


        virtual ~Shader()
        {

        }


        String content()
        {
          auto& stream=resource().streamInput();
          auto pos=stream.tellg();

          stream.seekg(0, ios::end);
          auto size=stream.tellg();

          stream.seekg(0, ios::beg);

          String content;
          content.reserve((Size)size);
          content.assign(
            istreambuf_iterator<char>(stream),
            istreambuf_iterator<char>()
          );

          stream.seekg(pos, ios::beg);

          return content;
        }

        String layout()
        {
          auto subject=this->content();

          smatch match;
          regex pattern{"#define layout (.*)"};

          if(regex_search(subject, match, pattern))
            return match.str().replace(0, 15, "");

          return "";
        }


        friend std::ostream& operator<<(std::ostream& stream, Shader const& shader)
        {
          stream << "Shader{id: " << &shader
            << ", name: " << shader.resource().source().path()
            << ", type: " << shader.resource().source().mimetype()
            << "}";

          return stream;
        }


      private:
        Boolean m_open=false;
    };
  }
}


#endif
