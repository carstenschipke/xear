/*
 * service.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_RENDERER_SERVICE_H_
#define XEAR_RENDERER_SERVICE_H_


#include <xear.h>
#include <xear/renderer/rdi.h>

#include <x/common/service.h>


namespace xear
{
  namespace renderer
  {
    using namespace std;

    using namespace x;


    class Service: public x::common::Service
    {
      public:
        virtual ~Service()
        {

        }


        virtual void setup() override = 0;
        virtual void teardown() override = 0;


        virtual Rdi& get(String const& name) = 0;
        virtual void destroy(String const& name) = 0;
    };
  }
}


#endif
