/*
 * gl.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_DRIVER_GL_H_
#define XEAR_DRIVER_GL_H_


#include <map>
#include <stdio.h>
#include <vector>

#include <xear.h>
#include <xear/common/primitive.h>

#include <x/io/mimetype.h>


#ifdef XEAR_GLES
# ifdef __APPLE__
#   include <OpenGLES/ES3/gl.h>
# else
#   include <GLES3/gl31.h>
# endif
#else
# ifdef __APPLE__
#   include <OpenGL/gl3.h>
#   include <OpenGL/gl3ext.h>
# else
#   include <GL/gl.h>
# endif
#endif


#ifdef DEBUG
# define DEVICE_CHECK_ERROR(message) { std::stringstream s; s << message; xear::driver::gl::checkError(s, __FILE__, __LINE__); }
#else
# define DEVICE_CHECK_ERROR(message)
#endif


namespace xear
{
  namespace driver
  {
    namespace gl
    {
      using namespace std;

      using namespace x;
      using namespace x::io;

      using namespace xear::common;


      struct Version
      {
        Int major;
        Int minor;
        Int glsl;

        String name;
        String string;
      };


      enum API
      {
        OPENGL,
        OPENGLES
      };

      enum CullFace: IntU
      {
        NONE,
        FRONT,
        BACK
      };

      enum BufferTarget: IntU
      {
        ARRAY,
        ARRAY_ELEMENT,
        FRAMEBUFFER,
        FRAMEBUFFER_READ,
        FRAMEBUFFER_DRAW,
        PIXEL_UNPACK,
        RENDERBUFFER,
        TEXTURE
      };

      enum BufferDataUsage: IntU
      {
        STATIC_COPY,
        STATIC_DRAW,
        STATIC_READ,
        STREAM_COPY,
        STREAM_DRAW,
        STREAM_READ,
        DYNAMIC_COPY,
        DYNAMIC_DRAW,
        DYNAMIC_READ
      };

      enum BufferAccess: IntU
      {
        READ,
        READ_WRITE,
        WRITE,
        DRAW
      };

      enum RenderbufferAttachment: IntU
      {
        DEPTH24_STENCIL8
      };

      enum FramebufferAttachment: IntU
      {
        COLOR_ATTACHMENT,
        DEPTH_ATTACHMENT,
        DEPTH_STENCIL_ATTACHMENT,
        STENCIL_ATTACHMENT
      };

      enum TextureType: IntU
      {
        TEXTURE_2D,
        TEXTURE_2D_ARRAY,
        TEXTURE_3D,
        TEXTURE_CUBEMAP,
        TEXTURE_2D_MULTISAMPLE,
        TEXTURE_2D_MULTISAMPLE_ARRAY,
        TEXTURE_BUFFER,
        TEXTURE_CUBEMAP_ARRAY,
        TEXTURE_RECTANGLE
      };

      enum DataType: IntU
      {
        BYTE,
        UNSIGNED_BYTE,
        SHORT,
        UNSIGNED_SHORT,
        INT,
        UNSIGNED_INT,
        FLOAT,
        FLOAT_HALF
      };

      enum TextureFilter: IntU
      {
        NEAREST,
        LINEAR,
        NEAREST_MIPMAP_NEAREST,
        NEAREST_MIPMAP_LINEAR,
        LINEAR_MIPMAP_NEAREST,
        LINEAR_MIPMAP_LINEAR
      };

      enum TextureFormat: IntU
      {
        BGR,
        BGRA,
        RGB,
        RGBA
      };


#     ifdef XEAR_GLES
        Int const GLSL_ES[4][2]={
          {100, 100},
          {100, 100},
          {100, 100},
          {300, 310}
        };
#     else
        Int const GLSL[5][6]={
          {100, 100, 100, 100, 100, 100},
          {100, 100, 100, 100, 100, 100},
          {110, 120, 120, 120, 120, 120},
          {130, 140, 150, 330, 330, 330},
          {400, 410, 420, 430, 440, 450}
        };
#     endif

      String const API_NAME[]={
        "OpenGL",
        "OpenGL ES"
      };

      String const GL_ERROR[]={
        "GL_INVALID_ENUM",
        "GL_INVALID_VALUE",
        "GL_INVALID_OPERATION",
        "GL_STACK_OVERFLOW",
        "GL_STACK_UNDERFLOW",
        "GL_OUT_OF_MEMORY"
      };

      IntU const PRIMITIVE[]={
        GL_LINES,
        GL_LINE_STRIP,
        GL_LINE_LOOP,
        GL_POINTS,
        GL_TRIANGLES,
        GL_TRIANGLE_STRIP,
        GL_TRIANGLE_FAN
      };

      IntU const PRIMITIVE_TYPE_SCALAR[]={
        GL_BYTE,
        GL_UNSIGNED_BYTE,
        GL_UNSIGNED_SHORT,
        GL_UNSIGNED_SHORT,
        GL_UNSIGNED_INT,
        GL_UNSIGNED_INT,
        GL_FLOAT
      };

      Size const PRIMITIVE_SIZE_SCALAR[]={
        sizeof(Byte),
        sizeof(Char),
        sizeof(Short),
        sizeof(ShortU),
        sizeof(Int),
        sizeof(IntU),
        sizeof(Float)
      };

      IntU const BUFFER_TARGET[]={
        GL_ARRAY_BUFFER,
        GL_ELEMENT_ARRAY_BUFFER,
        GL_FRAMEBUFFER,
        GL_READ_FRAMEBUFFER,
        GL_DRAW_FRAMEBUFFER,
        GL_PIXEL_UNPACK_BUFFER,
        GL_RENDERBUFFER,
#       if defined(GL_TEXTURE_BUFFER)
          GL_TEXTURE_BUFFER
#       else
          0u
#       endif
      };

      IntU const BUFFER_ACCESS[]={
        GL_MAP_READ_BIT,
        GL_MAP_READ_BIT|GL_MAP_WRITE_BIT,
        GL_MAP_WRITE_BIT
      };

      IntU const BUFFER_DATA_USAGE[]={
        GL_STATIC_COPY,
        GL_STATIC_DRAW,
        GL_STATIC_READ,
        GL_STREAM_COPY,
        GL_STREAM_DRAW,
        GL_STREAM_READ,
        GL_DYNAMIC_COPY,
        GL_DYNAMIC_DRAW,
        GL_DYNAMIC_READ
      };

      IntU const RENDERBUFFER_ATTACHMENT[]={
        GL_DEPTH24_STENCIL8
      };

      IntU const FRAMEBUFFER_ATTACHMENT[]={
        GL_COLOR_ATTACHMENT0,
        GL_DEPTH_ATTACHMENT,
        GL_DEPTH_STENCIL_ATTACHMENT,
        GL_STENCIL_ATTACHMENT
      };

#     ifndef GL_COMPUTE_SHADER
#       define GL_COMPUTE_SHADER 0u
#     endif
#     ifndef GL_GEOMETRY_SHADER
#       define GL_GEOMETRY_SHADER 0u
#     endif

      std::map<mime::Type, IntU> const SHADER_TYPE{
        {mime::Type::TEXT_GLSL_COMPUTE_SHADER, GL_COMPUTE_SHADER},
        {mime::Type::TEXT_GLSL_FRAGMENT_SHADER, GL_FRAGMENT_SHADER},
        {mime::Type::TEXT_GLSL_GEOMETRY_SHADER, GL_GEOMETRY_SHADER},
        {mime::Type::TEXT_GLSL_VERTEX_SHADER, GL_VERTEX_SHADER},
      };

      IntU const TEXTURE_TYPE[]={
        GL_TEXTURE_2D,
        GL_TEXTURE_2D_ARRAY,
        GL_TEXTURE_3D,
        GL_TEXTURE_CUBE_MAP,
#       ifdef XEAR_GLES
          0u,
          0u,
          0u,
          0u,
          0u
#       else
          GL_TEXTURE_2D_MULTISAMPLE,
          GL_TEXTURE_2D_MULTISAMPLE_ARRAY,
          GL_TEXTURE_BUFFER,
          GL_TEXTURE_CUBE_MAP_ARRAY,
          GL_TEXTURE_RECTANGLE
#       endif
      };

      IntU const DATA_TYPE[]={
        GL_BYTE,
        GL_UNSIGNED_BYTE,
        GL_SHORT,
        GL_UNSIGNED_SHORT,
        GL_INT,
        GL_UNSIGNED_INT,
        GL_FLOAT,
        GL_HALF_FLOAT
      };

      IntU const TEXTURE_FILTER[]={
        GL_NEAREST,
        GL_LINEAR,
        GL_NEAREST_MIPMAP_NEAREST,
        GL_NEAREST_MIPMAP_LINEAR,
        GL_LINEAR_MIPMAP_NEAREST,
        GL_LINEAR_MIPMAP_LINEAR
      };

      IntU const TEXTURE_FORMAT[]={
// FIXME Separate drivers per platform.
#       ifdef XEAR_GLES
          GL_RGB,
          GL_RGBA,
#       else
          GL_BGRA,
          GL_BGRA,
#       endif
        GL_RGB,
        GL_RGBA
      };


      inline void checkError(stringstream& message, String sourceFile, Int sourceLine)
      {
        auto error=glGetError();

        if(GL_NO_ERROR!=error)
          dbgse(message.str() << " [file: " << sourceFile << ", line: " << sourceLine << ", error: " << GL_ERROR[error-1280] << "].");
      }


      static Version* VERSION_GL;


      inline String apiName()
      {
#       ifdef XEAR_GLES
          return API_NAME[API::OPENGLES];
#       else
          return API_NAME[API::OPENGL];
#       endif
      }

      inline Version const& versionApi()
      {
        if(!VERSION_GL)
        {
          VERSION_GL=new Version();

          glGetIntegerv(GL_MAJOR_VERSION, &VERSION_GL->major);
          glGetIntegerv(GL_MINOR_VERSION, &VERSION_GL->minor);

          VERSION_GL->name=apiName();
          VERSION_GL->string={to_string(VERSION_GL->major)+"."+to_string(VERSION_GL->minor)};

          // FIXME Parse GL_SHADING_LANGUAGE_VERSION (format seems implementation specific).
#         ifdef XEAR_GLES
            auto major=min(3, VERSION_GL->major);
            auto minor=min(1, VERSION_GL->minor);

            VERSION_GL->glsl=GLSL_ES[major][minor];
#         else
            auto major=min(4, VERSION_GL->major);
            auto minor=min(5, VERSION_GL->minor);

            VERSION_GL->glsl=GLSL[major][minor];
#         endif
        }

        return *VERSION_GL;
      }

      inline Int versionApiConfig()
      {
        return XEAR_GL_VERSION_MAJOR;
      }

      inline Int versionApiMajor()
      {
        return versionApi().major;
      }

      inline void init()
      {
        dbgsi2("Initialize.");

        glPixelStorei(GL_PACK_ALIGNMENT, 4);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

        glEnable(GL_BLEND);
        DEVICE_CHECK_ERROR("Unable to enable blending");

        glEnable(GL_DEPTH_TEST);
        DEVICE_CHECK_ERROR("Unable to enable depth test");

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        DEVICE_CHECK_ERROR("Unable to configure blending");

        glDepthFunc(GL_LEQUAL);
        DEVICE_CHECK_ERROR("Unable to configure depth test");

#       ifdef XEAR_GLES
          glClearDepthf(1.0f);
#       else
          glClearDepth(1.0f);
#       endif
        DEVICE_CHECK_ERROR("Unable to configure clear depth");

        glClearStencil(0);
        DEVICE_CHECK_ERROR("Unable to configure clear stencil");

        glStencilMask(0xffffffff);
        DEVICE_CHECK_ERROR("Unable to configure stencil mask");

        glEnable(GL_STENCIL_TEST);
        DEVICE_CHECK_ERROR("Unable to enable stencil test");

        glFrontFace(GL_CCW);
        DEVICE_CHECK_ERROR("Unable to configure winding direction");

#       ifdef GL_LINE_SMOOTH
          glEnable(GL_LINE_SMOOTH);
          DEVICE_CHECK_ERROR("Unable to enable GL_LINE_SMOOTH");
#       endif
#       ifdef GL_POINT_SMOOTH
          glEnable(GL_POINT_SMOOTH);
          DEVICE_CHECK_ERROR("Unable to enable GL_POINT_SMOOTH");
#       endif
#       ifdef GL_MULTISAMPLE
          glEnable(GL_MULTISAMPLE);
          DEVICE_CHECK_ERROR("Unable to enable GL_MULTISAMPLE");
#       endif

#       ifdef GL_FRAMEBUFFER_SRGB
          // TODO Integrate GL_ARB_framebuffer_sRGB, GL_EXT_texture_sRGB & compile shaders accordingly (w/o manual linearization & gamma correction, proper attenuation).
          glEnable(GL_FRAMEBUFFER_SRGB);
#       endif
      }

      inline void clear(IntU mask=0)
      {
        if(0==mask)
          glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
        else
          glClear(mask);

        DEVICE_CHECK_ERROR("Unable to clear");
      }

      inline void bufferCreate(String name, IntU count, IntU* const buffers)
      {
        glGenBuffers(count, buffers);

        DEVICE_CHECK_ERROR("Unable to bind buffer(s) [name: " << name << ", count: " << count << "].");
      }

      inline void bufferBind(String name, BufferTarget target, IntU buffer)
      {
        glBindBuffer(BUFFER_TARGET[target], buffer);

        DEVICE_CHECK_ERROR("Unable to bind buffer(s) [name: " << name << ", target: " << target << ", buffer: " << buffer << "].");
      }

      inline void bufferDelete(String name, IntU count, IntU const* const buffers)
      {
        glDeleteBuffers(count, buffers);

        DEVICE_CHECK_ERROR("Unable to delete buffer(s) [name: " << name << ", count: " << count << "].");
      }

      inline void* const bufferMap(String name, BufferTarget target, Int offset, Size size, BufferAccess access)
      {
        auto buffer=glMapBufferRange(BUFFER_TARGET[target], offset, size, BUFFER_ACCESS[access]);

        DEVICE_CHECK_ERROR("Unable to map buffer [name: " << name << ", target: " << target << ", access: " << access << "].");

        return buffer;
      }

      inline void bufferUnmap(String name, BufferTarget target)
      {
        glUnmapBuffer(BUFFER_TARGET[target]);

        DEVICE_CHECK_ERROR("Unable to unmap buffer [name: " << name << ", target: " << target << "].");
      }

      inline void bufferData(String name, BufferTarget target, Int size, void const* const data, BufferDataUsage usage)
      {
        glBufferData(BUFFER_TARGET[target], size, data, BUFFER_DATA_USAGE[usage]);

        DEVICE_CHECK_ERROR("Unable to upload buffer data [name: " << name << ", target: " << target << ", size: " << size << "].");
      }

      inline void bufferDataSub(String name, BufferTarget target, Int offset, Int size, void const* const data)
      {
        glBufferSubData(BUFFER_TARGET[target], offset, size, data);

        DEVICE_CHECK_ERROR("Unable to upload buffer data range [name: " << name << ", target: " << target << ", size: " << size << "].");
      }

      // FIXME Neccessary?
      inline void pixelPackBufferInit(String name, Int width, Int height, Size size, IntU* const buffer, IntU& bufferCurrent)
      {
        dbgsi3("Setup PIXEL_PACK_BUFFER [name: " << name << ", width: " << width << ", height: " << height << ", size: " << size << "].");

        // FIXME Keep initial 4-byte alignment.
        glPixelStorei(GL_PACK_ALIGNMENT, 1);
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

        bufferCurrent=0;

        glBindBuffer(GL_PIXEL_PACK_BUFFER, buffer[0]);
        glBufferData(GL_PIXEL_PACK_BUFFER, size, nullptr, GL_STREAM_READ);
        DEVICE_CHECK_ERROR("Unable to bind pixel buffer [name: " << name << ", index: 0]");

        glBindBuffer(GL_PIXEL_PACK_BUFFER, buffer[1]);
        glBufferData(GL_PIXEL_PACK_BUFFER, size, nullptr, GL_STREAM_READ);
        DEVICE_CHECK_ERROR("Unable to bind pixel buffer [name: " << name << ", index: 1]");

        glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
        DEVICE_CHECK_ERROR("Unable to unbind pixel buffers [name: " << name << "]");
      }

      inline void pixelPackBufferSwap(String name, Int offsetX, Int offsetY, Int width, Int height, Size size, Byte* const data, IntU* const buffer, IntU& bufferCurrent)
      {
        dbgsi4("Read PIXEL_PACK_BUFFER [name: " << name << ", woffset-x: " << offsetX << ", offset-y: " << offsetY << ", width: " << width << ", height: " << height << ", size: " << size << "].");

        auto bufferRead=bufferCurrent;
        bufferCurrent=0==bufferCurrent?1:0;

        glBindBuffer(GL_PIXEL_PACK_BUFFER, buffer[bufferRead]);
        DEVICE_CHECK_ERROR("Unable to bind pixel pack buffer [name: " << name << ", index: 0]");

        glReadPixels(offsetX, offsetY, width, height, GL_RGB, GL_UNSIGNED_BYTE, nullptr);
        DEVICE_CHECK_ERROR("Unable to read pixel pack buffer [name: " << name << ", index: 0]");

        glBindBuffer(GL_PIXEL_PACK_BUFFER, buffer[bufferCurrent]);
        DEVICE_CHECK_ERROR("Unable to bind pixel pack buffer [name: " << name << ", index: 1]");

        Byte* bufferLocal=(Byte*)glMapBufferRange(GL_PIXEL_PACK_BUFFER, 0, size, GL_MAP_READ_BIT);
        DEVICE_CHECK_ERROR("Unable to map pixel pack buffer [name: " << name << ", index: 1]");

        if(bufferLocal)
        {
          dbgsi4("Copy from PIXEL_PACK_BUFFER [name: " << name << ", size: " << size << "].");

          memcpy(data, bufferLocal, size);
        }
        else
        {
          memset(data, 0x00, size);
        }

        glUnmapBuffer(GL_PIXEL_PACK_BUFFER);
        DEVICE_CHECK_ERROR("Unable to unmap pixel pack buffer [name: " << name << ", index: 1]");

        glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);
        DEVICE_CHECK_ERROR("Unable to reset pixel pack buffer [name: " << name << "]");
      }

      inline void renderbufferCreate(String name, IntU count, IntU* const buffers)
      {
        glGenRenderbuffers(count, buffers);

        DEVICE_CHECK_ERROR("Unable to generate renderbuffer(s) [name: " << name << ", count: " << count << "]");
      }

      inline void renderbufferBind(String name, BufferTarget target, IntU buffer)
      {
        glBindRenderbuffer(BUFFER_TARGET[target], buffer);

        DEVICE_CHECK_ERROR("Unable to bind renderbuffer [name: " << name << ", target: " << target << ", buffer: " << buffer << "]");
      }

      inline void renderbufferStorage(String name, BufferTarget target, RenderbufferAttachment attachment, Int width, Int height)
      {
        glRenderbufferStorage(BUFFER_TARGET[target], RENDERBUFFER_ATTACHMENT[attachment], width, height);

        DEVICE_CHECK_ERROR("Unable to initialize renderbuffer storage [name: " << name << ", target: " << target << ", attachment: " << attachment << ", width: " << width << ", height: " << height << "]");
      }

      inline void framebufferCreate(String name, IntU count, IntU* const buffers)
      {
        glGenFramebuffers(count, buffers);

        DEVICE_CHECK_ERROR("Unable to generate framebuffer(s) [name: " << name << ", count: " << count << "]");
      }

      inline void framebufferBind(String name, BufferTarget target, IntU buffer)
      {
        glBindFramebuffer(BUFFER_TARGET[target], buffer);

        DEVICE_CHECK_ERROR("Unable to bind framebuffer [name: " << name << ", target: " << target << ", buffer: " << buffer << "]");
      }

      inline void framebufferRenderbuffer(String name, BufferTarget targetFramebuffer, BufferTarget targetRenderbuffer, FramebufferAttachment attachment, IntU buffer)
      {
        glFramebufferRenderbuffer(BUFFER_TARGET[targetFramebuffer], FRAMEBUFFER_ATTACHMENT[attachment], BUFFER_TARGET[targetRenderbuffer], buffer);

        DEVICE_CHECK_ERROR("Unable to bind framebuffer renderbuffer [name: " << name << ", target-framebuffer: " << targetRenderbuffer << ", target-renderbuffer: " << targetRenderbuffer << ", attachment: " << attachment << ", buffer: " << buffer << "]");
      }

      inline void framebufferTexture2d(String name, BufferTarget target, FramebufferAttachment attachment, IntU offsetAttachment, TextureType type, IntU texture)
      {
        glFramebufferTexture2D(BUFFER_TARGET[target], FRAMEBUFFER_ATTACHMENT[attachment]+offsetAttachment, TEXTURE_TYPE[type], texture, 0);

        DEVICE_CHECK_ERROR("Unable to bind framebuffer [name: " << name << ", target: " << target << ", attachment: " << attachment << ", offset-attachment: " << offsetAttachment << ", type: " << type << ", texture: " << texture << "]");
      }

      inline Boolean framebufferStatus(String name, BufferTarget target)
      {
        IntU status=glCheckFramebufferStatus(BUFFER_TARGET[target]);

        if(GL_FRAMEBUFFER_COMPLETE==status)
          return true;

        dbgse("Framebuffer incomplete [name: " << name << ", target: " << target << ", status: " << status << "].");

        return false;
      }

      inline IntU programCreate(String name, vector<IntU> const& shaders)
      {
        auto program=glCreateProgram();

        for(auto shader : shaders)
        {
          glAttachShader(program, shader);
          glDeleteShader(shader);
        }

        glLinkProgram(program);

        auto status=0;
        glGetProgramiv(program, GL_LINK_STATUS, &status);

        if(status)
          return program;

        auto logMessageLength=0;
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logMessageLength);

        Char logMessage[logMessageLength];
        glGetProgramInfoLog(program, logMessageLength, nullptr, &logMessage[0]);

        dbgse("Unable to link program [name: " << name << ", error: " << logMessage << "].");

        glDeleteProgram(program);

        return -1;
      }

      inline void programUse(String name, IntU program)
      {
        glUseProgram(program);

        DEVICE_CHECK_ERROR("Unable to use program [name: " << name << ", program: " << program << "].");
      }

      inline void programDelete(String name, IntU program)
      {
        glDeleteProgram(program);

        DEVICE_CHECK_ERROR("Unable to delete program(s) [name: " << name << ", program: " << program << "].");
      }

      inline IntU shaderCreate(String name, mime::Type type)
      {
        auto shaderType=SHADER_TYPE.find(type);

        if(SHADER_TYPE.end()==shaderType)
        {
          dbgse("Unable to create shader of requested / unsupported type [name: " << name << ", type: " << to_string(type) << "].");

          return -1;
        }

        auto shader=glCreateShader(shaderType->second);

        DEVICE_CHECK_ERROR("Unable to create shader [name: " << name << ", type: " << type << "].");

        return shader;
      }

      inline void shaderSource(String name, IntU shader, IntU count, String source)
      {
        auto glsl="#version "+to_string(versionApi().glsl);

#       ifdef XEAR_GLES
          glsl.append(" es");
#       endif

        glsl.append("\n");
        glsl.append(source);

        auto length=(Int)glsl.length();
        auto cglsl=glsl.c_str();

        dbgsi4("Load shader source name: [" << name << ", shader: " << shader << ", source: " << glsl << "].");

        glShaderSource(shader, count, &cglsl, &length);

        DEVICE_CHECK_ERROR("Unable to load shader source [name: " << name << ", shader: " << shader << ", source: " << glsl << "].");
      }

      inline Boolean shaderCompile(String name, IntU shader)
      {
        glCompileShader(shader);

        auto status=0;
        glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

        if(status)
          return true;

        auto logMessageLength=0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logMessageLength);

        Char logMessage[logMessageLength];
        glGetShaderInfoLog(shader, logMessageLength, nullptr, &logMessage[0]);

        dbgse("Unable to compile shader [name: " << name << ", error: " << logMessage << "].");

        glDeleteShader(shader);

        return false;
      }

      inline void textureCreate(String name, IntU count, IntU* const textures)
      {
        glGenTextures(count, textures);

        DEVICE_CHECK_ERROR("Unable to generate texture(s) [name: " << name << ", count: " << count << "].");
      }

      inline void textureDelete(String name, IntU count, IntU* const textures)
      {
        glDeleteTextures(count, textures);

        DEVICE_CHECK_ERROR("Unable to delete texture(s) [name: " << name << ", count: " << count << "].");
      }

      inline void textureActive(String name, IntU texture)
      {
        glActiveTexture(GL_TEXTURE0+texture);

        DEVICE_CHECK_ERROR("Unable to activate texture [name: " << name << ", texture: " << texture << "].");
      }

      inline void textureBind(String name, TextureType type, IntU texture)
      {
        glBindTexture(TEXTURE_TYPE[type], texture);

        DEVICE_CHECK_ERROR("Unable to bind texture [name: " << name << ", type: " << type << ", texture: " << texture << "].");
      }

      inline void textureBuffer(String name, TextureType type, TextureFormat format, IntU buffer)
      {
#       ifdef XEAR_GLES
          dbgse("Method not supported [textureBuffer].");
#       else
          glTexBuffer(TEXTURE_TYPE[type], TEXTURE_FORMAT[format], buffer);

          DEVICE_CHECK_ERROR("Unable to bind texture buffer [name: " << name << ", buffer: " << buffer << ", format: " << format << "].");
#       endif
      }

      inline void textureImage2dCompressed(String name, TextureType type, Int level, IntU internalFormat, Int width, Int height, Int border, Int size, void const* const bytes)
      {
        glCompressedTexImage2D(TEXTURE_TYPE[type], level, internalFormat, width, height, border, size, bytes);

        DEVICE_CHECK_ERROR("Unable to upload compressed texture [name: " << name << ", type: " << type << ", mipmap-level: " << level << ", internal-format: " << internalFormat << ", width: " << width << ", height: " << height << ", size: " << size << "].");
      }

      inline void textureImage2d(String name, TextureType type, Int level, TextureFormat internalFormat, Int width, Int height, Int border, TextureFormat format, DataType dataType, void const* const bytes)
      {
        glTexImage2D(TEXTURE_TYPE[type], level, TEXTURE_FORMAT[internalFormat], width, height, border, TEXTURE_FORMAT[format], DATA_TYPE[dataType], bytes);

        DEVICE_CHECK_ERROR("Unable to upload texture [name: " << name << ", type: " << type << ", mipmap-level: " << level << ", internal-format: " << internalFormat << ", width: " << width << ", height: " << height << ", format: " << format << "].");
      }

      inline void textureImage2dSub(String name, TextureType type, Int level, Int offsetX, Int offsetY, Int width, Int height, TextureFormat internalFormat, DataType dataType, void const* const bytes)
      {
        glTexSubImage2D(TEXTURE_TYPE[type], level, offsetX, offsetY, width, height, TEXTURE_FORMAT[internalFormat], DATA_TYPE[dataType], bytes);

        DEVICE_CHECK_ERROR("Unable to upload sub texture [name: " << name << ", type: " << type << ", mipmap-level: " << level << ", internal-format: " << internalFormat << ", width: " << width << ", height: " << height << ", offset-x: " << offsetX << ", offset-y: " << offsetY << "].");
      }

      inline void textureFilter(String name, TextureType type, TextureFilter minification, TextureFilter magnification)
      {
        glTexParameteri(TEXTURE_TYPE[type], GL_TEXTURE_MIN_FILTER, TEXTURE_FILTER[minification]);

        DEVICE_CHECK_ERROR("Unable to bind texture minification filter [name: " << name << ", type: " << type << ", filter: " << minification << "].");

        glTexParameteri(TEXTURE_TYPE[type], GL_TEXTURE_MAG_FILTER, TEXTURE_FILTER[magnification]);

        DEVICE_CHECK_ERROR("Unable to bind texture magnification filter [name: " << name << ", type: " << type << ", filter: " << magnification << "].");
      }

      inline void textureParameteri(String name, TextureType type, IntU parameter, Int value)
      {
        glTexParameteri(TEXTURE_TYPE[type], parameter, value);

        DEVICE_CHECK_ERROR("Unable to bind texture parameter [name: " << name << ", type: " << type << ", parameter: " << parameter << ", value: " << value << "].");
      }

      inline void textureParametersDefault(String name, TextureType type, Int levelMin, Int levelMax, TextureFilter minification, TextureFilter magnification)
      {
        textureParameteri(name, type, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        textureParameteri(name, type, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        textureParameteri(name, type, GL_TEXTURE_BASE_LEVEL, levelMin);
        textureParameteri(name, type, GL_TEXTURE_MAX_LEVEL, levelMax);

        textureFilter(name, type, minification, magnification);
      }

      inline void mipmapCreate(String name, TextureType type)
      {
        glGenerateMipmap(TEXTURE_TYPE[type]);

        DEVICE_CHECK_ERROR("Unable to generate mipmap [name: " << name << ", type: " << type << "].");
      }

      inline void uniform1i(String name, Int location, Int value)
      {
        glUniform1i(location, value);

        DEVICE_CHECK_ERROR("Unable to bind uniform [name: " << name << ", type: int, location: " << location << ", value: " << value << "].");
      }

      inline void uniform3f(String name, Int location, vec3 value)
      {
        glUniform3f(location, value.x, value.z, value.z);

        DEVICE_CHECK_ERROR("Unable to bind uniform [name: " << name << ", type: int, location: " << location << ", value: " << glm::to_string(value) << "].");
      }

      inline void uniform4f(String name, Int location, vec4 value)
      {
        glUniform4f(location, value.x, value.z, value.z, value.w);

        DEVICE_CHECK_ERROR("Unable to bind uniform [name: " << name << ", type: int, location: " << location << ", value: " << glm::to_string(value) << "].");
      }

      inline void uniformMatrix3fv(String name, Int location, Int count, mat3 value)
      {
        glUniformMatrix3fv(location, count, GL_FALSE, glm::value_ptr(value));

        DEVICE_CHECK_ERROR("Unable to bind uniform [name: " << name << ", type: float[3][3], location: " << location << ", value: " << glm::to_string(value) << "].");
      }

      inline void uniformMatrix4fv(String name, Int location, Int count, mat4 value)
      {
        glUniformMatrix4fv(location, count, GL_FALSE, glm::value_ptr(value));

        DEVICE_CHECK_ERROR("Unable to bind uniform [name: " << name << ", type: float[4][4], location: " << location << ", value: " << glm::to_string(value) << "].");
      }

      inline void textureBufferResize(String name, Int width, Int height)
      {
        dbgsi4("Resize texture buffer [name: " << name << ", width: " << width << ", height: " << height << "].");

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
      }

      inline void textureBufferCreate(String name, Int width, Int height, Int bytes, IntU* const unpackBuffer)
      {
        dbgsi3("Create texture buffer [name: " << name << ", width: " << width << ", height: " << height << ", bytes: " << bytes << "].");

        auto size=sizeof(Byte)*bytes;

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
        DEVICE_CHECK_ERROR("Unable to initialize texture buffer texture [0].");

        textureParametersDefault(name, TextureType::TEXTURE_2D, 0, 0, TextureFilter::LINEAR, TextureFilter::LINEAR);
        DEVICE_CHECK_ERROR("Unable to initialize texture buffer texture parameters [0].");

        glGenBuffers(2, unpackBuffer);
        DEVICE_CHECK_ERROR("Unable to generate pixel unpack buffers");

        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, unpackBuffer[0]);
        DEVICE_CHECK_ERROR("Unable to bind pixel unpack buffer [0].");

        glBufferData(GL_PIXEL_UNPACK_BUFFER, size, 0, GL_STREAM_DRAW);
        DEVICE_CHECK_ERROR("Unable to discard pixel unpack buffer data [0].");

        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, unpackBuffer[1]);
        DEVICE_CHECK_ERROR("Unable to bind pixel unpack buffer [1].");

        glBufferData(GL_PIXEL_UNPACK_BUFFER, size, 0, GL_STREAM_DRAW);
        DEVICE_CHECK_ERROR("Unable to discard pixel unpack buffer data [1].");

        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
        DEVICE_CHECK_ERROR("Unable to unbind texture buffer [0].");
      }

      inline void textureBufferUpdate(String name, Int offsetX, Int offsetY, Int width, Int height, Int bytes, Byte const* const data, IntU* const unpackBuffer, IntU& unpackBufferCurrent)
      {
        dbgsi4("Update texture buffer [name: " << name << ", width: " << width << ", height: " << height << ", bytes: " << bytes << "].");

        auto unpackBufferWrite=unpackBufferCurrent;
        unpackBufferCurrent=0==unpackBufferCurrent?1:0;

        auto size=sizeof(Byte)*bytes;

        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, unpackBuffer[unpackBufferCurrent]);
        DEVICE_CHECK_ERROR("Unable to bind pixel unpack buffer [" << unpackBufferCurrent << "].");

        glTexSubImage2D(GL_TEXTURE_2D, 0, offsetX, offsetY, width, height, GL_RGB, GL_UNSIGNED_BYTE, 0);
        DEVICE_CHECK_ERROR("Unable to copy pixel unpack buffer to texture [" << unpackBufferCurrent << "].");

        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, unpackBuffer[unpackBufferWrite]);
        DEVICE_CHECK_ERROR("Unable to bind pixel unpack buffer [" << unpackBufferWrite << "].");

        glBufferData(GL_PIXEL_UNPACK_BUFFER, size, 0, GL_STREAM_DRAW);
        DEVICE_CHECK_ERROR("Unable to discard pixel unpack buffer data [" << unpackBufferWrite << "].");

        auto buffer=(Byte*)glMapBufferRange(GL_PIXEL_UNPACK_BUFFER, 0, size, GL_MAP_WRITE_BIT|GL_MAP_INVALIDATE_BUFFER_BIT);
        DEVICE_CHECK_ERROR("Unable to map pixel unpack buffer [" << unpackBufferWrite << "].");

        if(buffer)
        {
          memcpy(buffer, data, size);

          glUnmapBuffer(GL_PIXEL_UNPACK_BUFFER);
        }

        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
      }

      inline void clearColor(vec4 color)
      {
        glClearColor(color.r, color.g, color.b, color.a);

        DEVICE_CHECK_ERROR("Unable to set clear color [color: " << glm::to_string(color) << "].");
      }

      inline void viewport(vec4 viewport)
      {
        glViewport(viewport.x, viewport.y, viewport.z, viewport.w);

        DEVICE_CHECK_ERROR("Unable to set viewport [viewport: " << glm::to_string(viewport) << "].");
      }

      inline void enable(IntU mask)
      {
        glEnable(mask);

        DEVICE_CHECK_ERROR("Unable to enable feature [mask: " << mask << "].");
      }

      inline void disable(IntU mask)
      {
        glDisable(mask);

        DEVICE_CHECK_ERROR("Unable to disable feature [mask: " << mask << "].");
      }

      inline void cullFace(CullFace mode)
      {
        if(CullFace::NONE==mode)
        {
          glDisable(GL_CULL_FACE);
        }
        else
        {
          glEnable(GL_CULL_FACE);

          if(CullFace::BACK==mode)
            glCullFace(GL_BACK);
          else
            glCullFace(GL_FRONT);
        }

        DEVICE_CHECK_ERROR("Unable to change culling [mode: " << mode << "].");
      }

      inline void enablePrimitiveRestart()
      {
#       if defined(GL_PRIMITIVE_RESTART_FIXED_INDEX)
          glEnable(GL_PRIMITIVE_RESTART_FIXED_INDEX);
#       else
          glPrimitiveRestartIndex(0xffffffff);
#       endif

        DEVICE_CHECK_ERROR("Unable to enable primitive restart");
      }

      inline IntU primitive(IntU primitive)
      {
        return PRIMITIVE[primitive];
      }

      inline IntU primitive(Primitive primitive)
      {
        return PRIMITIVE[primitive];
      }

      inline IntU primitiveTypeScalar(IntU primitive)
      {
        return PRIMITIVE_TYPE_SCALAR[primitive];
      }

      inline IntU primitiveTypeScalar(Primitive primitive)
      {
        return PRIMITIVE_TYPE_SCALAR[primitive];
      }

      inline Size scalarSize(IntU primitive)
      {
        return PRIMITIVE_SIZE_SCALAR[primitive];
      }

      inline Size scalarSize(Primitive primitive)
      {
        return PRIMITIVE_SIZE_SCALAR[primitive];
      }

      inline Int attributeLocation(IntU program, Char const* const attributeName)
      {
        auto location=glGetAttribLocation(program, attributeName);

        DEVICE_CHECK_ERROR("Unable to resolve attribute location [name: " << attributeName << ", program: " << program << "].");

        return location;
      }

      inline Int uniformLocation(IntU program, Char const* const uniformName)
      {
        auto location=glGetUniformLocation(program, uniformName);

        DEVICE_CHECK_ERROR("Unable to resolve uniform location [name: " << uniformName << ", program: " << program << "].");

        return location;
      }

      inline void vertexAttributePointer(String attributeName, IntU attributeLocation, Int attributeCount, Scalar attributeTypeScalar, Int attributeStride, Long offset)
      {
        glVertexAttribPointer(attributeLocation,
                              attributeCount,
                              PRIMITIVE_TYPE_SCALAR[attributeTypeScalar],
                              GL_FALSE,
                              attributeStride,
                              (void*)offset);

        DEVICE_CHECK_ERROR("Unable to bind vertex attribute pointer [name: " << attributeName << ", location: " << attributeLocation << "].");
      }

      inline void vertexAttributeArrayEnable(String attributeName, IntU attributeLocation)
      {
        glEnableVertexAttribArray(attributeLocation);

        DEVICE_CHECK_ERROR("Unable to enable vertex attribute array [name: " << attributeName << ", location: " << attributeLocation << "].");
      }

      inline void elementsDraw(IntU primitiveType, Int count, Scalar primitiveTypeScalar, Long offset)
      {
        glDrawElements(primitiveType, count, PRIMITIVE_TYPE_SCALAR[primitiveTypeScalar], (void*)(offset*sizeof(IntU)));

        DEVICE_CHECK_ERROR("Unable to draw element(s) [primitiveType: " << primitiveType << ", scalar: " << primitiveTypeScalar << ", offset: " << offset << ", count: " << count << "].");
      }

      inline void elementsDrawRange(IntU primitiveType, IntU start, IntU end, Int count, Scalar primitiveTypeScalar, Long offset)
      {
        glDrawRangeElements(primitiveType, start, end, count, PRIMITIVE_TYPE_SCALAR[primitiveTypeScalar], (void*)(offset*sizeof(IntU)));

        DEVICE_CHECK_ERROR("Unable to draw element(s) range [primitiveType: " << primitiveType << ", scalar: " << primitiveTypeScalar << ", offset: " << offset << ", count: " << count << ", start: " << start << ", end: " << end << "].");
      }
    }
  }
}


#endif
