/*
 * surface.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_DRIVER_SURFACE_H_
#define XEAR_DRIVER_SURFACE_H_


#include <xear.h>

#include <x/common/service.h>


namespace xear
{
  namespace driver
  {
    using namespace std;

    using namespace x;


    class Surface: public x::common::Service
    {
      public:
        virtual ~Surface()
        {

        }


        virtual void setup() override = 0;
        virtual void teardown() override = 0;


        // TODO More explicit API for on- & off-screen initialization.
        virtual Boolean create(String name, vec4 const& viewport, Boolean fullscreen=false) = 0;
        virtual Boolean createContext(String name) = 0;
        virtual Boolean createBuffer(String name, vec4 const& viewport) = 0;

        virtual void destroy(String name) = 0;
        virtual void destroyContext(String name) = 0;
        virtual void destroyBuffer(String name) = 0;

        virtual void use(String name) = 0;
        virtual void sync(String name) = 0;
        virtual void flush(String name) = 0;

        virtual vec4 const& viewport(String name) = 0;
    };
  }
}


#endif
