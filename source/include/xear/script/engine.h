/*
 * engine.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef XEAR_SCRIPT_ENGINE_H_
#define XEAR_SCRIPT_ENGINE_H_


#include <xear.h>

#include <x/common/service.h>


namespace xear
{
  namespace script
  {
    using namespace std;

    using namespace x;
    using namespace x::common;


    class Context;
    class Module;
    class Engine;


    class Module: public Service
    {
      public:
        virtual ~Module()
        {

        }


        virtual void init(script::Context& context) = 0;
    };


    class Context
    {
      public:
        virtual ~Context()
        {

        }


        virtual void load(String path) = 0;
        virtual void compile() = 0;

        virtual void add(Module* module) = 0;
        virtual void add(String nameModule) = 0;

        virtual void call(String name) = 0;

        // TODO Want an oo api with fluent interface... thats really just prototyping.
        virtual void bind0(String name, function<void(Context&)> callback) = 0;
        virtual void bind1(String name, function<void(Context&, Any&)> callback) = 0;
        virtual void bind2(String name, function<void(Context&, Any&, Any&)> callback) = 0;
    };


    class Engine: public x::common::Service
    {
      public:
        virtual ~Engine()
        {

        }


        virtual void setup() override = 0;
        virtual void teardown() override = 0;


        virtual Context* create(String name) = 0;
        virtual void destroy(String name) = 0;
    };
  }
}


#endif
