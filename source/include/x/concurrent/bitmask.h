/*
 * bitmask.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_CONCURRENT_BITMASK_H_
#define X_CONCURRENT_BITMASK_H_


#include <atomic>
#include <chrono>
#include <condition_variable>
#include <limits>
#include <mutex>
#include <vector>

#include <x/common.h>


namespace x
{
  namespace concurrent
  {
    using namespace std;
    using namespace std::chrono;


    template<typename IntegralType>
    class Bitmask
    {
      public:
        Bitmask():
          m_mask{static_cast<IntegralType>(0)}
        {

        }

        Bitmask(IntegralType mask):
          m_mask{mask}
        {

        }


        ~Bitmask()
        {
          m_mask=static_cast<IntegralType>(0);
        }


        IntegralType mask()
        {
          return m_mask;
        }

        vector<IntegralType> bits()
        {
          vector<IntegralType> bits;

          auto mask=m_mask;
          auto bit=numeric_limits<IntegralType>::max();

          while(1<=bit)
          {
            if(-1<(mask-bit))
            {
              mask-=bit;
              bits.push_back(bit);
            }

            bit/=2;
          }

          return bits;
        }

        IntegralType set(IntegralType bit)
        {
          while(true)
          {
            auto mask=m_mask.load();

            if(m_mask.compare_exchange_strong(mask, mask|bit))
            {
              m_updated.notify_all();

              return mask;
            }
          }
        }

        Boolean isset(IntegralType bit)
        {
          return 0<(m_mask&bit);
        }

        IntegralType unset(IntegralType bit)
        {
          while(true)
          {
            auto mask=m_mask.load();

            if(m_mask.compare_exchange_strong(mask, mask&~bit))
            {
              m_updated.notify_all();

              return mask;
            }
          }
        }

        IntegralType reset(IntegralType mask)
        {
          while(true)
          {
            auto old=m_mask.load();

            if(m_mask.compare_exchange_strong(old, mask))
            {
              m_updated.notify_all();

              return old;
            }
          }
        }

        IntegralType all()
        {
          while(true)
          {
            auto mask=m_mask.load();

            if(m_mask.compare_exchange_strong(mask, numeric_limits<IntegralType>::max()))
            {
              m_updated.notify_all();

              return mask;
            }
          }
        }

        IntegralType clear()
        {
          while(true)
          {
            auto mask=m_mask.load();

            if(m_mask.compare_exchange_strong(mask, static_cast<IntegralType>(0)))
            {
              m_updated.notify_all();

              return mask;
            }
          }
        }

        IntegralType invert()
        {
          while(true)
          {
            auto mask=m_mask.load();

            if(m_mask.compare_exchange_strong(mask, ~mask))
            {
              m_updated.notify_all();

              return mask;
            }
          }
        }


        Boolean set_if_isset(IntegralType mask)
        {
          while(true)
          {
            auto old=m_mask.load();

            if(1>(old&mask))
              return false;

            if(m_mask.compare_exchange_strong(old, old|mask))
            {
              m_updated.notify_all();

              return true;
            }
          }
        }

        Boolean set_if_unset(IntegralType mask)
        {
          while(true)
          {
            auto old=m_mask.load();

            if(0<(old&mask))
              return false;

            if(m_mask.compare_exchange_strong(old, old|mask))
            {
              m_updated.notify_all();

              return true;
            }
          }
        }

        Boolean set_if_unset(IntegralType mask, IntegralType expected)
        {
          while(true)
          {
            auto old=m_mask.load();

            if(0<(old&expected))
              return false;

            if(m_mask.compare_exchange_strong(old, old|mask))
            {
              m_updated.notify_all();

              return true;
            }
          }
        }

        Boolean reset_if_unset(IntegralType mask)
        {
          while(true)
          {
            auto old=m_mask.load();

            if(0<(old&mask))
              return false;

            if(m_mask.compare_exchange_strong(old, mask))
            {
              m_updated.notify_all();

              return true;
            }
          }
        }

        Boolean reset_if_unset(IntegralType mask, IntegralType expected)
        {
          while(true)
          {
            auto old=m_mask.load();

            if(0<(old&expected))
              return false;

            if(m_mask.compare_exchange_strong(old, mask))
            {
              m_updated.notify_all();

              return true;
            }
          }
        }

        Boolean reset_to_first_match(IntegralType masks...)
        {
          vector<IntegralType> vec{masks};

          auto size=vec.size();

          for(auto mask : vec)
          {
            --size;

            while(true)
            {
              auto old=m_mask.load();

              if(0<(old&mask) || 1>size)
              {
                if(m_mask.compare_exchange_strong(old, mask))
                {
                  m_updated.notify_all();

                  return true;
                }
              }
            }
          }

          return false;
        }


        void wait()
        {
          unique_lock<decltype(m_monitor)> lock{m_monitor};

          m_updated.wait(lock);
        }

        void wait(IntegralType mask)
        {
          while(1>(m_mask&mask))
            wait();
        }

        Boolean wait(microseconds duration)
        {
          unique_lock<decltype(m_monitor)> lock{m_monitor};

          return cv_status::no_timeout==m_updated.wait_for(lock, duration);
        }

        Boolean wait(microseconds duration, IntegralType mask)
        {
          auto start=system_clock::now();

          if(wait(duration))
          {
            if(0<(m_mask&mask))
              return true;

            duration-=duration_cast<decltype(duration)>(system_clock::now()-start);

            if(0<duration.count())
              return wait(duration, mask);

            return false;
          }

          return 0<(m_mask&mask);
        }


      private:
        atomic<IntegralType> m_mask;

        mutex m_monitor;
        condition_variable m_updated;
    };


    using Bitmask8 =Bitmask<Byte >;
    using Bitmask16=Bitmask<Int16>;
    using Bitmask32=Bitmask<Int32>;
    using Bitmask64=Bitmask<Int64>;
  }
}


#endif
