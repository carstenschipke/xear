/*
 * queue.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_CONCURRENT_QUEUE_H_
#define X_CONCURRENT_QUEUE_H_


#include <atomic>
#include <chrono>
#include <future>
#include <queue>

#include <x/common.h>


namespace x
{
  namespace concurrent
  {
    using namespace std;
    using namespace std::chrono;


    template<typename Item, typename IntegralType=Long, IntegralType CAPACITY=0>
    class Queue
    {
      public:
        Queue():
          m_queue(), m_size(0)
        {

        }


        Queue(Queue const&) = delete;
        Queue& operator=(Queue const&) = delete;


        ~Queue()
        {

        }


        void offer(Item e)
        {
          lock_guard<mutex> lock{m_queueMonitor};

          if(0<CAPACITY && CAPACITY==m_size)
          {
            m_queue.pop();
            m_size.fetch_sub(1);
          }

          m_queue.push(move(e));
          m_size.fetch_add(1);

          m_queueUpdated.notify_one();
        }

        Item take()
        {
          unique_lock<mutex> lock{m_queueMonitor};

          m_queueUpdated.wait(lock, [&] {
            return !m_queue.empty();
          });

          Item head=move(m_queue.front());
          m_queue.pop();

          m_size.fetch_sub(1);

          return move(head);
        }

        Item take(microseconds duration)
        {
          unique_lock<mutex> lock{m_queueMonitor};

          if(!m_queueUpdated.wait_for(lock, duration, [&] { return !m_queue.empty(); }))
            return nullptr;

          Item head=move(m_queue.front());
          m_queue.pop();

          m_size.fetch_sub(1);

          return head;
        }

        Int size() noexcept
        {
          return m_size;
        }

        Boolean full() noexcept
        {
          return 0<CAPACITY && CAPACITY==m_size;
        }

        Boolean empty() noexcept
        {
          return 0==m_size;
        }

        void clear()
        {
          unique_lock<mutex> lock{m_queueMonitor};

          while(0<m_queue.size())
            m_queue.pop();

          m_size=0;

          m_queueUpdated.notify_all();
        }


      private:
        queue<Item> m_queue;
        atomic<IntegralType> m_size;

        mutex m_queueMonitor;
        condition_variable m_queueUpdated;
    };


    template<typename Item, typename IntegralType=Long, IntegralType CAPACITY=0>
    class QueueUniquePtr: public Queue<unique_ptr<Item>, IntegralType, CAPACITY>{};
  }
}


#endif
