/*
 * task.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_CONCURRENT_TASK_H_
#define X_CONCURRENT_TASK_H_


#include <atomic>
#include <chrono>
#include <future>
#include <thread>

#include <x/common.h>
#include <x/concurrent/bitmask.h>


namespace x
{
  namespace concurrent
  {
    using namespace std;
    using namespace std::chrono;

    using namespace x::common;


    enum State: Int16
    {
      INITIALIZED=0,
      STARTED=2,
      RUNNING=4,
      COMPLETED=8,
      INTERRUPTED=16,
      FAILED=32
    };


    template<typename ReturnType=Void, Boolean ThrowExceptions=true>
    class Worker
    {
      public:
        Worker():
          Worker("Worker")
        {

        }

        Worker(String name):
          m_name(name), m_state{State::INITIALIZED}
        {

        }


        virtual ~Worker()
        {
          if(m_promise)
            delete m_promise;
        }


        Boolean start()
        {
          lock_guard<decltype(m_threadMonitor)> lock{m_threadMonitor};

          if(0<(m_state&State::RUNNING))
            return false;

          m_promise=new promise<ReturnType>();
          m_future=m_promise->get_future();

          m_thread=new thread([this] {

            m_state=State::STARTED|State::RUNNING;
            m_stateUpdated.notify_all();

            try
            {
              m_id=this_thread::get_id();

              m_promise->set_value(call());

              m_state=State::STARTED|State::COMPLETED;
            }
            catch(std::exception const& e)
            {
              m_exception=current_exception();
              m_exceptionMessage=String{e.what()};

              m_promise->set_exception(current_exception());

              m_state=State::STARTED|State::INTERRUPTED|State::FAILED;
            }
            catch(Char const* e)
            {
              m_exception=current_exception();
              m_exceptionMessage=String{e};

              m_promise->set_exception(current_exception());

              m_state=State::STARTED|State::INTERRUPTED|State::FAILED;
            }

            m_stateUpdated.notify_all();
          });

#         ifdef _GLIBCXX_HAS_GTHREADS
            if(!m_name.empty())
              pthread_setname_np(native(), m_name.c_str());
#         endif

          // Necessary to ensure launch!?
          m_future.wait_for(microseconds{1});

          return true;
        }

        thread::id id()
        {
          return m_id;
        }

        String name()
        {
          return m_name;
        }

        Boolean current()
        {
          return m_id==this_thread::get_id();
        }

        ReturnType get()
        {
          try
          {
            return m_future.get();
          }
          catch(std::exception const& e)
          {
            if(ThrowExceptions)
              rethrow_exception(current_exception());
          }
          catch(Char const* e)
          {
            if(ThrowExceptions)
              rethrow_exception(current_exception());
          }

          return ReturnType{};
        }

        void wait()
        {
          wait(State::COMPLETED);
        }

        void wait(State state)
        {
          unique_lock<decltype(m_stateMonitor)> lock{m_stateMonitor};

          while(1>(m_state&state))
            m_stateUpdated.wait(lock);
        }

        Boolean wait(microseconds duration)
        {
          return wait(duration, State::COMPLETED);
        }

        Boolean wait(microseconds duration, State state)
        {
          unique_lock<decltype(m_stateMonitor)> lock{m_stateMonitor};

          auto start=system_clock::now();

          auto status=m_stateUpdated.wait_for(lock, duration, [this, &state] {
            return 0<(m_state&state);
          });

          if(0<(m_state&state))
            return true;

          if(status)
            return false;

          duration-duration_cast<decltype(duration)>(system_clock::now()-start);

          return wait(duration, state);
        }


        /**
         * Always returns true as soon as the task has been started for
         * the first time.
         */
        Boolean started()
        {
          return 0<(m_state&State::STARTED);
        }

        Boolean running()
        {
          return 0<(m_state&State::RUNNING);
        }

        Boolean completed()
        {
          return 0<(m_state&State::COMPLETED);
        }

        Boolean failed()
        {
          return 0<(m_state&State::FAILED);
        }

        Boolean interrupted()
        {
          return 0<(m_state&State::INTERRUPTED);
        }

        void interrupt()
        {
          m_state|=State::INTERRUPTED;

          m_stateUpdated.notify_all();
        }

        String exception()
        {
          if(0<(m_state&State::FAILED))
            return m_exceptionMessage;

          return "";
        }

        void exceptionRethrow()
        {
          if(0<(m_state&State::FAILED))
            rethrow_exception(m_exception);
        }

        void reset()
        {
          lock_guard<decltype(m_threadMonitor)> lock{m_threadMonitor};

          if(0<(m_state&State::RUNNING))
            throw "Can not reset running worker.";

          if(m_promise)
            delete m_promise;

          m_state=State::INITIALIZED;
        }


      protected:
        virtual ReturnType call() = 0;


        void name(String name)
        {
          m_name=name;
        }

        void sleep(microseconds duration)
        {
          if(0<(m_state&State::RUNNING))
          {
            if(!current())
              throw "Can not put foreign worker to sleep.";

            this_thread::sleep_for(duration);
          }
        }

        thread::native_handle_type native()
        {
          return m_thread->native_handle();
        }


      private:
        atomic<thread::id> m_id;
        String m_name;

        atomic<Int16> m_state;
        condition_variable m_stateUpdated;
        mutex m_stateMonitor;

        thread* m_thread=nullptr;
        promise<ReturnType>* m_promise=nullptr;
        future<ReturnType> m_future;
        mutex m_threadMonitor;

        exception_ptr m_exception;
        String m_exceptionMessage;
    };


    template<Boolean ThrowsException=true>
    class AbstractTask: public Worker<Void, ThrowsException>
    {
      public:
        AbstractTask(String name):
          Worker<Void, ThrowsException>(name)
        {

        }

        virtual ~AbstractTask()
        {

        }


      protected:
        virtual void run() = 0;


        virtual Void call() override
        {
          run();

          return Void{};
        }
    };


    class Task: public AbstractTask<true>
    {
      public:
        Task():
          AbstractTask("Task")
        {

        }

        Task(String name):
          AbstractTask(name)
        {

        }


        virtual ~Task()
        {

        }
    };


    class BackgroundTask: public AbstractTask<false>
    {
      public:
        BackgroundTask():
          AbstractTask("BackgroundTask")
        {

        }

        BackgroundTask(String name):
          AbstractTask(name)
        {

        }


        virtual ~BackgroundTask()
        {

        }
    };
  }
}


#endif
