/*
 * listener.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_WEB_REST_LISTENER_H_
#define X_WEB_REST_LISTENER_H_


#include <chrono>

#include <cpprest/http_listener.h>
#include <cpprest/uri.h>

#include <x/common.h>

#include <x/web/rest/resource.h>
#include <x/web/rest/resources.h>


#ifndef X_WEB_REST_SERVER
# define X_WEB_REST_SERVER "x.dev"
#endif


namespace x
{
  namespace web
  {
    namespace rest
    {
      using namespace std;
      using namespace std::chrono;

      using namespace utility;
      using namespace web;
      using namespace web::http;
      using namespace web::http::experimental::listener;


      class Listener
      {
      public:
        String const scheme;
        String const host;
        Int const port;
        String const baseUri;


        Listener(Resources* const resources, String scheme, String host, Int port, String baseUri):
          scheme(scheme), host(host), port(port), baseUri(baseUri), m_resources(resources)
        {
          uri_builder builder;
          builder.set_host(host);
          builder.set_port(port);
          builder.set_scheme(scheme);
          builder.append_path(baseUri);

          http_listener_config config;

          // XXX CDT bug 379684 User defined literals are highlighted as syntax errors.
          // config.set_timeout(120s)
          config.set_timeout(seconds{120});

          m_uri=builder.to_string();
          m_listener=make_unique<http_listener>(builder.to_uri(), config);

          for(auto& method : {methods::GET, methods::POST, methods::PUT, methods::DEL})
          {
            m_listener->support(method, [&](http_request request){
              dispatch(request);
            });
          }
        }


        void open()
        {
          cout << "Open listener at " << m_uri << endl;

          m_listener->open().wait();
        }

        void close()
        {
          cout << "Close listener at " << m_uri << endl;

          m_listener->close().wait();
        }


      private:
        String const m_uri;
        Resources* const m_resources;
        unique_ptr<http_listener> const m_listener;


        void dispatch(http_request const& request)
        {
          dbgi("DISPATCH " << request.relative_uri().to_string());

          http_response response;
          response.headers().add("Server", X_WEB_REST_SERVER);

          m_resources->dispatch(request, response);

          request.reply(response);
        }
      };
    }
  }
}


#endif
