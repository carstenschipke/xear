/*
 * resource.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_WEB_REST_RESOURCE_H_
#define X_WEB_REST_RESOURCE_H_


#include <regex>

#include <cpprest/http_msg.h>

#include <x/common.h>

#include <x/web/http/method.h>
#include <x/web/rest/serializable.h>


namespace x
{
  namespace web
  {
    namespace rest
    {
      using namespace std;

      using namespace web;
      using namespace web::http;


      class Resource
      {
      public:
        void resource(String name, http::Method method, function<unique_ptr<Serializable>(http_request const&)> resourceMethod)
        {
          m_methods[name]=http::METHODS[method];
          m_resourceMethods[name]=resourceMethod;
        }

        void scriptlet(String name, http::Method method, function<void(http_request const&, http_response&)> scriptletMethod)
        {
          m_methods[name]=http::METHODS[method];
          m_scriptletMethods[name]=scriptletMethod;
        }


        String match(http_request const& request)
        {
          auto path=String{request.relative_uri().path()};
          auto method=String{request.method()};

          for(auto& entry : m_methods)
          {
            if(0==strcmp(entry.second.c_str(), method.c_str()))
            {
              if(0==strcmp(entry.first.c_str(), path.c_str()))
                return entry.first;

              regex search{entry.first};

              if(regex_match(path, search))
                return entry.first;
            }
          }

          return "";
        }

        Boolean dispatch(String name, http_request const& request, http_response& response)
        {
          auto iteratorScriptletMethods=m_scriptletMethods.find(name);

          if(iteratorScriptletMethods!=m_scriptletMethods.end())
          {
            function<void(http_request const&, http_response&)> func=
              iteratorScriptletMethods->second;

            func(request, response);

            response.set_status_code(200);

            return true;
          }

          auto iteratorResourceMethods=m_resourceMethods.find(name);

          if(iteratorResourceMethods==m_resourceMethods.end())
            return false;

          function<unique_ptr<Serializable>(http_request const&)> func=
            iteratorResourceMethods->second;

          unique_ptr<Serializable> entity=func(request);

          response.headers().add("Content-Type", "application/json;charset=UTF-8");
          response.set_body(entity->serialize());
          response.set_status_code(200);

          return true;
        }


      private:
        map<String, String> m_methods;
        map<String, function<void(http_request const&, http_response&)>> m_scriptletMethods;
        map<String, function<unique_ptr<Serializable>(http_request const&)>> m_resourceMethods;
      };
    }
  }
}


#endif
