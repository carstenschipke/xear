/*
 * resources.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_WEB_REST_RESOURCES_H_
#define X_WEB_REST_RESOURCES_H_


#include <vector>

#include <cpprest/http_msg.h>

#include <x/common.h>

#include <x/web/http/method.h>

#include <x/web/rest/filter.h>
#include <x/web/rest/resource.h>


namespace x
{
  namespace web
  {
    namespace rest
    {
      using namespace std;

      using namespace web;
      using namespace web::http;


      struct FilterHolder
      {
        String pattern;
        unique_ptr<Filter> filter;
      };


      class Resources
      {
      public:
        void dispatch(http_request request, http_response response)
        {
          dbgi("Dispatch [" << request.relative_uri().to_string() << "]");

          response.set_status_code(500);

          Boolean found=false;

          auto path=request.relative_uri().path();

          String resourceMethod;
          vector<Filter*> filters;

          for(auto& holder : m_filters)
          {
            regex search{holder.pattern};

            if(regex_match(path, search))
              filters.push_back(holder.filter.get());
          }

          for(auto& resource : m_resources)
          {
            resourceMethod=resource->match(request);

            if(0<resourceMethod.size())
            {
              found=true;

              if(0<filters.size())
                FilterChain{filters, resource.get(), resourceMethod}.next(request, response);
              else
                resource->dispatch(resourceMethod, request, response);

              break;
            }
          }

          if(!found)
            response.set_status_code(404);
        }

        void add(unique_ptr<Resource> resource)
        {
          m_resources.push_back(move(resource));
        }

        void filter(String pattern, unique_ptr<Filter> filter)
        {
          m_filters.push_back({pattern, move(filter)});
        }


      private:
        vector<unique_ptr<Resource>> m_resources;
        vector<FilterHolder> m_filters;
      };
    }
  }
}


#endif
