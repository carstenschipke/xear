/*
 * serializable.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_WEB_REST_SERIALIZABLE_H_
#define X_WEB_REST_SERIALIZABLE_H_


#include <cpprest/json.h>

#include <x/common.h>


namespace x
{
  namespace web
  {
    namespace rest
    {
      using namespace std;

      using namespace web;


      class Serializable
      {
      public:
        virtual ~Serializable()
        {

        }


        virtual json::value serialize()
        {
          return json::value::null();
        }

        virtual void deserialize(json::value value)
        {

        }


      protected:
        json::value serialize(String value)
        {
          return json::value::string(value);
        }

        json::value serialize(Boolean value)
        {
          return json::value::boolean(value);
        }

        json::value serialize(Byte value)
        {
          return json::value::number(value);
        }

        json::value serialize(Char value)
        {
          return json::value::number(value);
        }

        json::value serialize(Short value)
        {
          return json::value::number(value);
        }

        json::value serialize(ShortU value)
        {
          return json::value::number(value);
        }

        json::value serialize(Int value)
        {
          return json::value::number(value);
        }

        json::value serialize(IntU value)
        {
          return json::value::number((Double)value);
        }

        json::value serialize(Long value)
        {
          return json::value::number((Double)value);
        }

        json::value serialize(LongU value)
        {
          return json::value::number((Double)value);
        }

        json::value serialize(Float value)
        {
          return json::value::number(value);
        }

        json::value serialize(Double value)
        {
          return json::value::number(value);
        }
        
        json::value serialize(Serializable value)
        {
          return value.serialize();
        }
      };
    }
  }
}


#endif
