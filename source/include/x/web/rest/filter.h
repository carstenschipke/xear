/*
 * filter.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_WEB_REST_FILTER_H_
#define X_WEB_REST_FILTER_H_


#include <regex>
#include <vector>

#include <cpprest/http_msg.h>

#include <x/common.h>

#include <x/web/http/method.h>
#include <x/web/rest/resource.h>


namespace x
{
  namespace web
  {
    namespace rest
    {
      using namespace std;

      using namespace web;
      using namespace web::http;


      class FilterChain;


      class Filter
      {
      public:
        virtual ~Filter()
        {

        }


        virtual void filter(FilterChain& chain, http_request const& request, http_response& response) = 0;
      };


      class FilterChain
      {
      public:
        FilterChain(vector<Filter* const> filters, Resource* const resource, String resourceMethod):
          m_filters(filters), m_resource(resource), m_resourceMethod(resourceMethod)
        {

        }

        void next(http_request const& request, http_response& response)
        {
          if(m_index<m_filters.size())
            m_filters[m_index++]->filter(*this, request, response);
          else
            m_resource->dispatch(m_resourceMethod, request, response);
        }


      private:
        vector<Filter* const> m_filters;
        Resource* const m_resource;
        String const m_resourceMethod;
        Int m_index=0;
      };
    }
  }
}


#endif
