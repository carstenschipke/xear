/*
 * app.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_WEB_REST_APP_H_
#define X_WEB_REST_APP_H_


#include <condition_variable>
#include <future>
#include <map>
#include <mutex>

#include <x/common.h>

#include <x/io/file.h>

#include <x/web/rest/listener.h>
#include <x/web/rest/resources.h>


#ifndef X_WEB_HTTP_NAME_DEFAULT
# define X_WEB_HTTP_NAME_DEFAULT "x.dev"
#endif
#ifndef X_WEB_HTTP_HOST_DEFAULT
# define X_WEB_HTTP_HOST_DEFAULT "x.dev"
#endif
#ifndef X_WEB_HTTP_PORT_DEFAULT
# define X_WEB_HTTP_PORT_DEFAULT 80
#endif
#ifndef X_WEB_HTTP_BASE_DEFAULT
# define X_WEB_HTTP_BASE_DEFAULT "/"
#endif
#ifndef X_WEB_HTTP_SCHEME_DEFAULT
# define X_WEB_HTTP_SCHEME_DEFAULT "http"
#endif


namespace x
{
  namespace web
  {
    namespace rest
    {
      using namespace std;

      using namespace x;
      using namespace x::common;
      using namespace x::io;


      class App
      {
      public:
        unique_ptr<Resources> resources;


        App():
          resources(make_unique<Resources>())
        {

        }

        App(String filePathConfig):
          App(), m_filePathConfig(filePathConfig)
        {

        }


        String config()
        {
          return m_filePathConfig;
        }

        void config(String filePathConfig)
        {
          m_filePathConfig=filePathConfig;
        }

        void listener(String name, String scheme, String host, Int port, String baseUri)
        {
          auto iterator=m_listeners.find(name);

          if(iterator!=m_listeners.end())
            return;

          auto listener=make_unique<Listener>(resources.get(), scheme, host, port, baseUri);
          cout << "Adding listener" << endl;
          cout << "Host " << host << endl;
          cout << "Port " << port << endl;
          cout << "Base " << baseUri << endl;

          m_listeners[name]=move(listener);
        }

        void start()
        {
          m_future=async(launch::async, [&]{

            if(1>m_listeners.size())
            {
              try
              {
                initialize();
              }
              catch(exception& e)
              {
                cout << "Invalid configuration." << endl;
                cout << e.what() << endl;

                return;
              }
            }

            open();

            unique_lock<mutex> shutdownLock{m_monitorStop};
            m_conditionStop.wait(shutdownLock, [&]{return m_stop;});

            close();
          });
        }

        void stop()
        {
          lock_guard<mutex> shutdownLock{m_monitorStop};

          m_stop=true;
          m_conditionStop.notify_all();
        }

        void wait()
        {
          m_future.wait();
        }

        void open()
        {
          for(auto& entry : m_listeners)
            entry.second->open();
        }

        void close()
        {
          for(auto& entry : m_listeners)
            entry.second->close();
        }


      private:
        future<void> m_future;

        bool m_stop{false};
        mutex m_monitorStop;
        condition_variable m_conditionStop;

        String m_filePathConfig;
        map<String, unique_ptr<Listener>> m_listeners;


        void initialize()
        {
          if(m_filePathConfig.empty())
          {
            initializeDefault();

            return;
          }

          auto config=Config::create(m_filePathConfig);

          if(!config)
          {
            initializeDefault();

            return;
          }

          listener(
            config->s("http.name", X_WEB_HTTP_NAME_DEFAULT),
            config->s("http.scheme", X_WEB_HTTP_SCHEME_DEFAULT),
            config->s("http.host", X_WEB_HTTP_HOST_DEFAULT),
            config->i("http.port", X_WEB_HTTP_PORT_DEFAULT),
            config->s("http.base", X_WEB_HTTP_BASE_DEFAULT));
        }

        void initializeDefault()
        {
          listener(
            X_WEB_HTTP_NAME_DEFAULT,
            X_WEB_HTTP_SCHEME_DEFAULT,
            X_WEB_HTTP_HOST_DEFAULT,
            X_WEB_HTTP_PORT_DEFAULT,
            X_WEB_HTTP_BASE_DEFAULT);
        }
      };
    }
  }
}


#endif
