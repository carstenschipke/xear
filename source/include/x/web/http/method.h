/*
 * method.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_WEB_HTTP_METHOD_H_
#define X_WEB_HTTP_METHOD_H_


#include <x/common.h>


namespace x
{
  namespace web
  {
    namespace http
    {
      static String const METHODS[6]={
        "GET",
        "POST",
        "PUT",
        "DELETE",
        "HEAD",
        "OPTIONS"
      };

      enum Method: IntU
      {
        GET,
        POST,
        PUT,
        DELETE,
        HEAD,
        OPTIONS
      };


      inline std::ostream& operator<<(std::ostream& stream, Method const& method)
      {
        stream << METHODS[method];

        return stream;
      }

      inline String nameForMethod(Method method)
      {
        return METHODS[method];
      }

      inline Method methodForName(String name)
      {
        if(2>name.length())
          return Method::GET;

        switch(toupper(name[0]))
        {
          case 'G':
            return Method::GET;

          case 'P':
            if('O'==toupper(name[1]))
              return Method::POST;

            return Method::PUT;

          case 'D':
            return Method::DELETE;

          case 'H':
            return Method::HEAD;

          case 'O':
            return Method::OPTIONS;

          default:
            return Method::GET;
        }
      }
    }
  }
}


#endif
