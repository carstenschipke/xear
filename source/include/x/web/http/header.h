/*
 * header.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_WEB_HTTP_HEADER_H_
#define X_WEB_HTTP_HEADER_H_


#include <x/common.h>


#define X_WEB_HTTP_HEADER_DELIMITER ": "
#define X_WEB_HTTP_HEADER_SEPARATOR "\r\n"


namespace x
{
  namespace web
  {
    namespace http
    {
      struct Header
      {
        String name;
        String value;


        friend std::ostream& operator<<(std::ostream& stream, Header const& header)
        {
          stream << header.name << X_WEB_HTTP_HEADER_DELIMITER << header.value;

          return stream;
        }
      };
    }
  }
}


#endif
