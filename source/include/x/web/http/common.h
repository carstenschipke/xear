/*
 * common.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_WEB_HTTP_COMMON_H_
#define X_WEB_HTTP_COMMON_H_


#include <chrono>
#include <regex>
#include <vector>

#include <uuid/uuid.h>
#include <smallsha1/sha1.h>

#include <x/common.h>

#include <x/io/mimetype.h>

#include <x/web/http/header.h>
#include <x/web/http/method.h>
#include <x/web/http/status.h>


namespace x
{
  namespace web
  {
    namespace http
    {
      using namespace std;
      using namespace std::chrono;

      using namespace x::io;
      using namespace x::common;


      class Session;


      struct Context
      {
        String name;
        String path;


        friend std::ostream& operator<<(std::ostream& stream, Context const& context)
        {
          stream << "Context{name: " << context.name << ", path: " << context.path << "}";

          return stream;
        }
      };


      struct Request
      {
        Method method=Method::GET;
        Version version={1, 0, 0};

        String scheme;
        String user;
        String host;
        Int port=0;

        String path;
        String query;
        String fragment;

        vector<Header> headers;

        String content;


        friend std::ostream& operator<<(std::ostream& stream, Request const& request)
        {
          stream << status_header(request.version) << endl;

          stream << request.method << " ";

          if(request.scheme.empty())
            stream << "//";
          else
            stream << request.scheme << "://";
          if(!request.user.empty())
            stream << request.user << "@";
          stream << request.host;
          if(0<request.port)
            stream << ":" << request.port;
          stream << request.path;
          if(!request.query.empty())
            stream << "?" << request.query;
          if(!request.fragment.empty())
            stream << "#" << request.fragment;

          stream << endl;

          for(auto& header : request.headers)
            stream << header;

          stream << "Content-Length: " << request.content.size() << endl;
          stream << endl;

          stream << request.content << endl;
          stream << endl;

          return stream;
        }
      };


      struct Response
      {
        Status status;
        Version version{1, 1, 0};

        Session* session=nullptr;

        vector<Header> headers;

        Mimetype mimetype=Mimetype::TEXT_HTML;
        String content;


        Header header(String name)
        {
          auto idx=find_if(headers.begin(), headers.end(), [&name](Header const& current) {
            return 0==strcasecmp(name.c_str(), current.name.c_str());
          });

          if(idx==headers.end())
            return Header{};

          return *idx;
        }

        void header(Header header)
        {
          auto idx=find_if(headers.begin(), headers.end(), [&header](Header const& current) {
            return 0==strcasecmp(header.name.c_str(), current.name.c_str());
          });

          if(idx==headers.end())
            headers.push_back(header);
          else
            idx->value=header.value;
        }


        friend std::ostream& operator<<(std::ostream& stream, Response const& response)
        {
          stream << status_header(response.status, response.version) << endl;

          for(auto& header : response.headers)
            stream << header;

          stream << "Content-Length" << X_WEB_HTTP_HEADER_DELIMITER
            << response.content.size()
            << X_WEB_HTTP_HEADER_SEPARATOR;

          stream << "Content-Type" << X_WEB_HTTP_HEADER_DELIMITER
            << response.mimetype << ";charset=utf-8"
            << X_WEB_HTTP_HEADER_SEPARATOR;

          stream << endl;

          stream << response.content << endl;
          stream << endl;

          return stream;
        }
      };


      class Session
      {
      public:
        String const id;
        time_point<system_clock> const createdAt;


        Session(String id, Int lifetime):
          id(id), createdAt(system_clock::now()), m_lifetime(lifetime)
        {
          dbgi6("Create [" << *this << "].");
        }

        ~Session()
        {
          dbgi6("Destroy [" << *this << "].");
        }


        Boolean expired()
        {
          return m_lifetime<duration_cast<seconds>(system_clock::now()-createdAt).count();
        }

        Boolean contains(String name)
        {
          return m_objects.find(name)!=m_objects.end();
        }

        Object* get(String name)
        {
          auto iterator=m_objects.find(name);

          if(iterator==m_objects.end())
            return nullptr;

          return iterator->second.get();
        }

        void set(String name, unique_ptr<Object>&& object)
        {
          m_objects[name]=std::move(object);
        }

        void remove(String name)
        {
          auto iterator=m_objects.find(name);

          if(iterator!=m_objects.end())
            m_objects.erase(name);
        }

        map<String, unique_ptr<Object>>::iterator begin()
        {
          return m_objects.begin();
        }

        map<String, unique_ptr<Object>>::iterator end()
        {
          return m_objects.end();
        }


        friend std::ostream& operator<<(std::ostream& stream, Session const& session)
        {
          stream << "Session{id: " << session.id << "}";

          return stream;
        }


      private:
        Int m_lifetime;

        map<String, unique_ptr<Object>> m_objects;
      };


      // FIXME Protect against hijacking.
      class Sessions
      {
      public:
        Sessions(String domain, String path, Int lifetime):
        m_wildcard(0==strcmp("*", domain.c_str())), m_domain(domain), m_path(path), m_lifetime(lifetime)
        {
          dbgi6("Create.");
        }

        ~Sessions()
        {
          dbgi6("Destroy.");
        }


        String createId()
        {
          Byte uuid[16];
          uuid_generate_random(uuid);

          Byte charSessionId[20];
          Char hexSessionId[41];

          sha1::calc(uuid, 16, charSessionId);
          sha1::toHexString(charSessionId, hexSessionId);

          String sessionId{hexSessionId};

          dbgi6("Generate session id [" << sessionId << "].");

          return sessionId;
        }

        String currentId(vector<Header> headers, Boolean create)
        {
          for(auto& header : headers)
          {
            if(String::npos!=header.name.find("Cookie"))
            {
              auto idx=header.value.find("sid=");

              if(String::npos==idx)
                break;

              return header.value.substr(idx+4, 40);
            }
          }

          if(!create)
            return "";

          return createId();
        }

        Header header(Request const& request, String sessionId)
        {
          if(m_wildcard)
            return Header{"Set-Cookie", "sid="+sessionId+"; path="+m_path+";"};

          return Header{"Set-Cookie", "sid="+sessionId+"; path="+m_path+"; domain=."+m_domain+";"};
        }

        Session* open(String sessionId)
        {
          dbgi("Open session [sessionId: " << sessionId << "]");

          auto iterator=m_sessions.find(sessionId);

          if(iterator==m_sessions.end() || iterator->second->expired())
          {
            m_sessions[sessionId]=make_unique<Session>(sessionId, m_lifetime);

            return m_sessions[sessionId].get();
          }

          return iterator->second.get();
        }

        void open(Request const& request, Response& response)
        {
          auto sessionId=currentId(request.headers, true);

          response.header(header(request, sessionId));
          response.session=open(sessionId);
        }

        Session* renew(String sessionId)
        {
          dbgi6("Renew session [" << sessionId << "].");

          destroy(sessionId);

          return open(createId());
        }

        String renew(Request const& request, Response& response)
        {
          auto sessionId=currentId(request.headers, false);

          dbgi6("Renew session [" << sessionId << "].");

          if(!sessionId.empty())
            destroy(sessionId);

          response.session=open(createId());
          response.header(header(request, response.session->id));

          return sessionId;
        }

        Boolean destroy(String sessionId)
        {
          auto iterator=m_sessions.find(sessionId);

          if(iterator==m_sessions.end())
            return false;

          dbgi("Destroy session [" << *iterator->second << "].");

          m_sessions.erase(iterator);

          return true;
        }

        void destroyExpired()
        {
          for(auto& entry : m_sessions)
          {
            if(entry.second->expired())
              destroy(entry.first);
          }
        }


      private:
        Boolean m_wildcard;
        String m_domain;
        String m_path;
        Int m_lifetime;

        map<String, unique_ptr<Session>> m_sessions;
      };


      class RequestHandler
      {
      public:
        virtual ~RequestHandler()
        {

        }


        virtual void dispatch(Request const& request, Response& response) = 0;
      };
    }
  }
}


#endif
