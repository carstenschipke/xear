/*
 * status.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_WEB_HTTP_STATUS_H_
#define X_WEB_HTTP_STATUS_H_


#include <map>

#include <x/common.h>


namespace x
{
  namespace web
  {
    namespace http
    {
      using namespace std;

      using namespace x::common;


      static map<IntU, String> const STATUS_MESSAGE{
        // Informational
        {100, "Continue"},
        {101, "Switching Protocols"},

        // Successful
        {200, "OK"},
        {201, "Created"},
        {202, "Accepted"},
        {204, "No Content"},
        {205, "Reset Content"},
        {206, "Partial Content"},

        // Redirection
        {300, "Multiple Choices"},
        {301, "Moved Permanently"},
        {302, "Found"},
        {303, "See Other"},
        {304, "Not Modified"},
        {305, "Use Proxy"},
        {307, "Temporary Redirect"},

        // Client Error
        {400, "Bad Request"},
        {401, "Unauthorized"},
        {402, "Payment Required"},
        {403, "Forbidden"},
        {404, "Not Found"},
        {405, "Method Not Allowed"},
        {406, "Not Acceptable"},
        {407, "Proxy Authentication Required"},
        {408, "Request Timeout"},
        {409, "Conflict"},
        {410, "Gone"},
        {411, "Length Required"},
        {412, "Precondition Failed"},
        {413, "Request Entity Too Large"},
        {414, "Request URI Too Long"},
        {415, "Unsupported Media Type"},
        {416, "Requested Range Not Satisfiable"},
        {417, "Expectation Failed"},

        // Server Error
        {500, "Internal Server Error"},
        {501, "Not Implemented"},
        {502, "Bad Gateway"},
        {503, "Service Unavailable"},
        {504, "Gateway Timeout"},
        {505, "HTTP Version Not Supported"}
      };


      enum Status: IntU
      {
        // Informational
        CONTINUE=100,
        SWITCHING_PROTOCOLS=101,

        // Successful
        OK=200,
        CREATED=201,
        ACCEPTED=202,
        NO_CONTENT=204,
        RESET_CONTENT=205,
        PARTIAL_CONTENT=206,

        // Redirection
        MULTIPLE_CHOICES=300,
        MOVED_PERMANENTLY=301,
        FOUND=302,
        SEE_OTHER=303,
        NOT_MODIFIED=304,
        USE_PROXY=305,
        TEMPORARY_REDIRECT=307,

        // Client Error
        BAD_REQUEST=400,
        UNAUTHORIZED=401,
        PAYMENT_REQUIRED=402,
        FORBIDDEN=403,
        NOT_FOUND=404,
        METHOD_NOT_ALLOWED=405,
        NOT_ACCEPTABLE=406,
        PROXY_AUTHENTICATION_REQUIRED=407,
        REQUEST_TIMEOUT=408,
        CONFLICT=409,
        GONE=410,
        LENGTH_REQUIRED=411,
        PRECONDITION_FAILED=412,
        REQUEST_ENTITY_TOO_LARGE=413,
        REQUEST_URI_TOO_LONG=414,
        UNSUPPORTED_MEDIA_TYPE=415,
        REQUESTED_RANGE_NOT_SATISFIABLE=416,
        EXPECTATION_FAILED=417,

        // Server Error
        INTERNAL_SERVER_ERROR=500,
        NOT_IMPLEMENTED=501,
        BAD_GATEWAY=502,
        SERVICE_UNAVAILABLE=503,
        GATEWAY_TIMEOUT=504,
        HTTP_VERSION_NOT_SUPPORTED=505
      };


      inline String status_message(Status status)
      {
        auto message=STATUS_MESSAGE.find(status);

        if(message==STATUS_MESSAGE.end())
          return "";

        return message->second;
      }

      inline String status_header(Version version)
      {
        return "HTTP/"+to_string(version.major)+"."+to_string(version.minor);
      }

      inline String status_header(Status status, Version version)
      {
        return "HTTP/"+to_string(version.major)+"."+to_string(version.minor)+" "+to_string(status)+" "+status_message(status);
      }

      inline String status_header(Status status)
      {
        return status_header(status, Version{1, 0, 0});
      }

      inline String status_html(Status status)
      {
        auto message=to_string(status)+" "+status_message(status);
        
        return "<html><head><title>"+message+"</title></head><body>"+message+"</body></html";
      }


      inline std::ostream& operator<<(std::ostream& stream, Status const& status)
      {
        stream << to_string(status)+" "+status_message(status);

        return stream;
      }
    }
  }
}


#endif
