/*
 * test.h
 *
 * @author carsten.schipke@gmail.com
 *
 * @see http://stackoverflow.com/a/29155677
 */
#ifndef X_MISC_TEST_H_
#define X_MISC_TEST_H_


#include <chrono>

#include <gtest/gtest.h>


#include <x/common.h>


namespace testing
{
  namespace internal
  {
    enum GTestColor
    {
      COLOR_DEFAULT,
      COLOR_RED,
      COLOR_GREEN,
      COLOR_YELLOW
    };


    extern void ColoredPrintf(GTestColor color, x::Char const* format, ...);
  }
}


#define TEST_INFO(...) \
  testing::internal::ColoredPrintf(testing::internal::COLOR_GREEN, "[  INFO    ] "); \
  testing::internal::ColoredPrintf(testing::internal::COLOR_GREEN, __VA_ARGS__); \
  testing::internal::ColoredPrintf(testing::internal::COLOR_GREEN, "\n");

#define TEST_WARN(...) \
  testing::internal::ColoredPrintf(testing::internal::COLOR_YELLOW, "[  WARN    ] "); \
  testing::internal::ColoredPrintf(testing::internal::COLOR_YELLOW, __VA_ARGS__); \
  testing::internal::ColoredPrintf(testing::internal::COLOR_YELLOW, "\n");

#define TEST_FAIL(...) \
  testing::internal::ColoredPrintf(testing::internal::COLOR_RED, "[  FAIL    ] "); \
  testing::internal::ColoredPrintf(testing::internal::COLOR_RED, __VA_ARGS__); \
  testing::internal::ColoredPrintf(testing::internal::COLOR_RED, "\n");


namespace x
{
  namespace misc
  {
    namespace test
    {
      using namespace std;
      using namespace std::chrono;


      namespace
      {
        vector<time_point<system_clock>> __STACK_TIMER;
      }


      void timer_begin()
      {
        __STACK_TIMER.push_back(system_clock::now());
      }

      nanoseconds timer_end()
      {
        auto duration=duration_cast<nanoseconds>(system_clock::now()-__STACK_TIMER[__STACK_TIMER.size()-1]);

        __STACK_TIMER.pop_back();

        return duration;
      }
      
      void timer_end(nanoseconds& duration)
      {
        duration=duration_cast<nanoseconds>(system_clock::now()-__STACK_TIMER[__STACK_TIMER.size()-1]);

        __STACK_TIMER.pop_back();
      }

      void timer_dump(String const& message)
      {
        auto duration=duration_cast<nanoseconds>(system_clock::now()-__STACK_TIMER[__STACK_TIMER.size()-1]);

        __STACK_TIMER.pop_back();

        TEST_INFO("%s [%d ns / %d ms]", message.c_str(), duration.count(), duration_cast<milliseconds>(duration).count());
      }

      void timer_dump_ms(String const& message)
      {
        auto duration=duration_cast<milliseconds>(system_clock::now()-__STACK_TIMER[__STACK_TIMER.size()-1]);

        __STACK_TIMER.pop_back();

        TEST_INFO("%s [%d ms].", message.c_str(), duration.count());
      }
      
      void timer_dump_ns(String const& message)
      {
        auto duration=duration_cast<nanoseconds>(system_clock::now()-__STACK_TIMER[__STACK_TIMER.size()-1]);

        __STACK_TIMER.pop_back();

        TEST_INFO("%s [%d ns].", message.c_str(), duration.count());
      }
    }
  }
}


#define TIMER_BEGIN() x::misc::test::timer_begin();
#define TIMER_END_GET() x::misc::test::timer_end();
#define TIMER_END(duration) x::misc::test::timer_end(duration);
#define TIMER_DUMP(message) x::misc::test::timer_dump(message);
#define TIMER_DUMP_MS(message) x::misc::test::timer_dump_ms(message);
#define TIMER_DUMP_NS(message) x::misc::test::timer_dump_ns(message);


#endif
