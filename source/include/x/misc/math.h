/*
 * math.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_MISC_MATH_H_
#define X_MISC_MATH_H_


#include <x/common.h>


namespace x
{
  namespace misc
  {
    namespace math
    {
      /**
       * Probably not working like that (focused on a single face)..
       * See (& implement according to) original:
       *
       * http://www.terathon.com/code/tangent.html
       */
      inline void tangentBiTangent(vec3 faceVertex0, vec3 faceVertex1, vec3 faceVertex2,
                                   vec2 faceUv0, vec2 faceUv1, vec2 faceUv2, vec3 faceNormal,
                                   vec3& outTangent, vec3& outBiTangent)
      {
        vec3 tan0;
        vec3 tan1;

        Float x0=faceVertex1.x-faceVertex0.x;
        Float x1=faceVertex2.x-faceVertex0.x;
        Float y0=faceVertex1.y-faceVertex0.y;
        Float y1=faceVertex2.y-faceVertex0.y;
        Float z0=faceVertex1.z-faceVertex0.z;
        Float z1=faceVertex2.z-faceVertex0.z;

        Float s0=faceUv1.s-faceUv0.s;
        Float s1=faceUv2.s-faceUv0.s;
        Float t0=faceUv1.t-faceUv0.t;
        Float t1=faceUv2.t-faceUv0.t;

        Float r=1.0f;

        if(0.0001f<fabsf(s0*t1-s1*t0))
          r/=(s0*t1-s1*t0);

        vec3 sdir=r*vec3{s0*x1-s1*x0, s0*y1-s1*y0, s0*z1-s1*z0};
        // vec3 tdir=r*vec3{t1*x0-t0*x1, t1*y0-t0*y1, t1*z0-t0*z1};

        outTangent=glm::normalize(sdir-faceNormal*glm::dot(faceNormal, sdir));

        // Handedness
        // outTangent.w=0.0f>glm::dot(glm::cross(faceNormal, sdir), tdir)?-1.0f:1.0f;

        outBiTangent=glm::normalize(glm::cross(faceNormal, outTangent));
      }


      /**
       * See http://www.terathon.com/code/tangent.html
       */
//      void CalculateTangentArray(long vertexCount, const Point3D *vertex, const Vector3D *normal,
//              const Point2D *texcoord, long triangleCount, const Triangle *triangle, Vector4D *tangent)
//      {
//          Vector3D *tan1 = new Vector3D[vertexCount * 2];
//          Vector3D *tan2 = tan1 + vertexCount;
//          ZeroMemory(tan1, vertexCount * sizeof(Vector3D) * 2);
//
//          for (long a = 0; a < triangleCount; a++)
//          {
//              long i1 = triangle->index[0];
//              long i2 = triangle->index[1];
//              long i3 = triangle->index[2];
//
//              const Point3D& v1 = vertex[i1];
//              const Point3D& v2 = vertex[i2];
//              const Point3D& v3 = vertex[i3];
//
//              const Point2D& w1 = texcoord[i1];
//              const Point2D& w2 = texcoord[i2];
//              const Point2D& w3 = texcoord[i3];
//
//              float x1 = v2.x - v1.x;
//              float x2 = v3.x - v1.x;
//              float y1 = v2.y - v1.y;
//              float y2 = v3.y - v1.y;
//              float z1 = v2.z - v1.z;
//              float z2 = v3.z - v1.z;
//
//              float s1 = w2.x - w1.x;
//              float s2 = w3.x - w1.x;
//              float t1 = w2.y - w1.y;
//              float t2 = w3.y - w1.y;
//
//              float r = 1.0F / (s1 * t2 - s2 * t1);
//              Vector3D sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
//                      (t2 * z1 - t1 * z2) * r);
//              Vector3D tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
//                      (s1 * z2 - s2 * z1) * r);
//
//              tan1[i1] += sdir;
//              tan1[i2] += sdir;
//              tan1[i3] += sdir;
//
//              tan2[i1] += tdir;
//              tan2[i2] += tdir;
//              tan2[i3] += tdir;
//
//              triangle++;
//          }
//
//          for (long a = 0; a < vertexCount; a++)
//          {
//              const Vector3D& n = normal[a];
//              const Vector3D& t = tan1[a];
//
//              // Gram-Schmidt orthogonalize
//              tangent[a] = (t - n * Dot(n, t)).Normalize();
//
//              // Calculate handedness
//              tangent[a].w = (Dot(Cross(n, t), tan2[a]) < 0.0F) ? -1.0F : 1.0F;
//          }
//
//          delete[] tan1;
//      }

      inline void tangentBiTangent(vec3 faceVertex0, vec3 faceVertex1, vec3 faceVertex2,
                                   vec2 faceUv0, vec2 faceUv1, vec2 faceUv2,
                                   vec3& outTangent, vec3& outBiTangent)
      {
        vec3 faceEdge0=faceVertex1-faceVertex0;
        vec3 faceEdge1=faceVertex2-faceVertex0;

        vec3 faceNormal=glm::cross(faceEdge0, faceEdge1);

        return tangentBiTangent(faceVertex0, faceVertex1, faceVertex2, faceUv0, faceUv1, faceUv2, faceNormal, outTangent, outBiTangent);
      }
    }
  }
}


#endif
