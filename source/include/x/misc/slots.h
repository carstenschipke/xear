/*
 * slots.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_MISC_SLOTS_H_
#define X_MISC_SLOTS_H_


#include <x/common.h>


namespace x
{
  namespace misc
  {
    using namespace std;


    template<typename Size>
    struct Slots
    {
      public:
        inline ShortU acquire()
        {
          unsigned idx=0;

          // TODO Port asm to ARM.
#         ifdef __x86_64__
            unsigned slt=slots;

            __asm__(
              "bsfl %[slt], %[idx]\n\t"
              "jne 1f\n\t"
              "movl $32, %[idx]\n"
              "1:"
                :[idx]"=r" (idx)
                :[slt] "r" (~slt)
            );
#         else
            while(32>idx)
            {
              if(1>(slots&(1<<idx)))
                break;

              idx++;
            }
#         endif

          slots|=(1<<idx);

          return (Size)idx;
        }

        inline void lose(ShortU slot)
        {
          slots&=~(1<<slot);
        }


      private:
        Size slots=0;
    };


    // TODO Implement 16bit version.
    typedef Slots<IntU16> Slots16;
    typedef Slots<IntU32> Slots32;
    // TODO Implement 64bit version.
    typedef Slots<IntU64> Slots64;
  }
}


#endif
