/*
 * string.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_MISC_STRING_H
#define X_MISC_STRING_H


#include <x/common.h>


namespace x
{
  namespace misc
  {
    namespace string
    {
      using namespace std;


      inline String strip(String const& str, Char const& search)
      {
        String stripped;
        stripped.reserve(str.size());

        for(auto& chr : str)
        {
          if(search!=chr)
            stripped+=chr;
        }

        stripped.shrink_to_fit();

        return stripped;
      }

      inline void lowercase(String& str)
      {
        for(auto& chr : str)
          chr=std::tolower(chr);
      }

      inline void lowercase(String const& source, String& target)
      {
        for(auto& chr : source)
          target+=std::tolower(chr);
      }
    }
  }
}


#endif
