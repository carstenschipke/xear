/*
 * uri.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_URI_H_
#define X_IO_URI_H_


#include <x/common.h>

#include <x/io/mimetype.h>
#include <x/misc/string.h>


namespace x
{
  namespace io
  {
    class Uri
    {
      public:
        Uri(String const& uri):
          m_value(uri)
        {
          dbgsi("FROM STRING " << this);

          parse();
        }


        Uri(Uri&& rhs):
          m_value(std::move(rhs.m_value)),
          m_scheme(std::move(rhs.m_scheme)),
          m_principal(std::move(rhs.m_principal)),
          m_credentials(std::move(rhs.m_credentials)),
          m_host(std::move(rhs.m_host)),
          m_port(std::move(rhs.m_port)),
          m_path(std::move(rhs.m_path)),
          m_file(std::move(rhs.m_file)),
          m_mimeType(std::move(rhs.m_mimeType)),
          m_queryString(std::move(rhs.m_queryString))
        {
          dbgsi("MOVE CONSTRUCTION " << &rhs << " -> " << this);
        }
        
        Uri& operator=(Uri&& rhs)
        {
          dbgsi("MOVE ASSIGNMENT " << &rhs << " -> " << this);

          m_value=std::move(rhs.m_value);
          m_scheme=std::move(rhs.m_scheme);
          m_principal=std::move(rhs.m_principal);
          m_credentials=std::move(rhs.m_credentials);
          m_host=std::move(rhs.m_host);
          m_port=std::move(rhs.m_port);
          m_path=std::move(rhs.m_path);
          m_file=std::move(rhs.m_file);
          m_mimeType=std::move(rhs.m_mimeType);
          m_queryString=std::move(rhs.m_queryString);

          return *this;
        }


        Uri(Uri const& rhs):
          m_value(rhs.m_value),
          m_scheme(rhs.m_scheme),
          m_principal(rhs.m_principal),
          m_credentials(rhs.m_credentials),
          m_host(rhs.m_host),
          m_port(rhs.m_port),
          m_path(rhs.m_path),
          m_file(rhs.m_file),
          m_mimeType(rhs.m_mimeType),
          m_queryString(rhs.m_queryString)
        {
          dbgsi("COPY CONSTRUCTION " << &rhs << " -> " << this);
        }
        
        Uri& operator=(Uri const& rhs)
        {
          dbgsi("COPY ASSIGNMENT " << &rhs << " -> " << this);

          m_value=rhs.m_value;
          m_scheme=rhs.m_scheme;
          m_principal=rhs.m_principal;
          m_credentials=rhs.m_credentials;
          m_host=rhs.m_host;
          m_port=rhs.m_port;
          m_path=rhs.m_path;
          m_file=rhs.m_file;
          m_mimeType=rhs.m_mimeType;
          m_queryString=rhs.m_queryString;

          return *this;
        }


        String const& scheme() const noexcept
        {
          return m_scheme;
        }

        String const& host() const noexcept
        {
          return m_host;
        }

        IntU const& port() const noexcept
        {
          return m_port;
        }

        String const& principal() const noexcept
        {
          return m_principal;
        }

        String const& credentials() const noexcept
        {
          return m_credentials;
        }

        String const& path() const noexcept
        {
          return m_path;
        }

        String const& file() const noexcept
        {
          return m_file;
        }

        String const& queryString() const noexcept
        {
          return m_queryString;
        }

        String const& fragment() const noexcept
        {
          return m_fragment;
        }

        mime::Type const& mimetype() const noexcept
        {
          return m_mimeType;
        }


        Boolean isAbsolute() const noexcept
        {
          return 0==m_path.find("/");
        }

        Boolean isRelative() const noexcept
        {
          return 0!=m_path.find("/");
        }


        String const& toString() const noexcept
        {
          // TODO Implement inverse of #parse() or use as immutable type and provide dedicated uri builder.
          return m_value;
        }


      private:
        String m_value;

        String m_scheme;

        String m_principal;
        String m_credentials;
        String m_host;
        IntU m_port;

        String m_path;
        String m_queryString;
        String m_fragment;

        String m_file;
        mime::Type m_mimeType;

//        vector<String> m_pathParams;
//        map<String, String> m_queryParams;


        void parse()
        {
          Size offset=0u;
          Size offsetLast=0u;

          auto limit=m_value.size();

          while(offset<limit)
          {
            switch(m_value[offset])
            {
              case '/':
                parseSegment(offset, offsetLast);
                break;
              case '@':
                parseAuthority(offset, offsetLast);
                break;
              case '?':
                parseQueryString(offset, offsetLast);
                break;
              case '#':
                parseFragment(offset, offsetLast);
                break;
              default:
                ++offset;
            }
          }

          // TODO parseQueryParams / from m_queryString.
          // TODO parsePathParams / from m_path.
        }

        void parseSegment(Size& offset, Size& offsetLast)
        {
          if(0<offset && ':'==m_value[offset-1] && '/'==m_value[offset+1])
          {
            m_scheme=m_value.substr(0, offset-1);

            offset+=2;
            offsetLast=offset;
          }
          else
          {
            auto end=m_value.find("?", offset);

            if(String::npos==end)
              end=m_value.find("#", offset);

            m_host=m_value.substr(offsetLast, offset-offsetLast);

            if(String::npos==end)
              m_path=m_value.substr(offset);
            else
              m_path=m_value.substr(offset, end-offset);

            offset=offsetLast+=m_host.length()+m_path.length();

            // Handle relative paths.
            if(0==std::strcmp(".", m_host.c_str()))
            {
              m_host.clear();
              m_path=m_path.substr(1);
            }

            if(m_scheme.empty() && m_host.empty())
              m_scheme="file";

            if(!m_host.empty())
            {
              auto separator=m_host.find(":");

              if(String::npos!=separator)
              {
                m_port=stoi(m_host.substr(separator+1));
                m_host=m_host.substr(0, separator);
              }
            }

            if(!m_path.empty())
            {
              auto offsetFile=m_path.find_last_of("/");
              auto offsetFileExtension=m_path.find(".", offsetFile);

              if(String::npos!=offsetFileExtension)
              {
                m_file=m_path.substr(offsetFile+1);
                m_mimeType=Mimetype::typeForExtension(m_path.substr(offsetFileExtension+1));
              }
              else
              {
                m_mimeType=mime::Type::NONE;
              }
            }
          }
        }

        void parseAuthority(Size& offset, Size& offsetLast)
        {
          auto authority=m_value.substr(offsetLast, offset-offsetLast);
          auto separator=authority.find(":");

          if(String::npos==separator)
          {
            m_principal=authority;
          }
          else
          {
            m_principal=authority.substr(0, separator);
            m_credentials=authority.substr(separator+1);
          }

          offsetLast=++offset;
        }

        void parseQueryString(Size& offset, Size& offsetLast)
        {
          auto end=m_value.find("#", ++offset);

          if(String::npos==end)
            m_queryString=m_value.substr(offset);
          else
            m_queryString=m_value.substr(offset, end-offset);

          offsetLast=offset+=m_queryString.length();
        }

        void parseFragment(Size& offset, Size& offsetLast)
        {
          m_fragment=m_value.substr(++offset);

          offsetLast=offset+=m_fragment.length();
        }
    };
  }
}


#endif
