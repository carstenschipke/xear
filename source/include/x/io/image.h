/*
 * image.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_IMAGE_H
#define X_IO_IMAGE_H


#include <x/common.h>

#include <x/io/file.h>

#include <x/io/image/ref.h>
#include <x/io/image/reader/png.h>


namespace x
{
  namespace io
  {
    using namespace std;

    using namespace x::io;


    class Image: public File
    {
      public:
        Image():
          File(""), m_ref({}), m_pt(&m_ref), m_loaded(false), m_wrapped(false)
        {
          dbgi7("Create [" << *this << "].");
        }

        Image(String name):
          File(name), m_ref(), m_pt(&m_ref), m_loaded(false), m_wrapped(false)
        {
          dbgi7("Create [" << *this << "].");
        }

        Image(String name, Path* parent):
          File(name, parent), m_ref(), m_pt(&m_ref), m_loaded(false), m_wrapped(false)
        {
          dbgi7("Create [" << *this << "].");
        }

        Image(image::Ref value):
          File("", nullptr), m_ref(move(value)), m_pt(&m_ref), m_loaded(true), m_wrapped(false)
        {
          dbgi7("Take [" << *this << "].");
        }

        Image(image::Ref* value):
          File("", nullptr), m_ref(), m_pt(value), m_loaded(true), m_wrapped(true)
        {
          dbgi7("Wrap [" << *this << "].");
        }

        Image(Int width, Int height, Colorspace colorspace):
          File("", nullptr), m_ref(width, height, colorspace), m_pt(&m_ref), m_loaded(true), m_wrapped(false)
        {
          dbgi7("Allocate [" << *this << "].");
        }

        // FIXME Evil.
        Image(image::Ref const* value):
          Image(const_cast<image::Ref*>(value))
        {

        }


        Image(Image const& image):
          File(image)
        {
          dbgi7("Copy-construct [" << image << "].");

          m_loaded=image.m_loaded;
          m_wrapped=image.m_wrapped;

          if(m_wrapped)
          {
            m_pt=image.m_pt;
          }
          else
          {
            m_ref=image.m_ref;
            m_pt=&m_ref;
          }
        }

        Image(Image&& image):
          File(image)
        {
          dbgi7("Move-construct [" << image << "].");

          m_loaded=image.m_loaded;
          m_wrapped=image.m_wrapped;

          if(m_wrapped)
          {
            m_pt=image.m_pt;
          }
          else
          {
            m_ref=move(image.m_ref);
            m_pt=&m_ref;
          }
        }


        Image& operator=(Image const& image)
        {
          dbgi7("Copy-assign [" << image << "].");

          m_name=image.m_name;
          m_parent=image.m_parent;
          m_loaded=image.m_loaded;

          m_wrapped=image.m_wrapped;

          if(m_wrapped)
          {
            m_pt=image.m_pt;
          }
          else
          {
            m_ref=image.m_ref;
            m_pt=&m_ref;
          }

          return *this;
        }

        Image& operator=(Image&& image)
        {
          dbgi7("Move-assign [" << image << "].");

          m_name=image.m_name;
          m_parent=image.m_parent;
          m_loaded=image.m_loaded;

          image.m_parent=nullptr;

          m_wrapped=image.m_wrapped;

          if(m_wrapped)
          {
            m_pt=image.m_pt;
          }
          else
          {
            m_ref=move(image.m_ref);
            m_pt=&m_ref;
          }

          return *this;
        }


        virtual ~Image()
        {
          dbgi7("Destroy [" << *this << "].");
        }


        Int width()
        {
          return read()->m_ref.width;
        }

        Int height()
        {
          return read()->m_ref.height;
        }

        Size size()
        {
          return read()->m_ref.size;
        }

        Byte* const data()
        {
          return read()->m_ref.data;
        }


        image::Ref& ref()
        {
          return read()->m_ref;
        }

        image::Ref* const get()
        {
          return read()->m_pt;
        }


        Image* const read()
        {
          if(!m_loaded)
          {
            dbgi2("Read image [" << *this << "].");

            reader()->read(name(), m_ref);

            m_loaded=true;
          }

          return this;
        }

        unique_ptr<image::Reader> const reader()
        {
          if(mime::Type::IMAGE_PNG==mimetype())
            return make_unique<image::reader::Png>();

          throw "Unsupported mimetype ["+Mimetype::nameForType(mimetype())+"].";
        }


        friend std::ostream& operator<<(std::ostream& stream, Image const& image)
        {
          stream << "Image{id: " << &image << ", name: " << image.name();

          if(image.m_parent)
            stream << ", parent: " << *image.m_parent;

          stream  << ", loaded: " << to_string(image.m_loaded)
            << ", image: " << (*image.m_pt)
            << "}";

          return stream;
        }


      private:
        image::Ref m_ref;
        image::Ref* m_pt;

        Boolean m_loaded;
        Boolean m_wrapped;
    };
  }
}


#endif
