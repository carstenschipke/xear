/*
 * keymap.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_KEYMAP_H_
#define X_IO_KEYMAP_H_


#include <x/common.h>


namespace x
{
  namespace io
  {
    enum Key: IntU
    {
      UP=1,
      DOWN=2,
      LEFT=4,
      RIGHT=8,
      MOVE_FORWARD=16,
      MOVE_BACKWARD=32,
      STRAFE_LEFT=64,
      STRAFE_RIGHT=128
      // ...
    };


    struct Keys
    {
      IntU mask;

      Float velocityX;
      Float velocityY;
      Float velocityZ;
    };
  }
}


#endif
