/*
 * mimetype.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_MIMETYPE_H
#define X_IO_MIMETYPE_H


#include <map>

#include <x/common.h>


namespace x
{
  namespace io
  {
    namespace mime
    {
      enum Type: IntU
      {
        NONE,
        APPLICATION_OCTET_STREAM,
        APPLICATION_JAVASCRIPT,
        APPLICATION_JSON,
        APPLICATION_XEAR_GEOMETRY,
        APPLICATION_XEAR_MESH,
        APPLICATION_XML,
        APPLICATION_ZIP,
        IMAGE_GIF,
        IMAGE_ICO,
        IMAGE_JPG,
        IMAGE_JPEG,
        IMAGE_KTX,
        IMAGE_PNG,
        IMAGE_SVG_XML,
        TEXT_CSS,
        TEXT_GLSL_COMPUTE_SHADER,
        TEXT_GLSL_FRAGMENT_SHADER,
        TEXT_GLSL_GEOMETRY_SHADER,
        TEXT_GLSL_VERTEX_SHADER,
        TEXT_HTML,
        TEXT_JAVASCRIPT,
        TEXT_JSON,
        TEXT_PLAIN,
        TEXT_XML
      };


      class Types
      {
        public:
          std::map<String, mime::Type> const MIMETYPE_FOR_NAME{
            {"application/octet-stream", mime::Type::APPLICATION_OCTET_STREAM},
            {"application/javascript", mime::Type::APPLICATION_JAVASCRIPT},
            {"application/json", mime::Type::APPLICATION_JSON},
            {"application/xml", mime::Type::APPLICATION_XML},
            {"application/vnd.io-xear-geometry", mime::Type::APPLICATION_XEAR_GEOMETRY},
            {"application/vnd.io-xear-mesh", mime::Type::APPLICATION_XEAR_MESH},
            {"application/zip", mime::Type::APPLICATION_ZIP},
            {"image/gif", mime::Type::IMAGE_GIF},
            {"image/ico", mime::Type::IMAGE_ICO},
            {"image/jpg", mime::Type::IMAGE_JPG},
            {"image/jpeg", mime::Type::IMAGE_JPEG},
            {"image/ktx", mime::Type::IMAGE_KTX},
            {"image/png", mime::Type::IMAGE_PNG},
            {"image/svg+xml", mime::Type::IMAGE_SVG_XML},
            {"text/css", mime::Type::TEXT_CSS},
            {"text/html", mime::Type::TEXT_HTML},
            {"text/javascript", mime::Type::TEXT_JAVASCRIPT},
            {"text/json", mime::Type::TEXT_JSON},
            {"text/plain", mime::Type::TEXT_PLAIN},
            {"text/x-glsl-compute", mime::Type::TEXT_GLSL_COMPUTE_SHADER},
            {"text/x-glsl-fragment", mime::Type::TEXT_GLSL_FRAGMENT_SHADER},
            {"text/x-glsl-geometry", mime::Type::TEXT_GLSL_GEOMETRY_SHADER},
            {"text/x-glsl-vertex", mime::Type::TEXT_GLSL_VERTEX_SHADER}
          };
          std::map<String, mime::Type> const MIMETYPE_FOR_EXTENSION{
            {"av", mime::Type::APPLICATION_XEAR_GEOMETRY},
            {"am", mime::Type::APPLICATION_XEAR_MESH},
            {"bin", mime::Type::APPLICATION_OCTET_STREAM},
            {"csh", mime::Type::TEXT_GLSL_COMPUTE_SHADER},
            {"css", mime::Type::TEXT_CSS},
            {"fsh", mime::Type::TEXT_GLSL_FRAGMENT_SHADER},
            {"gif", mime::Type::IMAGE_GIF},
            {"gsh", mime::Type::TEXT_GLSL_GEOMETRY_SHADER},
            {"html", mime::Type::TEXT_HTML},
            {"ico", mime::Type::IMAGE_ICO},
            {"jpg", mime::Type::IMAGE_JPG},
            {"jpeg", mime::Type::IMAGE_JPG},
            {"js", mime::Type::TEXT_JAVASCRIPT},
            {"json", mime::Type::TEXT_JSON},
            {"ktx", mime::Type::IMAGE_KTX},
            {"png", mime::Type::IMAGE_PNG},
            {"svg", mime::Type::IMAGE_SVG_XML},
            {"txt", mime::Type::TEXT_PLAIN},
            {"vsh", mime::Type::TEXT_GLSL_VERTEX_SHADER},
            {"xml", mime::Type::TEXT_XML},
            {"zip", mime::Type::APPLICATION_ZIP}
          };
          std::map<mime::Type, String> const NAME_FOR_MIMETYPE{
            {mime::Type::APPLICATION_OCTET_STREAM, "application/octet-stream"},
            {mime::Type::APPLICATION_JAVASCRIPT, "application/javascript"},
            {mime::Type::APPLICATION_JSON, "application/json"},
            {mime::Type::APPLICATION_XML, "application/xml"},
            {mime::Type::APPLICATION_ZIP, "application/zip"},
            {mime::Type::APPLICATION_XEAR_GEOMETRY, "application/vnd.io-xear-geometry"},
            {mime::Type::APPLICATION_XEAR_MESH, "application/vnd.io-xear-mesh"},
            {mime::Type::IMAGE_GIF, "image/gif"},
            {mime::Type::IMAGE_ICO, "image/ico"},
            {mime::Type::IMAGE_JPG, "image/jpg"},
            {mime::Type::IMAGE_JPEG, "image/jpg"},
            {mime::Type::IMAGE_KTX, "image/ktx"},
            {mime::Type::IMAGE_PNG, "image/png"},
            {mime::Type::IMAGE_SVG_XML, "image/svg+xml"},
            {mime::Type::TEXT_CSS, "text/css"},
            {mime::Type::TEXT_GLSL_COMPUTE_SHADER, "text/x-glsl-compute"},
            {mime::Type::TEXT_GLSL_FRAGMENT_SHADER, "text/x-glsl-fragment"},
            {mime::Type::TEXT_GLSL_GEOMETRY_SHADER, "text/x-glsl-geometry"},
            {mime::Type::TEXT_GLSL_VERTEX_SHADER, "text/x-glsl-vertex"},
            {mime::Type::TEXT_HTML, "text/html"},
            {mime::Type::TEXT_JAVASCRIPT, "text/javascript"},
            {mime::Type::TEXT_JSON, "text/json"},
            {mime::Type::TEXT_PLAIN, "text/plain"},
            {mime::Type::TEXT_XML, "text/xml"}
          };
          std::map<mime::Type, String> const EXTENSION_FOR_MIMETYPE{
            {mime::Type::APPLICATION_OCTET_STREAM, "bin"},
            {mime::Type::APPLICATION_JAVASCRIPT, "js"},
            {mime::Type::APPLICATION_JSON, "json"},
            {mime::Type::APPLICATION_XML, "xml"},
            {mime::Type::APPLICATION_ZIP, "zip"},
            {mime::Type::APPLICATION_XEAR_GEOMETRY, "av"},
            {mime::Type::APPLICATION_XEAR_MESH, "am"},
            {mime::Type::IMAGE_GIF, "gif"},
            {mime::Type::IMAGE_ICO, "ico"},
            {mime::Type::IMAGE_JPG, "jpg"},
            {mime::Type::IMAGE_JPEG, "jpg"},
            {mime::Type::IMAGE_KTX, "ktx"},
            {mime::Type::IMAGE_PNG, "png"},
            {mime::Type::IMAGE_SVG_XML, "svg"},
            {mime::Type::TEXT_CSS, "css"},
            {mime::Type::TEXT_GLSL_COMPUTE_SHADER, "csh"},
            {mime::Type::TEXT_GLSL_FRAGMENT_SHADER, "fsh"},
            {mime::Type::TEXT_GLSL_GEOMETRY_SHADER, "gsh"},
            {mime::Type::TEXT_GLSL_VERTEX_SHADER, "vsh"},
            {mime::Type::TEXT_HTML, "html"},
            {mime::Type::TEXT_JAVASCRIPT, "js"},
            {mime::Type::TEXT_JSON, "json"},
            {mime::Type::TEXT_PLAIN, "txt"},
            {mime::Type::TEXT_XML, "xml"}
          };


          Types()
          {

          }

          ~Types()
          {

          }
      };


      static Types const TYPES;
    }


    struct Mimetype
    {
      public:
        mime::Type const type;

        String const name;
        String const extension;


        Mimetype():
          type(mime::Type::NONE)
        {

        }

        Mimetype(mime::Type type):
          type(type), name(nameForType(type)), extension(extensionForType(type))
        {

        }

        Mimetype(mime::Type type, String const& name):
          type(type), name(name), extension(extensionForType(type))
        {

        }

        Mimetype(mime::Type type, String const& name, String const& extension):
          type(type), name(name), extension(extension)
        {

        }


        // STATIC ACCESSORS
        static Mimetype forName(String const& name)
        {
          return Mimetype{typeForName(name), name};
        }

        static Mimetype forType(mime::Type type)
        {
          return Mimetype{type};
        }

        static Mimetype forExtension(String const& name)
        {
          return Mimetype{typeForExtension(name)};
        }

        static Mimetype forFilename(String const& name)
        {
          return Mimetype{typeForFilename(name)};
        }


        static mime::Type typeForName(String const& name)
        {
          auto iterator=mime::TYPES.MIMETYPE_FOR_NAME.find(name);

          if(iterator==mime::TYPES.MIMETYPE_FOR_NAME.end())
            return mime::Type::NONE;

          return iterator->second;
        }

        static mime::Type typeForExtension(String const& extension)
        {
          auto iterator=mime::TYPES.MIMETYPE_FOR_EXTENSION.find(extension);

          if(iterator==mime::TYPES.MIMETYPE_FOR_EXTENSION.end())
            return mime::Type::NONE;

          return iterator->second;
        }

        static mime::Type typeForFilename(String const& filename)
        {
          auto idx=filename.rfind('.');

          if(idx==String::npos)
            return mime::Type::NONE;

          return typeForExtension(filename.substr(idx+1));
        }

        static String nameForType(mime::Type mimetype)
        {
          auto iterator=mime::TYPES.NAME_FOR_MIMETYPE.find(mimetype);

          if(iterator==mime::TYPES.NAME_FOR_MIMETYPE.end())
            return "";

          return iterator->second;
        }

        static String extensionForType(mime::Type const& mimetype)
        {
          auto iterator=mime::TYPES.EXTENSION_FOR_MIMETYPE.find(mimetype);

          if(iterator==mime::TYPES.EXTENSION_FOR_MIMETYPE.end())
            return "";

          return iterator->second;
        }

        static Boolean typeIs(mime::Type a, mime::Type b) noexcept
        {
          return a==b;
        }

        static Boolean typeIsImage(mime::Type type) noexcept
        {
          return String::npos!=nameForType(type).find("image/");
        }

        static Boolean typeIsText(mime::Type type) noexcept
        {
          return String::npos!=nameForType(type).find("text/");
        }


        Boolean is(mime::Type const& other) const noexcept
        {
          return type==other;
        }

        Boolean isImage() const noexcept
        {
          return String::npos!=name.find("image/");
        }

        Boolean isText() const noexcept
        {
          return String::npos!=name.find("text/");
        }
    };


    inline std::ostream& operator<<(std::ostream& stream, mime::Type const& mimetype)
    {
      stream << Mimetype::nameForType(mimetype);

      return stream;
    }

    inline std::ostream& operator<<(std::ostream& stream, Mimetype const& mimeType)
    {
      stream << mimeType.type;

      return stream;
    }
  }
}


#endif
