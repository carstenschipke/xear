/*
 * json.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_JSON_H_
#define X_IO_JSON_H_


#include <x/common.h>


namespace x
{
  namespace io
  {
    using namespace std;

    using namespace common;


    /**
     * TODO Re-implement / optimize for streams.. current version is just
     * a dirty refactoring of previous string based version.
     *
     *
     * TODO Need a way to isolate the parser.
     *
     * We dont want pay for tons of checks to gain a almost-reliable parser.
     * Yet we can not have everything crash due to malformed input.
     *
     * Or we let the client take responsibility, e.g. isolate its call in a thread.
     */
    class Json
    {
      public:
        class Value: public Any
        {
          public:
            // PROPERTIES
            String key;
            Value* parent=nullptr;


            // CONSTRUCTION
            using Any::Any;


            Value():
              Any()
            {
              dbgi7("Create.");
            }


            // FIXME Something is wrong with copy constructor/assignment (try to copy of an instance returned by Value.at<>(..)).
            Value(Value const& rhs):
              Any((Any const&)rhs), key(rhs.key), parent(rhs.parent)
            {
              dbgi7("Copy-construct.");

              for(auto& value : rhs.m_values)
                m_values.push_back(value);
            }


            // OVERRIDES/IMPLEMENTS
            Value& operator=(Value const& rhs)
            {
              dbgi7("Copy-assign.");

              key=rhs.key;
              parent=rhs.parent;

              m_type=rhs.m_type;

              if(rhs.m_holder)
                m_holder=rhs.m_holder->clone();

              for(auto& value : rhs.m_values)
                m_values.push_back(value);

              return *this;
            }

            Value& operator<<(Type const& type)
            {
              m_type=type;

              return *this;
            }


            // ACCESSORS/MUTATORS
            Value* append(String const& key, Type const& type)
            {
              auto idx=m_values.size();

              m_values.emplace_back(type);
              m_values[idx].parent=this;
              m_values[idx].key=key;

              return &m_values[idx];
            }

            template<typename ValueType>
            Value* append(String const& key, ValueType const& value)
            {
              auto idx=m_values.size();

              m_values.emplace_back(type_of<ValueType>(), new Holder<ValueType>(value));
              m_values[idx].parent=this;
              m_values[idx].key=key;

              return &m_values[idx];
            }

            Size size() const
            {
              return m_values.size();
            }

            Boolean has(String const& key) const
            {
              for(auto& value : m_values)
              {
                if(0==strcmp(key.c_str(), value.key.c_str()))
                  return true;
              }

              return false;
            }

            Value const& at(Size idx) const
            {
              return at(idx, Value{Type::VOID});
            }

            Value const& at(Size idx, Value const& defaultValue) const
            {
              if(idx<m_values.size())
                return m_values[idx];

              return defaultValue;
            }

            template<typename ValueType>
            ValueType const& at(Size idx, ValueType const& defaultValue) const
            {
              if(idx<m_values.size() && m_values[idx].is<ValueType>())
                return m_values[idx].as<ValueType>();

              return defaultValue;
            }

            Value const& at(String const& key) const
            {
              return at(key, Value{Type::VOID});
            }

            Value const& at(String const& key, Value const& defaultValue) const
            {
              for(auto& value : m_values)
              {
                if(0==strcmp(key.c_str(), value.key.c_str()))
                  return value;
              }

              return defaultValue;
            }

            template<typename ValueType>
            ValueType const& at(String const& key, ValueType const& defaultValue) const
            {
              for(auto& value : m_values)
              {
                if(0==strcmp(key.c_str(), value.key.c_str()) && value.is<ValueType>())
                  return value.as<ValueType>();
              }

              return defaultValue;
            }

            vector<Value>::const_iterator begin() const
            {
              return m_values.begin();
            }

            vector<Value>::const_iterator end() const
            {
              return m_values.end();
            }

            vector<Value>::const_iterator cbegin() const
            {
              return m_values.cbegin();
            }

            vector<Value>::const_iterator cend() const
            {
              return m_values.cend();
            }


          private:
            // IMPLEMENTATION
            vector<Value> m_values;
        };


        // PROPERTIES
        Value root;


        // CONSTRUCTION
        Json()
        {

        }


        Json(Json const&) = delete;


        // OVERRIDES/IMPLEMENTS
        Json& operator=(Json const&) = delete;


        Json& operator<<(std::istream& stream)
        {
          Char chr;

          while(!m_hasError && !stream.eof())
          {
            stream.get(chr);

            switch(chr)
            {
              case ' ':
              case '\b':
              case '\f':
              case '\t':
              case '\r':
                break;
              case '\n':
                ++m_line;
                m_column=0;
                break;
              case '[':
                enter(Type::ARRAY);
                break;
              case '{':
                enter(Type::OBJECT);
                break;
              case ']':
                leave(Type::ARRAY);
                break;
              case '}':
                leave(Type::OBJECT);
                break;
              case ':':
                m_isKey=false;
                break;
              case ',':
                m_isKey=true;
                break;
              case '-':
              case '1':
              case '2':
              case '3':
              case '4':
              case '5':
              case '6':
              case '7':
              case '8':
              case '9':
              case '0':
                parseNumber(stream);
                break;
              case '"':
                parseString(stream);
                break;
              case 't':
              case 'T':
                m_current->append<Boolean>(m_key, true);
                stream.seekg(3, ios::cur);
                break;
              case 'f':
              case 'F':
                m_current->append<Boolean>(m_key, false);
                stream.seekg(4, ios::cur);
                break;
              case 'n':
              case 'N':
                m_current->append(m_key, Type::VOID);
                stream.seekg(3, ios::cur);
                break;
              default:
                error("Unexpected character [line: "+to_string(m_line+1)+", column: "+to_string(m_column+1)+"].");
                break;
            }

            ++m_column;
          }

          return *this;
        }

        Json& operator<<(String const& json)
        {
          std::stringstream stream{json};

          return (*this) << stream;
        }


        // ACCESSORS/MUTATORS
        String const& error()
        {
          return m_error;
        }

        Boolean hasError()
        {
          return m_hasError;
        }


      private:
        // IMPLEMENTATION
        String m_key;
        Boolean m_isKey=true;
        Value* m_current;

        Int m_line=0;
        Int m_column=0;

        String m_error;
        Boolean m_hasError=false;


        inline void enter(Type type)
        {
          m_isKey=true;

          if(root.isNull())
          {
            root << type;

            m_current=&root;
          }
          else
          {
            m_current=m_current->append(m_key, type);
          }
        }

        inline void leave(Type type)
        {
          m_isKey=true;

          if(m_current->type()!=type)
          {
            error("Unexpected end of "+to_string(type)+" [line: "+to_string(m_line+1)+", column: "+to_string(m_column+1)+"].");

            return;
          }

          m_current=m_current->parent;
        }

        inline void parseNumber(istream& stream)
        {
          stream.unget();

          String str;

          Char chr;
          Boolean hasFloatingPoint=false;

          while(!stream.eof())
          {
            stream.get(chr);

            if('.'==chr)
              hasFloatingPoint=true;

            if(','==chr || ']'==chr || '}'==chr)
            {
              m_isKey=true;

              if(']'==chr || '}'==chr)
                stream.unget();

              break;
            }

            if(' '!=chr)
              str+=chr;
          }

          if(0<str.size())
          {
            if(hasFloatingPoint)
            {
              // TODO Verify... or stick to Double in general?
              if(9>str.length())
                m_current->append<Float>(m_key, stof(str));
              else
                m_current->append<Double>(m_key, stod(str));
            }
            else
            {
              m_current->append<Int>(m_key, stoi(str));
            }
          }
        }

        inline void parseString(istream& stream)
        {
          String str;

          Char chr;

          while(!stream.eof())
          {
            stream.get(chr);

            if('"'==chr)
              break;

            if('\\'==chr)
            {
              stream.get(chr);

              switch(chr)
              {
                case 'b':
                  chr='\b';
                  break;
                case 'f':
                  chr='\f';
                  break;
                case 'n':
                  chr='\n';
                  break;
                case 'r':
                  chr='\r';
                  break;
                case 't':
                  chr='\t';
                  break;
                default:
                  break;
              }
            }

            str+=chr;
          }

          if(m_isKey)
            m_key=str;
          else
            m_current->append<String>(m_key, str);
        }

        inline void error(String const& message)
        {
          m_error=message;
          m_hasError=true;
        }
    };
  }
}


#endif
