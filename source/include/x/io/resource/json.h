/*
 * json.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_RESOURCE_JSON_H_
#define X_IO_RESOURCE_JSON_H_


#include <x/common.h>

#include <x/io/json.h>

#include <x/io/resource.h>
#include <x/io/resource/handler.h>


namespace x
{
  namespace io
  {
    namespace resource
    {
      class Json: public Handler
      {
        public:
          using Handler::Handler;


          Json(Json&& rhs):
            Handler(std::forward<Json>(rhs))
          {
            dbgsi("MOVE CONSTRUCTION " << &rhs << " -> " << this);
          }


          Json(Json const&) = delete;

          Json& operator=(Json&&) = delete;
          Json& operator=(Json const&) = delete;


          virtual ~Json()
          {

          }


          io::Json::Value const& resolve()
          {
            if(resource().isValid())
              m_parser << resource().streamInput();

            return m_parser.root;
          }


          // ACCESSORS/MUTATORS
          io::Json& impl()
          {
            return m_parser;
          }


        private:
          // IMPLEMENTATION
          io::Json m_parser;
      };
    }
  }
}


#endif
