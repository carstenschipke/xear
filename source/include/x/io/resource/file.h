/*
 * file.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_RESOURCE_FILE_H_
#define X_IO_RESOURCE_FILE_H_


#include <fstream>
#include <sys/stat.h>

#include <x/common.h>

#include <x/io/mimetype.h>
#include <x/io/resource.h>


namespace x
{
  namespace io
  {
    namespace resource
    {
      class File: public Resource
      {
        public:
          using Resource::Resource;


          File(File&& rhs):
            Resource(std::forward<File>(rhs)),
            m_streamInput(std::move(rhs.m_streamInput)),
            m_streamOutput(std::move(rhs.m_streamOutput))
          {
            dbgsi("MOVE CONSTRUCTION " << &rhs << " -> " << this);
          }


          File(File const&) = delete;

          File& operator=(File&&) = delete;
          File& operator=(File const&) = delete;


          virtual ~File()
          {
            if(m_streamInput.is_open())
              m_streamInput.close();

            if(m_streamOutput.is_open())
              m_streamOutput.close();
          }


          virtual Boolean isValid() const override
          {
            if(mime::Type::NONE==source().mimetype())
              return false;

            struct stat s;

            return 0==stat(source().path().c_str(), &s);
          }

          virtual Boolean isBinary() const noexcept override
          {
            return !Mimetype::typeIsText(source().mimetype());
          }


          virtual std::istream& streamInput() override
          {
            if(!m_streamInput.is_open())
            {
              auto flags=std::ios::in;

              if(isBinary())
                flags|=std::ios::binary;

              m_streamInput.flags(flags);
              m_streamInput.open(source().path());
            }

            return m_streamInput;
          }

          virtual std::ostream& streamOutput() override
          {
            if(!m_streamOutput.is_open())
            {
              auto flags=std::ios::out;

              if(isBinary())
                flags|=std::ios::binary;

              m_streamOutput.flags(flags);
              m_streamOutput.open(source().path());
            }

            return m_streamOutput;
          }


        private:
          std::ifstream m_streamInput;
          std::ofstream m_streamOutput;
      };
    }
  }
}


#endif
