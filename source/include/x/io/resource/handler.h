/*
 * handler.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_RESOURCE_HANDLER_H_
#define X_IO_RESOURCE_HANDLER_H_


#include <x/common.h>

#include <x/io/resource.h>


namespace x
{
  namespace io
  {
    namespace resource
    {
      class Handler
      {
        public:
          Handler(std::unique_ptr<Resource> const& resource):
            m_resource(std::move((std::unique_ptr<Resource>&&)resource))
          {

          }


          Handler(Handler&& rhs):
            m_resource(std::move(rhs.m_resource))
          {
            dbgsi("MOVE CONSTRUCTION " << &rhs << " -> " << this);
          }


          Handler(Handler const&) = delete;

          Handler& operator=(Handler&&) = delete;
          Handler& operator=(Handler const&) = delete;


          virtual ~Handler()
          {

          }


          Resource& resource() const
          {
            return *m_resource;
          }


        private:
          std::unique_ptr<Resource> m_resource;
      };
    }
  }
}


#endif
