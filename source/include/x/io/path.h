/*
 * path.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_PATH_H
#define X_IO_PATH_H


#include <chrono>
#include <fstream>
#include <stdio.h>
#include <sys/stat.h>

#include <x/common.h>


namespace x
{
  namespace io
  {
    using namespace std;
    using namespace std::chrono;


    class Path
    {
      public:
        Path(String name):
          m_name(name), m_parent(nullptr)
        {
          dbgi7("Create [id: " << this << ", name: " << name << "].");
        }

        Path(String name, Path* parent):
          m_name(name), m_parent(parent)
        {
          dbgi7("Create [id: " << this << ", name: " << name << ", parent: " << parent << "].");
        }


        Path(Path const& path):
          m_name(path.m_name), m_parent(path.m_parent)
        {
          dbgi7("Copy-construct [" << path << "].");
        }

        Path(Path&& path):
          m_name(move(path.m_name)), m_parent(path.m_parent)
        {
          dbgi7("Move-construct [" << path << "].");

          path.m_parent=nullptr;
        }


        Path& operator=(Path const& path)
        {
          dbgi7("Copy-assign [" << path << "].");

          m_name=path.m_name;
          m_parent=path.m_parent;

          return *this;
        }

        Path& operator=(Path&& path)
        {
          dbgi7("Move-assign [" << path << "].");

          m_name=path.m_name;
          path.m_parent=nullptr;

          return *this;
        }


        ~Path()
        {
          dbgi7("Destroy [" << *this << "].");
        }


        String name() const noexcept
        {
          if(m_parent)
            return m_parent->name()+"/"+m_name;

          return m_name;
        }

        Path const* const parent() noexcept
        {
          return m_parent;
        }

        String basename() const noexcept
        {
          return m_name;
        }

        Boolean exists() const
        {
          struct stat s;

          return 0==stat(this->name().c_str(), &s);
        }

        Boolean isFile() const
        {
          struct stat s;

          if(0==stat(this->name().c_str(), &s))
            return S_ISREG(s.st_mode);

          return false;
        }

        time_point<system_clock> createdAt() const
        {
          struct stat s;

          stat(this->name().c_str(), &s);

#         ifdef __APPLE__
            return system_clock::from_time_t(s.st_ctimespec.tv_sec);
#         else
            return system_clock::from_time_t(s.st_ctim.tv_sec);
#         endif
        }

        time_point<system_clock> accessedAt() const
        {
          struct stat s;

          stat(this->name().c_str(), &s);

#         ifdef __APPLE__
            return system_clock::from_time_t(s.st_atimespec.tv_sec);
#         else
            return system_clock::from_time_t(s.st_atim.tv_sec);
#         endif
        }

        Boolean accessedSince(time_point<system_clock> timepoint) const
        {
          struct stat s;

          stat(this->name().c_str(), &s);

#         ifdef __APPLE__
            return timepoint<system_clock::from_time_t(s.st_atimespec.tv_sec);
#         else
            return timepoint<system_clock::from_time_t(s.st_atim.tv_sec);
#         endif
        }

        time_point<system_clock> modifiedAt() const
        {
          struct stat s;

          stat(this->name().c_str(), &s);

#         ifdef __APPLE__
            return system_clock::from_time_t(s.st_mtimespec.tv_sec);
#         else
            return system_clock::from_time_t(s.st_mtim.tv_sec);
#         endif
        }

        Boolean modifiedSince(time_point<system_clock> timepoint) const
        {
          struct stat s;

          stat(this->name().c_str(), &s);

#         ifdef __APPLE__
            return timepoint<system_clock::from_time_t(s.st_mtimespec.tv_sec);
#         else
            return timepoint<system_clock::from_time_t(s.st_mtim.tv_sec);
#         endif
        }

        Boolean contains(String name) const
        {
          String path{this->name()+"/"+name};

          struct stat s;

          return 0==stat(path.c_str(), &s);
        }


        friend std::ostream& operator<<(std::ostream& stream, Path const& path)
        {
          stream << "Path{id: " << &path << ", name: " << path.m_name;

          if(path.m_parent)
            stream << ", parent: " << *path.m_parent;

          stream << "}";

          return stream;
        }


      protected:
        String m_name;
        Path* m_parent;
    };
  }
}


#endif
