/*
 * colorspace.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_COLORSPACE_H_
#define X_IO_COLORSPACE_H_


#include <x/common.h>


namespace x
{
  namespace io
  {
    enum Channels: IntU
    {
      NONE,
      RGB,
      RGBA
    };

    String const CHANNEL_NAME[]={
      "NONE",
      "RGB",
      "RGBA"
    };

    Int const CHANNEL_BYTES[]={
      0,
      3,
      4
    };


    struct Colorspace
    {
      Channels id;
      Int depth;


      Int bits(Int width, Int height) const
      {
        return depth*width*height;
      }

      Size size(Int width, Int height) const
      {
        return sizeof(Byte)*bytes(width, height);
      }

      Int bytes(Int width, Int height) const
      {
        return bytesPerPixel()*width*height;
      }

      Int bitsPerPixel() const
      {
        return depth;
      }

      Int bytesPerPixel() const
      {
        return CHANNEL_BYTES[id];
      }

      Boolean const hasAlpha() const
      {
        return Channels::RGBA==id;
      }


      friend std::ostream& operator<<(std::ostream& stream, Colorspace const& colorspace)
      {
        stream << "Colorspace{name: " << CHANNEL_NAME[colorspace.id]
          << ", depth: " << colorspace.depth
          << "}";

        return stream;
      }
    };


    Colorspace const COLORSPACE_NONE{Channels::NONE, 0};
    Colorspace const COLORSPACE_RGB{Channels::RGB, 24};
    Colorspace const COLORSPACE_RGBA{Channels::RGBA, 32};
  }
}


#endif
