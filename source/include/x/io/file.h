/*
 * file.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_FILE_H
#define X_IO_FILE_H


#include <x/common.h>

#include <x/io/path.h>
#include <x/io/mimetype.h>


namespace x
{
  namespace io
  {
    using namespace std;


    class File: public Path
    {
      public:
        File(String name):
          Path(name)
        {
          dbgi7("Create [id: " << this << ", name: " << name << "].");
        }

        File(String name, Path* parent):
          Path(name, parent)
        {
          dbgi7("Create [id: " << this << ", name: " << name << ", parent: " << parent << "].");
        }


        File(File const& file):
          Path(file)
        {
          dbgi7("Copy-construct [" << file << "].");
        }

        File(File&& file):
          Path(file)
        {
          dbgi7("Move-construct [" << file << "].");
        }


        File& operator=(File const& file)
        {
          dbgi7("Copy-assign [" << file << "].");

          m_name=file.m_name;
          m_parent=file.m_parent;

          return *this;
        }

        File& operator=(File&& file)
        {
          dbgi7("Move-assign [" << file << "].");

          m_name=file.m_name;
          m_parent=file.m_parent;

          file.m_parent=nullptr;

          return *this;
        }


        ~File()
        {
          dbgi7("Destroy [" << *this << "].");
        }


        mime::Type mimetype()
        {
          return Mimetype::typeForFilename(name());
        }

        String readText()
        {
          ifstream stream{m_name, ios::in};

          stream.seekg(0, ios::end);
          auto size=stream.tellg();

          stream.seekg(0, ios::beg);

          String text;
          text.reserve((Size)size);

          text.assign(
            istreambuf_iterator<char>(stream),
            istreambuf_iterator<char>()
          );

          stream.close();

          return text;
        }


        friend std::ostream& operator<<(std::ostream& stream, File const& file)
        {
          stream << "File{id: " << &file << ", name: " << file.m_name;

          if(file.m_parent)
            stream << ", parent: " << *file.m_parent;

          stream << "}";

          return stream;
        }
    };
  }
}


#endif
