/*
 * resource.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_RESOURCE_H_
#define X_IO_RESOURCE_H_


#include <x/common.h>

#include <x/io/uri.h>


namespace x
{
  namespace io
  {
    class Resource
    {
      public:
        Resource(Uri const& uri):
          m_uri(std::move((Uri&&)uri))
        {

        }

        Resource(String const& location):
          m_uri(location)
        {

        }


        Resource(Resource&& rhs):
          m_uri(std::move((Uri&&)rhs.m_uri))
        {
          dbgsi("MOVE CONSTRUCTION " << &rhs << " -> " << this);
        }


        Resource(Resource const&) = delete;

        Resource& operator=(Resource&&) = delete;
        Resource& operator=(Resource const&) = delete;


        virtual ~Resource()
        {

        }


        Uri const& source() const
        {
          return m_uri;
        }


        virtual Boolean isBinary() const = 0;
        virtual Boolean isValid() const = 0;

        virtual std::istream& streamInput() = 0;
        virtual std::ostream& streamOutput() = 0;


      private:
        Uri m_uri;
    };
  }
}


#endif
