/*
 * resources.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_RESOURCES_H_
#define X_IO_RESOURCES_H_


#include <x/common.h>

#include <x/io/resource.h>
#include <x/io/resource/file.h>
#include <x/io/resource/handler.h>

#include <x/io/uri.h>


namespace x
{
  namespace io
  {
    class Resources
    {
      public:
        Resources()
        {
          m_factoryDefault=[](Uri const& location) {
            return std::make_unique<resource::File>(location);
          };

          registerResourceFactoryForScheme("file", m_factoryDefault);
        }


        void registerDefaultResourceFactory(std::function<std::unique_ptr<Resource>(Uri const&)> const& factory)
        {
          m_factoryDefault=factory;
        }

        void registerResourceFactoryForScheme(String const& scheme, std::function<std::unique_ptr<Resource>(Uri const&)> const& factory)
        {
          m_factories[scheme]=factory;
        }


        std::unique_ptr<Resource> get(Uri const& location)
        {
          auto scheme=location.scheme();
          auto factory=m_factories.find(scheme);

          if(scheme.empty() || m_factories.end()==factory)
            return m_factoryDefault(location);

          return factory->second(location);
        }


        template<class T>
        T get(Uri const& location)
        {
          static_assert(std::is_base_of<resource::Handler, T>::value, "Expected T of type x::io::resource::Handler.");

          return {get(location)};
        }


      private:
        std::function<std::unique_ptr<Resource>(Uri const&)> m_factoryDefault;
        std::map<String, std::function<std::unique_ptr<Resource>(Uri const&)>> m_factories;
    };
  }
}


#endif
