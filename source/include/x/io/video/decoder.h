/*
 * decoder.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_VIDEO_DECODER_H
#define X_IO_VIDEO_DECODER_H


#include <x/common.h>

#include <x/io/image/ref.h>


namespace x
{
  namespace io
  {
    namespace video
    {
      using namespace std;


      class Decoder
      {
        public:
          virtual ~Decoder()
          {

          }


          virtual void read(String path, image::Ref& image) const = 0;
      };
    }
  }
}


#endif
