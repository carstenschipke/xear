/*
 * reader.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_IMAGE_READER_H
#define X_IO_IMAGE_READER_H


#include <x/common.h>

#include <x/io/image/ref.h>


namespace x
{
  namespace io
  {
    namespace image
    {
      using namespace std;


      class Reader
      {
        public:
          virtual ~Reader()
          {

          }


          virtual void read(String path, Ref& image) const = 0;
      };
    }
  }
}


#endif
