/*
 * ref.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_IMAGE_REF_H
#define X_IO_IMAGE_REF_H


#include <x/common.h>

#include <x/io/colorspace.h>


namespace x
{
  namespace io
  {
    namespace image
    {
      using namespace std;


      struct Ref
      {
        Int width;
        Int height;
        Size size;

        Byte* data;
        Colorspace colorspace;


        Ref():
          width(0), height(0), size(0), data(nullptr), colorspace(COLORSPACE_NONE)
        {
          dbgi7("Create [" << *this << "].");
        }

        Ref(Int width, Int height, Size size, Colorspace colorspace):
          width(width), height(height), size(size), colorspace(colorspace)
        {
          dbgi7("Allocate [" << *this << "].");

          if(0<size)
            data=(Byte*)malloc(size);
        }

        Ref(Int width, Int height, Size size):
          Ref(width, height, size, COLORSPACE_NONE)
        {

        }

        Ref(Int width, Int height, Colorspace colorspace):
          Ref(width, height, colorspace.bytes(width, height), colorspace)
        {

        }


        Ref(Ref const& ref)
        {
          dbgi7("Copy-construct [" << ref << "].");

          width=ref.width;
          height=ref.height;
          size=ref.size;
          colorspace=ref.colorspace;

          if(0<size)
          {
            data=(Byte*)malloc(size);
            memcpy(data, ref.data, size);
          }
        }

        Ref(Ref&& ref)
        {
          dbgi7("Move-construct [" << ref << "].");

          width=ref.width;
          height=ref.height;
          size=ref.size;
          colorspace=ref.colorspace;

          data=ref.data;
          ref.data=nullptr;
        }


        Ref& operator=(Ref const& ref)
        {
          dbgi7("Copy-assign [" << ref << "].");

          width=ref.width;
          height=ref.height;
          size=ref.size;
          colorspace=ref.colorspace;

          if(0<size)
          {
            data=(Byte*)malloc(size);
            memcpy(data, ref.data, size);
          }

          return *this;
        }

        Ref& operator=(Ref&& ref)
        {
          dbgi7("Move-assign [" << ref << "].");

          width=ref.width;
          height=ref.height;
          size=ref.size;
          colorspace=ref.colorspace;

          data=ref.data;
          ref.data=nullptr;

          return *this;
        }


        ~Ref()
        {
          dbgi7("Destroy [" << *this << "].");

          if(0<size)
          {
            dbgi7("Free [" << *this << "].");

            free(data);
          }
        }


        void resize(Size sizeNew)
        {
          if(0<size)
            data=(Byte*)realloc(data, sizeNew);
          else
            data=(Byte*)malloc(sizeNew);

          size=sizeNew;
        }


        friend std::ostream& operator<<(std::ostream& stream, Ref const& ref)
        {
          stream << "Ref{"
            << "id: " << &ref
            << ", width: " << ref.width
            << ", height: " << ref.height
            << ", size: " << ref.size
            << ", colorspace: " << ref.colorspace
            << ", data: " << reinterpret_cast<Size>(ref.data)
            << "}";

          return stream;
        }
      };
    }
  }
}


#endif
