/*
 * png.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_IMAGE_READER_PNG_H
#define X_IO_IMAGE_READER_PNG_H


#include <x/common.h>

#include <x/io/image/reader.h>


namespace x
{
  namespace io
  {
    namespace image
    {
      namespace reader
      {
        using namespace std;


        class Png: public Reader
        {
          public:
            Png();
            virtual ~Png();


            virtual void read(String path, Ref& image) const override;
        };
      }
    }
  }
}


#endif
