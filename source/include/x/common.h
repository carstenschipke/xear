/*
 * common.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_COMMON_H_
#define X_COMMON_H_


#include <math.h>
#include <stdint.h>
#include <stdlib.h>

#include <iostream>
#include <sstream>
#include <memory>
#include <string>
#include <thread>
#include <map>
#include <vector>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>


#ifdef __cplusplus
# define EXTERN extern "C"
#else
# define EXTERN
#endif

#if defined(__GNUC__) && 4<=__GNUC__
# define EXPORT __attribute__ ((visibility("default")))
#else
# ifdef WIN32
#   define EXPORT __declspec(dllexport)
# else
#   define EXPORT
# endif
#endif



// FIXME Can surely be improved :).
// TODO Async logging library.
// TODO Remove dbg statements in 'stable' utility types and add unit tests.
// TODO Compatible with x/misc/test#dbgti.. (& redirect accordingly during testing)
#define sout(message) (std::cout) << "[" << std::time(nullptr) << "] [INFO] [" << std::this_thread::get_id() << "] [" << __FUNCTION__ << "] " << message << std::endl
#define serr(message) (std::cerr) << "[" << std::time(nullptr) << "] [FAIL] [" << std::this_thread::get_id() << "] [" << __FUNCTION__ << "] " << message << std::endl
#ifdef DEBUG
# define dbg(stream, tag, message) (stream) << "[" << std::time(nullptr) << "] [" << tag << "] [" << std::this_thread::get_id() << "] [" << __FUNCTION__ << "] " << message << std::endl << "  " << this << " " << __FILE__ << ":" << __LINE__ << std::endl
# define dbgs(stream, tag, message) (stream) << "[" << std::time(nullptr) << "] [" << tag << "] [" << std::this_thread::get_id() << "] [" << __FUNCTION__ << "] " << message << std::endl << "  0x000000000000 " << __FILE__ << ":" << __LINE__ << std::endl
# define dbgi(message) dbg(std::cout, "INFO", message)
# define dbge(message) dbg(std::cerr, "FAIL", message)
# define dbgsi(message) dbgs(std::cout, "INFO", message)
# define dbgse(message) dbgs(std::cerr, "FAIL", message)
# if DEBUG > 6
#   define dbgi7(message) dbg(std::cout, "INFO", message)
#   define dbge7(message) dbg(std::cerr, "FAIL", message)
#   define dbgsi7(message) dbgs(std::cout, "INFO", message)
#   define dbgse7(message) dbgs(std::cerr, "FAIL", message)
# else
#   define dbge7(message)
#   define dbgi7(message)
#   define dbgse7(message)
#   define dbgsi7(message)
# endif
# if DEBUG > 5
#   define dbgi6(message) dbg(std::cout, "INFO", message)
#   define dbge6(message) dbg(std::cerr, "FAIL", message)
#   define dbgsi6(message) dbgs(std::cout, "INFO", message)
#   define dbgse6(message) dbgs(std::cerr, "FAIL", message)
# else
#   define dbge6(message)
#   define dbgi6(message)
#   define dbgse6(message)
#   define dbgsi6(message)
# endif
# if DEBUG > 4
#   define dbgi5(message) dbg(std::cout, "INFO", message)
#   define dbge5(message) dbg(std::cerr, "FAIL", message)
#   define dbgsi5(message) dbgs(std::cout, "INFO", message)
#   define dbgse5(message) dbgs(std::cerr, "FAIL", message)
# else
#   define dbge5(message)
#   define dbgi5(message)
#   define dbgse5(message)
#   define dbgsi5(message)
# endif
# if DEBUG > 3
#   define dbgi4(message) dbg(std::cout, "INFO", message)
#   define dbge4(message) dbg(std::cerr, "FAIL", message)
#   define dbgsi4(message) dbgs(std::cout, "INFO", message)
#   define dbgse4(message) dbgs(std::cerr, "FAIL", message)
# else
#   define dbge4(message)
#   define dbgi4(message)
#   define dbgse4(message)
#   define dbgsi4(message)
# endif
# if DEBUG > 2
#   define dbgi3(message) dbg(std::cout, "INFO", message)
#   define dbge3(message) dbg(std::cerr, "FAIL", message)
#   define dbgsi3(message) dbgs(std::cout, "INFO", message)
#   define dbgse3(message) dbgs(std::cerr, "FAIL", message)
# else
#   define dbge3(message)
#   define dbgi3(message)
#   define dbgse3(message)
#   define dbgsi3(message)
# endif
# if DEBUG > 1
#   define dbgi2(message) dbg(std::cout, "INFO", message)
#   define dbge2(message) dbg(std::cerr, "FAIL", message)
#   define dbgsi2(message) dbgs(std::cout, "INFO", message)
#   define dbgse2(message) dbgs(std::cerr, "FAIL", message)
# else
#   define dbge2(message)
#   define dbgi2(message)
#   define dbgse2(message)
#   define dbgsi2(message)
# endif
# if DEBUG > 0
#   define dbgi1(message) dbg(std::cout, "INFO", message)
#   define dbge1(message) dbg(std::cerr, "FAIL", message)
#   define dbgsi1(message) dbgs(std::cout, "INFO", message)
#   define dbgse1(message) dbgs(std::cerr, "FAIL", message)
# else
#   define dbge1(message)
#   define dbgi1(message)
#   define dbgse1(message)
#   define dbgsi1(message)
# endif
#else
# define dbg(stream, tag, message)
# define dbgs(stream, tag, message)
# define dbge(message)
# define dbgi(message)
# define dbgse(message)
# define dbgsi(message)
# define dbge7(message)
# define dbgi7(message)
# define dbgse7(message)
# define dbgsi7(message)
# define dbge6(message)
# define dbgi6(message)
# define dbgse6(message)
# define dbgsi6(message)
# define dbge5(message)
# define dbgi5(message)
# define dbgse5(message)
# define dbgsi5(message)
# define dbge4(message)
# define dbgi4(message)
# define dbgse4(message)
# define dbgsi4(message)
# define dbge3(message)
# define dbgi3(message)
# define dbgse3(message)
# define dbgsi3(message)
# define dbge2(message)
# define dbgi2(message)
# define dbgse2(message)
# define dbgsi2(message)
# define dbge1(message)
# define dbgi1(message)
# define dbgse1(message)
# define dbgsi1(message)
#endif


#ifndef M_PI
# define M_PI 3.14159265358979323846
#endif
#ifndef M_PI_2
# define M_PI_2 1.57079632679489661923
#endif
#ifndef M_PI_4
# define M_PI_4 0.78539816339744830962
#endif


#if 201104L > __cplusplus
# define __cpp_lib_make_unique 201304
  namespace std
  {
    template<typename _Tp>
    struct _MakeUniq
    {
      typedef unique_ptr<_Tp> __single_object;
    };

    template<typename _Tp>
    struct _MakeUniq<_Tp[]>
    {
      typedef unique_ptr<_Tp[]> __array;
    };

    template<typename _Tp, size_t _Bound>
    struct _MakeUniq<_Tp[_Bound]>
    {
      struct __invalid_type {};
    };

    // std::make_unique for single objects
    template<typename _Tp, typename... _Args>
    inline typename _MakeUniq<_Tp>::__single_object make_unique(_Args&&... __args)
    {
      return unique_ptr<_Tp>(new _Tp(std::forward<_Args>(__args)...));
    }

    // std::make_unique for arrays of unknown bound
    template<typename _Tp>
    inline typename _MakeUniq<_Tp>::__array make_unique(size_t __num)
    {
      return unique_ptr<_Tp>(new typename remove_extent<_Tp>::type[__num]());
    }

    // Disable std::make_unique for arrays of known bound
    template<typename _Tp, typename... _Args>
    inline typename _MakeUniq<_Tp>::__invalid_type make_unique(_Args&&...) = delete;
  }
#endif


namespace x
{
  typedef bool Boolean;

  typedef unsigned char Byte;
  typedef char Char;

  typedef int8_t Int8;
  typedef uint8_t IntU8;

  typedef int16_t Int16;
  typedef uint16_t IntU16;

  typedef int32_t Int32;
  typedef uint32_t IntU32;

  typedef int64_t Int64;
  typedef uint64_t IntU64;

  typedef Int16 Short;
  typedef IntU16 ShortU;

  typedef Int32 Int;
  typedef IntU32 IntU;

  typedef Int64 Long;
  typedef IntU64 LongU;

  typedef float Float;
  typedef double Double;

  typedef size_t Size;

  typedef std::string String;


  typedef glm::vec2 vec2;
  typedef glm::vec3 vec3;
  typedef glm::vec4 vec4;

  typedef glm::mat2 mat2;
  typedef glm::mat3 mat3;
  typedef glm::mat4 mat4;

  typedef glm::dmat2 dmat2;
  typedef glm::dmat3 dmat3;
  typedef glm::dmat4 dmat4;
}


#include <x/common/type.h>
#include <x/common/version.h>


#endif
