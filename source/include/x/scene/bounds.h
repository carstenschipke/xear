/*
 * bounds.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_SCENE_BOUNDS_H_
#define X_SCENE_BOUNDS_H_


#include <x/common.h>


namespace x
{
  namespace scene
  {
    using namespace std;


    struct Bounds
    {
      vec3 origin;
      vec3 extents;
      vec3 dimensions;

      vec3 min;
      vec3 max;


      Bounds()
      {
        dbgi("Create [" << *this << "].");
      }

      Bounds(vec3&& origin, vec3&& extents):
        origin(forward<vec3>(origin)),
        extents(forward<vec3>(extents)),
        dimensions(extents+extents),
        min(origin-extents),
        max(origin+extents)
      {
        dbgi("Create [" << *this << "].");
      }


      Bounds(Bounds&& bounds):
        origin(move(bounds.origin)),
        extents(move(bounds.extents)),
        dimensions(move(bounds.dimensions)),
        min(move(bounds.min)),
        max(move(bounds.max))
      {
        dbgi("Move-construct [" << bounds << "].");
      }

      auto& operator=(Bounds&& bounds)
      {
        dbgi("Move-assign [" << bounds << "].");

        origin=move(bounds.origin);
        extents=move(bounds.extents);
        dimensions=move(bounds.dimensions);
        min=move(bounds.min);
        max=move(bounds.max);

        return *this;
      }


      Bounds(Bounds const&) = delete;
      auto& operator=(Bounds const&) = delete;


      ~Bounds()
      {
        dbgi("Destroy [" << this << "].");
      }


      friend ostream& operator<<(ostream& stream, Bounds const& bounds)
      {
        stream << "Bounds{id: " << &bounds
          << ", origin: " << glm::to_string(bounds.origin)
          << ", extents: " << glm::to_string(bounds.extents)
          << ", dimensions: " << glm::to_string(bounds.dimensions)
          << ", min: " << glm::to_string(bounds.min)
          << ", max: " << glm::to_string(bounds.max)
          << "}";

        return stream;
      }
    };
  }
}


#endif
