/*
 * model.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_SCENE_OBJECT_MODEL_H_
#define X_SCENE_OBJECT_MODEL_H_


#include <x/common.h>
#include <x/scene/object.h>


namespace x
{
  namespace scene
  {
    using namespace std;


    class Model: public Object
    {
      public:
        using Object::Object;
    };
  }
}


#endif
