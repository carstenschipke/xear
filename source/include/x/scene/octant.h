/*
 * octant.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_SCENE_OCTANT_H_
#define X_SCENE_OCTANT_H_


#include <x/common.h>
#include <x/scene/bounds.h>


#define X_SCENE_OCTANT_ALIGNMENT 8
#define X_SCENE_OCTANT_LOOSENESS 1.0F


namespace x
{
  namespace scene
  {
    using namespace std;

    using namespace x::common;


    namespace
    {
      struct Properties
      {
        Properties()
        {
          dbgi("Create [" << *this << "].");
        }


        Properties(Properties&& properties)
        {
          dbgi("Move-construct [" << properties << "].");
        }

        Properties& operator=(Properties&& properties)
        {
          dbgi("Move-assign [" << properties << "].");

          return *this;
        }


        Properties(Properties const&) = delete;
        Properties operator=(Properties const&) = delete;


        ~Properties()
        {
          dbgi("Destroy [" << this << "].");
        }


        friend ostream& operator<<(ostream& stream, Properties const& properties)
        {
          stream << "Properties{id: " << &properties << "}";

          return stream;
        }
      };
    }


    template<typename Attachment>
    class Octant
    {
      public:
        // CONSTRUCTION
        Octant(Bounds& bounds):
          m_children(8), m_bounds(move(bounds))
        {
          dbgi("Create [" << *this << "].");
        }

        Octant(Bounds&& bounds):
          m_children(8), m_bounds(move(bounds))
        {
          dbgi("Create [" << *this << "].");
        }

        Octant(Bounds& bounds, unique_ptr<Attachment>& attachment):
          m_children(8), m_bounds(move(bounds)), m_attachment(move(attachment))
        {
          dbgi("Create [" << *this << "].");
        }

        Octant(Bounds&& bounds, unique_ptr<Attachment>&& attachment):
          m_children(8), m_bounds(move(bounds)), m_attachment(move(attachment))
        {
          dbgi("Create [" << *this << "].");
        }


        // CONSTRUCTION (ROOT)
        Octant(Bounds&& bounds, unique_ptr<Properties>&& properties):
          m_root(this), m_children(8), m_bounds(move(bounds)), m_properties(move(properties))
        {
          dbgi("Create root [" << *this << "].");
        }


        Octant(Octant&& octant):
          m_root(octant.m_root),
          m_parent(octant.m_parent),
          m_children(move(octant.m_children)),
          m_bounds(move(octant.m_bounds)),
          m_attachment(move(octant.m_attachment)),
          m_properties(move(octant.m_properties))
        {
          dbgi("Move-construct [" << octant << "].");

          octant.m_root=nullptr;
          octant.m_parent=nullptr;
        }

        Octant& operator=(Octant&& octant)
        {
          dbgi("Move-assign [" << octant << "].");

          m_root=octant.m_root;
          m_parent=octant.m_parent;
          m_children=move(octant.m_children);
          m_bounds=move(octant.m_bounds);
          m_attachment=move(octant.m_attachment);
          m_properties=move(octant.m_properties);

          octant.m_root=nullptr;
          octant.m_parent=nullptr;

          return *this;
        }


        Octant(Octant const&) = delete;
        Octant& operator=(Octant const&) = delete;


        ~Octant()
        {
          dbgi("Destroy [" << this << "].");
        }


        void add(Bounds& bounds, unique_ptr<Attachment>& attachment)
        {
          add(forward<Bounds>(bounds), forward<unique_ptr<Attachment>>(attachment));
        }

        void add(Bounds&& bounds, unique_ptr<Attachment>&& attachment)
        {
          auto octant=make_unique<Octant<Attachment>>(forward<Bounds>(bounds), forward<unique_ptr<Attachment>>(attachment));
          octant->m_root=m_root;
          octant->m_parent=this;

          m_children.push_back(move(octant));
        }


        friend ostream& operator<<(ostream& stream, Octant<Attachment> const& octant)
        {
          stream << "Octant{id: " << &octant
            << ", bounds: " << octant.m_bounds
            << ", attachment: " << octant.m_attachment.get()
            << "}";

          return stream;
        }


      private:
        Bounds m_bounds;
        // TODO rvalue member lifetime = undefined behavior?
        // Temporary seems to stay alive after destruction.
        //
        // If there is no move/copy construction-free solution, we need a
        // custom allocator or global "memory bank" for some properties.
        //
        // We may want that anyway for locality & contiguous (cache-friendly) memory.
        // that can be utilized by certain operations (e.g. collecting collisions).
        //
        // Bounds&& m_bounds;

        vector<unique_ptr<Octant<Attachment>>> m_children;

        Octant<Attachment>* m_root=nullptr;
        Octant<Attachment>* m_parent=nullptr;

        unique_ptr<Attachment> m_attachment=nullptr;
        unique_ptr<Properties> m_properties=nullptr;


        Properties const* properties()
        {
          return m_root->m_properties;
        }
    };



    template<typename Attachment>
    unique_ptr<Octant<Attachment>> make_octree(Bounds& bounds)
    {
      return make_unique<Octant<Attachment>>(forward<Bounds>(bounds), make_unique<Properties>());
    }

    template<typename Attachment>
    unique_ptr<Octant<Attachment>> make_octree(Bounds&& bounds)
    {
      return make_unique<Octant<Attachment>>(forward<Bounds>(bounds), make_unique<Properties>());
    }
  }
}


#endif
