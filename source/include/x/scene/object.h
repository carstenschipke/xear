/*
 * object.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_SCENE_OBJECT_H_
#define X_SCENE_OBJECT_H_


#include <x/common.h>


namespace x
{
  namespace scene
  {
    using namespace std;


    class Object
    {
      public:
        Object()
        {
          dbgi("Create [" << *this << "].");
        }


        Object(Object const&) = delete;
        Object operator=(Object const&) = delete;


        virtual ~Object()
        {
          dbgi("Destroy [" << this << "].");
        }


        friend ostream& operator<<(ostream& stream, Object const& object)
        {
          stream << "Object{id: " << &object << "}";

          return stream;
        }
    };
  }
}


#endif
