/*
 * version.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_COMMON_VERSION_H_
#define X_COMMON_VERSION_H_


#include <x/common.h>


namespace x
{
  namespace common
  {
    struct Version
    {
      Int major;
      Int minor;
      Int revision;


      friend std::ostream& operator<<(std::ostream& stream, Version const& version)
      {
        stream << version.major << "." << version.minor;

        if(0<version.revision)
          stream << "." << version.revision;

        return stream;
      }
    };
  }
}


#endif
