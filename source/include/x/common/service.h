/*
 * service.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_COMMON_SERVICE_H_
#define X_COMMON_SERVICE_H_


#include <x/common.h>

#include <x/concurrent/task.h>


namespace x
{
  namespace common
  {
    class Service
    {
      public:
        virtual ~Service()
        {

        }


        virtual void setup()
        {

        }

        virtual void teardown()
        {

        }
    };


    class ServiceWorker: public Service, public x::concurrent::BackgroundTask
    {
      public:
        virtual ~ServiceWorker()
        {

        }


        virtual void setup() override = 0;
        virtual void teardown() override = 0;


      protected:
        virtual void run() override = 0;
    };


    class ServiceManagerBackend
    {
      public:
        virtual ~ServiceManagerBackend()
        {

        }


        virtual Service* resolve(String const& name) = 0;

        virtual void create(String const& name, std::unique_ptr<Service> service) = 0;
        virtual void createWorker(String const& name, std::unique_ptr<ServiceWorker> service) = 0;

        virtual void destroy(String const& name) = 0;
    };
  }
}


EXTERN x::common::ServiceManagerBackend* __x_common_service_manager_backend();


namespace x
{
  namespace common
  {
    class ServiceManager
    {
      public:
        template<class ServiceType>
        static ServiceType* resolve(String const& name)
        {
          return static_cast<ServiceType*>(__x_common_service_manager_backend()->resolve(name));
        }

        template<class ServiceTypeImpl>
        static void create(String const& name)
        {
          dbgsi2("Create service [" << name << "].");

          __x_common_service_manager_backend()->create(name, static_cast<std::unique_ptr<Service>>(std::make_unique<ServiceTypeImpl>()));
        }

        template<class ServiceWorkerTypeImpl>
        static void createWorker(String const& name)
        {
          dbgsi2("Create service worker [" << name << "].");

          __x_common_service_manager_backend()->createWorker(name, static_cast<std::unique_ptr<ServiceWorker>>(std::make_unique<ServiceWorkerTypeImpl>()));
        }

        static void destroy(String const& name)
        {
          dbgsi2("Destroy service [" << name << "].");

          __x_common_service_manager_backend()->destroy(name);
        }
    };
  }
}


#endif
