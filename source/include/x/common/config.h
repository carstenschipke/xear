/*
 * config.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_COMMON_CONFIG_H_
#define X_COMMON_CONFIG_H_


#include <map>

#include <x/common.h>

#include <x/io/file.h>
#include <x/io/json.h>


namespace x
{
  namespace common
  {
    using namespace std;

    using namespace x::io;


    class Config
    {
      public:
        static unique_ptr<Config> create(String const& filepath)
        {
          auto config=make_unique<Config>();

          if(!config->load(filepath))
          {
            serr("Unable to load configuration [" << filepath << "].");

            return nullptr;
          }

          return config;
        }


        Boolean load(String const& filepath)
        {
          File file{filepath};

          Json json{file.readText()};

          if(!json.root.isObject())
            return false;

          for(auto& value : json.root)
            m_values[value.key]=value;

          return true;
        }


        Boolean has(String key) const
        {
          return m_values.end()!=m_values.find(key);
        }


        template<typename ValueType>
        ValueType& get(String key, ValueType& defaultValue) const
        {
          auto idx=m_values.find(key);

          if(idx==m_values.end())
            return defaultValue;

          return idx->second.as<ValueType>();
        }

        template<typename ValueType>
        ValueType const& get(String const& key, ValueType const& defaultValue) const
        {
          auto idx=m_values.find(key);

          if(idx==m_values.end())
            return defaultValue;

          return idx->second.as<ValueType>();
        }


        friend std::ostream& operator<<(std::ostream& stream, Config const& config)
        {
          stream << "Config{size: " << config.m_values.size();

          for(auto& entry : config.m_values)
            stream << ", " << entry.first << ": " << to_string(entry.second);

          stream << "}";

          return stream;
        }


      private:
        map<String, Any> m_values;
    };
  }
}


#endif
