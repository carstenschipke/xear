/*
 * type.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_COMMON_TYPE_H_
#define X_COMMON_TYPE_H_


#include <vector>

#include <x/common.h>


namespace x
{
  namespace common
  {
    using namespace std;


    enum Type: IntU
    {
      VOID,
      BOOLEAN,
      DOUBLE,
      FLOAT,
      INT,
      STRING,
      ARRAY,
      OBJECT
    };


    static Type TYPES[]={
      Type::VOID,
      Type::BOOLEAN,
      Type::DOUBLE,
      Type::FLOAT,
      Type::INT,
      Type::STRING,
      Type::ARRAY,
      Type::OBJECT
    };


    struct Object
    {

    };


    struct Void: Object
    {
      public:
        friend ostream& operator<<(ostream& stream, Void const& v)
        {
          stream << "{void}";

          return stream;
        }
    };


    template<typename ValueType> inline Type type_of()
    {
      if(is_same<Boolean, ValueType>())
        return Type::BOOLEAN;
      if(is_same<Double, ValueType>())
        return Type::DOUBLE;
      if(is_same<Float, ValueType>())
        return Type::FLOAT;
      if(is_same<Int, ValueType>())
        return Type::INT;
      if(is_same<String, ValueType>())
        return Type::STRING;

      return Type::VOID;
    }


    template<typename ValueType> inline Boolean is_boolean() { return false; }
    template<> inline Boolean is_boolean<Boolean>() { return true; }


    struct Any: Object
    {
      public:
        struct AbstractHolder
        {
          public:
            virtual ~AbstractHolder()
            {

            }

            virtual AbstractHolder* clone() = 0;
        };


        template<typename ValueType>
        struct Holder: AbstractHolder
        {
          ValueType value;


          Holder(ValueType const& value):
            value(std::move((ValueType&&)value))
          {
            dbgi7("Create.");
          }


          Holder(Holder const& rhs):
            value(rhs.value)
          {
            dbgi7("Copy-construct.");
          }

          Holder& operator=(Holder const& rhs)
          {
            dbgi7("Copy-assign.");

            value=rhs.value;

            return *this;
          }

          Holder(Holder&& rhs):
            value(std::move(rhs.value))
          {
            dbgi7("Move-construct.");
          }

          Holder& operator=(Holder&& rhs)
          {
            dbgi7("Move-assign.");

            value=std::move(rhs.value);

            return *this;
          }


          virtual ~Holder()
          {
            dbgi7("Destroy.");
          }


          virtual AbstractHolder* clone() override
          {
            dbgi7("Clone.");

            return new Holder<ValueType>(value);
          }
        };


        Any():
          m_type(Type::VOID), m_holder(nullptr)
        {
          dbgi7("Create.");
        }

        Any(Type type):
          m_type(type), m_holder(nullptr)
        {
          dbgi7("Create.");
        }

        Any(Type type, AbstractHolder* value):
          m_type(type), m_holder(value)
        {
          dbgi7("Create.");
        }

        // TODO Support arrays.
        template<typename ValueType>
        Any(ValueType const& value):
          m_type(type_of<ValueType>()), m_holder(new Holder<ValueType>(value))
        {
          dbgi7("Create.");
        }


        Any(Any&& rhs):
          m_type(std::move(rhs.m_type)), m_holder(std::move(rhs.m_holder))
        {
          dbgi7("Move-construct.");

          rhs.m_holder=nullptr;
        }

        Any& operator=(Any&& rhs)
        {
          dbgi7("Move-assign.");

          m_type=std::move(rhs.m_type);
          m_holder=std::move(rhs.m_holder);

          return *this;
        }

        Any(Any const& rhs):
          m_type(rhs.m_type), m_holder(rhs.m_holder?rhs.m_holder->clone():nullptr)
        {
          dbgi7("Copy-construct.");
        }

        Any& operator=(Any const& rhs)
        {
          dbgi7("Copy-assign.");

          m_type=rhs.m_type;

          if(rhs.m_holder)
            m_holder=rhs.m_holder->clone();

          return *this;
        }


        virtual ~Any()
        {
          dbgi7("Destroy.");

          if(nullptr!=m_holder)
            delete m_holder;
        }


        static Any null()
        {
          return Any{};
        }

        template<typename ValueType>
        static Any of()
        {
          return Any{type_of<ValueType>(), nullptr};
        }

        template<typename ValueType>
        static Any of(ValueType const& value)
        {
          return Any{type_of<ValueType>(), new Holder<ValueType>(std::move((ValueType&&)value))};
        }

        template<typename ValueType>
        static Any of(ValueType type, ValueType const& value)
        {
          return Any{type, new Holder<ValueType>(std::move((ValueType&&)value))};
        }


        Boolean empty() const
        {
          return Type::VOID==m_type || nullptr==m_holder;
        }

        Type type() const
        {
          return TYPES[m_type];
        }

        // TODO Support OBJECT types / collect neccessary type information without rtti.
        template<typename ValueType>
        Boolean is() const
        {
          return m_type==type_of<ValueType>();
        }

        Boolean is(Type type_) const
        {
          return m_type==type_;
        }

        Boolean isNull() const
        {
          return Type::VOID==m_type;
        }

        Boolean isScalar() const
        {
          return Type::OBJECT!=m_type && Type::ARRAY!=m_type;
        }

        Boolean isNumber() const
        {
          return Type::INT==m_type || Type::FLOAT==m_type || Type::DOUBLE==m_type;
        }

        Boolean isArray() const
        {
          return Type::ARRAY==m_type;
        }

        Boolean isObject() const
        {
          return Type::OBJECT==m_type;
        }


        template<typename ValueType>
        ValueType& as() const
        {
          return ((Holder<ValueType>*)m_holder)->value;
        }

        template<typename ValueType>
        ValueType& as(ValueType const& defaultValue) const
        {
          if(m_holder)
            return ((Holder<ValueType>*)m_holder)->value;

          return defaultValue;
        }

        template<typename ValueType>
        ValueType& as(ValueType& defaultValue) const
        {
          if(m_holder)
            return ((Holder<ValueType>*)m_holder)->value;

          return defaultValue;
        }


      protected:
        Type m_type;
        AbstractHolder* m_holder;
    };


    class AnyArray: public Any
    {
      public:
        AnyArray():
          AnyArray(Type::ARRAY, new Holder<vector<Any>>(vector<Any>{}))
        {
          dbgi7("Create.");
        }

        AnyArray(Type type, AbstractHolder* value):
          Any(type, value)
        {
          dbgi7("Create.");
        }


        virtual ~AnyArray()
        {
          dbgi7("Destroy.");
        }


        template<typename ValueType>
        static AnyArray of()
        {
          return AnyArray{type_of<ValueType>(), new Holder<vector<ValueType>>(vector<ValueType>{})};
        }

        template<typename ValueType>
        static AnyArray of(vector<ValueType> value)
        {
          return AnyArray{type_of<ValueType>(), new Holder<vector<ValueType>>(value)};
        }


        template<typename ValueType>
        vector<ValueType>& as()
        {
          if(m_holder)
            return ((Holder<vector<ValueType>>*)m_holder)->value;

          throw "No value.";
        }


        template<typename ValueType>
        Size size()
        {
          if(m_holder)
            return ((Holder<vector<ValueType>>*)m_holder)->value.size();

          return 0;
        }


        template<typename ValueType>
        typename vector<ValueType>::iterator begin()
        {
          return ((Holder<vector<ValueType>>*)m_holder)->value.begin();
        }

        template<typename ValueType>
        typename vector<ValueType>::iterator end()
        {
          return ((Holder<vector<ValueType>>*)m_holder)->value.end();
        }

        template<typename ValueType>
        typename vector<ValueType>::iterator const begin()
        {
          return ((Holder<vector<ValueType>>*)m_holder)->value.begin();
        }

        template<typename ValueType>
        typename vector<ValueType>::iterator const end()
        {
          return ((Holder<vector<ValueType>>*)m_holder)->value.end();
        }
    };


    template<typename ValueType>
    struct Optional
    {
      public:
        Optional():
          m_set(false)
        {

        }

        Optional(ValueType const& value):
          m_set(true), m_value(value)
        {

        }


        Boolean isset()
        {
          return m_set;
        }

        template<Boolean THROW=false>
        ValueType get()
        {
          if(m_set)
            return m_value;

          if(THROW)
            throw "No value.";

          return ValueType{};
        }

        template<Boolean THROW=false>
        ValueType& get()
        {
          if(false==m_set && THROW)
            throw "No value.";

          return m_value;
        }

        ValueType& get_either(ValueType const& defaultValue)
        {
          if(m_set)
            return m_value;

          return defaultValue;
        }

        ValueType& get_either(ValueType& defaultValue)
        {
          if(m_set)
            return m_value;

          return defaultValue;
        }


      private:
        Boolean m_set=false;
        ValueType m_value;
    };


    inline String to_string(Type type)
    {
      switch(type)
      {
        case Type::BOOLEAN:
          return "boolean";
        case Type::INT:
          return "int";
        case Type::FLOAT:
          return "float";
        case Type::DOUBLE:
          return "double";
        case Type::STRING:
          return "string";
        case Type::VOID:
          return "null";
        case Type::ARRAY:
          return "array";
        case Type::OBJECT:
          return "object";
      }
    }

    inline String to_string(Any const& any)
    {
      switch(any.type())
      {
        case Type::BOOLEAN:
          return std::to_string(any.as<Boolean>());
        case Type::INT:
          return std::to_string(any.as<Int>());
        case Type::FLOAT:
        case Type::DOUBLE:
          return std::to_string(any.as<Double>());
        case Type::STRING:
          return any.as<String>();
        case Type::VOID:
          return "null";
        case Type::ARRAY:
          return "array";
        case Type::OBJECT:
          return "object";
      }
    }
  }
}


#endif
