/*
 * main.cpp
 *
 * @author carsten.schipke@gmail.com
 */
#include <gtest/gtest.h>
#include <fstream>

#include <config.h>
#include <x/common.h>
#include <x/io/json.h>
#include <x/io/resource/json.h>
#include <xear/common/resources.h>
#include <xear/renderer/texture.h>

#include "io/json-config.h"
#include "io/json-scene.h"
#include "misc/slots.h"


Int main(Int argc, Char* argv[])
{
  xear::common::Resources resources;

  resources.registerResourceType(xear::common::ResourceType::SCENE, "file:///tmp/");
  resources.registerResourceType(xear::common::ResourceType::TEXTURE, "file:///tmp/");

  auto location="foo.ktx";

  auto bar=[&]() {
    auto tmp=resources.get<xear::renderer::Texture>(xear::common::ResourceType::TEXTURE, location);

    dbgsi("A " << &tmp);
    dbgsi("A.R " << &(tmp.resource()));
    dbgsi("A.R.U " << &(tmp.resource().source()));
    dbgsi("A.R.U.P " << (tmp.resource().source().path()));

    return tmp;
  }();

  dbgsi("B " << &bar);
  dbgsi("B.R " << &(bar.resource()));
  dbgsi("B.R.U " << &(bar.resource().source()));
  dbgsi("B.R.U.P " << (bar.resource().source().path()));

  auto foo=std::move(bar);

  dbgsi("C " << &foo);
  dbgsi("C.R " << &(foo.resource()));
  dbgsi("C.R.U " << &(foo.resource().source()));
  dbgsi("C.R.U.P " << (foo.resource().source().path()));

  dbgsi("L " << location);


  auto json=resources.get<x::io::resource::Json>(xear::common::ResourceType::SCENE, "foo.json");

  auto& root=json.resolve();

  dbgsi("ROOT " << &root);

  dbgsi("ROOT E? " << json.impl().hasError());
  dbgsi("ROOT EX " << json.impl().error());

  dbgsi("ROOT " << root.isObject());
  dbgsi("ROOT S " << root.size());
  dbgsi("ROOT / T " << root.has("T"));

//  dbgsi("PATH " << tex.resource.uri.path());
//  dbgsi("TYPE " << tex.resource.uri.mimetype());
//  dbgsi("VALD " << tex.resource.isValid());
//  dbgsi("BINY " << tex.resource.isBinary());
//
//  dbgsi("PATH " << foo.resource.uri.path());
//  dbgsi("TYPE " << foo.resource.uri.mimetype());
//  dbgsi("VALD " << foo.resource.isValid());
//  dbgsi("BINY " << foo.resource.isBinary());
//

//  tex.load();
//
//  dbgsi("WIDH " << tex.width);
//  dbgsi("HEIG " << tex.height);
//  dbgsi("MIPS " << tex.mipmap.size());


//  x::io::Uri a{"file://./tmp/foo.txt?foo=bar&bar=foo#bar"};
//  dbgsi(a.scheme());
//  dbgsi(a.principal());
//  dbgsi(a.credentials());
//  dbgsi(a.host());
//  dbgsi(a.path());
//  dbgsi(a.queryString());
//  dbgsi(a.fragment());
//
//  x::io::Uri b{"file://foo@domain.tld/tmp/foo.txt?a=0#bar"};
//  dbgsi(b.scheme());
//  dbgsi(b.principal());
//  dbgsi(b.credentials());
//  dbgsi(b.host());
//  dbgsi(b.path());
//  dbgsi(b.queryString());
//  dbgsi(b.fragment());
//
//  x::io::Uri c{"file:///tmp/foo.txt"};
//  dbgsi(c.scheme());
//  dbgsi(c.principal());
//  dbgsi(c.credentials());
//  dbgsi(c.host());
//  dbgsi(c.path());
//  dbgsi(c.file());
//  dbgsi(c.mimetype());
//  dbgsi(c.queryString());
//  dbgsi(c.fragment());
//
//  x::io::Uri d{"./tmp/foo.txt"};
//  dbgsi(d.scheme());
//  dbgsi(d.principal());
//  dbgsi(d.credentials());
//  dbgsi(d.host());
//  dbgsi(d.path());
//  dbgsi(d.file());
//  dbgsi(d.mimetype());
//  dbgsi(d.queryString());
//  dbgsi(d.fragment());
//
//  x::io::Uri e{"/tmp/foo.txt"};
//  dbgsi(e.scheme());
//  dbgsi(e.principal());
//  dbgsi(e.credentials());
//  dbgsi(e.host());
//  dbgsi(e.path());
//  dbgsi(e.file());
//  dbgsi(e.mimetype());
//  dbgsi(e.queryString());
//  dbgsi(e.fragment());
//
//  x::io::Uri f{"domain.tld:8080/tmp/foo.txt"};
//  dbgsi(f.scheme());
//  dbgsi(f.principal());
//  dbgsi(f.credentials());
//  dbgsi(f.host());
//  dbgsi(f.port());
//  dbgsi(f.path());
//  dbgsi(f.file());
//  dbgsi(f.mimetype());
//  dbgsi(f.queryString());
//  dbgsi(f.fragment());
//
//  x::io::Resources resources;
//
//
//  Resource_File r{"/tmp/foo.json"};
//  dbgsi("RESOURCE " << r.uri.path());
//  dbgsi("RESOURCE " << r.uri.mimetype());
//  dbgsi("RESOURCE " << r.isBinary());
////  dbgsi("RESOURCE " << r.resolve().is<String>());
////  dbgsi("RESOURCE " << r.resolve().as<String>());
//  dbgsi("RESOURCE STREAM " << r.streamInput().get());
//
//  auto resource=resources.get({"/tmp/foo.av"});
//  dbgsi("RESOURCE " << resource->uri.path());
//  dbgsi("RESOURCE " << resource->uri.mimetype());
//  dbgsi("RESOURCE " << resource->isBinary());
//  resource=resources.get({"/tmp/foo.am"});
//  dbgsi("RESOURCE " << resource->uri.path());
//  dbgsi("RESOURCE " << resource->uri.mimetype());
//  dbgsi("RESOURCE " << resource->isBinary());
//  resource=resources.get({"/tmp/foo.jpg"});
//  dbgsi("RESOURCE " << resource->uri.path());
//  dbgsi("RESOURCE " << resource->uri.mimetype());
//  dbgsi("RESOURCE " << resource->isBinary());
//  resource=resources.get({"/tmp/foo.ktx"});
//  dbgsi("RESOURCE " << resource->uri.path());
//  dbgsi("RESOURCE " << resource->uri.mimetype());
//  dbgsi("RESOURCE " << resource->isBinary());
//  resource=resources.get({"/tmp/foo.png"});
//  dbgsi("RESOURCE " << resource->uri.path());
//  dbgsi("RESOURCE " << resource->uri.mimetype());
//  dbgsi("RESOURCE " << resource->isBinary());
//  resource=resources.get({"/tmp/foo.txt"});
//  dbgsi("RESOURCE " << resource->uri.path());
//  dbgsi("RESOURCE " << resource->uri.mimetype());
//  dbgsi("RESOURCE " << resource->isBinary());
//  resource=resources.get({"/tmp/foo.json"});
//  dbgsi("RESOURCE " << resource->uri.path());
//  dbgsi("RESOURCE " << resource->uri.mimetype());
//  dbgsi("RESOURCE " << resource->isBinary());
//  resource=resources.get({"/tmp/foo.csh"});
//  dbgsi("RESOURCE " << resource->uri.path());
//  dbgsi("RESOURCE " << resource->uri.mimetype());
//  dbgsi("RESOURCE " << resource->isBinary());
//  resource=resources.get({"./tmp/foo.fsh"});
//  dbgsi("RESOURCE " << resource->uri.path());
//  dbgsi("RESOURCE " << resource->uri.mimetype());
//  dbgsi("RESOURCE " << resource->isBinary());
//  resource=resources.get({"file://./tmp/foo.gsh"});
//  dbgsi("RESOURCE " << resource->uri.path());
//  dbgsi("RESOURCE " << resource->uri.mimetype());
//  dbgsi("RESOURCE " << resource->isBinary());
//  resource=resources.get({"file:///tmp/foo.vsh"});
//  dbgsi("RESOURCE " << resource->uri.path());
//  dbgsi("RESOURCE " << resource->uri.mimetype());
//  dbgsi("RESOURCE " << resource->isBinary());

//  auto& v=resource->resolve();
//  dbgsi("RESOURCE IS TEXT? " << v.is<String>());
//  dbgsi("RESOURCE TEXT " << v.as<String>());



//  String currentFile{__FILE__};
//  auto idx=currentFile.find_last_of("/");
//
//  String currentDirectory{currentFile.substr(0, idx)};
//
//  x::io::Json json;
//
//  stringstream ss{"foo"};
//  dbgsi("" << ss.get());
//  dbgsi("" << ss.get());
//  dbgsi("" << ss.get());
//  dbgsi("" << ss.get());
//  dbgsi("" << ss.get());
//
//  std::ifstream fs;
//  fs.flags(ios::in|ios::binary);
//  fs.open(currentDirectory+"/io/asset/config.json");
//  dbgsi("" << fs.get());
//  dbgsi("" << fs.get());
//  dbgsi("" << fs.get());
//  dbgsi("" << fs.get());
//  dbgsi("" << fs.get());
//
//  x::io::Resource resource{mime::Type::TEXT_JSON, currentDirectory+"/io/asset/config.json"};
//
//  auto s=resource.inputStream();
//  dbgsi("" << (*s).get());
//  dbgsi("" << (*s).get());
//  dbgsi("" << (*s).get());
//  dbgsi("" << (*s).get());
//  dbgsi("" << (*s).get());


  return EXIT_SUCCESS;

//  ::testing::InitGoogleTest(&argc, argv);

//  return RUN_ALL_TESTS();
}
