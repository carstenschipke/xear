/*
 * json-config.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_JSON_CONFIG_TEST_
#define X_IO_JSON_CONFIG_TEST_


#include <chrono>

#include <x/misc/test.h>

#include <x/io/file.h>
#include <x/io/json.h>


using namespace x;
using namespace x::io;


TEST(x_io_json_config, parse)
{
  String currentFile{__FILE__};
  auto idx=currentFile.find_last_of("/");

  String currentDirectory{currentFile.substr(0, idx)};
  File fileJson{currentDirectory+"/asset/config.json"};

  auto text=fileJson.readText();

  TIMER_BEGIN();
  Json json;
  json << text;

  TIMER_DUMP("Parsed");

  ASSERT_FALSE(json.hasError()) << json.error();

  ASSERT_TRUE(json.root.isObject());

  auto root=json.root.at<String>("web.root", "");

  ASSERT_STREQ("/usr/local/share/xear/web", root.c_str());
}


#endif
