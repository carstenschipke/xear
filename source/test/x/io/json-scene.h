/*
 * json-scene.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_IO_JSON_SCENE_TEST_
#define X_IO_JSON_SCENE_TEST_


#include <chrono>

#include <x/misc/test.h>

#include <x/io/file.h>
#include <x/io/json.h>


using namespace x;
using namespace x::io;


TEST(x_io_json_scene, parse)
{
  String currentFile{__FILE__};
  auto idx=currentFile.find_last_of("/");

  String currentDirectory{currentFile.substr(0, idx)};
  File fileJson{currentDirectory+"/asset/scene.json"};

  auto text=fileJson.readText();

  TIMER_BEGIN();
  Json json;
  json << text;

  TIMER_DUMP("Parsed");

  ASSERT_FALSE(json.hasError()) << json.error();

  auto name=json.root.at<String>("N", "");
  auto type=json.root.at<Int>("T", -1);

  ASSERT_STREQ("world", name.c_str());
  ASSERT_EQ(0, type);

  auto& children=json.root.at("C");

  ASSERT_TRUE(children.isArray());
  ASSERT_EQ(2, children.size());

  auto& node0=children.at(0);
  auto& node1=children.at(1);

  ASSERT_TRUE(node0.isObject());
  ASSERT_TRUE(node1.isObject());

  ASSERT_TRUE(node0.at("N").isNull());

  auto nodeName0=node0.at<String>("N", "default");
  auto nodeType0=node0.at<Int>("T", -1);

  ASSERT_STREQ("default", nodeName0.c_str());
  ASSERT_EQ(1, nodeType0);

  auto& nodeGeometry0=node0.at("G");

  ASSERT_TRUE(nodeGeometry0.is<String>());
  ASSERT_STREQ("sphere", nodeGeometry0.as<String>().c_str());

  auto nodeName1=node1.at<String>("N", "");
  auto nodeType1=node1.at<Int>("T", -1);

  ASSERT_STREQ("sphere1", nodeName1.c_str());
  ASSERT_EQ(1, nodeType1);

  auto nodeGeometry1=node1.at<String>("G", "");

  ASSERT_STREQ("sphere", nodeGeometry1.c_str());

  ASSERT_TRUE(node0.has("P"));
  ASSERT_TRUE(node0.has("R"));
  ASSERT_TRUE(node0.has("S"));

  auto& nodePosition0=node0.at("P");

  ASSERT_TRUE(nodePosition0.isArray());

  auto nodePositionVec0=vec3{
    nodePosition0.at<Float>(0, 0.0f),
    nodePosition0.at<Float>(1, 0.0f),
    nodePosition0.at<Float>(2, 0.0f)};

  ASSERT_FLOAT_EQ(15.0f, nodePositionVec0.x);
  ASSERT_FLOAT_EQ(0.0f, nodePositionVec0.y);
  ASSERT_FLOAT_EQ(-50.0f, nodePositionVec0.z);

  auto& nodeScale0=node0.at("S");

  ASSERT_TRUE(nodeScale0.isArray());

  auto nodeScaleVec0=vec3{
    nodeScale0.at<Float>(0, 1.0f),
    nodeScale0.at<Float>(1, 1.0f),
    nodeScale0.at<Float>(2, 1.0f)};

  ASSERT_FLOAT_EQ(15.0f, nodeScaleVec0.x);
  ASSERT_FLOAT_EQ(15.0f, nodeScaleVec0.y);
  ASSERT_FLOAT_EQ(15.0f, nodeScaleVec0.z);

  auto& nodeRotation0=node0.at("R");

  ASSERT_TRUE(nodeRotation0.isArray());

  auto nodeRotationVec0=vec3{
    nodeRotation0.at<Float>(0, 0.0f),
    nodeRotation0.at<Float>(1, 0.0f),
    nodeRotation0.at<Float>(2, 0.0f)};

  ASSERT_FLOAT_EQ(0.0f, nodeRotationVec0.x);
  ASSERT_FLOAT_EQ(0.0f, nodeRotationVec0.y);
  ASSERT_FLOAT_EQ(0.0f, nodeRotationVec0.z);


  ASSERT_TRUE(node1.has("P"));
  ASSERT_TRUE(node1.has("R"));
  ASSERT_TRUE(node1.has("S"));

  auto& nodePosition1=node1.at("P");

  ASSERT_TRUE(nodePosition1.isArray());

  auto nodePositionVec1=vec3{
    nodePosition1.at<Float>(0, 0.0f),
    nodePosition1.at<Float>(1, 0.0f),
    nodePosition1.at<Float>(2, 0.0f)};

  ASSERT_FLOAT_EQ(-25.0f, nodePositionVec1.x);
  ASSERT_FLOAT_EQ(0.0f, nodePositionVec1.y);
  ASSERT_FLOAT_EQ(-70.0f, nodePositionVec1.z);

  auto& nodeScale1=node1.at("S");

  ASSERT_TRUE(nodeScale1.isArray());

  auto nodeScaleVec1=vec3{
    nodeScale1.at<Float>(0, 1.0f),
    nodeScale1.at<Float>(1, 1.0f),
    nodeScale1.at<Float>(2, 1.0f)};

  ASSERT_FLOAT_EQ(15.0f, nodeScaleVec1.x);
  ASSERT_FLOAT_EQ(15.0f, nodeScaleVec1.y);
  ASSERT_FLOAT_EQ(15.0f, nodeScaleVec1.z);

  auto& nodeRotation1=node1.at("R");

  ASSERT_TRUE(nodeRotation1.isArray());

  auto nodeRotationVec1=vec3{
    nodeRotation1.at<Float>(0, 0.0f),
    nodeRotation1.at<Float>(1, 0.0f),
    nodeRotation1.at<Float>(2, 0.0f)};

  ASSERT_FLOAT_EQ(0.0f, nodeRotationVec1.x);
  ASSERT_FLOAT_EQ(0.0f, nodeRotationVec1.y);
  ASSERT_FLOAT_EQ(0.0f, nodeRotationVec1.z);

  auto& nodeMeshes0=node0.at("C");
  auto& nodeMeshes1=node0.at("C");

  ASSERT_TRUE(nodeMeshes0.isArray());
  ASSERT_TRUE(nodeMeshes1.isArray());

  for(auto& mesh : nodeMeshes0)
  {
    ASSERT_EQ(2, mesh.at<Int>("T", 0));
    ASSERT_STREQ("sphere-0-gold", mesh.at<String>("N", "").c_str());
    ASSERT_STREQ("pbs", mesh.at<String>("M", "").c_str());
  }

  for(auto& mesh : nodeMeshes1)
  {
    ASSERT_EQ(2, mesh.at<Int>("T", 0));
    ASSERT_STREQ("sphere-0-gold", mesh.at<String>("N", "").c_str());
    ASSERT_STREQ("pbs", mesh.at<String>("M", "").c_str());
  }
}


#endif
