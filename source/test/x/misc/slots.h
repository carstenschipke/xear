/*
 * slots.h
 *
 * @author carsten.schipke@gmail.com
 */
#ifndef X_MISC_SLOTS_TEST_
#define X_MISC_SLOTS_TEST_


#include <gtest/gtest.h>


#include <x/misc/slots.h>


using namespace x::misc;


TEST(x_misc_slots, aquire)
{
  Slots32 slots0;

  for(auto i=0; i<32; i++)
    EXPECT_EQ(i, slots0.acquire());

  slots0.lose(10);
  EXPECT_EQ(10, slots0.acquire());

  slots0.lose(0);
  EXPECT_EQ(0, slots0.acquire());

  slots0.lose(31);
  EXPECT_EQ(31, slots0.acquire());

  slots0.lose(15);
  EXPECT_EQ(15, slots0.acquire());


  Slots32 slots1;

  for(auto i=0; i<16; i++)
    EXPECT_EQ(i, slots1.acquire());

  slots1.lose(10);
  slots1.lose(11);

  EXPECT_EQ(10, slots1.acquire());
  EXPECT_EQ(11, slots1.acquire());

  EXPECT_EQ(16, slots1.acquire());
  EXPECT_EQ(17, slots1.acquire());
  EXPECT_EQ(18, slots1.acquire());
}


#endif
