#define layout plain


precision highp float;
precision lowp int;


const int FLAG_TRANSFORM=2;
const int FLAG_SHADE=4;
const int FLAG_TEXTURE=8;
const int FLAG_TEXTURE_BUFFER=16;


uniform int u_intFlags;


uniform mat3 u_matNormal;
uniform mat4 u_matModel;
uniform mat4 u_matViewProjection;


in vec3 a_vecPosition;
in vec4 a_vecTexCoords;
in vec3 a_vecNormal;
in vec3 a_vecTangent;
in vec3 a_vecBiTangent;


out vec4 m_vecTexCoords;
out vec3 m_vecNormal;
out vec3 m_vecTangent;
out vec3 m_vecBiTangent;


void main()
{
  vec4 vecPosition=vec4(a_vecPosition, 1.0);

  if(0<(u_intFlags&FLAG_TRANSFORM))
    vecPosition=u_matViewProjection*u_matModel*vecPosition;

  if(0<(u_intFlags&FLAG_SHADE))
  {
    m_vecNormal=u_matNormal*a_vecNormal;
    m_vecTangent=u_matNormal*a_vecTangent;
    m_vecBiTangent=u_matNormal*a_vecBiTangent;
  }

  m_vecTexCoords=a_vecTexCoords;

  gl_Position=vecPosition;
}
