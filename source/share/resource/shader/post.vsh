#define layout plain


precision highp float;
precision lowp int;


in vec3 a_vecPosition;
in vec4 a_vecTexCoords;
in vec3 a_vecNormal;
in vec3 a_vecTangent;
in vec3 a_vecBiTangent;


void main()
{
  gl_Position=vec4(a_vecPosition, 1.0);
}
