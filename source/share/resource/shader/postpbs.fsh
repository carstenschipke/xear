precision highp float;
precision lowp int;


const float M_PI=3.1415926;

const vec3 ALBEDO_CHRYSTAL=vec3(0.1111111, 0.1111111, 0.1111111);
const vec3 ALBEDO_GLASS=vec3(0.04,  0.04,  0.04);
const vec3 ALBEDO_SILVER=vec3(0.971519, 0.959915, 0.915324);
const vec3 ALBEDO_COPPER=vec3(0.955008, 0.637427, 0.538163);
const vec3 ALBEDO_GOLD=vec3(1, 0.765557, 0.336057);
const vec3 ALBEDO_TITANIUM=vec3(0.541931, 0.496791, 0.449419);
const vec3 ALBEDO_CHROMIUM=vec3(0.549585, 0.556114, 0.554256);


uniform sampler2D u_smpTextureAlbedo;
uniform sampler2D u_smpTextureSpecular;
uniform sampler2D u_smpTextureNormal;
uniform sampler2D u_smpTextureEnvironment;

uniform vec4 u_vecViewport;


uniform mat3 u_matNormal;
uniform mat4 u_matModel;
uniform mat4 u_matViewProjection;


// 0X : Light Position X
// 0Y : Light Position Y
// 0Z : Light Position Z
// 1X : Light Direction X
// 1Y : Light Direction Y
// 1Z : Light Direction Z
// 2X : Light Color Red
// 2Y : Light Color Green
// 2Z : Light Color Blue
// 3X : Light Intesity
// 3Y : Ambient Light Intensity
// 3Z : Reflection Intensity
uniform mat4 u_matLight0;

// 0X : Camera Position X
// 0Y : Camera Position Y
// 0Z : Camera Position Z
// 1X : Camera Direction X
// 1Y : Camera Direction Y
// 1Z : Camera Direction Z
// 2X : Camera Property A
// 2Y : Camera Property B
// 2Z : Camera Property C
uniform mat3 u_matCamera;

// R  : SUBSURFACE
// G  : SPECULAR
// B  : SPECULAR TINT
// A  : ANISOTROPIC
uniform sampler2D u_smpTextureSurface0;

// R  : SHEEN
// G  : SHEEN TINT
// B  : CLEARCOAT
// A  : CLEARCOAT GLOSS
uniform sampler2D u_smpTextureSurface1;


out vec4 FragColor;


float sqr(float x)
{
  return x*x;
}

float normal_distribution_gtr1(float dotNormalHalf, float alpha)
{
  if(1.0<=alpha)
    return 1.0/M_PI;

  float tmp=1.0+(alpha-1.0)*dotNormalHalf*dotNormalHalf;

  return (alpha-1.0)/(M_PI*log(alpha)*tmp);
}

float normal_distribution_gtr2(float dotNormalHalf, float alpha)
{
  float tmp=1.0+(alpha-1.0)*dotNormalHalf*dotNormalHalf;

  return alpha/(M_PI*tmp*tmp);
}

float normal_distribution_gtr2_anisotropic(float dotNormalHalf, float dotHalfTangent, float dotHalfBiTangent, float anisotropicX, float anisotropicY)
{
  return 1.0/(M_PI*anisotropicX*anisotropicY*sqr(sqr(dotHalfTangent/anisotropicX)+sqr(dotHalfBiTangent/anisotropicY)+dotNormalHalf*dotNormalHalf));
}

float fresnel_schlick(float dotXY)
{
  float m=clamp(1.0-dotXY, 0.0, 1.0);
  float m2=m*m;

  return m2*m2*m;
}

vec3 specular_fresnel_none(vec3 vecSpecularColor, float alpha, vec3 vecHalf, vec3 vecViewDirection)
{
  return vecSpecularColor;
}

vec3 specular_fresnel_cook_torrance(vec3 vecSpecularColor, float alpha, vec3 vecHalf, vec3 vecViewDirection)
{
  vec3 n=(1.0+sqrt(vecSpecularColor))/(1.0-sqrt(vecSpecularColor));

  float c=clamp(dot(vecViewDirection, vecHalf), 0.0, 1.0);
  vec3 g=sqrt(n*n+c*c-1.0);

  vec3 part1=(g-c)/(g+c);
  vec3 part2=((g+c)*c-1.0)/((g-c)*c+1.0);

  return max(vec3(0.0), 0.5*part1*part1*(1.0+part2*part2));
}

vec3 specular_fresnel_schlick(vec3 vecSpecularColor, float alpha, vec3 vecHalf, vec3 vecViewDirection)
{
  return vecSpecularColor+(max(vec3(1.0)-alpha, vecSpecularColor)-vecSpecularColor)*pow((1.0-clamp(dot(vecViewDirection, vecHalf), 0.0, 1.0)), 5.0);
}

float geometry_ggx(float dotNormalX, float alpha)
{
  float alpha2=alpha*alpha;
  float dotNormalX2=dotNormalX*dotNormalX;

  return 1.0/(dotNormalX+sqrt(alpha2+dotNormalX2-alpha2*dotNormalX2));
}

vec3 mon2lin(vec3 color)
{
  return vec3(pow(color.x, 2.2), pow(color.y, 2.2), pow(color.z, 2.2));
}


// Adoption of Disney BRDF
vec3 BRDF(vec3 vecAlbedoColor, vec3 vecSpecularColor, float metallic, float alpha, vec3 vecViewDirection, vec3 vecLightDirection, vec3 vecNormal, vec3 vecTangent, vec3 vecBiTangent, out vec3 vecSpecularColorOut, out vec3 vecDiffuseColorOut)
{
  float dotNormalLight=clamp(dot(vecNormal, vecLightDirection), 0.0, 1.0);
  float dotNormalView=clamp(dot(vecNormal, vecViewDirection), 0.0, 1.0);

  vec3 vecHalf=normalize(vecLightDirection+vecViewDirection);

  float dotNormalHalf=clamp(dot(vecNormal, vecHalf), 0.0, 1.0);
  float dotViewHalf=clamp(dot(vecViewDirection, vecHalf), 0.0, 1.0);
  float dotLightView=clamp(dot(vecLightDirection, vecViewDirection), 0.0, 1.0);
  float dotLightHalf=clamp(dot(vecLightDirection, vecHalf), 0.0, 1.0);

  vecSpecularColor=mon2lin(vecSpecularColor);

  float luminance=0.3*vecSpecularColor.r
    +0.6*vecSpecularColor.g
    +0.1*vecSpecularColor.b;

  // R  : SUBSURFACE
  // G  : SPECULAR
  // B  : SPECULAR TINT
  // A  : ANISOTROPIC
  vec4 smpSurface0;//=texture(u_smpTextureSurface0, m_vecTexCoord0);
  // R  : SHEEN
  // G  : SHEEN TINT
  // B  : CLEARCOAT
  // A  : CLEARCOAT GLOSS
  vec4 smpSurface1;//=texture(u_smpTextureSurface1, m_vecTexCoord0);

  // TODO Remove dummies / bake maps..
  smpSurface0=vec4(0.0, 1.0, 1.0, 1.0);
  smpSurface1=vec4(0.0, 0.0, 0.0, 0.0);


  vec3 vecSpecularTintColor=0.0<luminance?vecSpecularColor/luminance:vec3(1.0);
  vecSpecularColorOut=mix(smpSurface0.g*0.08*mix(vec3(1.0), vecSpecularTintColor, smpSurface0.b), vecSpecularColor, metallic);

  vec3 vecSheenColor=mix(vec3(1.0), vecSpecularTintColor, smpSurface1.g);

  // Diffuse fresnel
  float fresnelLight=fresnel_schlick(dotNormalLight);
  float fresnelView=fresnel_schlick(dotNormalView);
  float fresnelDiffuse90=2.0*dotLightHalf*dotLightHalf*alpha+0.5;
  float fresnelDiffuse=mix(1.0, fresnelDiffuse90, fresnelLight)*mix(1.0, fresnelDiffuse90, fresnelView);

  float fresnelSubSurface90=dotLightHalf*dotLightHalf*alpha;
  float fresnelSubSurface=mix(1.0, fresnelSubSurface90, fresnelLight)*mix(1.0, fresnelSubSurface90, fresnelView);
  float subSurface=1.25*(fresnelSubSurface*(1.0/(dotNormalLight+dotNormalView+0.0001)-0.5)+0.5);

  // Specular fresnel
  float aspect=sqrt(1.0-smpSurface0.a*0.9);
  float anisotropicX=max(0.001, sqr(alpha)/aspect);
  float anisotropicY=max(0.001, sqr(alpha)*aspect);

  float normalDistribution;

  if(0.1>smpSurface0.a)
    normalDistribution=normal_distribution_gtr2(dotNormalHalf, alpha);
  else
    normalDistribution=normal_distribution_gtr2_anisotropic(dotNormalHalf, dot(vecHalf, vecTangent), dot(vecHalf, vecBiTangent), anisotropicX, anisotropicY);

  float fresnelHalf=fresnel_schlick(dotLightHalf);

  vec3 fresnelSpecular=mix(vecSpecularColorOut, vec3(1.0), fresnelHalf);

  // Geometry
  float alphaGeometry=sqr(0.5*alpha+0.5);
  float geometry=geometry_ggx(dotNormalLight, alphaGeometry)*geometry_ggx(dotNormalView, alphaGeometry);

  // Sheen fresnel
  vec3 fresnelSheen=fresnelHalf*smpSurface1.r*vecSheenColor;

  // clear coat
  float clearcoatNormalDistribution=normal_distribution_gtr1(dotNormalHalf, mix(0.1, 0.001, smpSurface1.a));
  float clearcoatFresnel=mix(0.04 /* albedo? */, 1.0, fresnelHalf);
  float clearcoatGeometry=geometry_ggx(dotNormalLight, 0.25)*geometry_ggx(dotNormalView, 0.25);

  vecDiffuseColorOut=((1.0/M_PI)*mix(fresnelDiffuse, subSurface, smpSurface0.r)*vecAlbedoColor+fresnelSheen)*(1.0-metallic);

  return (vecDiffuseColorOut
      +geometry*fresnelSpecular*normalDistribution
      +0.25*smpSurface1.b*clearcoatGeometry*clearcoatFresnel*clearcoatNormalDistribution)
    *dotNormalLight;
}


void computeTangentVectors(vec3 normal, out vec3 tangent, out vec3 biTangent)
{
  tangent=abs(normal.x)<0.999?vec3(1.0, 0.0, 0.0):vec3(0.0, 1.0, 0.0);

  tangent=normalize(cross(normal, tangent));
  biTangent=normalize(cross(normal, tangent));
}


void main(void)
{
  vec2 vecXY=gl_FragCoord.xy/u_vecViewport.zw;

  vec4 vecAlbedo=texture(u_smpTextureAlbedo, vecXY);
  vec4 vecSpecular=texture(u_smpTextureSpecular, vecXY);

  vec4 vecTmp=texture(u_smpTextureNormal, vecXY);

  vec3 vecTangent;
  vec3 vecBiTangent;
  vec3 vecNormal=vecTmp.xyz;

  computeTangentVectors(vecNormal, vecTangent, vecBiTangent);

  vecNormal=normalize(vecNormal+(vecNormal.x*vecTangent+vecNormal.y*vecBiTangent));

  vec4 vecPosition=vec4(vecXY.x, vecXY.y, vecTmp.w, 1.0);

  //vecNormal=u_matNormal*vecNormal;
  //vecTangent=u_matNormal*vecTangent;
  //vecBiTangent=u_matNormal*vecBiTangent;

  mat3 matTangentTranspose=transpose(mat3(vecTangent, vecBiTangent, vecNormal));

  vec3 vecVertexPosition=normalize(matTangentTranspose*vecPosition.xyz);

  vec3 vecCameraDirection=normalize(matTangentTranspose*vec3(u_matCamera[1]));
  vecCameraDirection=vecCameraDirection-vecVertexPosition;

  vec3 vecLight0Position=normalize(matTangentTranspose*vec3(u_matLight0[0]));
  vec3 vecLight0Direction=normalize(matTangentTranspose*vec3(u_matLight0[1]));

  float light0Distance=distance(vecLight0Position, vecVertexPosition);
  float light0Attenuation=(M_PI/(light0Distance*light0Distance));

  vec3 vecLight0Color=vec3(u_matLight0[2]);
  vec3 vecLight0Properties=vec3(u_matLight0[3]);

  float metallic=1.0/vecAlbedo.w;
  float roughness=1.0/vecSpecular.w;

  float alpha=max(0.001, roughness*roughness);

  // vec3 vecAlbedoColor=vecAlbedo.rgb*(1.0-u_vecSurface.z);

  // gamma correction
  vec3 vecAlbedoColor=pow(vecAlbedo.rgb, vec3(2.2));

  vec3 vecLight0SpecularColor=vec3(0.0);
  vec3 vecLight0DiffuseColor=vec3(0.0);

  vec3 vecLight0SpecularFresnel=BRDF(vecAlbedoColor, vecSpecular.rgb, metallic, alpha, vecCameraDirection, vecLight0Direction, vecNormal, vecTangent, vecBiTangent, vecLight0SpecularColor, vecLight0DiffuseColor);

  // Environment reflection - Specular fresnel
  vec3 vecLight0SpecularFresnelEnvironment=specular_fresnel_cook_torrance(vecLight0SpecularColor, alpha, vecNormal, vecCameraDirection);

  // Looks quite good currently. Though, still not sure yet where diffuse color is really supposed to be added.
  vecLight0DiffuseColor=clamp(vecLight0DiffuseColor, 0.0, 1.0);

  // Light 0 Intensity
  FragColor=vec4(light0Attenuation*vecLight0Properties.x*vecLight0SpecularFresnel
      // Light 0 Environment Reflection Intensity
      +vecLight0SpecularFresnelEnvironment*vec3(0.8, 0.8, 0.8)/*vecEnvironmentColor*/*vecLight0Properties.z
      // Light 0 Ambient Intensity
      +vecLight0DiffuseColor*/*vecIrradiance**/vecLight0Properties.y,
    1.0);
}
