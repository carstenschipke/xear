precision highp float;
precision lowp int;


const int FLAG_TRANSFORM=2;
const int FLAG_SHADE=4;
const int FLAG_TEXTURE=8;
const int FLAG_TEXTURE_BUFFER=16;
const int FLAG_WIREFRAME=32;
const int FLAG_COLOR=64;
const int FLAG_RED=128;
const int FLAG_GREEN=256;
const int FLAG_BLUE=512;


uniform int u_intFlags;
uniform int u_intLights;


uniform sampler2D u_smpTextureLights;

uniform sampler2D u_smpTextureAlbedo;
uniform sampler2D u_smpTextureBuffer;


in vec4 m_vecTexCoords;
in vec3 m_vecNormal;
in vec3 m_vecTangent;
in vec3 m_vecBiTangent;


out vec4 FragColor;


vec4 color()
{
  if(0<(u_intFlags&FLAG_RED))
    return vec4(1.0, 0.0, 0.0, 1.0);

  if(0<(u_intFlags&FLAG_GREEN))
    return vec4(0.0, 1.0, 0.0, 1.0);

  if(0<(u_intFlags&FLAG_BLUE))
    return vec4(0.0, 0.0, 1.0, 1.0);

  return vec4(1.0, 1.0, 1.0, 1.0);
}


void main()
{
  vec4 vecFragColor;

  if(0<(u_intFlags&FLAG_TEXTURE))
  {
    if(0<(u_intFlags&FLAG_TEXTURE_BUFFER))
      vecFragColor=texture(u_smpTextureBuffer, m_vecTexCoords.xy);
    else
      vecFragColor=texture(u_smpTextureAlbedo, m_vecTexCoords.xy);
  }
  else
  {
    if(0<(u_intFlags&FLAG_COLOR))
      vecFragColor=color();
    else
      vecFragColor=m_vecTexCoords;
  }

  if(0<(u_intFlags&FLAG_SHADE))
  {
    // TODO Implement ...
  }


  // Gamma correction
  FragColor=vec4(pow(vecFragColor.rgb, vec3(1.0/2.2)), vecFragColor.a);
}
