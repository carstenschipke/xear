#define layout plain


precision highp float;
precision lowp int;


uniform mat3 u_matNormal;
uniform mat4 u_matModel;
uniform mat4 u_matViewProjection;


in vec3 a_vecPosition;
in vec2 a_vecTexCoord0;
in vec3 a_vecNormal;
in vec3 a_vecTangent;
in vec3 a_vecBiTangent;


void main()
{
  gl_Position=vec4(a_vecPosition, 1.0);
}
