precision highp float;
precision lowp int;


const int FLAG_TRANSFORM=2;
const int FLAG_SHADE=4;
const int FLAG_TEXTURE=8;
const int FLAG_TEXTURE_BUFFER=16;
const int FLAG_WIREFRAME=32;
const int FLAG_COLOR=64;
const int FLAG_RED=128;
const int FLAG_GREEN=256;
const int FLAG_BLUE=512;


uniform int u_intFlags;


uniform sampler2D u_smpTextureAlbedo;
uniform sampler2D u_smpTextureSpecular;
uniform sampler2D u_smpTextureNormal;

uniform sampler2D u_smpTextureBuffer;


in float m_depth;
in vec4 m_vecTexCoords;
in vec3 m_vecNormal;
in vec3 m_vecTangent;
in vec3 m_vecBiTangent;


layout (location=0) out vec4 albedo;
layout (location=1) out vec4 specular;
layout (location=2) out vec4 normal;
layout (location=3) out vec4 lightmap;


vec4 color()
{
  if(0<(u_intFlags&FLAG_RED))
    return vec4(1.0, 0.0, 0.0, 1.0);

  if(0<(u_intFlags&FLAG_GREEN))
    return vec4(0.0, 1.0, 0.0, 1.0);

  if(0<(u_intFlags&FLAG_BLUE))
    return vec4(0.0, 0.0, 1.0, 1.0);

  return vec4(1.0, 1.0, 1.0, 1.0);
}


void main()
{
  if(0<(u_intFlags&FLAG_TEXTURE))
  {
    if(0<(u_intFlags&FLAG_TEXTURE_BUFFER))
      albedo=texture(u_smpTextureBuffer, m_vecTexCoords.xy);
    else
      albedo=texture(u_smpTextureAlbedo, m_vecTexCoords.xy);
  }
  else
  {
    if(0<(u_intFlags&FLAG_COLOR))
      albedo=color();
    else
      albedo=m_vecTexCoords;
  }

  if(0<(u_intFlags&FLAG_SHADE))
  {
    mat3 tangentToWorld=mat3(m_vecTangent, m_vecBiTangent, m_vecNormal);

    normal=vec4(10.0*tangentToWorld*texture(u_smpTextureNormal, m_vecTexCoords.xy).xyz, 0.01*m_depth);

    // TODO Implement.
    lightmap=vec4(m_vecTangent, 1.0);

    specular.rgb=texture(u_smpTextureSpecular, m_vecTexCoords.xy).rgb;
    specular.a=0.2;
  }
  else
  {
    normal=vec4(0.0, 0.0, 0.0, 0.1*m_depth);
    lightmap=vec4(0.0);
    specular=vec4(0.0);
  }
}
