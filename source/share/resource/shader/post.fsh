precision highp float;
precision lowp int;


uniform sampler2D u_smpTextureAlbedo;
uniform sampler2D u_smpTextureSpecular;
uniform sampler2D u_smpTextureNormal;
uniform sampler2D u_smpTextureEnvironment;
uniform sampler2D u_smpTextureDepth;

uniform vec4 u_vecViewport;


out vec4 FragColor;


void main()
{
  vec2 vecXY=gl_FragCoord.xy/u_vecViewport.zw;

  vec4 vecAlbedo=texture(u_smpTextureAlbedo, vecXY);
  vec4 vecSpecular=texture(u_smpTextureSpecular, vecXY);

  vec4 vecNormal=texture(u_smpTextureNormal, vecXY);
  vec4 vecPosition=vec4(vecNormal.a, vecNormal.a, vecNormal.a, 1.0);
  vec4 vecDepth=texture(u_smpTextureDepth, vecXY);


  if(0.5<vecXY.x)
  {
    if(0.5<vecXY.y)
    {
      FragColor=vecAlbedo;
    }
    else
    {
      FragColor=vecSpecular;
    }
  }
  else
  {
    if(0.5<vecXY.y)
    {
      FragColor=vecNormal;
    }
    else
    {
      FragColor=vecPosition;
    }
  }
}
