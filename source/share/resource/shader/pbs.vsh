#define layout plain


precision highp float;
precision lowp int;


const int FLAG_TRANSFORM=2;
const int FLAG_TEXTURE=4;
const int FLAG_SHADE=8;
const int FLAG_TEXTURE_BUFFER=16;


const float M_PI=3.1415926;


uniform int u_intFlags;
uniform int u_intLights;


uniform mat4 u_matModel;
uniform mat3 u_matNormal;
uniform mat4 u_matViewProjection;

uniform mat3 u_matCamera;

// uniform mat4 u_matLight0;
uniform sampler2D u_smpTextureLights;


in vec3 a_vecPosition;
in vec4 a_vecTexCoords;
in vec3 a_vecNormal;
in vec3 a_vecTangent;
in vec3 a_vecBiTangent;


out vec4 m_vecTexCoords;
out vec3 m_vecNormal;
out vec3 m_vecTangent;
out vec3 m_vecBiTangent;

out vec3 m_vecCameraDirection;
out vec3 m_vecLight0Direction;
out float m_light0Attenuation;


void main()
{
  vec4 vecPosition=vec4(a_vecPosition, 1.0);

  if(0<(u_intFlags&FLAG_TRANSFORM))
    vecPosition=u_matViewProjection*u_matModel*vecPosition;

  if(0<(u_intFlags&FLAG_SHADE))
  {
    m_vecNormal=normalize(u_matNormal*a_vecNormal);
    m_vecTangent=normalize(u_matNormal*a_vecTangent);
    m_vecBiTangent=normalize(u_matNormal*a_vecBiTangent);

    mat3 matTangentTranspose=transpose(mat3(m_vecTangent, m_vecBiTangent, m_vecNormal));

    // vec3 tmp;
    // vec3 vecLight0Direction=vec3(u_matLight0[1]);
    // tmp.x=dot(m_vecTangent, vecLight0Direction);
    // tmp.y=dot(m_vecBiTangent, vecLight0Direction);
    // tmp.z=dot(m_vecNormal, vecLight0Direction);
    // m_vecLight0Direction=normalize(tmp);

    vec3 vecLightPosition=vec3(0.0, 30.0, -40.0);
    vec3 vecLightDirection=vec3(0.0, 0.0, -50.0);

    vec3 vecCameraDirection=normalize(matTangentTranspose*vec3(u_matCamera[1]));
    vec3 vecLight0Position=normalize(matTangentTranspose*vecLightPosition);
    // vec3 vecLight0Direction=matTangentTranspose*vecLightDirection);
    vec3 vecVertexPosition=normalize(matTangentTranspose*vec3(u_matModel*vec4(a_vecPosition, 1.0)));

    m_vecCameraDirection=vecCameraDirection;
    // FIXME matTangentTranspose includes model rotation = rotates the light = wrong.
    m_vecLight0Direction=vecLight0Position-vecVertexPosition;

    float light0Distance=0.1f*distance(vecLight0Position, vecVertexPosition);
    // m_light0Attenuation=(M_PI/(light0Distance*light0Distance));
    m_light0Attenuation=1.0/light0Distance;
  }

  m_vecTexCoords=a_vecTexCoords;

  gl_Position=vecPosition;
}
