

  function update()
  {
    rdi_clear();

    scene_update();
  }

  function render()
  {
    rdi_flags(14);
    // rdi_path_draw("path");
    // rdi_draw("box");

    // if(marker_active("xear"))
    scene_render();
  }


  function init()
  {
    log("Initialize.");

    marker_create("xear", "marker/xear.png");

    rdi_material_create("pbs");
    rdi_material_use("pbs");

//    rdi_path_create("path", "M0,0 L40,0 L40,40 L0,40 z");

//          rdi_geometry_create("box",
//            "LINE_LOOP",
//            "plain",
//            [0xffffffff, 0, 1, 2, 3],
//            [
//              -1.0f, -1.0f, 0.0f,  1.0f, 1.0f,  0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
//               1.0f, -1.0f, 0.0f,  0.0f, 1.0f,  0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
//               1.0f,  1.0f, 0.0f,  0.0f, 0.0f,  1.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,
//              -1.0f,  1.0f, 0.0f,  1.0f, 0.0f,  0.0f, 1.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f,  0.0f, 0.0f, 0.0f
//            ]
//          );

    scene_create("world");
  }
