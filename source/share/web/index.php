<?


  $script=__DIR__.'/../script/default/main.js';


  if(isset($_POST['content']))
    file_put_contents($script, $_POST['content']);


  $content=file_get_contents($script);


?><!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8"/>
    <title>xear</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,minimal-ui"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <link href="/css/common.css" type="text/css" rel="stylesheet" media="all">
  </head>
  <body>
    <div id="editor">
      <form enctype="application/x-www-form-urlencoded" accept-charset="utf-8" method="POST">
        <textarea name="content"><?= $content; ?></textarea>
        <br />
        <button type="submit">Submit</button>
      </form>
    </div>
  </body>
</html>
