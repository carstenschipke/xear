

  /**
   * xear
   *
   * @package js
   * @subpackage xear
   *
   * @author carsten.schipke@gmail.com
   */
  xear=function(module_, callbackSuccess_)
  {
    if(module_ in xear)
    {
      if(callbackSuccess_)
      {
        xear.async(function() {
          callbackSuccess_(xear[module_]);
        });
      }
    }
    else
    {
      var module="__xear_"+module_;

      xear.info("xear", "Resolve module.", module_);

      if(!(module in window))
        xear.include(module_);

      if(module in window)
      {
        xear.info("xear", "Instantiate module.", module_);

        xear[module_]=new window[module]();
      }

      var moduleExports=module+"_export";

      if(moduleExports in window)
      {
        xear.info("xear", "Propagate module exports.", module_);

        window[moduleExports]();
      }

      if(module_ in xear)
      {
        if("init" in xear[module_])
        {
          xear.info("xear", "Initialize module.", module_);

          if(callbackSuccess_)
          {
            xear.async(function() {
              xear[module_].init();
              callbackSuccess_(xear[module_]);
            });
          }
          else
          {
            xear[module_].init();
          }
        }
        else if(callbackSuccess_)
        {
          xear.async(function() {
            callbackSuccess_(xear[module_]);
          });
        }
      }
    }

    return xear[module_];
  };


  // PREDEFINED PROPERTIES
  xear.COLOR_LOG="color:#000";
  xear.COLOR_INFO="color:#006";
  xear.COLOR_WARN="color:#f70";
  xear.COLOR_ERROR="color:#600";
  xear.COLOR_DUMP="color:#060";


  // INTERNAL PREDEFINED PROPERTIES
  xear.__LOADER=null;
  xear.__SCHEDULER=null;
  xear.__REQUEST_ID=0;
  xear.__REQUEST_ID_MAX=0xffff;


  // STATIC ACCESSORS
  xear.stdout_flush=function(color_, string_, block_, level_)
  {
    if(!color_)
      color_=xear.COLOR_LOG;

    if("undefined"==typeof(level_))
      level_=0;

    level_<<=4;

    var element=document.createElement(block_?"div":"span");
    element.setAttribute("style", color_+";padding-left:"+level_+"px;");
    element.innerHTML=string_;

    if("undefined"==typeof(stdout))
      window.addEventListener("load", function() {stdout.appendChild(element);});
    else
      stdout.appendChild(element);

    xear.run(function() {
      if("undefined"!=typeof(stdout))
        stdout.scrollTop=stdout.scrollHeight;
    }, 100);
  };

  xear.find=function(iterable_, closure_)
  {
    if("length" in iterable_)
    {
      for(var i=0; i<iterable_.length; i++)
      {
        if(closure_(i, iterable_[i]))
          return i;
      }
    }
    else if("object"==typeof(iterable_))
    {
      var keys=Object.keys(iterable_);

      for(var i=0; i<keys.length; i++)
      {
        if(closure_(keys[i], iterable_[keys[i]]))
          return keys[i];
      }
    }

    return null;
  };

  xear.include=function(module_, callback_)
  {
    xear.info("xear/include", "Include module.", module_);
    xear.warn("xear/include", "Not implemented.");
    xear.error("xear/include", "Not implemented.");
  };


  xear.DEBUG=function()
  {
    var tags=document.getElementsByTagName("meta");
    var tag=xear.find(tags, function(key_, tag_) {
      return "xear.debug"==tag_.name;
    });

    if(tag)
      return parseInt(tags[tag].content);

    return -1;
  }();


  if(0<xear.DEBUG)
  {
    window.addEventListener("load", function() {
      stdout.setAttribute("style", "display:inherit;");
      stdoutbg.setAttribute("style", "display:inherit;");
    });

    xear.stdout_append=function(color_, arg_, level_)
    {
      if("undefined"==typeof(level_))
        level_=0;

      var argTypeName=typeof(arg_);

      if("object"==argTypeName)
      {
        if(null==arg_)
        {
          xear.stdout_flush(color_, "null", false);

          return;
        }

        if("constructor" in arg_)
          argTypeName=arg_.constructor.name;

        var argTypeNameLc=argTypeName.toLowerCase();

        if("date"==argTypeNameLc)
        {
          xear.stdout_flush(color_, arg_.getFullYear()
            +"-"+("00"+(parseInt(arg_.getMonth())+1)).substr(-2, 2)
            +"-"+("00"+arg_.getDate()).substr(-2, 2)
            +" "+("00"+arg_.getHours()).substr(-2, 2)
            +":"+("00"+arg_.getMinutes()).substr(-2, 2)
            +":"+("00"+arg_.getSeconds()).substr(-2, 2)
            +"."+arg_.getMilliseconds(), false);
        }
        else
        {
          var isArray=Array.isArray(arg_);

          xear.stdout_flush(color_, argTypeName+(isArray?" [":" {"), true, level_);

          xear.each(arg_,
            function(key_, value_)
            {
            xear.stdout_flush(color_, key_+":", false, level_+1);
            xear.stdout_append(color_, value_, level_+1);
            xear.stdout_flush(color_, "", true);
            }
          );

          xear.stdout_flush(color_, isArray?"]":"}", true, level_);
        }
      }
      else
      {
        var valueTypeName=typeof(arg_);

        if("object"==valueTypeName && ("constructor" in arg_))
          valueTypeName=arg_.constructor.name;

        if("undefined"==valueTypeName)
          xear.stdout_flush(color_, "", 0==level_);
        else
          xear.stdout_flush(color_, " "+valueTypeName+"{"+arg_+"}", 0==level_);
      }
    };

    xear.append=function(method_, color_, namespace_, message_, arg_)
    {
      if("undefined"==typeof(arg_))
      {
        xear.stdout_append(color_, new Date());
        if(namespace_)
          xear.stdout_flush(color_, " ["+namespace_+"]", false);
        if(message_)
          xear.stdout_flush(color_, " "+message_, false);
        xear.stdout_flush(color_, "", true);

        console[method_]("%c[%s] %s", color_, namespace_, message_);
      }
      else
      {
        if("object"==typeof(arg_) && ("type" in arg_) && "xear/exception"==arg_.type)
        {
          console[method_]("%c[%s] %s\n\n%s:%s\n\n%s",
            color_,
            namespace_?namespace_:arg_.namespace,
              message_?message_:arg_.message,
                arg_.file,
                arg_.line,
                arg_.stack
          );
        }
        else if("object"==typeof(arg_) && ("stack" in arg_) && arg_.stack)
        {
          console[method_]("%c[%s] %s\n\n%s:%s\n\n%s",
            color_,
            namespace_?namespace_:arg_.name,
              message_?message_:arg_.message,
                "unknown",
                0,
                arg_.stack
          );
        }
        else
        {
          xear.stdout_append(color_, new Date());

          if(namespace_ || message_)
          {
            console[method_]("%c[%s] %s %O", color_, namespace_, message_, arg_);

            if(namespace_)
              xear.stdout_flush(color_, " ["+namespace_+"]", false);
            if(message_)
              xear.stdout_flush(color_, " "+message_, false);
          }
          else
          {
            console[method_]("%c", color_, arg_);
          }

          if(arg_)
            xear.stdout_append(color_, arg_, 0);
          else
            xear.stdout_flush(color_, "", true);
        }
      }
    };

    xear.info=function(namespace_, message_, arg_)
    {
      xear.append("debug", xear.COLOR_INFO, namespace_, message_, arg_);
    }

    xear.error=function(namespace_, message_, arg_)
    {
      xear.append("error", xear.COLOR_ERROR, namespace_, message_, arg_);
    };

    xear.warn=function(namespace_, message_, arg_)
    {
      xear.append("warn", xear.COLOR_WARN, namespace_, message_, arg_);
    };

    xear.assert=function(namespace_, message_, assertion_)
    {
      console.assert(assertion_, "["+namespace_+"] "+message_);
    };

    xear.log=function(namespace_, message_, arg_)
    {
      xear.append("log", null, namespace_, message_, arg_);
    };

    xear.dump=function(args_, message_)
    {
      if("undefined"==typeof(message_))
        message_="";

      xear.append("debug", xear.COLOR_DUMP, "xear/debug", message_, args_, true);
    }
  }
  else
  {
    xear.stdout_append=function(color_, arg_, level_) {};
    xear.append=function(method_, color_, namespace_, message_, arg_) {};
    xear.info=function(namespace_, message_, arg_) {};
    xear.error=function(namespace_, message_, arg_) {};
    xear.warn=function(namespace_, message_, arg_) {};
    xear.assert=function(namespace_, message_, assertion_) {};
    xear.log=function(namespace_, message_, arg_) {};
    xear.dump=function(args_, message_) {};
  }


  xear.each=function(iterable_, closure_)
  {
    if("length" in iterable_)
    {
      for(var i=0; i<iterable_.length; i++)
        closure_(i, iterable_[i]);
    }
    else if("object"==typeof(iterable_))
    {
      for(var p in iterable_)
      {
        if(undefined==p)
          break;

        closure_(p, iterable_[p]);
      }
    }
  };

  xear.get=function(uri_, callback_, principal_, credential_)
  {
    var requestId=xear.__requestId();

    xear.info("xear", ">>> ["+requestId+"] GET "+uri_, {
      id: requestId,
      method: "GET",
      uri: uri_,
      principal: principal_
    });

    var request=new XMLHttpRequest();

    request.onreadystatechange=function()
    {
      if(4==request.readyState)
      {
        xear.info("xear", "<<< ["+requestId+"] GET "+uri_, {
          status: request.status,
          statusText: request.statusText,
          response: request.responseText
        });

        if(callback_)
          callback_(request);
      }
    };

    request.open("GET", uri_, true, principal_, credential_);
    request.send();
  };

  xear.post=function(uri_, data_, callback_, principal_, credential_)
  {
    var requestId=xear.__requestId();

    xear.info("xear", ">>> ["+requestId+"] POST "+uri_, {
      id: requestId,
      method: "POST",
      uri: uri_,
      data: data_,
      principal: principal_
    });

    var request=new XMLHttpRequest();

    request.onreadystatechange=function() {

      if(4==request.readyState)
      {
        xear.info("xear", "<<< ["+requestId+"] POST "+uri_, {
          status: request.status,
          statusText: request.statusText,
          response: request.responseText
        });

        if(callback_)
          callback_(request);
      }
    };

    request.open("POST", uri_, true, principal_, credential_);
    request.send(data_);
  };

  xear.async=function(closure_)
  {
    setTimeout(function() {closure_();}, 0);
  };

  xear.run=function(runnable_, defer_)
  {
    if("undefined"==typeof(defer_))
      defer_=0;

    if(!("run" in runnable_))
      runnable_=new xear.Runnable(runnable_);

    setTimeout(function() {
      runnable_.run();
    }, defer_);

    return runnable_;
  };

  xear.timestamp=function()
  {
    return Date.now()/1000|0;
  };

  xear.typeForName=function(name_)
  {
    var type=window || this;

    var path=name_.split("/");
    path[path.length-1]=path[path.length-1].charAt(0).toUpperCase()+path[path.length-1].slice(1);

    for(var i=0; i<path.length; i++)
    {
      type=type[path[i]];

      if(!type)
        break;
    }

    return type;
  };

  xear.__requestId=function()
  {
    if(xear.__REQUEST_ID_MAX<=xear.__REQUEST_ID)
      xear.__REQUEST_ID=0;

    return ++xear.__REQUEST_ID;
  };
  //----------------------------------------------------------------------------


  //----------------------------------------------------------------------------
  // MODULES
  //----------------------------------------------------------------------------
  /**
   * __xear_scheduler
   *
   * @package js
   * @subpackage xear.scheduler
   *
   * @author carsten.schipke@gmail.com
   */
  __xear_scheduler=function()
  {
    // PROPERTIES
    this.tasks={};
  };


  // ACCESSORS/MUTATORS
  /**
   * @memberOf xear.scheduler
   */
  __xear_scheduler.prototype.run=function()
  {
    xear.each(this.tasks, function(name_, runnable_) {
      xear.run(runnable_);
    });

    xear.run(function() {xear.__SCHEDULER.run();}, 50);
  };

  /**
   * @memberOf xear.scheduler
   */
  __xear_scheduler.prototype.schedule=function(name_, closure_)
  {
    xear.info("xear/scheduler", "Schedule task ["+name_+"].");

    if(closure_.run)
      this.tasks[name_]=closure_;
    else
      this.tasks[name_]=new xear.Runnable(closure_);
  };

  /**
   * @memberOf xear.scheduler
   */
  __xear_scheduler.prototype.unschedule=function(name_)
  {
    xear.info("xear/scheduler", "Remove scheduled task ["+name_+"].");

    var tasks={};

    for(var name in this.tasks)
    {
      if(name!=name_)
        tasks[name]=this.tasks[name];
    }

    this.tasks=tasks;
  };


  // INSTANTIATION
  xear.__SCHEDULER=new __xear_scheduler();


  // ALIASES
  xear.schedule=function(name_, closure_) {xear.__SCHEDULER.schedule(name_, closure_);};
  xear.unschedule=function(name_) {xear.__SCHEDULER.unschedule(name_);};


  setTimeout(function() {xear.__SCHEDULER.run();}, 0);
  //----------------------------------------------------------------------------


  /**
   * __xear_string
   *
   * @package js
   * @subpackage xear.string
   *
   * @author carsten.schipke@gmail.com
   */
  __xear_string=function()
  {

  };


  // ACCESSORS/MUTATORS
  /**
   * @memberOf xear.string
   */
  __xear_string.prototype.repeat=function(string_, count_)
  {
    if(1>count_ || !string_)
      return "";

    if("string"!=typeof(string_))
    {
      if("toString" in string_)
        string_=string_.toString();
    }

    while(0<count_--)
      string_+=string_;

    return string_;
  };
  //----------------------------------------------------------------------------


  /**
   * __xear_cache
   *
   * @package js
   * @subpackage xear.cache
   *
   * @author carsten.schipke@gmail.com
   */
  __xear_cache=function()
  {
    if("sessionStorage" in window)
      this.cache=new xear.Cache.Backend.SessionStorage("xear/cache/default");
    else
      this.cache=new xear.Cache.Backend.Cookie("xear/cache/default");
  };


  // ACCESSORS/MUTATORS
  /**
   * @memberOf xear.cache
   */
  __xear_cache.prototype.has=function(key_)
  {
    return this.cache.has(key_);
  };

  /**
   * @memberOf xear.cache
   */
  __xear_cache.prototype.has_t=function(key_)
  {
    return this.cache.has_t(key_);
  };

  /**
   * @memberOf xear.cache
   */
  __xear_cache.prototype.get=function(key_)
  {
    return this.cache.get(key_);
  };

  /**
   * @memberOf xear.cache
   */
  __xear_cache.prototype.get_t=function(key_)
  {
    return this.cache.get_t(key_);
  };

  /**
   * @memberOf xear.cache
   */
  __xear_cache.prototype.set=function(key_, value_)
  {
    return this.cache.set(key_, value_);
  };

  /**
   * @memberOf xear.cache
   */
  __xear_cache.prototype.set_t=function(key_, value_, ttl_)
  {
    return this.cache.set_t(key_, value_, ttl_);
  };

  /**
   * @memberOf xear.cache
   */
  __xear_cache.prototype.add=function(key_, value_)
  {
    return this.cache.add(key_, value_);
  };

  /**
   * @memberOf xear.cache
   */
  __xear_cache.prototype.add_t=function(key_, value_, ttl_)
  {
    return this.cache.add_t(key_, value_, ttl_);
  };

  /**
   * @memberOf xear.cache
   */
  __xear_cache.prototype.remove=function(key_)
  {
    return this.cache.remove(key_);
  };

  /**
   * @memberOf xear.cache
   */
  __xear_cache.prototype.clear=function()
  {
    return this.cache.clear();
  };
  //----------------------------------------------------------------------------


  // TYPES
  /**
   * Object
   *
   * @package js
   * @subpackage xear
   *
   * @author carsten.schipke@gmail.com
   *
   * @abstract
   * @class
   */
  xear.Object=function(type_)
  {
    this.type=type_;
  };


  // ACCESSORS/MUTATORS
  /**
   * @abstract
   *
   * @access public
   */
  xear.Object.prototype.toString=function()
  {
    return this.type+"{}";
  };

  xear.Object.prototype.log=function(message_, arg_)
  {
    xear.log(this.type, message_, arg_);
  };

  xear.Object.prototype.info=function(message_, arg_)
  {
    xear.info(this.type, message_, arg_);
  };

  xear.Object.prototype.warn=function(message_, arg_)
  {
    xear.warn(this.type, message_, arg_);
  };

  xear.Object.prototype.error=function(message_, arg_)
  {
    xear.error(this.type, message_, arg_);
  };


  /**
   * Cache
   *
   * @package js
   * @subpackage xear.cache
   *
   * @author carsten.schipke@gmail.com
   *
   * @class
   */
  xear.Cache=function(type_, namespace_)
  {
    xear.Object.call(this, type_);

    if("undefined"==typeof(window.sessionStorage))
      this.backend=new xear.Cache.Backend.Cookie(namespace_);
    else
      this.backend=new xear.Cache.Backend.SessionStorage(namespace_);
  };

  xear.Cache.Backend={};

  xear.Cache.prototype=new xear.Object();
  xear.Cache.prototype.constructor=xear.Cache;


  // ACCESSORS/MUTATORS
  xear.Cache.prototype.has=function(key_)
  {
    return this.backend.has(key_);
  };

  xear.Cache.prototype.has_t=function(key_)
  {
    return this.backend.has_t(key_);
  };

  xear.Cache.prototype.get=function(key_)
  {
    return this.backend.get(key_);
  };

  xear.Cache.prototype.get_t=function(key_)
  {
    return this.backend.get_t(key_);
  };

  xear.Cache.prototype.set=function(key_, value_)
  {
    return this.backend.set(key_, value_);
  };

  xear.Cache.prototype.set_t=function(key_, value_, ttl_)
  {
    return this.backend.set_t(key_, value_, ttl_);
  };

  xear.Cache.prototype.add=function(key_, value_)
  {
    return this.backend.add(key_, value_);
  };

  xear.Cache.prototype.add_t=function(key_, value_, ttl_)
  {
    return this.backend.add_t(key_, value_, ttl_);
  };

  xear.Cache.prototype.remove=function(key_)
  {
    return this.backend.remove(key_);
  };

  xear.Cache.prototype.clear=function()
  {
    return this.backend.clear();
  };

  xear.Cache.prototype.key=function(key_)
  {
    return this.namespace+":"+key_;
  };
  //----------------------------------------------------------------------------


  /**
   * SessionStorage
   *
   * @package js
   * @subpackage xear.cache.backend
   *
   * @author carsten.schipke@gmail.com
   *
   * @class
   */
  xear.Cache.Backend.SessionStorage=function(namespace_)
  {
    xear.Object.call(this, "xear/cache/backend/session-storage", namespace_);

    this.namespace=namespace_;
    this.storage=window.sessionStorage;
  };

  xear.Cache.Backend.SessionStorage.prototype=new xear.Cache();
  xear.Cache.Backend.SessionStorage.prototype.constructor=xear.Cache.Backend.SessionStorage;


  // ACCESSORS/MUTATORS
  xear.Cache.Backend.SessionStorage.prototype.has=function(key_)
  {
    return "undefined"!=typeof(this.storage[this.key(key_)]);
  };

  xear.Cache.Backend.SessionStorage.prototype.has_t=function(key_)
  {
    key_=this.key(key_);

    if("undefined"==typeof(this.storage[key_]))
      return false;

    var entry=JSON.parse(this.storage[key_]);

    if(0<entry.ttl && xear.timestamp()>(entry.ttl+entry.time))
    {
      this.storage.removeItem(key_);

      return false;
    }

    return true;
  };

  xear.Cache.Backend.SessionStorage.prototype.get=function(key_)
  {
    key_=this.key(key_);

    if("undefined"==typeof(this.storage[key_]))
      return false;

    return JSON.parse(this.storage[key_]);
  };

  xear.Cache.Backend.SessionStorage.prototype.get_t=function(key_)
  {
    key_=this.key(key_);

    if("undefined"==typeof(this.storage[key_]))
      return false;

    var entry=JSON.parse(this.storage[key_]);

    if(0<entry.ttl && xear.timestamp()>(entry.ttl+entry.time))
    {
      this.storage.removeItem(key_);

      return false;
    }

    return entry.value;
  };

  xear.Cache.Backend.SessionStorage.prototype.set=function(key_, value_)
  {
    this.storage.setItem(this.key(key_), JSON.stringify(value_));

    return true;
  };

  xear.Cache.Backend.SessionStorage.prototype.set_t=function(key_, value_, ttl_)
  {
    key_=this.key(key_);

    if("undefined"==typeof(ttl_))
      ttl_=0;

    this.storage.setItem(key_, JSON.stringify({
      value: value_,
      time: xear.timestamp(),
      ttl: ttl_
    }));

    return true;
  };

  xear.Cache.Backend.SessionStorage.prototype.add=function(key_, value_)
  {
    key_=this.key(key_);

    if("undefined"!=typeof(this.storage[key_]))
      return false;

    this.storage.setItem(key_, JSON.stringify(value_));

    return true;
  };

  xear.Cache.Backend.SessionStorage.prototype.add_t=function(key_, value_, ttl_)
  {
    key_=this.key(key_);

    if("undefined"!=typeof(this.storage[key_]))
    {
      var entry=JSON.parse(this.storage[key_]);

      if(1>entry.ttl || xear.timestamp()<(entry.ttl+entry.time))
        return false;
    }

    if("undefined"==typeof(ttl_))
      ttl_=0;

    this.storage.setItem(key_, JSON.stringify({
      value: value_,
      time: xear.timestamp(),
      ttl: ttl_
    }));

    return true;
  };

  xear.Cache.Backend.SessionStorage.prototype.remove=function(key_)
  {
    this.storage.removeItem(this.key(key_));

    return true;
  };

  xear.Cache.Backend.SessionStorage.prototype.clear=function()
  {
    var s=this;

    xear.each(this.storage, function(key_, value_) {
      if(key_.startsWith(s.namespace))
      {
        s.storage.removeItem(key_);
      }
    });

    return true;
  };
  //----------------------------------------------------------------------------


  /**
   * Cookie
   *
   * @package js
   * @subpackage xear.cache.backend
   *
   * @author carsten.schipke@gmail.com
   *
   * @class
   */
  xear.Cache.Backend.Cookie=function(namespace_)
  {
    xear.Object.call(this, "xear/cache/backend/cookie", namespace_);

    this.namespace=namespace_;
  };

  xear.Cache.Backend.Cookie.prototype=new xear.Cache();
  xear.Cache.Backend.Cookie.prototype.constructor=xear.Cache.Backend.Cookie;


  // ACCESSORS/MUTATORS
  xear.Cache.Backend.Cookie.prototype.has=function(key_)
  {
    return false;
  };

  xear.Cache.Backend.Cookie.prototype.has_t=function(key_)
  {
    return false;
  };

  xear.Cache.Backend.Cookie.prototype.get=function(key_)
  {
    return null;
  };

  xear.Cache.Backend.Cookie.prototype.get_t=function(key_)
  {
    return null;
  };

  xear.Cache.Backend.Cookie.prototype.set=function(key_, value_)
  {
    return false;
  };

  xear.Cache.Backend.Cookie.prototype.set_t=function(key_, value_, ttl_)
  {
    return false;
  };

  xear.Cache.Backend.Cookie.prototype.add=function(key_, value_)
  {
    return false;
  };

  xear.Cache.Backend.Cookie.prototype.add_t=function(key_, value_, ttl_)
  {
    return false;
  };

  xear.Cache.Backend.Cookie.prototype.remove=function(key_)
  {
    return true;
  };

  xear.Cache.Backend.Cookie.prototype.clear=function()
  {
    return true;
  };
  //----------------------------------------------------------------------------


  /**
   * Exception
   *
   * @package js
   * @subpackage xear
   *
   * @author carsten.schipke@gmail.com
   *
   * @class
   */
  xear.Exception=function(namespace_, message_, file_, line_, stack_)
  {
    xear.Object.call(this, "xear/exception");

    if(!file_)
      file_="unknown";
    if(!line_)
      line_=0;
    if(!stack_)
      stack_=[];


    // PROPERTIES
    this.namespace=namespace_;
    this.message=message_;
    this.file=file_;
    this.line=line_;
    this.stack=stack_;
  };

  xear.Exception.prototype=new xear.Object();
  xear.Exception.prototype.constructor=xear.Exception;
  //----------------------------------------------------------------------------


  /**
   * Runnable
   *
   * @package js
   * @subpackage xear
   *
   * @author carsten.schipke@gmail.com
   *
   * @class
   * @callback
   */
  // CONSTRUCTION
  xear.Runnable=function(closure_)
  {
    xear.Object.call(this, "xear/runnable");

    this.closure=closure_;
    this.result=null;
  };

  xear.Runnable.prototype=new xear.Object();
  xear.Runnable.prototype.constructor=xear.Runnable;


  // ACCESSORS/MUTATORS
  xear.Runnable.prototype.run=function()
  {
    var c=this.closure;

    this.result=c();
  };

  xear.Runnable.prototype.result=function()
  {
    return this.result;
  };
  //----------------------------------------------------------------------------


  // COMPATIBILITY
  if("undefined"==typeof(window.console))
  {
    window.console=function()
    {

    };

    window.console.log=function()
    {

    };
  }

  if("undefined"==typeof(window.console.assert))
    window.console.assert=window.console.log;

  if("undefined"==typeof(window.console.debug))
    window.console.debug=window.console.log;


  // INITIALIZE
  xear("cache");
  xear("string");

