

  /**
   * xear_client
   *
   * @package js
   * @subpackage xear.client
   *
   * @author carsten.schipke@gmail.com
   */
  __xear_client=function()
  {
    // PREDEFINED PROPERTIES
    __xear_client.STUN_HOST="stun.l.google.com";
    __xear_client.STUN_PORT=19302;

    __xear_client.FPS_DEFAULT=60;


    // PROPERTIES
    this.fps=__xear_client.FPS_DEFAULT,
    this.initialized=false,


    // ACCESSORS/MUTATORS
    /**
     * @memberOf xear.client
     */
    this.init=function()
    {
      if("undefined"==typeof(URL))
      {
        if("webkitURL" in window)
          window.URL=webkitURL;
        else if("mozURL" in window)
          window.URL=mozURL;
      }

      if("undefined"==typeof(RTCIceCandidate))
      {
        if("webkitRTCIceCandidate" in window)
          window.RTCIceCandidate=webkitRTCIceCandidate;
        else if("mozRTCIceCandidate" in window)
          window.RTCIceCandidate=mozRTCIceCandidate;
      }

      if("undefined"==typeof(RTCPeerConnection))
      {
        if("webkitRTCPeerConnection" in window)
          window.RTCPeerConnection=webkitRTCPeerConnection;
        else if("mozRTCPeerConnection" in window)
          window.RTCPeerConnection=mozRTCPeerConnection;
      }

      if("undefined"==typeof(RTCSessionDescription))
      {
        if("webkitRTCSessionDescription" in window)
          window.RTCSessionDescription=webkitRTCSessionDescription;
        else if("mozRTCSessionDescription" in window)
          window.RTCSessionDescription=mozRTCSessionDescription;
      }

      this.initialized=true;
    }
  };
  //----------------------------------------------------------------------------


  // EXPORT
  __xear_client_export=function()
  {
    /**
     * Connection
     *
     * @package js
     * @subpackage xear.client
     *
     * @author carsten.schipke@gmail.com
     *
     * @class
     */
    // CONSTRUCTION
    xear.client.Connection=function(config_)
    {
      xear.Object.call(this, "xear/client/connection");

      if("undefined"==typeof(config_))
        config_={};

      this.config=config_;

      if(!("iceServers" in this.config))
      {
        this.config.iceServers=[{
          url: "stun:"+__xear_client.STUN_HOST+":"+__xear_client.STUN_PORT
        }];
      }

      if(!("bundlePolicy" in this.config))
        this.config.bundlePolicy="max-compat";

      this.configOffer={};

      if(0>navigator.appVersion.indexOf("Bowser"))
      {
        this.configOffer={
          "offerToReceiveVideo": 1,
          "offerToReceiveAudio": 1
        };
      }

      this.constraints={
        optional: [
          {"internalSctpDataChannels": "true"},
          {"DtlsSrtpKeyAgreement": "true"},
          {"enableRtpDataChannels": "false"}
      ]};
      
      this.candidates=0;
      this.connected=false;
      this.channel=null;

      var self=this;

      this.connection=new RTCPeerConnection(this.config, this.constraints);
      this.connection.onaddstream=function(event_) {return self.onAddStream(event_);};
      this.connection.onremovestream=function(event_) {return self.onRemoveStream(event_);};
      this.connection.onicecandidate=function(event_) {return self.onIceCandidate(event_);};
      this.connection.ondatachannel=function(event_) {return self.onDataChannel(event_);};
    };

    xear.client.Connection.prototype=new xear.Object();
    xear.client.Connection.prototype.constructor=xear.client.Connection;


    // ACCESSORS/MUTATORS
    xear.client.Connection.prototype.connect=function()
    {
      var self=this;

      console.log("Create data channel");

      this.channel=this.connection.createDataChannel("xear", {
        maxRetransmits: 1,
        ordered: true,
        reliable: true
      });

      // this.channel.binaryType="arraybuffer"; // default [arraybuffer|blob]
      this.channel.onmessage=function(event_) {return self.onSendChannelMessage(event_);};
      this.channel.onopen=function(event_) {return self.onSendChannelOpen(event_);};
      this.channel.onclose=function(event_) {return self.onSendChannelClose(event_);};

      var callbackSuccess=function(stream_)
      {
        var tracks=stream_.getVideoTracks();

        self.info("Obtained camera stream.", tracks);

        self.connection.addStream(stream_);

        self.connection.createOffer(
          function(offer_) {self.createOffer(offer_);},
          function(error_) {self.onError(error_);},
          self.configOffer);
      };

      var callbackFailure=function(error_)
      {
        self.error("Unable to obtain camera stream.", error_);

        self.connection.createOffer(
          function(offer_) {self.createOffer(offer_);},
          function(error_) {self.onError(error_);},
          self.configOffer);
      };

      xear("device",
        function(device)
        {
          var w=remote.offsetWidth;
          var h=remote.offsetHeight;

          var constraints={
            video: {
              optional: [
                 {width: w},
                 {height: h},
                 {minWidth: w},
                 {minHeight: h},
                 {maxWidth: w},
                 {maxHeight: h}
              ]
            }
          };

          device.mediaStream(0, xear.client.fps, true, callbackSuccess, callbackFailure, constraints);
        }
      );
    };

    xear.client.Connection.prototype.createOffer=function(offer_)
    {
      var self=this;
      var localDescription=new RTCSessionDescription(offer_);

      var callback=function()
      {
        var offer=[
          "sdp",
          self.connection.localDescription,
          [remote.offsetWidth, remote.offsetHeight],
          xear.device.screenAngle(),
          xear.device.colorDepth(),
          xear.device.pixelRatio(),
          xear.device.screenOrientation(),
          xear.device.networkQuality(),
          xear.device.locale(),
          xear.client.fps
        ];

        xear.post("/setup", JSON.stringify(offer),
          function(request_)
          {
            self.fetchAnswer();
          }
        );
      };

      this.connection.setLocalDescription(localDescription, callback, this.onError);
    };

    xear.client.Connection.prototype.fetchCandidate=function()
    {
      var self=this;

      xear.get("/candidate",
        function(request_)
        {
          var response={};

          if(200==request_.status && request_.responseText)
            response=JSON.parse(request_.responseText);

          if(response.candidate)
          {
            self.connection.addIceCandidate(new RTCIceCandidate(response), function() {}, self.onError);
            self.candidates++;
          }
          else if(0<self.candidates)
          {
            return;
          }

          setTimeout(function() { self.fetchCandidate(); }, 10);
        }
      );
    };

    xear.client.Connection.prototype.fetchAnswer=function()
    {
      var self=this;

      xear.get("/session",
        function(request_)
        {
          var sdp=JSON.parse(request_.responseText);

          self.connection.setRemoteDescription(new RTCSessionDescription(sdp),
            function(s) {self.info("Set remote SDP.", sdp);},
            function(e) {self.info("Unable to set remote SDP.", [e, sdp]);
          });

          self.fetchCandidate();
        }
      );
    };


    xear.client.Connection.prototype.onAddStream=function(event_)
    {
      this.info("Add stream.", event_.stream.getTracks()[0].applyConstraints);

      remote.src=URL.createObjectURL(event_.stream);
      remote.play();
    };

    xear.client.Connection.prototype.onRemoveStream=function(event_)
    {
      this.info("Remove stream.");
    };

    xear.client.Connection.prototype.onIceCandidate=function(event_)
    {
      if(event_.candidate && event_.candidate.candidate)
      {
        this.info("Add ICE candidate", event_.candidate.candidate);

        xear.post("/message", JSON.stringify(["cnd", event_.candidate]));
      }
      else
      {
        this.info("Added ICE candidates");
      }
    };

    xear.client.Connection.prototype.onDataChannel=function(event_)
    {
      // Do nothing ...
      console.log("onDataChannel", event_);
    };

    xear.client.Connection.prototype.onError=function(error_)
    {
      this.error("onError", error_);
    };

    xear.client.Connection.prototype.onSendChannelMessage=function(event_)
    {
      this.info("onSendChannelMessage");
      console.log("onSendChannelMessage", event_);

      var message=JSON.parse(event_.data);

      if(("s" in message) && 0==message.s)
        this.connected=true;
    };

    xear.client.Connection.prototype.onSendChannelOpen=function(event_)
    {
      this.info("onSendChannelOpen");
      console.log("onSendChannelOpen", event_);

      xear.dump(event_);

      var self=this;
      var heartbeat=null;

      heartbeat=function()
      {
        console.log("HEARBEAT");
        self.channel.send(JSON.stringify([0]));

        setTimeout(heartbeat, 3000);
      };

      heartbeat();

      this.monitorEvents();
    };
    
    xear.client.Connection.prototype.onSendChannelClose=function(event_)
    {
      this.connected=false;
    };

    xear.client.Connection.prototype.monitorEvents=function()
    {
      var self=this;

      xear.device.monitorState("default", 1000/xear.client.fps,
        function(state_)
        {
          self.channel.send(JSON.stringify(state_));
        }
      );
    };
    //--------------------------------------------------------------------------
  }


  // INITIALIZE
  var __xear_client_init=function() {
    if("undefined"==typeof(xear))
      setTimeout(__xear_client_init, 10);
    else
      xear("client");
  }();
  //----------------------------------------------------------------------------
