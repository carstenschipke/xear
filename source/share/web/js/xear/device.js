

  /**
   * xear_device
   *
   * @package js
   * @subpackage xear.device
   *
   * @author carsten.schipke@gmail.com
   */
  __xear_device=function()
  {
    // PREDEFINED PROPERTIES
    /**
     * @public
     * @constant
     */
    __xear_device.CAMERA_FACING_BACK=0;
    /**
     * @public
     * @constant
     */
    __xear_device.CAMERA_FACING_FACE=1;


    /**
     * @private
     * @constant
     */
    __xear_device.COLOR_DEPTH_DEFAULT=24;


    /**
     * @public
     * @constant
     */
    __xear_device.NETWORK_TYPE_WIFI=0;
    /**
     * @public
     * @constant
     */
    __xear_device.NETWORK_TYPE_GPRS=1;
    /**
     * @public
     * @constant
     */
    __xear_device.NETWORK_TYPE_EDGE=2;
    /**
     * @public
     * @constant
     */
    __xear_device.NETWORK_TYPE_3G=3;
    /**
     * @public
     * @constant
     */
    __xear_device.NETWORK_TYPE_4G=4;
    /**
     * @public
     * @constant
     */
    __xear_device.NETWORK_TYPE_5G=5;


    /**
     * @public
     * @constant
     */
    __xear_device.NETWORK_QUALITY_HIG=0;
    /**
     * @public
     * @constant
     */
    __xear_device.NETWORK_QUALITY_MID=1;
    /**
     * @public
     * @constant
     */
    __xear_device.NETWORK_QUALITY_LOW=2;


    /**
     * @public
     * @constant
     */
    __xear_device.SCREEN_ORIENTATION_PORTRAIT=0;
    /**
     * @public
     * @constant
     */
    __xear_device.SCREEN_ORIENTATION_LANDSCAPE=1;


    /**
     * @public
     * @constant
     */
    __xear_device.EVENT_HEARTBEAT=0;
    /**
     * @public
     * @constant
     */
    __xear_device.EVENT_STATE=1;

    /**
     * @public
     * @constant
     */
    __xear_device.EVENT_TOUCH_START=2;
    /**
     * @public
     * @constant
     */
    __xear_device.EVENT_TOUCH_MOVE=4;
    /**
     * @public
     * @constant
     */
    __xear_device.EVENT_TOUCH_END=8;


    /**
     * @public
     * @enum
     */
    __xear_device.CAMERA_FACING={};
    __xear_device.CAMERA_FACING[__xear_device.CAMERA_FACING_BACK]="environment";
    __xear_device.CAMERA_FACING[__xear_device.CAMERA_FACING_FACE]="user";

    /**
     * @public
     * @enum
     */
    __xear_device.NETWORK_TYPES={
      "wifi": __xear_device.NETWORK_TYPE_WIFI,
      "gprs": __xear_device.NETWORK_TYPE_GPRS,
      "edge": __xear_device.NETWORK_TYPE_EDGE,
      "3g": __xear_device.NETWORK_TYPE_3G,
      "4g": __xear_device.NETWORK_TYPE_4G,
      "5g": __xear_device.NETWORK_TYPE_5G
    };

    /**
     * @public
     * @enum
     */
    __xear_device.NETWORK_QUALITY={};
    __xear_device.NETWORK_QUALITY[__xear_device.NETWORK_TYPE_WIFI]=__xear_device.NETWORK_QUALITY_HIG;
    __xear_device.NETWORK_QUALITY[__xear_device.NETWORK_TYPE_GPRS]=__xear_device.NETWORK_QUALITY_LOW;
    __xear_device.NETWORK_QUALITY[__xear_device.NETWORK_TYPE_EDGE]=__xear_device.NETWORK_QUALITY_LOW;
    __xear_device.NETWORK_QUALITY[__xear_device.NETWORK_TYPE_3G]=__xear_device.NETWORK_QUALITY_MID;
    __xear_device.NETWORK_QUALITY[__xear_device.NETWORK_TYPE_4G]=__xear_device.NETWORK_QUALITY_HIG;
    __xear_device.NETWORK_QUALITY[__xear_device.NETWORK_TYPE_5G]=__xear_device.NETWORK_QUALITY_HIG;


    // PROPERTIES
    /**
     * @private
     *
     * @var {Double} locationLast[latitude]
     * @var {Double} locationLast[longitude]
     * @var {Double} locationLast[accuracy]
     * @var {Double} locationLast[altitude]
     * @var {Double} locationLast[altitudeAccuracy]
     * @var {Double} locationLast[heading]
     * @var {Double} locationLast[speed]
     */
    this.locationLast=[0, 0, 0, 0, 0, 0, 0],

    /**
     * @private
     *
     * @var {Number} locationMonitorPid
     */
    this.locationMonitorPid=-1,
    /**
     * @private
     *
     * @var {Scalar:Function} locationMonitors
     */
    this.locationMonitors={},

    /**
     * @private
     *
     * @var {Double} motionLast[x]
     * @var {Double} motionLast[y]
     * @var {Double} motionLast[z]
     */
    this.motionLast=[0, 0, 0],

    /**
     * @private
     *
     * @var {Double} orientationLast[x]
     * @var {Double} orientationLast[y]
     * @var {Double} orientationLast[z]
     * @var {Number} orientationLast[absolute]
     */
    this.orientationLast=[0, 0, 0, 0],

    /**
     * @private
     *
     * @var {Number} screenOrientationLast
     */
    this.screenOrientationLast=__xear_device.SCREEN_ORIENTATION_PORTRAIT,

    /**
     * @private
     *
     * @var {Number} screenDimensionsLast[width]
     * @var {Number} screenDimensionsLast[height]
     */
    this.screenDimensionsLast=[0, 0],


    // INITIALIZATION
    /**
     * @return void
     */
    this.init=function()
    {
      if("undefined"==typeof(navigator.getUserMedia))
      {
        if("undefined"!=typeof(navigator.webkitGetUserMedia))
          navigator.getUserMedia=navigator.webkitGetUserMedia;
        else if("undefined"!=typeof(navigator.mozGetUserMedia))
          navigator.getUserMedia=navigator.mozGetUserMedia;
      }

      if("undefined"==typeof(MediaStreamTrack) || "undefined"==typeof(MediaStreamTrack.getSources))
      {
        if("webkitMediaStreamTrack" in window && "getSources" in window.webkitMediaStreamTrack)
          window.MediaStreamTrack=webkitMediaStreamTrack;
        else if("mozMediaStreamTrack" in window && "getSources" in window.mozMediaStreamTrack)
          window.MediaStreamTrack=mozMediaStreamTrack;
      }
    },


    // ACCESSORS/MUTATORS
    /**
     * @memberOf xear.device
     *
     * @return {Double}
     */
    this.aspectRatio=function()
    {
      var dimensions=xear.device.screenDimensions();
      var ratio=dimensions[0]/dimensions[1];

      return ratio.toFixed(6);
    },

    /**
     * @memberOf xear.device
     *
     * @param {Object} constraints_
     * @param {Function} callback_
     * @param {Function} callbackFailure_
     */
    this.camera=function(constraints_, callback_, callbackFailure_)
    {
      if(!("getUserMedia" in navigator))
        throw new Error("Not supported.");

      if("undefined"==typeof(callbackFailure_))
        callbackFailure_=callback_;

      xear.info("xear/device", "Open camera stream.", constraints_);

      navigator.getUserMedia(constraints_, callback_, callbackFailure_);
    },

    /**
     * @memberOf xear.device
     *
     * @param {Number} facing_ - value of enum {@link xear.device.CAMERA_FACING}
     * @param {Number} fps_
     * @param {Object} constraints_
     * @param {Function} callback_
     * @param {Function} callbackFailure_
     */
    this.cameraFacing=function(facing_, fps_, constraints_, callback_, callbackFailure_)
    {
      if("undefined"==facing_)
        facing_=__xear_device.CAMERA_FACING_BACK;
      if("undefined"==typeof(fps_))
        fps_=30;

      if("undefined"==typeof(constraints_))
        constraints_={};
      if(!("video" in constraints_))
        constraints_.video={};
      if(!("optional" in constraints_.video))
        constraints_.video.optional=[];

      if(-1<navigator.appVersion.indexOf("Bowser")
        && !("facingMode" in constraints_.video))
        constraints_.video.facingMode=__xear_device.CAMERA_FACING[facing_];

      /**
       * TODO 'facing' does not exist according to MDN.
       * Documented 'facingMode' does not work (yet). 'facing' seems to be
       * naming that has been adopted internally - so we try both until
       * we know more.
       */
      constraints_.video.optional[constraints_.video.optional.length]={facing: __xear_device.CAMERA_FACING[facing_]};
      constraints_.video.optional[constraints_.video.optional.length]={facingMode: __xear_device.CAMERA_FACING[facing_]};
      constraints_.video.optional[constraints_.video.optional.length]={frameRate: fps_};

      if("undefined"==typeof(MediaStreamTrack) || "undefined"==typeof(MediaStreamTrack.getSources))
      {
        xear.device.camera(constraints_, callback_, callbackFailure_);

        return;
      }

      MediaStreamTrack.getSources(
        function(sources_)
        {
          if(!("mandatory" in constraints_.video))
            constraints_.video.mandatory={};

          var cameras=[];
          var cameraId=null;

          for(var i=0; i<sources_.length; i++)
          {
            var source=sources_[i];

            // No way yet to detect back/front-facing camera.. We just go with the last one for now.
            if("video"==source.kind)
            {
              cameras[cameras.length]=source;

              if(__xear_device.CAMERA_FACING[__xear_device.CAMERA_FACING_BACK]==source.facing)
              {
                xear.info("xear/device", "Detected back-facing camera.", source);

                cameraId=source.id;
              }
            }
          }

          if(null==cameraId && 0<cameras.length)
          {
            xear.info("xear/device", "Unable to detect a back-facing camera.", cameras);
            xear.info("xear/device", "Falling back to next available camera.", cameras[cameras.length-1]);

            cameraId=cameras[cameras.length-1].id;
          }

          if(null!=cameraId)
            constraints_.video.mandatory.sourceId=cameraId;

          xear.device.camera(constraints_, callback_, callbackFailure_);
        }
      );
    },


    /**
     * @memberOf xear.device
     */
    this.colorDepth=function()
    {
      if("undefined"!=typeof(screen) && "colorDepth" in screen)
        return screen.colorDepth;

      return __xear_device.COLOR_DEPTH_DEFAULT;
    },

    /**
     * @memberOf xear.device
     */
    this.compassAvailable=function()
    {
      return "compass" in navigator;
    },

    /**
     * @memberOf xear.device
     */
    this.fullscreenSupported=function(element_)
    {
      if("undefined"==typeof(element_))
        element_=document.body.parentNode;

      return "requestFullScreen" in element_
        || "mozRequestFullScreen" in element_
        || "webkitRequestFullScreen" in element_;
    },

    /**
     * @memberOf xear.device
     */
    this.gamepadAvailable=function()
    {
      return -1<xear.device.gamepadIndex();
    },

    /**
     * @memberOf xear.device
     */
    this.gamepadIndex=function()
    {
      if("getGamepads" in navigator)
      {
        var gamepads=navigator.getGamepads();

        for(var i=0; i<gamepads.length; i++)
        {
          if("undefined"==typeof(gamepads[i]))
            return i-1;
        }
      }

      return -1;
    },

    /**
     * @memberOf xear.device
     */
    this.locale=function()
    {
      return navigator.language;
    },

    /**
     * @memberOf xear.device
     */
    this.location=function(callback_, options_)
    {
      if(-1!=this.locationMonitorPid || !this.locationAvailable())
        return callback_(this.locationLast);

      xear.info("xear/device", "OBTAIN DEVICE LOCATION");

      var self=this;

      if("undefined"==typeof(options_))
      {
        options_={
          enableHighAccuracy: true,
          maximumAge: 0
        };
      }

      navigator.geolocation.getCurrentPosition(
        function(position_)
        {
          var location=[
            position_.coords.latitude?position_.coords.latitude.toFixed(6):0,
            position_.coords.longitude?position_.coords.longitude.toFixed(6):0,
            position_.coords.accuracy?position_.coords.accuracy.toFixed(6):0,
            position_.coords.altitude?position_.coords.altitude.toFixed(6):0,
            position_.coords.altitudeAccuracy?position_.coords.altitudeAccuracy.toFixed(6):0,
            position_.coords.heading?position_.coords.heading.toFixed(6):0,
            position_.coords.speed?position_.coords.speed.toFixed(6):0
          ];

          xear.info("xear/device", "Obtained device location.", location);

          if(location[0]!=self.locationLast[0]
            || location[1]!=self.locationLast[1]
            || location[3]!=self.locationLast[3])
          {
            self.locationLast=location;

            callback_(location);
          }
        },
        function(error_)
        {
          xear.error("xear/device", "Unable to obtain device location.", error_);

          callback_(self.locationLast);
        },
        options_
      );
    },

    /**
     * @memberOf xear.device
     */
    this.locationAvailable=function()
    {
      return "geolocation" in navigator;
    },

    /**
     * @memberOf xear.device
     *
     * @param {Number} cameraFacing_
     * @param {Number} cameraFps_
     * @param {Boolean} audioEnabled_
     * @param {Function} callback_
     * @param {Function} callbackFailure_
     * @param {Object} constraints_
     */
    this.mediaStream=function(cameraFacing_, cameraFps_, audioEnabled_, callback_, callbackFailure_, constraints_)
    {
      if("undefined"==typeof(constraints_))
        constraints_={};
      if(!("audio" in constraints_) && ("undefined"==typeof(audioEnabled_) || audioEnabled_))
        constraints_.audio=true;

      return xear.device.cameraFacing(cameraFacing_, cameraFps_, constraints_, callback_, callbackFailure_);
    },

    /**
     * @memberOf xear.device
     */
    this.networkQuality=function()
    {
      return __xear_device.NETWORK_QUALITY[xear.device.networkType()];
    },

    /**
     * @memberOf xear.device
     */
    this.networkType=function()
    {
      if("connection" in navigator)
      {
        if("type" in navigator.connection)
          return __xear_device.NETWORK_TYPES[navigator.connection.type.toLowerCase()];
      }

      return __xear_device.NETWORK_TYPE_WIFI;
    },

    /**
     * @memberOf xear.device
     */
    this.pixelRatio=function()
    {
      return window.devicePixelRatio;
    },

    /**
     * @memberOf xear.device
     */
    this.screenAngle=function()
    {
      if("undefined"==typeof(screen) && ("orientation" in window))
        return window.orientation;

      if("orientation" in screen)
        return screen.orientation.angle;

      if("mozOrientation" in screen)
      {
        if(-1<screen.mozOrientation.indexOf("landscape"))
          return 90;

        return 0;
      }

      return 0;
    },

    /**
     * @memberOf xear.device
     */
    this.screenDimensions=function()
    {
      return [window.innerWidth, window.innerHeight];
    },

    /**
     * @memberOf xear.device
     */
    this.screenDimensionsNative=function()
    {
      return [
        parseInt(window.devicePixelRatio*window.innerWidth),
        parseInt(window.devicePixelRatio*window.innerHeight)
      ];
    },

    /**
     * @memberOf xear.device
     */
    this.screenOrientation=function()
    {
      var dimensions=xear.device.screenDimensions();

      if(dimensions[0]<dimensions[1])
        return __xear_device.SCREEN_ORIENTATION_PORTRAIT;

      return __xear_device.SCREEN_ORIENTATION_LANDSCAPE;
    },

    /**
     * @memberOf xear.device
     */
    this.vibrateOnce=function(duration_)
    {
      if("undefined"==typeof(duration_))
        duration_=200;

      return xear.device.vibrateSequence([duration_]);
    },

    /**
     * @memberOf xear.device
     */
    this.vibrateSequence=function(sequence_)
    {
      if(xear.device.vibrationAvailable())
      {
        navigator.vibrate(sequence_);

        return true;
      }

      return false;
    },

    /**
     * @memberOf xear.device
     */
    this.vibrationAvailable=function()
    {
      return "vibrate" in navigator;
    },

    /**
     * @memberOf xear.device
     */
    this.monitorLocation=function(id_, callback_, options_)
    {
      if(!this.locationAvailable())
        return false;

      this.locationMonitors[id_]=callback_;

      xear.info("xear/device", "Add monitor: device location.", id_);

      if(-1==this.locationMonitorPid)
      {
        var self=this;

        if("undefined"==typeof(options_))
        {
          options_={
            enableHighAccuracy: true,
            maximumAge: 0
          };
        }

        xear.info("xear/device", "Attach monitor: device location.");

        this.locationMonitorPid=navigator.geolocation.watchPosition(
          function(position_)
          {
            var location=[
              position_.coords.latitude?position_.coords.latitude.toFixed(6):0,
              position_.coords.longitude?position_.coords.longitude.toFixed(6):0,
              position_.coords.accuracy?position_.coords.accuracy.toFixed(6):0,
              position_.coords.altitude?position_.coords.altitude.toFixed(6):0,
              position_.coords.altitudeAccuracy?position_.coords.altitudeAccuracy.toFixed(6):0,
              position_.coords.heading?position_.coords.heading.toFixed(6):0,
              position_.coords.speed?position_.coords.speed.toFixed(6):0
            ];

            if(location[0]!=self.locationLast[0]
              || location[1]!=self.locationLast[1]
              || location[3]!=self.locationLast[3])
            {
              self.locationLast=location;

              xear.each(self.locationMonitors,
                function(monitorId_, monitor_)
                {
                  monitor_(location);
                }
              );
            }
          },
          function(error_)
          {
            xear.error("xear/device", "Unable to monitor: device location.", error_);
          },
          options_
        );
      }
    },

    /**
     * @memberOf xear.device
     */
    this.monitorLocationRemove=function(id_)
    {
      xear.info("xear/device", "Remove monitor: device location.", id_);

      var monitors={};

      xear.each(this.locationMonitors,
        function(monitorId_, monitor_)
        {
          if(id_!=monitorId_)
            monitors[monitorId_]=monitor_;
        }
      );

      this.locationMonitors=monitors;

      var monitorIds=Object.keys(this.locationMonitors);

      if(1>monitorIds.length && -1!=this.locationMonitorPid)
      {
        xear.info("xear/device", "Detach monitor: device location.");

        navigator.geolocation.clearWatch(this.locationMonitorPid);

        this.locationMonitorPid=-1;
      }
    },

    /**
     * @memberOf xear.device
     */
    this.monitorMotion=function(id_, callback_)
    {
      xear.info("xear/device", "Add monitor: device motion.", id_);

      var self=this;

      window.addEventListener("devicemotion",
        function(event_)
        {
          var motion=[0, 0, 0];

          if(("acceleration" in event_)
            && null!=event_.acceleration.x)
          {
            motion[0]=event_.acceleration.x.toFixed(6);
            motion[1]=event_.acceleration.y.toFixed(6);
            motion[2]=event_.acceleration.z.toFixed(6);
          }
          else if(("accelerationIncludingGravity" in event_)
            && null!=event_.accelerationIncludingGravity.x)
          {
            motion[0]=event_.accelerationIncludingGravity.x.toFixed(6);
            motion[1]=event_.accelerationIncludingGravity.y.toFixed(6);
            motion[2]=event_.accelerationIncludingGravity.z.toFixed(6);
          }

          if(self.motionLast[0]!=motion[0]
            || self.motionLast[1]!=motion[1]
            || self.motionLast[2]!=motion[2])
          {
            self.motionLast=motion;

            callback_(motion);
          }
        }
      );
    },

    /**
     * @memberOf xear.device
     */
    this.monitorOrientation=function(id_, callback_)
    {
      xear.info("xear/device", "Add monitor: device orientation.", id_);

      var self=this;

      window.addEventListener("deviceorientation",
        function(event_)
        {
          var orientation=[
            event_.beta?event_.beta.toFixed(6):0,
            event_.gamma?event_.gamma.toFixed(6):0,
            event_.alpha?event_.alpha.toFixed(6):0,
            event_.absolute?1:0
          ];

          if(self.orientationLast[0]!=orientation[0]
            || self.orientationLast[1]!=orientation[1]
            || self.orientationLast[2]!=orientation[2])
          {
            self.orientationLast=orientation;

            callback_(orientation);
          }
        }
      );
    },

    /**
     * @memberOf xear.device
     */
    this.monitorScreenDimensions=function(id_, callback_)
    {
      xear.info("xear/device", "Add monitor: screen dimensions.", id_);

      window.addEventListener("resize",
        function()
        {
          callback_(xear.device.screenDimensions());
        }
      );
    },

    /**
     * @memberOf xear.device
     */
    this.monitorScreenOrientation=function(id_, callback_)
    {
      xear.info("xear/device", "Add monitor: screen orientation.", id_);

      var self=this;

      var eventName="orientationchange";

      if("undefined"!=typeof(screen) && "mozOrientation" in screen)
        eventName="mozorientationchange";

      window.addEventListener(eventName,
        function(event_)
        {
          var screenOrientation=self.screenOrientation();

          if(self.screenOrientationLast!=screenOrientation)
          {
            self.screenOrientationLast=screenOrientation;

            callback_(screenOrientation);
          }
        }
      );
    },

    /**
     * @callback monitorStateCallback
     * @param {Array:Scalar} state - Array containing following scalar values
     *   @var {Scalar} state[ 0] - motion.x
     *   @var {Scalar} state[ 1] - motion.y
     *   @var {Scalar} state[ 2] - motion.z
     *   @var {Scalar} state[ 3] - orientation.x
     *   @var {Scalar} state[ 4] - orientation.y
     *   @var {Scalar} state[ 5] - orientation.z
     *   @var {Scalar} state[ 6] - orientation.absolute
     *   @var {Scalar} state[ 7] - screen-orientation
     *   @var {Scalar} state[ 8] - screen-dimensions.w
     *   @var {Scalar} state[ 9] - screen-dimensions.h
     *   @var {Scalar} state[10] - latitude
     *   @var {Scalar} state[11] - longitude
     *   @var {Scalar} state[12] - altitude
     */
    /**
     * @memberOf xear.device
     *
     * @param {String} id_
     * @param {Number} interval_
     * @param {monitorStateCallback} callback_
     *
     * @return void
     */
    this.monitorState=function(id_, interval_, callback_)
    {
      xear.info("xear/device", "Add monitor: device state.", id_);

      var self=this;
      var monitor=null;

      var state=[__xear_device.EVENT_STATE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, {}];
      var stateLast=[__xear_device.EVENT_STATE, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, {}];

      this.monitorLocation(id_, function(location_) { state[11]=location_[0]; state[12]=location_[1]; state[13]=location_[3]; });
      this.monitorMotion(id_, function(motion_) { state[1]=motion_[0]; state[2]=motion_[1]; state[3]=motion_[2]; });
      this.monitorOrientation(id_, function(orientation_) { state[4]=orientation_[0]; state[5]=orientation_[1]; state[6]=orientation_[2]; state[7]=orientation_[3]; });
      this.monitorTouch(id_, function(touch_) { if(!(touch_[0] in state[14])) state[14][touch_[0]]=[]; state[14][touch_[0]].push(touch_); });

      monitor=function()
      {
        var screenDimensions=self.screenDimensions();

        state[8]=self.screenOrientation();
        state[9]=screenDimensions[0];
        state[10]=screenDimensions[1];

        var equals=true;

        for(var i=1; i<14; i++)
        {
          if(state[i]!=stateLast[i])
            equals=false;

          stateLast[i]=state[i];
        }

        if(!equals || 0<Object.keys(state[14]).length)
        {
          callback_(state);

          state[14]={};
        }

        setTimeout(monitor, interval_);
      };

      monitor();
    },

    /**
     * @memberOf xear.device
     */
    this.monitorTouch=function(id_, callback_)
    {
      xear.info("xear/device", "Add monitor: touch.", id_);

      var self=this;
      var monitor=null;

      monitor=function(type_, event_)
      {
        var touch=event_.changedTouches.item(0);

        callback_([
          touch.identifier,
          type_,
          touch.clientX?touch.clientX.toFixed(6):0,
          touch.clientY?touch.clientY.toFixed(6):0,
          touch.radiusX?touch.radiusX.toFixed(6):0,
          touch.radiusY?touch.radiusY.toFixed(6):0,
          touch.rotationAngle?touch.rotationAngle.toFixed(6):0
        ]);
      };

      document.addEventListener("touchstart", function(event_) {monitor(__xear_device.EVENT_TOUCH_START, event_);});
      document.addEventListener("touchmove", function(event_) {monitor(__xear_device.EVENT_TOUCH_MOVE, event_);});
      document.addEventListener("touchend", function(event_) {monitor(__xear_device.EVENT_TOUCH_END, event_);});
    }
  };
  //----------------------------------------------------------------------------


  // EXPORT
  __xear_device_export=function()
  {

  }
  //----------------------------------------------------------------------------


  // INITIALIZE
  var __xear_device_init=function() {
    if("undefined"==typeof(xear))
      setTimeout(__xear_device_init, 10);
    else
      xear("device");
  }();
  //----------------------------------------------------------------------------
